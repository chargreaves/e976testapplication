using System;
using System.Reflection;
using System.Diagnostics;
//using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;

namespace E976TestApplication
{
    /// <summary>
    /// Summary description for FirmwareLogger
    /// </summary>
    public class FirmwareLogger
    {
        ILogging log;
        LVClient m_CoreToolkit;
        ICommInterface diag;
        private static ManualResetEvent threadCreated;
        private static System.Threading.Thread t;
        private static Object thisLock = new Object();
        private static string m_ReadBuffer;
        private static bool m_exit;
        private static bool m_initialized;

        public FirmwareLogger() : this(0)
        {
        }

        public FirmwareLogger(int testSocket)
        {
            log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, testSocket.ToString());
            log.Debug("Connecting to LV Server");
            m_CoreToolkit = new LVClient();


            m_exit = true; // false;
            m_initialized = false;
            m_ReadBuffer = string.Empty;
        }

        ~FirmwareLogger()
        {
            StopReadDiagPort();
            Clear();

            m_CoreToolkit.UnregisterChannel();
            m_CoreToolkit = null;
            // delete the thread also?
        }

        public void Initialize() // use a bool?
        {
            if (m_initialized == true) return;
            //diag = (ICommInterface)m_CoreToolkit.getEquipmentObject("UUT_COMM_2");
            diag = (ICommInterface)m_CoreToolkit.getEquipmentObject("ARMADILLO_BOOT_LOGGER");
            if (diag != null)
            {
                m_exit = false;
                try
                {
                    diag.open();
                    diag.clear();
                }
                catch
                {
                }
                threadCreated = new ManualResetEvent(false);
                t = new System.Threading.Thread(new System.Threading.ThreadStart(this.ReadDiagPort));
                //t = new System.Threading.Thread(ReadDiagPort);
                t.Start();
                threadCreated.WaitOne();
                /*if (!t.IsAlive)
                {
                    t = new System.Threading.Thread(new System.Threading.ThreadStart(this.ReadDiagPort));
                    log.Info("Serial Port Thread not alive, re-trying...");
                    System.Threading.Thread.Sleep(100);
                    t.Start();
                    threadCreated.WaitOne();
                }*/
                // check the status on the thread? maybe here instead of the WaitForOr method
                if (t.IsAlive)
                {
                    m_initialized = true;
                    log.Info("DiagPort initialized.");
                }
                else
                {
                    log.Error("DiagPort not initialized!");
                }
            }
            else
            {
                log.Error("DiagPort could not be initialized!");
            }
        }

        public int WaitForString(string strInput, int iTimeout)
        {
            return WaitForOr(strInput, string.Empty, iTimeout);
        }

        public int WaitForOr(string strInput1, string strInput2, int iTimeout)
        {
            System.DateTime begin = System.DateTime.Now, timeout = System.DateTime.Now;
            timeout = timeout.AddMilliseconds(iTimeout);
            int pos;

            while (true)
            {
                if (System.DateTime.Now > timeout)
                {
                    string tmpMsg = string.Format("{2}: Timout ({0} ms) when waiting for string '{1}'", iTimeout, strInput1, t.ManagedThreadId);
                    if (strInput2.Length > 0)
                        tmpMsg += string.Format(" or '{0}'", strInput2);

                    log.ErrorFormat(tmpMsg);
                    return -1;
                }

                if (m_ReadBuffer.Length > 0)
                {                    
                    if (strInput1.Length > 0 && (pos = m_ReadBuffer.IndexOf(strInput1)) != -1)
                    {
                        lock (thisLock)
                        {
                            m_ReadBuffer = m_ReadBuffer.Remove(0, pos);
                        }
                        log.InfoFormat("Retreived '{0}' in {1} ms.", strInput1, System.DateTime.Now - begin);
                        return 0;
                    }
                    else if ((strInput2.Length > 0) && (pos = m_ReadBuffer.IndexOf(strInput2)) != -1)
                    {
                        lock(thisLock)
                        {
                            m_ReadBuffer = m_ReadBuffer.Remove(0, pos);
                        }
                        log.InfoFormat("Retreived '{0}' in {1} ms.", strInput2, System.DateTime.Now - begin);
                        return 1;
                    }

                    // Decide whether or not we want ot remove the searched data.
                    /*
                    pos = m_ReadBuffer.LastIndexOf('\r');
                    if (pos != -1)
                    {
                        m_ReadBuffer = m_ReadBuffer.Remove(0, pos);
                    }
                    */
                }
                else
                {
                    System.Threading.Thread.Sleep(100);

                }
            }
        }

        public bool IsInitialized()
        {
            return m_initialized;
        }

        public void PollThread()
        {
            if (!t.IsAlive)
            {
                log.Info("Serial Port Thread dead, re-initializing...");
                m_initialized = false; //redundant?
                m_exit = true; // false;
                Initialize();
            }
        }

        public void Clear()
        {
            lock (thisLock)
            {
                log.Debug("Clearing the buffer");
                m_ReadBuffer = string.Empty;
            }
        }

        public void StopReadDiagPort()
        {
            if (m_initialized)
            {
                log.DebugFormat("Stopping the thread:{0}...", t.ManagedThreadId);
                m_exit = true;
                m_initialized = false;
                t.Join();
            }
        }

        private void read()
        {
            log.Debug("Starting to read...");
            string strReadBuffer = string.Empty;
            string tmpBuffer;
            int iBytesToRead;
            int status = 0;

            if (m_initialized)
                m_exit = false;

            while (!m_exit)
            {
                tmpBuffer = string.Empty;
                iBytesToRead = 512; // = diag.BytesToRead();
                if (iBytesToRead > 1)
                {
                    try
                    {
                        //iBytesToRead = diag.BytesToRead();
                        status = diag.read(ref tmpBuffer, iBytesToRead);
                        tmpBuffer = tmpBuffer.Replace("\0", "");
                        if (tmpBuffer.Length > 0)
                        {
                            log.DebugFormat("{1}: {0}", tmpBuffer, t.ManagedThreadId);
                            strReadBuffer += tmpBuffer;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(100);
                            continue;
                        }
                    }
                    catch
                    {
                        // do nothing while read timeout occured
                        // maybe sleep some while...
                        //log.Debug("Unhandeled exception...");
                    }

                    lock (thisLock)
                    {
                        m_ReadBuffer += strReadBuffer;
                        strReadBuffer = string.Empty;
                    }
                }
                else
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }

        // redundant?
        private void ReadDiagPort()
        {
            threadCreated.Set();
            this.read();
        }
    }
}