using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Windows.Forms;
//using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;
using System.Net.NetworkInformation;

//using CoreToolkit;
using CommInterface;
//using Logging;
//using TestParameters;
//using LV_Client;
//using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void SendReceive(SequenceContext seqContext, out bool result, out string reportText, string strSend, string strToLookFor, int iTimeout)
        {
            int status = SendReceive(strSend, strToLookFor, iTimeout, out reportText, uut_ts);
            result = (status == 0 ? true : false);
        }

        private int SendReceive(string strSend, string strToLookFor, int iTimeout)
        {
            string strReceived = string.Empty;
            return SendReceive(strSend, strToLookFor, iTimeout, out strReceived, uut);
        }

        private int SendReceive(string strSend, string strToLookFor, int iTimeout, ICommInterface session)
        {
            string strReceived = string.Empty;
            return SendReceive(strSend, strToLookFor, iTimeout, out strReceived, session);
        }

        private int SendReceive(string strSend, string strToLookFor, int iTimeout, out string strReceived)
        {
            return SendReceive(strSend, strToLookFor, iTimeout, out strReceived, uut);
        }

        private int m_iPingTimeout = 10000;
        private int SendReceive(string strSend, string strToLookFor, int iTimeout, out string strReceived, ICommInterface session)
        {
            int status = -1;
            bool success = false;

            int iEmptyCounter = 0;

            PingReply reply;
            Ping pingSender = new Ping();
            int iPingTimeout = m_iPingTimeout; //ms

            if (iPingTimeout > iTimeout)
            {
                iPingTimeout = iTimeout;
            }

            string strSessionName = session.Name;

            bool bPingCheck = false;
            if (strSessionName == "UUT_COMM") // Ping only "UUT_COMM" sessions
            {
                bPingCheck = (m_iPingDutDisabled == 0 ? true : false);
            }

            bool bPingOK = true;
            int iPingFailCounter = 0;
            int iPingOKCounter = 0;

            // if you wish to send command, otherwise wait for response
            if (strSend.Length > 0)
                status = session.write(strSend);
            else
                status = 0;

            string strReadBuffer = string.Empty;
            string strTmp = string.Empty;

            System.DateTime begin = System.DateTime.Now;
            begin = begin.AddMilliseconds(iTimeout);

            if (status == 0)
            {
                do
                {
                    strReadBuffer = string.Empty;
                    status = session.read(ref strReadBuffer, 512);//dont check return status here because pbist button does not have / # prompt
                    strReadBuffer = strReadBuffer.Replace("\0", "");

                    if (strReadBuffer == string.Empty)
                    {
                        System.Threading.Thread.Sleep(5); // decrease CPU usage when read buffer is empty

                        if ((iEmptyCounter % 100) == 0 && bPingCheck)
                        {
                            // Check DUT connection
                            System.DateTime beginPing = System.DateTime.Now;
                            beginPing = beginPing.AddMilliseconds(iPingTimeout);

                            do
                            {
                                reply = pingSender.Send(m_sClientIpAddress, 100);
                                if (reply.Status == IPStatus.Success)
                                {
                                    bPingOK = true;
                                    iPingOKCounter++;
                                    //log.Debug("Ping OK!");                                    
                                }
                                else
                                {
                                    bPingOK = false;
                                    iPingFailCounter++;
                                    System.Threading.Thread.Sleep(20);
                                    //log.Debug("No Ping response");
                                }
                            }
                            while (!bPingOK && (System.DateTime.Compare(System.DateTime.Now, beginPing) <= 0));
                        }
                        iEmptyCounter++;
                    }
                    else
                    {                       
                        strTmp += strReadBuffer.Replace("\0", string.Empty); // Remove unwanted \0
                        if (status == 0 && strTmp.Contains(strToLookFor))
                        {
                            success = true;
                        }
                    }
                }
                while (bPingOK && status == 0 && (System.DateTime.Compare(System.DateTime.Now, begin) <= 0) && !success);
            }

            strReceived = strTmp;

            string echo = strSend + "\r\n"; // The sent string + CrLf is the echo

            // Remove echo
            if (strReceived.StartsWith(echo))
            {
                strReceived = strReceived.Remove(0, echo.Length);
            }
            
            if (iEmptyCounter > 0) log.Debug("emptyCounter: " + iEmptyCounter);
            if (bPingCheck && iPingOKCounter > 0) log.Debug("Ping OK Counter: " + iPingOKCounter);
            if (iPingFailCounter > 0) log.Debug("Ping Fail Counter: " + iPingFailCounter);

            if (!bPingOK)
            {
                log.Error("No Ping response from DUT");
                return -2;
            }

            if (success || strToLookFor == "")
                return 0;
            else
                return -1;
     
        }//SendReceive
    }
}