using System;
using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;
using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void TFTPKernelDownload(SequenceContext seqContext, out double result, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            result = -99;

            try
            {
                int iTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 60000);
                if (iTimeout < 200)
                    iTimeout *= 1000;

                string sRootPath = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "ROOT PATH", out sRootPath, m_sNfsShare + "shared\\Kernels\\");
                StringBuilder sbRootPath = new StringBuilder(sRootPath);

                string strKernelFile1 = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "KERNEL_FILE_1", out strKernelFile1, "");
                StringBuilder sbKernelFile1 = new StringBuilder(strKernelFile1);

                string strKernelFile2 = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "KERNEL_FILE_2", out strKernelFile2, "");
                StringBuilder sbKernelFile2 = new StringBuilder(strKernelFile2);

                int iRetries = 0;
                bool bBlank = false;

                //result = TFTPKernelDownload();
                //result = TFTPDownloadKernel(m_iUutInstance, iTimeout, ref iRetries, sbRootPath, sbKernelFile1, sbKernelFile2, ref bBlank);
                bool bWrapper = false;
                string requestFileName = null;
                //result = TFTPKernelDownload(m_iUutInstance, iTimeout, ref iRetries, sbRootPath, sbKernelFile1, sbKernelFile2, ref bWrapper, ref bBlank);
                int status = m_TFTPServer.DownloadKernel(iTimeout, out iRetries, sRootPath, strKernelFile1, strKernelFile2, out bBlank,
                    out bWrapper, false, out requestFileName);
                if (status != 0)
                {
                    result = status;
                    reportText = String.Format("{0}:Error: TFTP Kernel Download failed.", Math.Abs(result));
                    log.Error(reportText);
                    return;
                }


                if (bWrapper)
                {
                    log.Debug("Waiting for DCHP request from wrapper kernel...");
                    
                    //if (!DHCPRequest(m_iUutInstance, iRetryTimeOut, sbRootPath, sbSkipTftpDownload, sbHttpsDownload, ref bFalse, sbVendorClassID))
                    string retVendorId = "";
                    int retBootloaderType = 0;
                    status = m_DHCPServer.WaitForRequest(iTimeout, sRootPath, false, "", "", true, false, false, out retVendorId, out retBootloaderType);
                    if (status != 0)
                    {
                        result = status - 10;
                        reportText = String.Format("{0}:Error: DHCP request failed!", Math.Abs(result));
                        log.Error(reportText);
                        return;
                    }

                    log.Debug("TFTP Wrapper Kernel Download...");

                    status = m_TFTPServer.DownloadKernel(iTimeout, out iRetries, sRootPath, strKernelFile1, strKernelFile2, out bBlank,
                    out bWrapper, false, out requestFileName);
                    if (status != 0)
                    {
                        result = status - 20;
                        reportText = String.Format("{0}:Error: TFTP Wrapper Kernel Download failed.", Math.Abs(result));
                        log.Error(reportText);
                        return;
                    }
                }

                result = 0;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }
        }
    }
}