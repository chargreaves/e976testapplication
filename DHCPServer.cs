using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    public enum eBootLoaderType
    {
        UNKNOWN = 0,
        UBOOT,
        CFE,
        MTC,
        KREATV_FW,
        KREATV_PLATFORM,
        NSN_FW,
        MEDIAROOM,
        TELEFONICA,
        BOLT
    }

    class CDHCPServer
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int DHCPServer_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool DHCPServer_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, bool EnableSNMPSupport, bool DisableOverridePacket, bool DisableFirmwareLogging, string szNetworkCardIP,  string szClientIP, UInt32 ForceBroadcastAddressOverride, UInt32 ForceBroadcastAddressDHCP, string IniFileName);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool DHCPServer_SetClientAddress(int Handle, string szClientIP);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void DHCPServer_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void DHCPServer_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void DHCPServer_Destroy(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int DHCPServer_WaitForRequest(int Handle, int Timeout, string szRootPath, bool UpdateProgressBar, string Param2, string Param3, bool useNetLogging, bool WaitOnEmptyVendorClassID, bool bDNSEnabled, StringBuilder retVendorClassId, int retVendorClassIdLen, out int BootLoaderType);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void DHCPServer_SetFwLoggingPort(int Handle, string pPort);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void DHCPServer_SetOverridePacket(int Handle, bool enabled);

        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, bool EnableSNMPSupport, bool DisableOverridePacket, bool DisableFirmwareLogging, string szNetworkCardIP, string szClientIP, UInt32 ForceBroadcastAddressOverride, UInt32 ForceBroadcastAddressDHCP, string IniFileName)
        {
            if (DHCPServer_GetInterfaceVersion() != 2) return false;
            return DHCPServer_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, EnableSNMPSupport, DisableOverridePacket, DisableFirmwareLogging, szNetworkCardIP, szClientIP, ForceBroadcastAddressOverride, ForceBroadcastAddressDHCP, IniFileName);
        }
        public bool SetClientAddress(int FixturePos, string szClientIP)
        {
            return DHCPServer_SetClientAddress(m_Handle, szClientIP);
        }
        public void Stop()
        {
            DHCPServer_Stop(m_Handle);
        }
        public void Reset()
        {
            DHCPServer_Reset(m_Handle);
        }
        public void Destroy()
        {
            DHCPServer_Destroy(m_Handle);
        }
        public int WaitForRequest(int Timeout, string szRootPath, bool UpdateProgressBar, string Param2, string Param3, bool useNetLogging, bool WaitOnEmptyVendorClassID, bool bDNSEnabled, out string retVendorClassId, out int BootLoaderType)
        {
            int res;
            StringBuilder VendorClassId = new StringBuilder(1000000);
            res = DHCPServer_WaitForRequest(m_Handle, Timeout, szRootPath, UpdateProgressBar, Param2, Param3, useNetLogging, WaitOnEmptyVendorClassID, bDNSEnabled, VendorClassId, VendorClassId.Capacity, out BootLoaderType);
            retVendorClassId = VendorClassId.ToString();
            return res;
        }
        public void SetFwLoggingPort(int FixturePos, string pPort)
        {
            DHCPServer_SetFwLoggingPort(m_Handle, pPort);
        }
        public void SetOverridePacket(int FixturePos, bool enabled)
        {
            DHCPServer_SetOverridePacket(m_Handle, enabled);
        }
    }
}
