﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NationalInstruments.TestStand.Interop.API;
using PFTSLibrary;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //=====================================================================================================================
        public void PWR_006_00_00_Power_Output_DC(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[20];

            PrintTestName(seqContext.Step.Name);

            //pause 2000ms to let voltages settle
            System.Threading.Thread.Sleep(2000);

            result[1] = AMB.SK1_A1.MeasureDC();     //TP4749      +1V8_SW
            result[2] = AMB.SK1_A2.MeasureDC();     //TP2410      +2V5_DDR
            result[3] = AMB.SK1_A3.MeasureDC();     //TP4870      +5V0_AON
            result[4] = AMB.SK1_A4.MeasureDC();     //TP4736      +1V8_AON
            result[5] = AMB.SK1_A5.MeasureDC();     //TP4737      +1V0_AON
            result[6] = AMB.SK1_A6.MeasureDC();     //TP2503      D0.95V_AVS_STB
            result[7] = AMB.SK1_A7.MeasureDC();     //TP4677      D0.95V_AVS_CPU
            result[8] = AMB.SK1_A8.MeasureDC();     //TP4904      +1V0_D
            result[9] = AMB.SK1_A9.MeasureDC();     //TP4894      +1V8
            result[10] = AMB.SK1_A10.MeasureDC();   //TP4893      +1V2
            result[11] = AMB.SK1_A11.MeasureDC();   //TP4879      +3V3_AON
            result[12] = AMB.SK1_A12.MeasureDC();   //TP4741      3V3HPNA_D
            result[13] = AMB.SK1_A13.MeasureDC();   //TP4862      1V2HPNA_D
            //result[14] = AMB.SK1_A15.MeasureDC();   //TP4732      USB1_EXT_VBUS
            result[14] = AMB.SK1_A16.MeasureDC();   //TP4650      HDMI-CEC
            result[15] = AMB.SK1_A17.MeasureDC();   //TP4660      5V_HDMI
            result[16] = AMB.SK1_A18.MeasureDC();   //TP4871      +12V
            result[17] = AMB.SK1_A19.MeasureDC();   //TP4872      +12V
            result[18] = AMB.SK1_A24.MeasureDC();   //TP4772      36KHz IR +3V3_AON
            result[19] = AMB.SK1_A25.MeasureDC();   //TP4775	  56KHz IR +3V3_AON

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

    }

}
