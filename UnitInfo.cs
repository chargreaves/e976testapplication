using System;
using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;
using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {        
        struct UnitInfo
        {
            public String ScanSerialNumber;
            public String SerialNumber;
            public String PcbSerialNumber;
            public String MacAddress;
            public String WiFiMacAddress;
            public String HpnaMacAddress;
            public String GUID;
            public String CustomerSerial;
            public String ChipID;
            public String IrdetoPincode;
            public String SecKey;
            public String HdcpKey;
            public String OrderNumber;
            public String CustomerOrderNumber;
            public String ShipmentDate;
            public String PcbVersion;
            public String TopBom;
            public String BoxBom;
            public String PcbaBom;
            public String ProductionData;
            public String ConfigParam;
            public String TestControl;
            public String Firmware;
            public String GoldenImage;
            public String BootImage;
            public String SplashImage;
            public String MotoPlyrBootImage;
            public String MotoPlyrEbImage;
            public String MotoPlyrAppImage;
            public String MotoPlyrGhostImage;
            public String FrontPanelFirmware;
            public String Model;
            public String NetworkPath;
            /*public String TCProductDescription;
            public String TCModel;
            public String TCFamily;
            public String TCStationType;*/
            public String NetworkLogFolder;
            public String NetworkLogFolderFailed;
            public String NetworkLogParams;
            public String EanCode;
            public String Customer;
            public String HwFeatures;
            public String MadeInCountry;
            public String ProductLabelLayout;
            public String GiftBoxLabelLayout;
            public String PowerRatingProductLabel;
            public String PowerRatingGiftBoxLabel;
            public String ProfileName;
            public String HardwareRevision;
            public String CASEID;
            public String BluetoothMac;
            public String PsKeys;
            public String BluetoothFirmware;
        }

        private int GetUnitInfo(ref UnitInfo unitInfo)
        {
            unitInfo.ScanSerialNumber = scannedSerialNumber;
            // Parse from test sequence?
            /*unitInfo.TCProductDescription;
            unitInfo.TCModel;
            unitInfo.TCFamily;
            unitInfo.TCStationType;*/
            try
            {
                ICAM cam = (ICAM)m_CoreToolkit.getEquipmentObject("CAM", m_iUutInstance.ToString());

                string[] sKeys, sValues;
                string errorMsg;
                int status = cam.GetUnitInfo(null, null, out sKeys, out sValues, out errorMsg);

                if (status != 0)
                {
                    log.ErrorFormat("Error, Failed to get unit info: '{0}'", errorMsg);
                    return -1;
                }

                log.Debug("Fetching UnitInfo...");
                for (int i = 0; i < sKeys.Length; i++)
                {
                    string sPrintValue = sValues[i];
                    switch (sKeys[i].ToUpperInvariant())
                    {
                        case "SERIALNUMBER":
                        case "SERIALNO":
                            unitInfo.SerialNumber = sValues[i];
                            break;
                        case "PCBSERIAL":
                        case "PCBSERIALNO":
                            unitInfo.PcbSerialNumber = sValues[i];
                            break;
                        case "MACADDRESS":
                            unitInfo.MacAddress = sValues[i];
                            
                            if (sValues[i].Length == 12)
                            {
                                for (int j = 5; j > 0; j--)
                                {
                                    unitInfo.MacAddress = unitInfo.MacAddress.Insert(2 * j, ":");
                                }
                                log.Debug("Colon added to MAC Address.");
                                sPrintValue = unitInfo.MacAddress;
                            }

                            if (unitInfo.MacAddress.Length > 0 && unitInfo.MacAddress.Length != 17)
                            {
                                log.WarningFormat("Wrong format of MAC Address! Key={0}, Value={1}", sKeys[i], sValues[i]);
                            }
                            break;
                        case "WIFIMACADDRESS":
                            unitInfo.WiFiMacAddress = sValues[i];

                            if (sValues[i].Length == 12)
                            {
                                for (int j = 5; j > 0; j--)
                                {
                                    unitInfo.WiFiMacAddress = unitInfo.WiFiMacAddress.Insert(2 * j, ":");
                                }
                                log.Debug("Colon added to Wi-Fi MAC Address.");
                                sPrintValue = unitInfo.WiFiMacAddress;
                            }

                            if (unitInfo.WiFiMacAddress.Length > 0 && unitInfo.WiFiMacAddress.Length != 17)
                            {
                                log.WarningFormat("Wrong format of Wi-Fi MAC Address! Key={0}, Value={1}", sKeys[i], sValues[i]);
                            }
                            break;
                        case "HPNAMACADDRESS":
                            unitInfo.HpnaMacAddress = sValues[i];

                            if (sValues[i].Length == 12)
                            {
                                for (int j = 5; j > 0; j--)
                                {
                                    unitInfo.HpnaMacAddress = unitInfo.HpnaMacAddress.Insert(2 * j, ":");
                                }
                                log.Debug("Colon added to HPNA MAC Address.");
                                sPrintValue = unitInfo.HpnaMacAddress;
                            }

                            if (unitInfo.HpnaMacAddress.Length > 0 && unitInfo.HpnaMacAddress.Length != 17)
                            {
                                log.WarningFormat("Wrong format of HPNA MAC Address! Key={0}, Value={1}", sKeys[i], sValues[i]);
                            }
                            break;
                        case "GUID":
                            unitInfo.GUID = sValues[i];
                            break;
                        case "CUSTOMERSERIAL":
                            unitInfo.CustomerSerial = sValues[i];
                            break;
                        case "CHIPID":
                            unitInfo.ChipID = sValues[i];
                            break;
                        case "IRDETOPINCODE":
                            unitInfo.IrdetoPincode = sValues[i];
                            break;
                        case "KEY": // Update driver to mask the log output with ******** !?!?
                            unitInfo.SecKey = sValues[i];
                            if (sValues.Length > 0) sPrintValue = "**********";
                            break;
                        case "HDCPKEY": // Update driver to mask the log output with ******* !?!?!
                            unitInfo.HdcpKey = sValues[i];
                            if (sValues.Length > 0) sPrintValue = "**********";
                            break;
                        case "ORDERNO":
                            unitInfo.OrderNumber = sValues[i];
                            break;
                        case "CUSTOMERORDERNO":
                            unitInfo.CustomerOrderNumber = sValues[i];
                            break;
                        case "SH�PMENTDATE":
                            unitInfo.ShipmentDate = sValues[i];
                            break;
                        case "PCBVERSION":
                            unitInfo.PcbVersion = sValues[i];
                            break;
                        case "TOPBOM":
                            unitInfo.TopBom = sValues[i];
                            break;
                        case "BOXBOM":
                            unitInfo.BoxBom = sValues[i];
                            break;
                        case "PCBABOM":
                            unitInfo.PcbaBom = sValues[i];
                            break;
                        case "PRODUCTIONDATA":
                            unitInfo.ProductionData = sValues[i];
                            break;
                        case "CONFIGPARAM":
                            unitInfo.ConfigParam = sValues[i];
                            break;
                        case "TESTCONTROL":
                            unitInfo.TestControl = sValues[i];
                            break;
                        case "FIRMWARE":
                            unitInfo.Firmware = sValues[i];
                            break;
                        case "GOLDENIMAGE":
                            unitInfo.GoldenImage = sValues[i];
                            break;
                        case "BOOTIMAGE":
                            unitInfo.BootImage = sValues[i];
                            break;
                        case "SPLASHIMAGE":
                            unitInfo.SplashImage = sValues[i];
                            break;
                        case "MOTOPLYRBOOTIMAGE":
                            unitInfo.MotoPlyrBootImage = sValues[i];
                            break;
                        case "MOTOPLYREBIMAGE":
                            unitInfo.MotoPlyrEbImage = sValues[i];
                            break;
                        case "MOTOPLYRAPPIMAGE":
                            unitInfo.MotoPlyrAppImage = sValues[i];
                            break;
                        case "MOTOPLYRGHOSTIMAGE":
                            unitInfo.MotoPlyrGhostImage = sValues[i];
                            break;
                        case "FRONTPANELFIRMWARE":
                            unitInfo.FrontPanelFirmware = sValues[i];
                            break;
                        case "MODEL":
                        case "PRODUCTTEXT": // guard for doubles?
                            unitInfo.Model = sValues[i];
                            break;
                        case "NETWORKPATH":
                            unitInfo.NetworkPath = sValues[i];
                            if (unitInfo.NetworkPath.Length > 0 && !unitInfo.NetworkPath.EndsWith("\\"))
                                unitInfo.NetworkPath += "\\";
                            break;
                        case "NETWORKLOGFOLDER":
                            unitInfo.NetworkLogFolder = sValues[i];
                            break;
                        case "NETWORKLOGFOLDERFAILED":
                            unitInfo.NetworkLogFolderFailed = sValues[i];
                            break;
                        case "NETWORKLOGPARAMS":
                            unitInfo.NetworkLogParams = sValues[i];
                            break;
                        case "EANCODE":
                            unitInfo.EanCode = sValues[i];
                            break;
                        case "CUSTOMER":
                            unitInfo.Customer = sValues[i];
                            break;
                        case "HWFEATURES":
                            unitInfo.HwFeatures = sValues[i];
                            break;
                        case "MADEINCOUNTRY":
                            unitInfo.MadeInCountry = sValues[i];
                            break;
                        case "PRODUCTLABELLAYOUT":
                            unitInfo.ProductLabelLayout = sValues[i];
                            break;
                        case "GIFTBOXLABELLAYOUT":
                            unitInfo.GiftBoxLabelLayout = sValues[i];
                            break;
                        case "POWERRATINGPRODUCTLABEL":
                            unitInfo.PowerRatingProductLabel = sValues[i];
                            break;
                        case "POWERRATINGGIFTBOXLABEL":
                            unitInfo.PowerRatingGiftBoxLabel = sValues[i];
                            break;
                        case "PROFILENAME":
                            unitInfo.ProfileName = sValues[i];
                            break;
                        case "HARDWAREREVISION":
                            unitInfo.HardwareRevision = sValues[i];
                            break;
                        case "CASEID":
                            unitInfo.CASEID = sValues[i];
                            break;
                        case "BLUETOOTHMACADDRESS":
                            unitInfo.BluetoothMac = sValues[i];

                            if (sValues[i].Length == 12)
                            {
                                for (int j = 5; j > 0; j--)
                                {
                                    unitInfo.BluetoothMac = unitInfo.BluetoothMac.Insert(2 * j, ":");
                                }
                                log.Debug("Colon added to Bluetooth MAC Address.");
                                sPrintValue = unitInfo.BluetoothMac;
                            }

                            if (unitInfo.BluetoothMac.Length > 0 && unitInfo.BluetoothMac.Length != 17)
                            {
                                log.WarningFormat("Wrong format of Bluetooth MAC Address! Key={0}, Value={1}", sKeys[i], sValues[i]);
                            }
                            break;
                        case "PSKEYS":
                            unitInfo.PsKeys = sValues[i];
                            break;
                        case "BLUETOOTHFIRMWARE":
                            unitInfo.BluetoothFirmware = sValues[i];
                            break;
                        default:
                            log.DebugFormat("Unknown unit info data, Key={0}, Value={1}", sKeys[i], sPrintValue);
                            break;

                    }

                    log.DebugFormat("{0}=\"{1}\"", sKeys[i], sPrintValue);
                }
                log.Debug("Done fetching UnitInfo!");
                return 0;
            }
            catch (Exception ex)
            {
                log.Error("Exception: " + ex.Message);
                return -99;
            }
        }
    }
}