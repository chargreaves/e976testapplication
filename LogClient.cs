using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using Logging;

namespace CommonCS
{
    class LogClient
    {
        Socket m_LogSocket;
        String m_LogSocketAddress;
        ILogging log;
        
        public LogClient(string testSocket)
        {
            log = LogControl.GetLogger(ILogging.LogSource.Client, "VIPServices", testSocket);
            m_LogSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            
            try
            {
                m_LogSocket.Bind(new IPEndPoint(IPAddress.Loopback, 0));
                m_LogSocketAddress = m_LogSocket.LocalEndPoint.ToString();
                m_LogSocket.ReceiveBufferSize = 100000;

                Thread receiveThread = new Thread(new ThreadStart(ReceiveData));
                receiveThread.Start();

            }
            catch
            {
                //tbLogWindow.Text = "Error: Failed to bind to port \nVerify that no other server is running\n"; ;
                log.Error("Error: Failed to bind to port \nVerify that no other server is running\n");
                return;
            }
        }

        public void Close()
        {
            m_LogSocket.Close();
        }

        public string GetSocketAddress()
        {
            return m_LogSocketAddress;
        }

        public enum eLogCommand
        {
            PROGRESS = 0,
            PROGRESS_BAR,
            PROGRESS_STATUS,
            PROGRESS_DIRECT,
            LOG,
            MAIN,
            MAIN_ONLY,
            MAIN_DIRECT,
            STATUSBAR,
            ERASE,
            IMAGE,
            TVCARDIMAGE,
            POSITION_COLOR,
            SET_CLEAR_POS,
            CLEAR_TEXT,
            SCROLL_TO_BOTTOM,
            SAVE_LOG_TO_FILE
        }
        public void ReceiveData()
        {
            byte[] data;
            EndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
            data = new byte[65536];
            int[] header = new int[12];
            string msg;

            while (true)
            {
                try
                {
                    m_LogSocket.ReceiveFrom(data, ref remoteEP);
                }
                catch
                {
                    return;
                }
                Buffer.BlockCopy(data, 0, header, 0, 4 * 12);
                switch ((eLogCommand)header[2])
                {
                    case eLogCommand.PROGRESS:
                    case eLogCommand.PROGRESS_STATUS:
                    case eLogCommand.PROGRESS_DIRECT:
                    case eLogCommand.LOG:
                    case eLogCommand.MAIN:
                    case eLogCommand.MAIN_ONLY:
                    case eLogCommand.MAIN_DIRECT:
                        msg = Encoding.ASCII.GetString(data, header[1], header[0] - header[1]);
                        //AddText(msg, header[4], header[5]);
                        log.Debug(msg.Replace("\n\0", ""));
                        break;
                    case eLogCommand.IMAGE:
                        /*bitmap = new byte[header[0] - header[1]];
                        Buffer.BlockCopy(data, header[1] + 14, bitmap, 0, header[0] - header[1] - 14);
                        AddImage(bitmap, header[6], header[7]);*/
                        break;
                    case eLogCommand.TVCARDIMAGE:
                        /*bitmap = new byte[header[0] - header[1]];
                        Buffer.BlockCopy(data, header[1], bitmap, 0, header[0] - header[1]);
                        AddImageToTV(bitmap);*/
                        break;
                    default:
                        break;
                }
            }

        }
    }
}
