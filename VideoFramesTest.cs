using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
//using System.Drawing.Imaging;
using System.Collections;
using System.Xml;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;
using VideoFrameAnalyzer;
//using StcComm;
//using ImageApplication;


namespace E976TestApplication
{
    public partial class TestApp
    {
        
        //private void GetValueFromXmlAsString(string sConfigFile, string sName, string Key, string Value, ref string sData) { }
        //private void GetValueFromXmlAsInt(string sConfigFile, string sName, string Key, string Value, ref int iData) { }
        public enum VIDEO_FRAME_TEST_ERRORCODE
        {
            EC_INVALID_PARAM = 99,
            EC_TIMEOUT = 98,
            EC_OUT_OF_SPEC = 97,
            EC_UNKNOWN_ERROR = 96,
            EC_START_VIDEO_ERROR = 101,
            EC_STOP_VIDEO_ERROR = 102,
            EC_START_CAPTURE_SEQ_PIC = 103,
            EC_SOP_CAPTURE_SEQ_PIC = 104,
            //CAP_EC_INIT_DEVICE_FAILED = -1,
            //CAP_EC_DEVICE_IN_USE = -2,
            //CAP_EC_NOT_SUPPORTED = -3,
            //CAP_EC_INVALID_PARAM = -4,
            //CAP_EC_TIMEOUT = -5,
            //CAP_EC_NOT_ENOUGH_MEMORY = -6,
            //CAP_EC_UNKNOWN_ERROR = -7,
            //CAP_EC_ERROR_STATE = -8
        }
        /*
        public void CompositeStreamVideoTestAE(SequenceContext seqContext, out double measurement, out string reportText, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            reportText = String.Empty;
            errorOccurred = false;
            errorCode = 0;
            errorMsg = "";
            measurement = -99;

            int status = 0;
            int iTimeout = 0;
            int switchDelay = 0;
            int captureDelay = 0;
            int startDelay = 0;
            string strVideoOutput = "";
            IVideoFrameAnalyzer videoCardMaster;
            IVideoFrameAnalyzer videoCardDUT;
            DateTime end;
            DateTime startTime; startTime = DateTime.Now;
            log.DebugFormat("===== {0} =====", System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 3000);

                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCH_DELAY", out switchDelay, 100);
                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_DELAY", out captureDelay, 1000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "START_DELAY", out startDelay, 10);
                //testParameter.getParameterAsInt(seqContext.Step.Name, "RETRIES", out retries, 1);
                //testParameter.getParameterAsString(seqContext.Step.Name, "CONFIG_FILE", out sConfigFile, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_OUTPUT", out strVideoOutput, "");

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;

                videoCardMaster = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE_MASTER");
                if (videoCardMaster == null)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Equipment_Object_Failed;
                    errorMsg = string.Format("Failed to create video card objct");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }

                videoCardDUT = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE_DUT");
                if (videoCardDUT == null)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Equipment_Object_Failed;
                    errorMsg = string.Format("Failed to create video card objct");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }

                // ignore the returned value of SelectVideoFormat() for now
                videoCardMaster.SelectVideoFormat(videoSource);
                log.DebugFormat("Wait {0} ms for Video Input to switch before capture", switchDelay);
                System.Threading.Thread.Sleep(switchDelay);

                IVideoFrameAnalyzer.AVVideoStandard videoStandard = IVideoFrameAnalyzer.AVVideoStandard.NTSC_M;
                videoCardMaster.SelectColorCoding(videoStandard);
                log.DebugFormat("AVVideoStandard = {0}", videoStandard);

                videoCardDUT.SelectVideoFormat(videoSource);
                log.DebugFormat("Wait {0} ms for Video Input to switch before capture", switchDelay);
                System.Threading.Thread.Sleep(switchDelay);

                //IVideoFrameAnalyzer.AVVideoStandard videoStandard = IVideoFrameAnalyzer.AVVideoStandard.NTSC_M;
                videoCardDUT.SelectColorCoding(videoStandard);
                log.DebugFormat("AVVideoStandard = {0}", videoStandard);


                int iSignalPresence = 0;
                end = System.DateTime.Now.AddMilliseconds(10000);
                while (iSignalPresence == 0 && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    iSignalPresence = videoCardMaster.GetSignalPresence();
                    System.Threading.Thread.Sleep(10);
                }
                if (iSignalPresence == 0)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Video_Source_Failed;
                    errorMsg = string.Format("Failed to get master video source");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }

                iSignalPresence = 0;
                end = System.DateTime.Now.AddMilliseconds(10000);
                while (iSignalPresence == 0 && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    iSignalPresence = videoCardDUT.GetSignalPresence();
                    System.Threading.Thread.Sleep(10);
                }
                if (iSignalPresence == 0)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Video_Source_Failed;
                    errorMsg = string.Format("Failed to get DUT video source");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }

                //videoCardMaster.StartVideo();


                //videoCardDUT.StartVideo();
                //iSignalPresence = videoCardDUT.GetSignalPresence();
                string sCapturePathMaster = @"R:\CaptureTempMaster\";
                string sCapturePathDUT = @"R:\CaptureTempDUT\";
                if (Directory.Exists(sCapturePathMaster))
                {
                    Directory.Delete(sCapturePathMaster, true);
                    System.Threading.Thread.Sleep(100);
                }
                if (Directory.Exists(sCapturePathDUT))
                {
                    Directory.Delete(sCapturePathDUT, true);
                    System.Threading.Thread.Sleep(100);
                }
                
                int iCount = 31;
                if (0 != videoCardMaster.StartCaptureSequencePicture(iCount, sCapturePathMaster))
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }

                if (0 != videoCardDUT.StartCaptureSequencePicture(iCount, sCapturePathDUT))
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }


                string[] files = new string[0];
                end = System.DateTime.Now.AddMilliseconds(5000);
                while (files.Length != iCount && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    files = Directory.GetFiles(sCapturePathMaster);
                    System.Threading.Thread.Sleep(10);
                }
                if (files.Length != iCount)
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }

                files = new string[0];
                end = System.DateTime.Now.AddMilliseconds(5000);
                while (files.Length != iCount && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    files = Directory.GetFiles(sCapturePathDUT);
                    System.Threading.Thread.Sleep(10);
                }
                if (files.Length != iCount)
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement = Convert.ToDouble(errorCode);
                    return;
                }
                //System.Threading.Thread.Sleep(1000);
                //videoCardMaster.StopVideo();
                //videoCardDUT.StopVideo();

                measurement = 0;
            }
            catch (Exception ex)
            {
                reportText = "Exception: " + ex.Message;
                log.Error(reportText);
            }
            finally
            {
                log.DebugFormat("*** {0} - Test time:{1}", System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ((TimeSpan)(DateTime.Now - startTime)).TotalSeconds.ToString("00.00"));
            }
        }
        */
        public void CompositeStreamVideoTest(SequenceContext seqContext, out double[] measurement, out string reportText, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            reportText = String.Empty;
            errorOccurred = false;
            errorCode = 0;
            errorMsg = "";
            measurement = new double[2] { -99, -99 };

            int status = 0;
            int iTimeout = 0;
            int iDelay = 0;
            int iRetry = 0;
            int switchDelay = 0;
            int captureDelay = 0;
            int startDelay = 0;
            string strVideoOutput = "";
            string sMasterImagePath = "";
            string sParendMasterImagePath = "";
            string sChildMasterImagePath = "";
            string sDutImagePath = "";
            string sOcrRectangle = "";
            string sPipRectangle = "";
            string sParentOcrRectangle = "";
            string sChildOcrRectangle = "";
            int iCaptureCount = 0;
            double iMseThreshold = 0;
            IVideoFrameAnalyzer imageServer = null;
            IVideoFrameAnalyzer videoAnalyzer = null;

            string uutResponse = string.Empty;

            DateTime end;
            DateTime elapseTime;
            DateTime startTime; startTime = DateTime.Now;
            log.DebugFormat("===== {0} ===== {1}()", seqContext.Step.Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
            try
            {
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 3000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "DELAY", out iDelay, 0);
                log.DebugFormat("Delay {0}ms..", iDelay);
                System.Threading.Thread.Sleep(iDelay);

                testParameter.getParameterAsInt(seqContext.Step.Name, "RETRIES", out iRetry, 0);
                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCH_DELAY", out switchDelay, 100);
                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_DELAY", out captureDelay, 200);
                testParameter.getParameterAsInt(seqContext.Step.Name, "START_DELAY", out startDelay, 1000);
                testParameter.getParameterAsDouble(seqContext.Step.Name, "MSE_THRESHOLD", out iMseThreshold, 5);
                testParameter.getParameterAsString(seqContext.Step.Name, "MASTER_IMAGE_PATH", out sMasterImagePath, "");
                if (sMasterImagePath == "")
                {
                    errorCode = (int)Global.ERROR_CODES.Get_TestParameter_Value_Failed;
                    errorMsg = string.Format("Failed to get test parameter, {0}", "MASTER_IMAGE_PATH");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }
                sMasterImagePath = Path.Combine(sMasterImagePath, string.Format("S{0}", m_iUutInstance));
                log.DebugFormat("Master image file path={0}", sMasterImagePath);

                //testParameter.getParameterAsString(seqContext.Step.Name, "PARENT_MASTER_IMAGE_PATH", out sParendMasterImagePath, "");
                //testParameter.getParameterAsString(seqContext.Step.Name, "CHILD_MASTER_IMAGE_PATH", out sChildMasterImagePath, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "DUT_IMAGE_PATH", out sDutImagePath, "");
                if (sDutImagePath == "")
                {
                    errorCode = (int)Global.ERROR_CODES.Get_TestParameter_Value_Failed;
                    errorMsg = string.Format("Failed to get test parameter, {0}", "DUT_IMAGE_PATH");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }
                sDutImagePath = Path.Combine(sDutImagePath, string.Format("S{0}", m_iUutInstance));

                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_COUNT", out iCaptureCount, 0);
                if (iCaptureCount == 0)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_TestParameter_Value_Failed;
                    errorMsg = string.Format("Failed to get test parameter, {0}", "CAPTURE_COUNT");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }

                string sVideoStream;
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_STREAM", out sVideoStream, "BeePNG_2m30s_ch26.ts");

                //testParameter.getParameterAsString(seqContext.Step.Name, "OCR_RECTANGLE", out sOcrRectangle, "");
                //testParameter.getParameterAsString(seqContext.Step.Name, "PIP_RECTANGLE", out sPipRectangle, "");
                //testParameter.getParameterAsString(seqContext.Step.Name, "PARENT_OCR_RECTANGLE", out sParentOcrRectangle, "");
                //testParameter.getParameterAsString(seqContext.Step.Name, "CHILD_OCR_RECTANGLE", out sChildOcrRectangle, "");
                //testParameter.getParameterAsString(seqContext.Step.Name, "CONFIG_FILE", out sConfigFile, "");

                //Clean the capture buffer
                //if (Directory.Exists(sDutImagePath))
                //{
                //    Directory.Delete(sDutImagePath, true);
                //    System.Threading.Thread.Sleep(100);
                //}

                //Start Image server
                //ImageServer imageServer = new ImageServer();
                //elapseTime = DateTime.Now;
                //imageServer.Initialize(this.m_CoreToolkit, this.log);
                //log.DebugFormat("ImageServer initialize test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                //elapseTime = DateTime.Now;
                //if (0 != imageServer.SetConfig(sDutImagePath, sMasterImagePath, iCaptureCount, sOcrRectangle))
                //{
                //    errorCode = (int)Global.ERROR_CODES.Get_Equipment_Object_Failed;
                //    errorMsg = string.Format("Failed SetConfig to Image server.");
                //    log.Error(errorMsg);
                //    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                //    return;
                //}
                //log.DebugFormat("ImageServer SetConfig test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                //elapseTime = DateTime.Now;
                //imageServer.Measure();
                //log.DebugFormat("ImageServer Measure test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                elapseTime = DateTime.Now;

                ICoreToolkit localCoreToolkit = new ICoreToolkit();
                if (File.Exists(CoreToolkit.InstallPath.InstPath.BuildFullPath("cfg\\equipmentConfig_Video.xml")))
                {
                    localCoreToolkit.activateEquipmentManager(CoreToolkit.InstallPath.InstPath.BuildFullPath("cfg\\equipmentConfig_Video.xml"));
                    string[] equipmentList = localCoreToolkit.getEquipmentsOnline();
                }

                // video card 1 for socket 1-4, video card 2 for socket 5-8
                string sVideoCaptureName = string.Empty;//= "VIDEO_CAPTURE_COMPOSITE_1";
                string sImageServerName = string.Empty;//"IMAGE_SERVER_1";

                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_EQUIPMENT", out sVideoCaptureName, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "IMAGE_SERVER_EQUIPMENT", out sImageServerName, "");

                if (m_iUutInstance > 3)
                {
                    sVideoCaptureName = "VIDEO_CAPTURE_COMPOSITE_2";
                    sImageServerName = "IMAGE_SERVER_2";

                    testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_EQUIPMENT", out sVideoCaptureName, "");
                    testParameter.getParameterAsString(seqContext.Step.Name, "IMAGE_SERVER_EQUIPMENT", out sImageServerName, "");
                }

                if (sVideoCaptureName.Length == 0)
                {
                    log.Error("VIDEO_EQUIPMENT not define in XML testParameter file.");
                    return;
                }
                if (sImageServerName.Length == 0)
                {
                    log.Error("IMAGE_SERVER_EQUIPMENT not define in XML testParameter file.");
                    return;
                }

                // create Image server object.
                imageServer = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject(sImageServerName, m_iUutInstance.ToString());
                if (imageServer == null)
                {
                    imageServer = (IVideoFrameAnalyzer)localCoreToolkit.getEquipmentObject(sImageServerName, m_iUutInstance.ToString());
                    if (imageServer == null)
                    {
                        errorCode = (int)Global.ERROR_CODES.Get_Equipment_Object_Failed;
                        errorMsg = string.Format("Failed to create Image Server objct");
                        log.Error(errorMsg);
                        seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                        return;
                    }
                    log.DebugFormat("using localCoreToolkit imageServer equipment object {0}", imageServer.Name);
                }
                else
                {
                    // share equipment driver.
                    status = m_CoreToolkit.acquireEquipmentObject(imageServer);
                    log.DebugFormat("Acquire imageServer equipment object with status={0}", status);
                }

                //System.Threading.Thread.Sleep(3000);

                // send configuration to image server.                
                imageServer.directWrite(string.Format("{0}|CaptureBuffer={1}|CaptureNumber={2}|MasterPath={3}",
                    "SetConfig", sDutImagePath, iCaptureCount, sMasterImagePath));

                // create video card object
                videoAnalyzer = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject(sVideoCaptureName, m_iUutInstance.ToString());
                if (videoAnalyzer == null)
                {
                    videoAnalyzer = (IVideoFrameAnalyzer)localCoreToolkit.getEquipmentObject(sVideoCaptureName, m_iUutInstance.ToString());
                    if (videoAnalyzer == null)
                    {
                        errorCode = (int)Global.ERROR_CODES.Get_Equipment_Object_Failed;
                        errorMsg = string.Format("Failed to create video card objct");
                        log.Error(errorMsg);
                        seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                        return;
                    }
                    log.DebugFormat("using localCoreToolkit videoAnalyzer equipment object {0}", videoAnalyzer.Name);
                }
                else
                {
                    // share equipment driver.
                    status = m_CoreToolkit.acquireEquipmentObject(videoAnalyzer);
                    log.DebugFormat("Acquire videoAnalyzer equipment object with status={0}", status);
                }

                status = SendReceive("pbist mpeg -f /shared/streams/" + sVideoStream, "STARTING STREAM", iTimeout * 2, out uutResponse);

                if (sVideoCaptureName.Contains("HDMI"))
                    status = SendReceive("hd_format 1080p30", "HDMI PHY: CLK: Drive:", iTimeout, out uutResponse);

                System.Threading.Thread.Sleep(startDelay);

                status = videoAnalyzer.StartVideo();
                log.DebugFormat("Video capture StartVideo() status = {0}", status);
                //System.Threading.Thread.Sleep(200);

                // check signal existing
                elapseTime = DateTime.Now;
                int iSignalPresence = 0;
                end = System.DateTime.Now.AddMilliseconds(10000);
                log.DebugFormat("Check Signal Presence status.");
                while (iSignalPresence == 0 && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    iSignalPresence = videoAnalyzer.GetSignalPresence();
                    System.Threading.Thread.Sleep(10);
                }
                if (iSignalPresence == 0)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Video_Source_Failed;
                    errorMsg = string.Format("Failed to get DUT video source");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    //videoAnalyzer.StopVideo();
                    //System.Threading.Thread.Sleep(200);
                    //videoAnalyzer.StartVideo();
                    measurement[0] = Convert.ToDouble(errorCode);
                    return;
                }
                log.DebugFormat("Found Signal Presence test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                // starting capture sequence picture
                System.Threading.Thread.Sleep(captureDelay);

                elapseTime = DateTime.Now;
                if (0 != videoAnalyzer.StartCaptureSequencePicture(iCaptureCount, sDutImagePath))
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement[0] = Convert.ToDouble(errorCode);
                    return;
                }
                log.DebugFormat("StartCaptureSequencePicture test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                // send measure command to image server.
                imageServer.directWrite(string.Format("{0}|", "Measure"));

                // get measure results from image server.
                imageServer.directWrite(string.Format("{0}|", "GetResults"));

                // parsing test results.
                double dPER = -1;
                double dDER = -1;
                string sReturn = "";
                Dictionary<int, int> dicOcr = new Dictionary<int, int>();
                imageServer.directRead(out sReturn, 0);

                GET_PER_DER(sReturn, iCaptureCount, iMseThreshold, ref dPER, ref dDER);

                log.DebugFormat("PER={0}, DER={1}", dPER, dDER);
                measurement[0] = dPER;
                measurement[1] = dDER;
            }
            catch (Exception ex)
            {
                reportText = "Exception: " + ex.Message;
                log.Error(reportText);
            }
            finally
            {
                status = SendReceive("quit", "/ #", 3000, out uutResponse);

                //if (videoCardDUT != null)
                //    System.Threading.Thread.Sleep(200);

                log.DebugFormat("Stopping video...");
                status = videoAnalyzer.StopVideo();
                log.DebugFormat("Video capture StopVideo() status={0}", status);
                //System.Threading.Thread.Sleep(3000);
                status = m_CoreToolkit.releaseEquipmentObject(imageServer);
                status = m_CoreToolkit.releaseEquipmentObject(videoAnalyzer);

                log.DebugFormat("*** {0} - Test time:{1}", seqContext.Step.Name, ((TimeSpan)(DateTime.Now - startTime)).TotalSeconds.ToString("00.00"));
            }
        }//CompositeStreamVideoTest

        private void GET_PER_DER(string sText, int numCapture, double limitMSE, ref double PER, ref double DER)
        {
            int iPixErr = 0;
            int iDropFrameErr = 0;
            string[] sSplit = sText.Split(new string[] { "|", "=" }, StringSplitOptions.None);
            IMAGE_TEST_INFO[] imageTestInfo = new IMAGE_TEST_INFO[numCapture];
            int Index = 0;
            int iBegin = 0;
            int iEnd = 30;

            try
            {
                for (int i = 0; i < sSplit.Length; i++)
                {
                    if (sSplit[i] == "File")
                    {
                        imageTestInfo[Index].File = sSplit[i + 1];
                    }
                    if (sSplit[i] == "OCR")
                    {
                        imageTestInfo[Index].OCR = sSplit[i + 1];
                    }
                    if (sSplit[i] == "MSE")
                    {
                        imageTestInfo[Index].MSE = Convert.ToDouble(sSplit[i + 1]);
                        if (imageTestInfo[Index].MSE > limitMSE)
                        {
                            iPixErr++;
                        }
                        Index++;
                    }
                }

                if (imageTestInfo.Length != numCapture)
                {
                    return;
                }

                double posOcr = Convert.ToDouble(imageTestInfo[0].OCR);
                for (int m = 0; m < imageTestInfo.Length; m++)
                {
                    if (posOcr != Convert.ToDouble(imageTestInfo[m].OCR))
                    {
                        iDropFrameErr++;
                        if (m < imageTestInfo.Length - 1)
                        {
                            posOcr = Convert.ToDouble(imageTestInfo[m + 1].OCR);
                        }
                    }
                    else
                    {
                        posOcr++;
                    }
                    if (posOcr > iEnd)
                        posOcr = 0;
                }

                PER = Math.Round(((double)iPixErr / (double)numCapture), 2);
                DER = Math.Round(((double)iDropFrameErr / (double)(double)numCapture), 2);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("GET_PER_DER() Exception: {0}", ex.Message);
            }
            //if (sSplit.Length == (iCaptureCount * 6 + 1))
            //{
            //    for (int i = 0; i < sSplit.Length; i++)
            //    {
            //        if (sSplit[i] == "MSE")
            //        {
            //            if (double.Parse(sSplit[i + 1]) > iMseHighLimit)
            //            {
            //                iPixErr++;
            //            }
            //        }
            //        else if (sSplit[i] == "OCR")
            //        {
            //            //int iOcr = int.Parse(sSplit[i + 1]);
            //            //if ((iOcr < 0) || (iOcr > 32))
            //            //{
            //            //    iDropFrameErr++;
            //            //}
            //            //else
            //            //{
            //            //    if (dicOcr.ContainsKey(iOcr))
            //            //    {
            //            //        iDropFrameErr++;
            //            //    }
            //            //    else
            //            //    {
            //            //        dicOcr.Add(iOcr, iOcr);
            //            //    }
            //            //}
            //        }
            //    }
            //    //log.DebugFormat("File={0}, OCR={1}, MSE={2}", imageMeasureInfo[index].File, imageMeasureInfo[index].OCR, imageMeasureInfo[index].MSE);
            //}
        }
        public void HDMI_PIP_VideoTest(SequenceContext seqContext, out double measurement, out string reportText, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            reportText = String.Empty;
            errorOccurred = false;
            errorCode = 0;
            errorMsg = "";
            measurement = -99;
            int result = -1;
            int retries = -1;
            int status = 0;
            IVideoFrameAnalyzer videoCard = null;
            int switchDelay = 100;
            int captureDelay = 1000;
            int startDelay = 10;
            string sConfigFile = "";
            int iCaptureNum = 0;
            int iPixelErrThresh = 0;
            int iHighLimit = 0;
            int iMseHighLimit = 0;
            string sMasterImagePath = "";
            string sNoFrameCounterImage = "";
            string sOcrRectangele = "";
            string sCaptureBuffPath = "";
            int PixelErrorCount = 0;
            string sImageFailBuff = "";
            int iPicErrCount = 0;
            bool bTestResult = true;
            int failCount = 0;

            DateTime thisTime = DateTime.Now;
            DateTime loopTimeout;
            DateTime elapseTime;

            log.DebugFormat("===== {0} =====", System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                int iTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 3000);

                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCH_DELAY", out switchDelay, 100);
                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_DELAY", out captureDelay, 1000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "START_DELAY", out startDelay, 10);
                testParameter.getParameterAsInt(seqContext.Step.Name, "RETRIES", out retries, 1);
                testParameter.getParameterAsString(seqContext.Step.Name, "CONFIG_FILE", out sConfigFile, "");

                string strVideoOutput = "COMPOSITE"; //COMPOSITE, SVIDEO, COMPONENT, HDMI
                GetValueFromXmlAsString(sConfigFile, "Name", "VIDEO_OUTPUT", "Value", ref strVideoOutput);
                if (strVideoOutput == null)
                {
                    result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_INVALID_PARAM;
                    reportText = String.Format("{0}:Error: You must define VIDEO_OUTPUT in you xmlTestParameter file", (int)result);
                    log.Error(reportText);
                    return;
                }

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.Unknown;

                // turn ON the proper port on Video Card
                if (status == 0)
                {
                    switch (strVideoOutput)
                    {
                        case "COMPOSITE":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "SVIDEO":
                            videoSource = IVideoFrameAnalyzer.VideoSource.SVIDEO;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_SVIDEO");
                            break;
                        case "COMPONENT":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPONENT;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPONENT");
                            break;
                        case "HDMI":
                            videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            //videoCard = (IVideoFrameAnalyzer)localCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                    }


                    // ignore the returned value of SelectVideoFormat() for now
                    videoCard.SelectVideoFormat(videoSource);
                    log.DebugFormat("Wait {0} ms for Video Input to switch before capture", switchDelay);
                    System.Threading.Thread.Sleep(switchDelay);

                    IVideoFrameAnalyzer.AVVideoStandard videoStandard = IVideoFrameAnalyzer.AVVideoStandard.NTSC_M;
                    videoCard.SelectColorCoding(videoStandard);
                    log.DebugFormat("AVVideoStandard = {0}", videoStandard);

                    GetValueFromXmlAsString(sConfigFile, "Name", "CAPTURE_BUFF_PATH", "Value", ref sCaptureBuffPath);
                    GetValueFromXmlAsInt(sConfigFile, "Name", "IMAGE_CAPTURE_NUM", "Value", ref iCaptureNum);
                    GetValueFromXmlAsInt(sConfigFile, "Name", "IMAGE_PIXEL_ERROR_THRESHOLD", "Value", ref iPixelErrThresh);
                    GetValueFromXmlAsInt(sConfigFile, "Name", "IMAGE_PIXEL_ERROR_HIGH_LIMIT", "Value", ref iHighLimit);
                    GetValueFromXmlAsString(sConfigFile, "Name", "MASTER_IMAGE_PATH", "Value", ref sMasterImagePath);
                    GetValueFromXmlAsString(sConfigFile, "Name", "NO_FRAME_COUNTER_IMAGE", "Value", ref sNoFrameCounterImage);
                    GetValueFromXmlAsString(sConfigFile, "Name", "OCR_RECTANGLE", "Value", ref sOcrRectangele);
                    GetValueFromXmlAsInt(sConfigFile, "Name", "MSE_HIGH_LIMIT", "Value", ref iMseHighLimit);

                    string command = "";
                    string strBuffer = "";
                    ICommInterface serverComm = (ICommInterface)m_CoreToolkit.getEquipmentObject("IMAGE_SERVER", m_iUutInstance.ToString());
                    if (serverComm == null)
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_UNKNOWN_ERROR;
                        reportText = String.Format("{0}:Error: Create IMAGE_SERVER equipment object failed");
                        log.Error(reportText);
                        return;
                    }
                    //serverComm.write("hello");

                    //return;
                    //open tcpip
                    //string strBuffer = "";
                    //string tcpCommand = "";

                    //ICommInterface tcpComm = (ICommInterface)m_CoreToolkit.getEquipmentObject("TCPIP_CLIENT", sTestSocket);

                    //tcpComm.open();
                    //tcpComm.close();
                    //tcpComm.initialize();
                    //tcpComm.open();
                    //tcpComm.clear();
                    command = string.Format("TX MESSAGE:START");
                    serverComm.clear();
                    serverComm.write(command + "<EOF>");
                    //serverComm.clear();
                    status = serverComm.read(ref strBuffer, 1024, 30000);
                    //tcpComm.write(tcpCommand + "<EOF>");
                    //status = tcpComm.read(ref strBuffer, 1024, 30000);
                    if (status != 0 || !strBuffer.Contains(command + "<ACK><EOF>"))
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_TIMEOUT;
                        reportText = String.Format("{0}:Error: Read message from Image Server failed");
                        log.Error(reportText);
                        return;
                    }

                    //Send test config to image server
                    elapseTime = DateTime.Now;
                    command = string.Format("TX MESSAGE:CONFIG|{0}|{1}|{2}|{3}|{4}|{5}|{6}",
                        sCaptureBuffPath, iCaptureNum, iPixelErrThresh, iHighLimit, sMasterImagePath, "bmp", sOcrRectangele);
                    serverComm.write(command + "<EOF>");
                    status = serverComm.read(ref strBuffer, 1024, 30000);
                    if (status != 0 || !strBuffer.Contains(command + "<ACK><EOF>"))
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_TIMEOUT;
                        reportText = String.Format("{0}:Error: Read message from Image Server failed");
                        log.Error(reportText);
                        return;
                    }
                    log.DebugFormat("Set Config test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                    System.Threading.Thread.Sleep(200);

                    //Send Test command to server
                    elapseTime = DateTime.Now;
                    command = string.Format("TX MESSAGE:TEST");
                    serverComm.write(command + "<EOF>");

                    //Starting capture pictures
                    elapseTime = DateTime.Now;
                    if (videoCard.StartVideo() != 0)
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_START_VIDEO_ERROR;
                        reportText = String.Format("{0}:Error: videoCard.StartVideo() error", (int)result);
                        log.Error(reportText);
                        return;
                    }
                    log.DebugFormat("StartVideo test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                    //System.Threading.Thread.Sleep(3000);

                    //for (int i = 0; i < 3; i++)
                    //{
                    //    //Send Test command to server
                    //elapseTime = DateTime.Now;
                    //command = string.Format("TX MESSAGE:TEST");
                    //serverComm.write(command + "<EOF>");
                    //read back the test result
                    status = serverComm.read(ref strBuffer, 1024, 30000);
                    if (status != 0 || !strBuffer.Contains(command + "<ACK><EOF>"))
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_TIMEOUT;
                        reportText = String.Format("{0}:Error: Read message from Image Server failed");
                        log.Error(reportText);
                        return;
                    }
                    log.DebugFormat("Image process test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));
                    //}                    

                    //Get Test Result                    
                    elapseTime = DateTime.Now;
                    command = string.Format("TX MESSAGE:GET_RESULTS");
                    serverComm.write(command + "<EOF>");
                    status = serverComm.read(ref strBuffer, 1024, 30000);
                    if (status != 0 || !strBuffer.Contains("<ACK><EOF>"))
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_TIMEOUT;
                        reportText = String.Format("{0}:Error: Read message from Image Server failed");
                        log.Error(reportText);
                        return;
                    }

                    string sTestResult = strBuffer.Replace("TX MESSAGE:GET_RESULTS:", "");
                    sTestResult = sTestResult.Replace("<ACK><EOF>", "");
                    ArrayList alTestResult = null;
                    ImageTestDataParse(sTestResult, ref alTestResult);

                    if (alTestResult.Count != iCaptureNum)
                    {
                        result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_INVALID_PARAM;
                        reportText = String.Format("{0}:Error: test result error.");
                        log.Error(reportText);
                        return;
                    }
                    //foreach (ImageTestResults test in alTestResult)
                    //{
                    //    if (test.MSE > iMseHighLimit)
                    //    {
                    //        failCount++;
                    //    }
                    //    log.DebugFormat("ImageFile={0},OCR={1},MSE={2}", test.File, test.OCR, test.MSE);
                    //}
                    log.DebugFormat("Get Test result test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));


                    //Stop the image server
                    //tcpCommand = string.Format("TX MESSAGE:STOP");
                    //tcpComm.write(tcpCommand + "<EOF>");
                    //status = tcpComm.read(ref strBuffer, 1024, 30000);
                    //if (status != 0 && strBuffer.Contains(tcpCommand + "<ACK>"))
                    //{
                    //    result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_TIMEOUT;
                    //    reportText = String.Format("{0}:Error: Read message from Image Server failed");
                    //    log.Error(reportText);
                    //    return;
                    //}
                    //tcpComm.close();

                    //Stop capture.
                    //if (videoCard.StopVideo() != 0)
                    //{
                    //    result = (int)VIDEO_FRAME_TEST_ERRORCODE.EC_SOP_CAPTURE_SEQ_PIC;
                    //    reportText = String.Format("{0}:Error: videoCard.StopCaptureSequencePicture() error", (int)result);
                    //    log.Error(reportText);
                    //    return;
                    //}


                    //System.DateTime loopTimeout = System.DateTime.Now;
                    //loopTimeout = loopTimeout.AddMilliseconds(10000);
                    //int iGetFilesCount = 0;
                    //while ((iGetFilesCount != iCaptureNum) && loopTimeout.CompareTo(System.DateTime.Now) > 0)
                    //{
                    //    iGetFilesCount = Directory.GetFiles(sCaptureBuffPath, "*.bmp", SearchOption.AllDirectories).Length;                        
                    //}



                }
                if (failCount > 3)
                {
                    result = -1;
                }
                else
                {
                    result = 0;
                }
            }
            catch (Exception e)
            {
                reportText = e.Message;
                log.Debug(reportText);
            }
            finally
            {
                measurement = result;
                log.DebugFormat("{0} test time={1}", System.Reflection.MethodBase.GetCurrentMethod().Name, Math.Round(((TimeSpan)(DateTime.Now - thisTime)).TotalSeconds, 2));
            }
        }
        /*
        public void CompositePIPStreamVideoTest(SequenceContext seqContext, out double[] measurement, out string reportText, out bool errorOccurred, out int errorCode, out string errorMsg)
        {
            reportText = String.Empty;
            errorOccurred = false;
            errorCode = 0;
            errorMsg = "";
            measurement = new double[4] { -99, -99, -99, -99 };

            int status = 0;
            int iTimeout = 0;
            int iDelay = 0;
            int iRetry = 0;
            int switchDelay = 0;
            int captureDelay = 0;
            int startDelay = 0;
            string strVideoOutput = "";
            string sMasterImagePath = "";
            string sParendMasterImagePath = "";
            string sChildMasterImagePath = "";
            string sDutImagePath = "";
            string sOcrRectangle = "";
            string sPipRectangle = "";
            string sParentOcrRectangle = "";
            string sChildOcrRectangle = "";
            int iCaptureCount = 0;
            int iMseHighLimit = 0;
            int iParentMseHighLimit = 0;
            int iChildMseHighLimit = 0;
            IVideoFrameAnalyzer videoCardMaster;
            IVideoFrameAnalyzer videoCardDUT;
            DateTime end;
            DateTime elapseTime;
            startTime = DateTime.Now;
            log.DebugFormat("===== {0} =====", System.Reflection.MethodBase.GetCurrentMethod().Name);

            try
            {
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 3000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "DELAY", out iDelay, 0);
                log.DebugFormat("Delay {0}ms..", iDelay);
                System.Threading.Thread.Sleep(iDelay);

                testParameter.getParameterAsInt(seqContext.Step.Name, "RETRIES", out iRetry, 0);
                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCH_DELAY", out switchDelay, 100);
                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_DELAY", out captureDelay, 1000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "START_DELAY", out startDelay, 10);
                testParameter.getParameterAsInt(seqContext.Step.Name, "MSE_HIGH_LIMIT", out iMseHighLimit, 5);
                testParameter.getParameterAsInt(seqContext.Step.Name, "PARENT_MSE_HIGH_LIMIT", out iParentMseHighLimit, 10);
                testParameter.getParameterAsInt(seqContext.Step.Name, "CHILD_MSE_HIGH_LIMIT", out iChildMseHighLimit, 10);
                testParameter.getParameterAsString(seqContext.Step.Name, "MASTER_IMAGE_PATH", out sMasterImagePath, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "PARENT_MASTER_IMAGE_PATH", out sParendMasterImagePath, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "CHILD_MASTER_IMAGE_PATH", out sChildMasterImagePath, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "DUT_IMAGE_PATH", out sDutImagePath, "");
                if (sDutImagePath == "")
                {
                    errorCode = (int)Global.ERROR_CODES.Get_TestParameter_Value_Failed;
                    errorMsg = string.Format("Failed to get test parameter, {0}", "DUT_CAPTURE_BUFFER");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }
                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_COUNT", out iCaptureCount, 0);
                if (iCaptureCount == 0)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_TestParameter_Value_Failed;
                    errorMsg = string.Format("Failed to get test parameter, {0}", "CAPTURE_COUNT");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }
                testParameter.getParameterAsString(seqContext.Step.Name, "OCR_RECTANGLE", out sOcrRectangle, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "PIP_RECTANGLE", out sPipRectangle, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "PARENT_OCR_RECTANGLE", out sParentOcrRectangle, "");
                testParameter.getParameterAsString(seqContext.Step.Name, "CHILD_OCR_RECTANGLE", out sChildOcrRectangle, "");
                //testParameter.getParameterAsString(seqContext.Step.Name, "CONFIG_FILE", out sConfigFile, "");

                //Clean the capture buffer
                if (Directory.Exists(sDutImagePath))
                {
                    Directory.Delete(sDutImagePath, true);
                    System.Threading.Thread.Sleep(100);
                }

                //Start Image server
                ImageServer imageServer = new ImageServer();
                elapseTime = DateTime.Now;
                imageServer.Initialize(this.m_CoreToolkit, this.log);
                log.DebugFormat("ImageServer initialize test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                elapseTime = DateTime.Now;
                imageServer.SetPipConfig(sDutImagePath, sParendMasterImagePath, sChildMasterImagePath, iCaptureCount, sPipRectangle, sParentOcrRectangle, sChildOcrRectangle);
                log.DebugFormat("ImageServer SetPipConfig test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                elapseTime = DateTime.Now;
                imageServer.MeasurePIP();
                log.DebugFormat("ImageServer Measure test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                elapseTime = DateTime.Now;
                videoCardDUT = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                if (videoCardDUT == null)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Equipment_Object_Failed;
                    errorMsg = string.Format("Failed to create video card objct");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    return;
                }
                log.DebugFormat("Create video card object test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                elapseTime = DateTime.Now;
                int iSignalPresence = 0;
                end = System.DateTime.Now.AddMilliseconds(10000);
                while (iSignalPresence == 0 && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    iSignalPresence = videoCardDUT.GetSignalPresence();
                    System.Threading.Thread.Sleep(10);
                    log.DebugFormat("Signal Presence status = {0}", iSignalPresence);
                }
                if (iSignalPresence == 0)
                {
                    errorCode = (int)Global.ERROR_CODES.Get_Video_Source_Failed;
                    errorMsg = string.Format("Failed to get DUT video source");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement[0] = Convert.ToDouble(errorCode);
                    return;
                }
                log.DebugFormat("Check signal presence test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));
                //if (Directory.Exists(sDutImagePath))
                //{
                //    Directory.Delete(sDutImagePath, true);
                //    System.Threading.Thread.Sleep(100);
                //}

                elapseTime = DateTime.Now;
                if (0 != videoCardDUT.StartCaptureSequencePicture(iCaptureCount, sDutImagePath))
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement[0] = Convert.ToDouble(errorCode);
                    return;
                }
                log.DebugFormat("StartCaptureSequencePicture test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                elapseTime = DateTime.Now;
                string[] files = new string[0];
                end = System.DateTime.Now.AddMilliseconds(5000);
                while (files.Length != iCaptureCount && DateTime.Compare(DateTime.Now, end) <= 0)
                {
                    files = Directory.GetFiles(sDutImagePath);
                    System.Threading.Thread.Sleep(10);
                }
                if (files.Length != iCaptureCount)
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to capture picture");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement[0] = Convert.ToDouble(errorCode);
                    return;
                }
                log.DebugFormat("Check image file count test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                //imageServer.Measure();
                elapseTime = DateTime.Now;
                ImageServer.IMAGE_MEASURE_INFO[] imageMeasureInfo = null;
                imageServer.FetchPIP(ref imageMeasureInfo);
                log.DebugFormat("Fetch image reading test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                measurement[0] = 0;
                if (imageMeasureInfo == null)
                {
                    errorCode = (int)Global.ERROR_CODES.Capture_Picture_Failed;
                    errorMsg = string.Format("Failed to fetch pictures measurement reading ");
                    log.Error(errorMsg);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, string.Format("ErrorCode = {0}, {1}", errorCode, errorMsg), null, false);
                    measurement[0] = Convert.ToDouble(errorCode);
                    return;
                }

                //int iPixErr = 0;
                //int iDropFrameErr = 0;
                int iParentPixErr = 0;
                int iParentDropFrameErr = 0;
                int iChildPixErr = 0;
                int iChildDropFrameErr = 0;
                elapseTime = DateTime.Now;
                string sParentOCR = "";
                string sChildOCR = "";

                for (int index = 0; index < imageMeasureInfo.Length; index++)
                {
                    //if (imageMeasureInfo[index].MSE == 999)
                    //{
                    //    Console.Write("");
                    //}
                    if (imageMeasureInfo[index].ParentMSE > iParentMseHighLimit)
                    {
                        iParentPixErr++;
                    }
                    if (sParentOCR == imageMeasureInfo[index].ParentOCR)
                    {
                        iParentDropFrameErr++;
                    }
                    sParentOCR = imageMeasureInfo[index].ParentOCR;

                    if (imageMeasureInfo[index].ChildMSE > iChildMseHighLimit)
                    {
                        iChildPixErr++;
                    }
                    if (sChildOCR == imageMeasureInfo[index].ChildOCR)
                    {
                        iChildDropFrameErr++;
                    }
                    sChildOCR = imageMeasureInfo[index].ChildOCR;

                    log.DebugFormat("File={0}, ParentOCR={1}, ParentMSE={2}, ChildOCR={3}, ChildMSE={4}",
                        imageMeasureInfo[index].File, imageMeasureInfo[index].ParentOCR, imageMeasureInfo[index].ParentMSE, imageMeasureInfo[index].ChildOCR, imageMeasureInfo[index].ChildMSE);
                }

                double dParentPER = Math.Round(((double)iParentPixErr / (double)imageMeasureInfo.Length), 2);
                double dParentDER = Math.Round(((double)iParentDropFrameErr / (double)imageMeasureInfo.Length), 2);
                double dChildPER = Math.Round(((double)iChildPixErr / (double)imageMeasureInfo.Length), 2);
                double dChildDER = Math.Round(((double)iChildDropFrameErr / (double)imageMeasureInfo.Length), 2);

                log.DebugFormat("Parent PER={0}, Parent DER={1}", dParentPER, dParentDER);
                log.DebugFormat("Child PER={0}, Child DER={1}", dChildPER, dChildDER);

                log.DebugFormat("print OCR and MSE test time={0}", Math.Round(((TimeSpan)(DateTime.Now - elapseTime)).TotalSeconds, 2));

                //System.Threading.Thread.Sleep(1000);
                //videoCardMaster.StopVideo();
                //videoCardDUT.StopVideo();

                measurement[0] = dParentPER;
                measurement[1] = dParentDER;
                measurement[2] = dChildPER;
                measurement[3] = dChildDER;

                double dParentPerHigh = seqContext.AsPropertyObject().GetValNumber(string.Format("Step.Result.Measurement[0].Limits.High"), 0);
                double dParentDerHigh = seqContext.AsPropertyObject().GetValNumber(string.Format("Step.Result.Measurement[1].Limits.High"), 0);
                double dChildPerHigh = seqContext.AsPropertyObject().GetValNumber(string.Format("Step.Result.Measurement[2].Limits.High"), 0);
                double dChildDerHigh = seqContext.AsPropertyObject().GetValNumber(string.Format("Step.Result.Measurement[3].Limits.High"), 0);

                if (dParentDER > dParentPerHigh || dParentDER > dParentDerHigh || dChildPER > dChildPerHigh || dChildDER > dChildDerHigh)
                {
                    System.Threading.Thread.Sleep(3000);
                }

                //if (dParentDER > 0.1)
                //{                    
                //    string[] sFiles = Directory.GetFiles(@"R:\CaptureTempDUT");
                //    foreach (string s in sFiles)
                //    { 
                //        FileInfo fi = new FileInfo(s);
                //        File.Copy(s, @"C:\StationLogs\Temp\" + fi.Name);
                //    }
                //}
            }
            catch (Exception ex)
            {
                reportText = "Exception: " + ex.Message;
                log.Error(reportText);
            }
            finally
            {
                log.DebugFormat("*** {0} - Test time:{1}", System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ((TimeSpan)(DateTime.Now - startTime)).TotalSeconds.ToString("00.00"));
            }
        }//CompositePIPStreamVideoTest
        */

        public void StartVideo(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;
            reportText = "";
            PrintTestName(seqContext.Step.Name);

            int status = 0;
            //int retries = 1;
            IVideoFrameAnalyzer videoCard = null;
            try
            {
                string strVideoOutput;//COMPOSITE, SVIDEO, COMPONENT, HDMI
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_OUTPUT", out strVideoOutput);
                if (strVideoOutput == null)
                {
                    status = -99;
                    reportText = string.Format("you must define VIDEO_OUTPUT in you xmlTestParameter file");
                    log.Debug(reportText);
                }

                string strReadBuffer = string.Empty;

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.Unknown;

                // turn ON the proper port on Video Card
                if (status == 0)
                {
                    switch (strVideoOutput)
                    {
                        case "COMPOSITE":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "SVIDEO":
                            videoSource = IVideoFrameAnalyzer.VideoSource.SVIDEO;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_SVIDEO");
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "COMPONENT":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPONENT;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPONENT");
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "HDMI":
                            videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                    }

                    // Start Video
                    if (videoCard != null)
                    {
                        videoCard.SelectVideoFormat(videoSource);
                        result = videoCard.StartVideo();
                    }
                    else
                        log.Error("videoCard is OFFLINE.");
                }
            }
            catch (Exception ex)
            {
                log.Debug("StartVideo Exception:\r\n", ex);
                result = -5;
            }
        }//StartVideo

        public void StopVideo(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;
            reportText = "";
            PrintTestName(seqContext.Step.Name);

            int status = 0;
            //int retries = 1;
            IVideoFrameAnalyzer videoCard = null;
            try
            {
                int stopDelay;
                testParameter.getParameterAsInt(seqContext.Step.Name, "STOP_DELAY", out stopDelay, 1000);

                string strVideoOutput;//COMPOSITE, SVIDEO, COMPONENT, HDMI
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_OUTPUT", out strVideoOutput);
                if (strVideoOutput == null)
                {
                    status = -99;
                    reportText = string.Format("you must define VIDEO_OUTPUT in you xmlTestParameter file");
                    log.Debug(reportText);
                }

                string strReadBuffer = string.Empty;

                //IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.Unknown;

                // turn ON the proper port on Video Card
                if (status == 0)
                {
                    switch (strVideoOutput)
                    {
                        case "COMPOSITE":
                            //videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "SVIDEO":
                            //videoSource = IVideoFrameAnalyzer.VideoSource.SVIDEO;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_SVIDEO");
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "COMPONENT":
                            //videoSource = IVideoFrameAnalyzer.VideoSource.COMPONENT;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPONENT");
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "HDMI":
                            //videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                    }

                    // Stop Video
                    if (videoCard != null)
                    {
                        log.DebugFormat("Wait {0} ms for DS Driver before stop", stopDelay);
                        System.Threading.Thread.Sleep(stopDelay);

                        result = videoCard.StopVideo();
                    }
                    else
                        log.Error("videoCard is OFFLINE.");
                }
            }
            catch (Exception ex)
            {
                log.Debug("StopVideo Exception:\r\n", ex);
                result = -5;
            }
        }//StopVideo
        
        private void ImageTestDataParse(string text, ref ArrayList TestResult)
        {
            try
            {
                //TestResult = new ArrayList();
                //string[] sSplit = text.Split(new string[] {","}, StringSplitOptions.None);
                //if (sSplit.Length > 0)
                //{
                //    for (int i = 0; i < sSplit.Length; i++)
                //    {
                //        string[] sSubSplit = sSplit[i].Split(new string[] { "|" }, StringSplitOptions.None);
                //        if (sSubSplit.Length == 3)
                //        {
                //            ImageTestResults itr = new ImageTestResults();
                //            itr.File = sSubSplit[0];
                //            itr.OCR = sSubSplit[1];
                //            itr.MSE = Convert.ToDouble(sSubSplit[2]);

                //            TestResult.Add(itr);

                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                log.DebugFormat("ImageTestDataParse Exception:{0}", ex.Message);
            }
        }//ImageTestDataParse
        /*
        public void SetVideoMode(SequenceContext seqContext, out double measurement, out string reportText)
        {
            measurement = -99.0;
            reportText = string.Empty;
            string Error = string.Empty;
            int status = -1;
            int timeout = 5000;
            string vOutMode = "";
            int vModeDelay = 0;

            try
            {
                //log.DebugFormat("SetVideoOutMode() - {0}", seqContext.Step.Name);

                //testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out timeout, 5000);
                //testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCHDELAY", out vModeDelay, 100);
                //testParameter.getParameterAsString(seqContext.Step.Name, "VIDEOMODE", out vOutMode, "CVBS");

                //log.DebugFormat("Set Video out Mode is ", vOutMode);

                //switch (vOutMode)
                //{
                //    case "CVBS":
                //        //status = uut.VideoTest(HmtStbComm.IHmtStbComm.VideoOutput.CVBS, timeout, out Error);
                //        break;
                //    case "YPBPR":
                //        //status = uut.VideoTest(HmtStbComm.IHmtStbComm.VideoOutput.YPBPR, timeout, out Error);
                //        break;
                //    case "SVIDEO":
                //    case "HDMI":
                //        break;
                //}
                //if (status == 0)
                //{
                //    log.DebugFormat("Wait {0} ms for video output swap", vModeDelay);
                //    System.Threading.Thread.Sleep(vModeDelay);
                //    measurement = (double)status;
                //}
            }
            catch (Exception ex)
            {
                log.Error("SetVideoOutMode() Exception:\r\n", ex);
                measurement = -5.0;
            }
        }   //setVideoMode()

        public void SetPadAVSelect(SequenceContext seqContext, out double measurement, out string reportText)
        {
            measurement = -99.0;
            reportText = string.Empty;
            string Error = string.Empty;
            int status = -1;
            int timeout = 5000;
            string videoSrc = "";
            string audioSrc = "";
            int switchDelay = 0;

            try
            {
                //log.DebugFormat("SetPadAVSelect() - {0}", seqContext.Step.Name);

                //testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out timeout, 5000);
                //testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCHDELAY", out switchDelay, 100);
                //testParameter.getParameterAsString(seqContext.Step.Name, "VIDEOSRC", out videoSrc, "STB");
                //testParameter.getParameterAsString(seqContext.Step.Name, "AUDIOSRC", out audioSrc, "STB");

                //log.DebugFormat("Set Video source - {0} | Audio source - {1} ", videoSrc,audioSrc);

                //switch (videoSrc)
                //{
                //    case "STB":
                //        status = uut.SetPADVGASelect(HmtStbComm.IHmtStbComm.PADVGA.STB, out Error);
                //        break;
                //    case "PAD":
                //        status = uut.SetPADVGASelect(HmtStbComm.IHmtStbComm.PADVGA.PAD, out Error);
                //        break;
                //    case "VGA":
                //        status = uut.SetPADVGASelect(HmtStbComm.IHmtStbComm.PADVGA.ExternalVGA, out Error);
                //        break;
                //    case "N/A":
                //        status = 0;
                //        log.Debug("Parameter set to N/A, skip Video Source switch.");
                //        break;
                //    default:
                //        break;
                //}
                //System.Threading.Thread.Sleep(100);
                //if (0 == status)
                //{
                //    switch (audioSrc)
                //    {
                //        case "STB":
                //            status = uut.SetPADAudioSelect(HmtStbComm.IHmtStbComm.PADAudio.STB, out Error);
                //            break;
                //        case "PAD":
                //            status = uut.SetPADAudioSelect(HmtStbComm.IHmtStbComm.PADAudio.PAD, out Error);
                //            break;
                //        case "N/A":
                //            log.Debug("Parameter set to N/A, skip Audio Source switch.");
                //            break;
                //        default:
                //            break;
                //    }
                //}
                //if (status == 0)
                //{
                //    log.DebugFormat("Wait {0} ms for AV source selection", switchDelay);
                //    System.Threading.Thread.Sleep(switchDelay);
                //    measurement = (double)status;
                //}
            }
            catch (Exception ex)
            {
                log.Error("SetPadAVSelect() Exception:\r\n", ex);
                measurement = -5.0;
            }
        }  //setVideoMode()
        */

    }
    public struct IMAGE_TEST_INFO
    {
        public int ID;
        public string File;
        public string OCR;
        public string ParentOcr;
        public string ChildOcr;
        public int PixelErrorCount;
        public double MSE;
        public double ParentMse;
        public double ChildMse;
        public Bitmap ComparedBitmap;
        public string Error;
    }
}