﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {

        //=====================================================================================================================
        public bool LED_500_00_01_LED_Standby_Blue(SequenceContext seqContext)
        {
            TestTask.Send_TT_Command("7", "4", "1899");

            return true;
        }

        //=====================================================================================================================
        public void LED_500_00_A1_LEDS_Off(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99; //error

            PrintTestName(seqContext.Step.Name);

            //Turn off all the leds
            //pbist led_off
            //"NO RESPONSE"
            command = "pbist led_off";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            //pass
            result[0] = 0;

        }

        //=====================================================================================================================
        public void LEDS_Off(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -99; //error

            PrintTestName(seqContext.Step.Name);

            //Turn off all the leds
            //pbist led_off
            //"NO RESPONSE"
            command = "pbist led_off";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            //pass
            result = 0;

        }
        //=====================================================================================================================
        public void LED_500_00_A2_LED_Red_On(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99; //error

            PrintTestName(seqContext.Step.Name);

            //Turn on the status_red led
            //pbist led_status_red
            //OK LED status_red
            command = "pbist led_status_red";
            response = "OK LED status_red";

            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                result[0] = -1;
                reportText = "Failed to Turn on the status_red led";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void LED_Red_On(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -99; //error

            PrintTestName(seqContext.Step.Name);

            //Turn on the status_red led
            //pbist led_status_red
            //OK LED status_red
            command = "pbist led_status_red";
            response = "OK LED status_red";

            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result = 0;
            }
            else
            {
                //fail
                result = -1;
                reportText = "Failed to Turn on the status_red led";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void LED_500_00_A3_LED_Green_On(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99; //error

            PrintTestName(seqContext.Step.Name);

            //Turn on the status_green led
            //pbist led_status_green
            //OK LED status_green
            command = "pbist led_status_green";
            response = "OK LED status_green";

            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                result[0] = -1;
                reportText = "Failed to Turn on the status_green led";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void LED_Green_On(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -99; //error

            PrintTestName(seqContext.Step.Name);

            //Turn on the status_green led
            //pbist led_status_green
            //OK LED status_green
            command = "pbist led_status_green";
            response = "OK LED status_green";

            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result = 0;
            }
            else
            {
                //fail
                result = -1;
                reportText = "Failed to Turn on the status_green led";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void LED_004_00_00_LEDS(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[5];

            //set result to fail
            result[0] = -99; //error
            result[1] = 0; //LED_004_00_01_LED_Status_Red_Off
            result[2] = 0; //LED_004_00_02_LED_Status_Green_Off
            result[3] = 0; //LED_004_00_03_LED_Status_Red_On
            result[4] = 0; //LED_004_00_04_LED_Status_Green_On

            PrintTestName(seqContext.Step.Name);

            //Turn off all the leds
            //pbist led_off
            //"NO RESPONSE"
            command = "pbist led_off";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            //pause 50ms to let voltages settle
            System.Threading.Thread.Sleep(50);

            //Measure status_red led voltage off
            result[1] = AMB.SK1_A20.MeasureDC(); //LED_004_00_01_LED_Status_Red_Off

            //Measure status_green led voltage off
            result[2] = AMB.SK1_A21.MeasureDC(); //LED_004_00_02_LED_Status_Green_Off

            //Turn on the status_red led
            //pbist led_status_red
            //OK LED status_red
            command = "pbist led_status_red";
            response = "OK LED status_red";

            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
            }
            else
            {
                //fail
                reportText = "Failed to Turn on the status_red led";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            //pause 50ms to let voltage settle
            System.Threading.Thread.Sleep(50);

            //Measure status_red led voltage on
            result[3] = AMB.SK1_A20.MeasureDC(); //LED_004_00_03_LED_Status_Red_On

            //Turn off all the leds
            //pbist led_off
            //"NO RESPONSE"
            command = "pbist led_off";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            //pause 50ms to let voltage settle
            System.Threading.Thread.Sleep(50);

            //Turn on the status_green led
            //pbist led_status_green
            //OK LED status_green
            command = "pbist led_status_green";
            response = "OK LED status_green";

            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
            }
            else
            {
                //fail
                reportText = "Failed to Turn on the status_green led";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            //pause 50ms to let voltage settle
            System.Threading.Thread.Sleep(50);

            //Measure status_green led voltage on
            result[4] = AMB.SK1_A21.MeasureDC(); //LED_004_00_04_LED_Status_Green_On

            //Turn off all the leds
            //pbist led_off
            //"NO RESPONSE"
            command = "pbist led_off";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

    }

}
