﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //public Tests()
        //{
        //initialisation code here = OnFileLoad

        //}

        //test steps here
        //=====================================================================================================================
        public void UUT_001_00_A1_UUT_Comms_Initialize(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            result = UUT_Comms_Initialize(seqContext);
            reportText = "";

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void INT_001_00_A1_PFTS_Check_Hardware_FAT(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            PFTS_Check_Hardware_FAT(seqContext);
            result = !PFTS.ErrorOccurred();
            reportText = PFTS.ErrorLog();
            PFTS.ClearErrorOccurred();
            PFTS.ClearErrorLog();

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void INT_001_00_A2_PFTS_Check_Hardware_PCB(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            PFTS_Check_Hardware_PCB(seqContext);
            result = !PFTS.ErrorOccurred();
            reportText = PFTS.ErrorLog();
            PFTS.ClearErrorOccurred();
            PFTS.ClearErrorLog();

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void INT_001_00_A3_PFTS_Check_Hardware_DEV(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            PFTS_Check_Hardware_DEV(seqContext);
            result = !PFTS.ErrorOccurred();
            reportText = PFTS.ErrorLog();
            PFTS.ClearErrorOccurred();
            PFTS.ClearErrorLog();

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void INT_001_00_A4_PFTS_Reset_Hardware(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            PFTS_Reset_Hardware(seqContext);
            result = !PFTS.ErrorOccurred();
            reportText = PFTS.ErrorLog();
            PFTS.ClearErrorOccurred();
            PFTS.ClearErrorLog();

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void UUT_001_00_01_Power_On_PFTS(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            // test code at this level direct from seq file
            MainsCard.ProductPowerOn();
            result = !MainsCard.ErrorOccurred();
            reportText = MainsCard.ErrorLog();
            MainsCard.ClearErrorOccurred();
            MainsCard.ClearErrorLog();

            // test code at MTS level
            //bool _result;
            //string _reportText;

            //UUT.Power_On_PFTS(seqContext, out _result, out _reportText);
            //result = _result;
            //reportText = _reportText;

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void UUT_002_00_02_Boot_UUT(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            result = Boot_UUT(seqContext);
            reportText = "";

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void UUT_004_00_01_Check_Product_ID(SequenceContext seqContext, out string result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            TestTask.Send_TT_Command(":", "0", "");
            result = TestTask.GetReturnedData();
            reportText = "";

            //UUT.Check_Product_ID(seqContext);

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void LED_500_00_01_LED_Standby_Blue(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            result = LED_500_00_01_LED_Standby_Blue(seqContext);
            reportText = "";
            //return MTS_UUT.TestTask.Send_TT_Command("7", "4", "1899");

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void UUT_001_00_02_Power_Off_PFTS(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            // test code at this level direct from seq file
            MainsCard.ProductPowerOff();
            result = !MainsCard.ErrorOccurred();
            reportText = MainsCard.ErrorLog();
            MainsCard.ClearErrorOccurred();
            MainsCard.ClearErrorLog();

            // test code at MTS level
            //bool _result;
            //string _reportText;

            //UUT.Power_Off_PFTS(seqContext, out _result, out _reportText);
            //result = _result;
            //reportText = _reportText;

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================
        public void UUT_001_00_A2_UUT_Common_Terminate(SequenceContext seqContext, out bool result, out string reportText)
        {
            PrintTestName(seqContext.Step.Name);

            result = UUT_Comms_Terminate(seqContext);
            reportText = "";

            log.Debug(""); //add blank line to Output
        }

        //=====================================================================================================================

    }

}
