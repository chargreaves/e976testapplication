using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CBootLogger
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int BootLogger_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool BootLogger_InitializeArmadillo(out int Handle, string szLogSocketAddress, int FixturePosition, string ArmadilloNetworkCard);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool BootLogger_InitializeComPort(out int Handle, string szLogSocketAddress, int FixturePosition, string portName, int bitRate, bool flowControlOn);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int BootLogger_WaitForString(int Handle, string string1, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool BootLogger_WaitForAndReturn(int Handle, StringBuilder ret_string, int ret_string_len, string string1, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int BootLogger_WaitForOr(int Handle, string string1, string string2, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int BootLogger_WaitForOr3(int Handle, string string1, string string2, string string3, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_SetBreak(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_ClearBreak(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_SetRTS(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_ClearRTS(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_SetDTR(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_ClearDTR(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool BootLogger_GetCTS(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_ClearRubishOnPort(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_SetSpecialStringToWatch(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_ClearSpecialStringWatch(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool BootLogger_CheckSpecialStringWatch(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool BootLogger_IsOpen(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_ToggleRTS(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void BootLogger_Destroy(int Handle);

        private int m_Handle;


        public bool Initialize(string szLogSocketAddress, int FixturePosition, string ArmadilloNetworkCard)
        {
            if (BootLogger_GetInterfaceVersion() != 1) return false;
            return BootLogger_InitializeArmadillo(out m_Handle, szLogSocketAddress, FixturePosition, ArmadilloNetworkCard);
        }
        public bool Initialize(string szLogSocketAddress, int FixturePosition, string portName, int bitRate, bool flowControlOn)
        {
            if (BootLogger_GetInterfaceVersion() != 1) return false;
            return BootLogger_InitializeComPort(out m_Handle, szLogSocketAddress, FixturePosition, portName, bitRate, flowControlOn);
        }
        public int WaitForString(string string1, int timeout)
        {
            return BootLogger_WaitForString(m_Handle, string1, timeout);
        }
        public string SendReceive(string string1, int timeout)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            BootLogger_WaitForAndReturn(m_Handle, ret_string, ret_string.Capacity, string1, timeout);
            return ret_string.ToString();
        }
        public int WaitForOr(string string1, string string2, int timeout)
        {
            return BootLogger_WaitForOr(m_Handle, string1, string2, timeout);
        }
        public int WaitForOr3(string string1, string string2, string string3, int timeout)
        {
            return BootLogger_WaitForOr3(m_Handle, string1, string2, string3, timeout);
        }
        public void SetBreak()
        {
            BootLogger_SetBreak(m_Handle);
        }
        public void ClearBreak()
        {
            BootLogger_ClearBreak(m_Handle);
        }
        public void SetRTS()
        {
            BootLogger_SetRTS(m_Handle);
        }
        public void ClearRTS()
        {
            BootLogger_ClearRTS(m_Handle);
        }
        public void SetDTR()
        {
            BootLogger_SetDTR(m_Handle);
        }
        public void ClearDTR()
        {
            BootLogger_ClearDTR(m_Handle);
        }
        public bool GetCTS()
        {
            return BootLogger_GetCTS(m_Handle);
        }
        public void ClearRubishOnPort()
        {
            BootLogger_ClearRubishOnPort(m_Handle);
        }
        public void SetSpecialStringToWatch()
        {
            BootLogger_SetSpecialStringToWatch(m_Handle);
        }
        public void ClearSpecialStringWatch()
        {
            BootLogger_ClearSpecialStringWatch(m_Handle);
        }
        public bool CheckSpecialStringWatch()
        {
            return BootLogger_CheckSpecialStringWatch(m_Handle);
        }
        public bool IsOpen()
        {
            return BootLogger_IsOpen(m_Handle);
        }
        public void Stop()
        {
            BootLogger_Stop(m_Handle);
        }
        public void Reset()
        {
            BootLogger_Reset(m_Handle);
        }
        public void ToggleRTS()
        {
            BootLogger_ToggleRTS(m_Handle);
        }
        public void Destroy()
        {
            BootLogger_Destroy(m_Handle);
        }
    }
}
