using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CFirmwareLogger
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int FirmwareLogger_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool FirmwareLogger_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, int port, string szNetworkCardIP, string szClientIP);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool FirmwareLogger_SetClientAddress(int Handle, string szClientIP);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void FirmwareLogger_Start(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void FirmwareLogger_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void FirmwareLogger_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void FirmwareLogger_Destroy(int Handle);


        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, int port, string szNetworkCardIP, string szClientIP)
        {
            if (FirmwareLogger_GetInterfaceVersion() != 1) return false;
            return FirmwareLogger_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, port, szNetworkCardIP, szClientIP);
        }
        public bool SetClientAddress(int FixturePos, string szClientIP)
        {
            return FirmwareLogger_SetClientAddress(m_Handle, szClientIP);
        }
        public void Start()
        {
            FirmwareLogger_Stop(m_Handle);
        }
        public void Stop()
        {
            FirmwareLogger_Stop(m_Handle);
        }
        public void Reset()
        {
            FirmwareLogger_Reset(m_Handle);
        }
        public void Destroy()
        {
            FirmwareLogger_Destroy(m_Handle);
        }
    }
}
