using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CTVCard
    {
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileIntW", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetPrivateProfileInt(string Section, string Key, int Default, string FilePath);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TVCard_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool TVCard_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, IntPtr hTVWnd, bool useArmadillo, string ArmadilloNetworkCard);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TVCard_CaptureAndVerifyVideo(int Handle, bool AveragePixels, bool SVideoColorTest, out double LargestDiffR,
            out double LargestDiffG, out double LargestDiffB, out double ErrorSumR, out double ErrorSumG, out double ErrorSumB, out double BWSum,
            double DiffLimitR, double DiffLimitG, double DiffLimitB, double ErrorSumLimitR, double ErrorSumLimitG, double ErrorSumLimitB, double BWSumLimit,
            string MeasLargestDiffR, string MeasLargestDiffG, string MeasLargestDiffB, string MeasErrorSumR, string MeasErrorSumG, string MeasErrorSumB, string MeasBWSum);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TVCard_CaptureAndVerifyVideoHDMI(int Handle, out double DiffR, out double DiffG, out double DiffB,
                                                                   double DiffHdmiLimitR, double DiffHdmiLimitG, double DiffHdmiLimitB,
                                                                   string MeasDiffR, string MeasDiffG, string MeasDiffB);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TVCard_SelectVideoFormat(int Handle, int TvFormat);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TVCard_SelectColorCoding(int Handle, int ColorCoding);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TVCard_SetTVSettings(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TVCard_CreateTvCardWnd(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TVCard_ReStartHdmiDecoder(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TVCard_CheckStablePicture(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TVCard_Destroy(int Handle);

        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, IntPtr hTVWnd, bool useArmadillo, string ArmadilloNetworkCard)
        {
            if (TVCard_GetInterfaceVersion() != 1) return false;
            return TVCard_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, hTVWnd, useArmadillo, ArmadilloNetworkCard);
        }
        public int CaptureAndVerifyVideo(bool AveragePixels, bool SVideoColorTest, out double LargestDiffR,
            out double LargestDiffG, out double LargestDiffB, out double ErrorSumR, out double ErrorSumG, out double ErrorSumB, out double BWSum)
        {
            double DiffLimitR, DiffLimitG, DiffLimitB, ErrorSumLimitR, ErrorSumLimitG, ErrorSumLimitB, BWSumLimit;

            // Set wide enough hard coded limits for now (MTP handels the limit on its own)
            // For now, open backdoor to settings.ini
            int diff = GetPrivateProfileInt("VideoTest", "DiffLimit", 300, ".\\settings.ini");
            //Message.Format("Set fix largest diff high limits to %d\n", diff);
            DiffLimitR = diff;
            DiffLimitG = diff;
            DiffLimitB = diff;

            int errorSum = GetPrivateProfileInt("VideoTest", "ErrorSumLimit", 10000, ".\\settings.ini");
            //Message.Format("Set fix error sum high limits to %d\n", errorSum);
            ErrorSumLimitR = errorSum;
            ErrorSumLimitG = errorSum;
            ErrorSumLimitB = errorSum;

            int bwSum = GetPrivateProfileInt("VideoTest", "BWSumLimit", 100000, ".\\settings.ini");
            //Message.Format("Set fix bw sum high limit to %d\n", bwSum);
            BWSumLimit = bwSum;

            return TVCard_CaptureAndVerifyVideo(m_Handle, AveragePixels, SVideoColorTest, out LargestDiffR,
              out LargestDiffG, out LargestDiffB, out ErrorSumR, out ErrorSumG, out ErrorSumB, out BWSum,
              DiffLimitR, DiffLimitG, DiffLimitB, ErrorSumLimitR, ErrorSumLimitG, ErrorSumLimitB, BWSumLimit,
              "LargestDiffR", "LargestDiffG", "LargestDiffB", "ErrorSumR", "ErrorSumG", "ErrorSumB", "BWSum");
        }
        public int CaptureAndVerifyVideoHDMI(out double DiffR, out double DiffG, out double DiffB)
        {
            double DiffHdmiLimitR, DiffHdmiLimitG, DiffHdmiLimitB;

            // Set wide enough hard coded dummy limits (MTP handels the limit on its own)
            // Backdoor opened in settings.ini
            int diffHdmi = GetPrivateProfileInt("VideoTest", "DiffHdmiLimit", 500, ".\\settings.ini");
            //Message.Format("Set fix diff high limits to %d\n", diffHdmi);
            DiffHdmiLimitR = diffHdmi;
            DiffHdmiLimitG = diffHdmi;
            DiffHdmiLimitB = diffHdmi;

            return TVCard_CaptureAndVerifyVideoHDMI(m_Handle, out DiffR, out DiffG, out DiffB,
                                                    DiffHdmiLimitR, DiffHdmiLimitG, DiffHdmiLimitB,
                                                    "DiffR", "DiffG", "DiffB");
        }

        public enum TV_FORMAT
        {
            TV_FORMAT_UNKNOWN=-1,
            TV_FORMAT_CVBS=0,
            TV_FORMAT_SVIDEO=1,
            TV_FORMAT_LUMA_ONLY=2,
            TV_FORMAT_HDMI=3
        }
        public void SelectVideoFormat(TV_FORMAT TvFormat)
        {
            TVCard_SelectVideoFormat(m_Handle, (int)TvFormat);
        }
        public enum AVVideoFormat
        {
            AVVideoFormat_NONE = 0x0000,
            AVVideoFormatNTSC_M = 0x0001,
            AVVideoFormatPAL_BGHIDK = 0x0002,
            AVVideoFormatSECAM = 0x0004,
            AVVideoFormatPAL_M = 0x0008,
            AVVideoFormatPAL_N =0x0010,
            AVVideoFormatNTSC_443 = 0x0020,
            AVVideoFormatPAL_NCOMBO = 0x0040,
            AVVideoFormatNTSC_Japan = 0x0080,
            AVVideoFormatPAL_DK = 0x0100,
            TV_FORMAT_UNKNOWN = -1,
            TV_FORMAT_CVBS = 0,
            TV_FORMAT_SVIDEO = 1,
            TV_FORMAT_LUMA_ONLY = 2,
            TV_FORMAT_HDMI = 3
        }
        public void SelectColorCoding(AVVideoFormat ColorCoding)
        {
            TVCard_SelectColorCoding(m_Handle, (int)ColorCoding);
        }
        public void SetTVSettings()
        {
            TVCard_SetTVSettings(m_Handle);
        }
        public void CreateTvCardWnd()
        {
            TVCard_CreateTvCardWnd(m_Handle);
        }
        public void ReStartHdmiDecoder()
        {
            TVCard_ReStartHdmiDecoder(m_Handle);
        }
        public void CheckStablePicture()
        {
            TVCard_CheckStablePicture(m_Handle);
        }
        public void Destroy()
        {
            TVCard_Destroy(m_Handle);
        }
    }
}
