using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

// static GetInterfaceVersion()

namespace CommonCS
{
    class CJtagST
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int JtagST_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool JtagST_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, bool useTelnet);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool JtagST_SetIpAddress(int Handle, string ip_addr, string networkCard);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int JtagST_Send(int Handle, string send_string);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool JtagST_SendReceive(int Handle, StringBuilder ret_string, int ret_string_len, string send_string, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool JtagST_Receive(int Handle, StringBuilder ret_string, int ret_string_len, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool JtagST_StartGDB(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool JtagST_IsGDBRunning(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void JtagST_SetPrompt(int Handle, string Prompt);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void JtagST_SetDefaultPrompt(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void JtagST_Flush(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void JtagST_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void JtagST_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void JtagST_Destroy(int Handle);

        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, bool useTelnet)
        {
            if (JtagST_GetInterfaceVersion() != 1) return false;
            return JtagST_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, useTelnet);
        }
        public bool SetIpAddress(string ip_addr, string networkCard)
        {
            return JtagST_SetIpAddress(m_Handle, ip_addr, networkCard);
        }
        public int Send(string send_string)
        {
            return JtagST_Send(m_Handle, send_string);
        }
        public string SendReceive(string send_string, int timeout)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            JtagST_SendReceive(m_Handle, ret_string, ret_string.Capacity, send_string, timeout);
            return ret_string.ToString();
        }
        public string Receive(int timeout)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            JtagST_Receive(m_Handle, ret_string, ret_string.Capacity, timeout);
            return ret_string.ToString();
        }
        public bool StartGDB()
        {
            return JtagST_StartGDB(m_Handle);
        }
        public bool IsGDBRunning()
        {
            return JtagST_IsGDBRunning(m_Handle);
        }
        public void SetPrompt(string Prompt)
        {
            JtagST_SetPrompt(m_Handle, Prompt);
        }
        public void SetDefaultPrompt()
        {
            JtagST_SetDefaultPrompt(m_Handle);
        }
        public void Flush()
        {
            JtagST_Flush(m_Handle);
        }
        public void Stop()
        {
            JtagST_Stop(m_Handle);
        }
        public void Reset()
        {
            JtagST_Reset(m_Handle);
        }
        public void Destroy()
        {
            JtagST_Destroy(m_Handle);
        }
    }
}
