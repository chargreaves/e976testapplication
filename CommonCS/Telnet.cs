using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CTelnet
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Telnet_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, string host, string networkCard, bool verbose);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_SetHostAddress(int Handle, string szClientIP);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Telnet_Send(int Handle, string send_string);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Telnet_SendRaw(int Handle, string send_string);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_SendReceive(int Handle, StringBuilder ret_string, int ret_string_len, string send_string, int timeout, bool autoUpdateProgressBar);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_Receive(int Handle, StringBuilder ret_string, int ret_string_len, int timeout, bool bEchoCheck, bool bForceQuiet);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_SetPrompt(int Handle, StringBuilder ret_string, int ret_string_len, string Promt);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_SetDefaultPrompt(int Handle, StringBuilder ret_string, int ret_string_len, string Promt);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_SetTimeout(int Handle, int milliseconds);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Telnet_GetTimeout(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_Connect(int Handle, int timeout, bool ReadLoginString);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_DisConnect(int Handle, bool Kill);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_IsConnected(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_SetTerminalType(int Handle, int type);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_SetReceiveColor(int Handle, UInt32 receiveColor);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_SetSendColor(int Handle, UInt32 sendColor);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_SetQuietMode(int Handle, bool quiet);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_SetVerbose(int Handle, bool verbose);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Telnet_GetVerbose(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern UInt32 Telnet_SetKeepAlive(int Handle, UInt32 KeepAliveTime);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_Reconnect(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_Start(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Telnet_Destroy(int Handle);

        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, string host, string networkCard, bool verbose)
        {
            if (Telnet_GetInterfaceVersion() != 1) return false;
            return Telnet_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, host, networkCard, verbose);
        }
        public bool SetHostAddress(int FixturePos, string szClientIP)
        {
            return Telnet_SetHostAddress(m_Handle, szClientIP);
        }
        public int Send(string send_string)
        {
            return Telnet_Send(m_Handle, send_string);
        }
        public int SendRaw(string send_string)
        {
            return Telnet_SendRaw(m_Handle, send_string);
        }
        public string SendReceive(string send_string, int timeout)
        {
            return SendReceive(send_string, timeout, false);
        }
        public string SendReceive(string send_string, int timeout, bool autoUpdateProgressBar)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            Telnet_SendReceive(m_Handle, ret_string, ret_string.Capacity, send_string, timeout, autoUpdateProgressBar);
            return ret_string.ToString();
        }
        public string Receive(int timeout)
        {
            return Receive(timeout, false, false);
        }
        public string Receive(int timeout, bool bEchoCheck, bool bForceQuiet)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            Telnet_Receive(m_Handle, ret_string, ret_string.Capacity, timeout, bEchoCheck, bForceQuiet);
            return ret_string.ToString();
        }
        public string SetPrompt(string Promt)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            Telnet_SetPrompt(m_Handle, ret_string, ret_string.Capacity, Promt);
            return ret_string.ToString();
        }
        public string SetDefaultPrompt(string Promt)
        {
            StringBuilder ret_string = new StringBuilder(1000000);
            Telnet_SetDefaultPrompt(m_Handle, ret_string, ret_string.Capacity, Promt);
            return ret_string.ToString();
        }
        public void SetTimeout(int milliseconds)
        {
            Telnet_SetTimeout(m_Handle, milliseconds);
        }
        public int GetTimeout()
        {
            return Telnet_GetTimeout(m_Handle);
        }
        public bool Connect(int timeout, bool ReadLoginString)
        {
            return Telnet_Connect(m_Handle, timeout, ReadLoginString);
        }
        public void DisConnect(bool Kill)
        {
            Telnet_DisConnect(m_Handle, Kill);
        }
        public bool IsConnected()
        {
            return Telnet_IsConnected(m_Handle);
        }
        public void SetTerminalType(int type)
        {
            Telnet_SetTerminalType(m_Handle, type);
        }
        public void SetReceiveColor(UInt32 receiveColor)
        {
            Telnet_SetReceiveColor(m_Handle, receiveColor);
        }
        public void SetSendColor(UInt32 sendColor)
        {
            Telnet_SetSendColor(m_Handle, sendColor);
        }
        public void SetQuietMode(bool quiet)
        {
            Telnet_SetQuietMode(m_Handle, quiet);
        }
        public void SetVerbose(bool verbose)
        {
            Telnet_SetVerbose(m_Handle, verbose);
        }
        public bool GetVerbose()
        {
            return Telnet_GetVerbose(m_Handle);
        }
        public UInt32 SetKeepAlive(UInt32 KeepAliveTime)
        {
            return Telnet_SetKeepAlive(m_Handle, KeepAliveTime);
        }
        public void Reconnect()
        {
            Telnet_Reconnect(m_Handle);
        }
        public void Start()
        {
            Telnet_Start(m_Handle);
        }
        public void Stop()
        {
            Telnet_Stop(m_Handle);
        }
        public void Reset()
        {
            Telnet_Reset(m_Handle);
        }
        public void Destroy()
        {
            Telnet_Destroy(m_Handle);
        }
    }
}
