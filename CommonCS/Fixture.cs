using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CFixture
    {
        [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileIntW", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern int GetPrivateProfileInt(string Section, string Key, int Default, string FilePath);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Fixture_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, bool useArmadillo, string ArmadilloNetworkCard);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_InitializeIr(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_ActivatePowerSupply(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_DeActivatePowerSupply(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_ActivateDUTPowerSupply(int Handle, string channel, double voltage);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_DeActivateDUTPowerSupply(int Handle, string channel);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ResetDUT(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ReleaseReset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SetPowerButton(int Handle, bool on);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SetAuxFunctionSelect(int Handle, int func_sel);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SetAuxBlanking(int Handle, int blank);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SelectSoundSource(int Handle, int source);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureVoltageDrop(int Handle, int source);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureFunctionSelect(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureBlanking(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_MeasureSPDIF(int Handle, out double VoltageRead_neg);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_SupportSPDIFAutoTest(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_TestSPDIF(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_TestTOSLINK(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ActivateIR(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_DeActivateIR(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ActivateSmartCard(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_DeActivateSmartCard(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ActivateVacuum(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_DeActivateVacuum(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Fixture_CheckBoard(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Fixture_CheckLid(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_CheckSound(int Handle, int channel, double ExpectedFrequency, bool bGenAudio, out double measuredAmplitude,
							  out double measuredFrequency, out double measuredHarmonics, out double measuredHarmonicsFrequency,
                              double LowestAcceptedFrequency, double HighestAcceptedFrequency, double HarmonicsMargin, double MinAcceptedLevel, double MaxAcceptedLevel,
                              string MeasAcceptedFrequency, string MeasHarmonicsMargin, string MeasAcceptedLevel,
                              StringBuilder szError, int nErrorLen, bool bShowBitmap);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SelectSoundInput(int Handle, int destination);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SelectVideoSource(int Handle, int source);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_EnableVideoInput(int Handle, bool enable);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SetVideoGain(int Handle, string channelName, int gain);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SelectFlash(int Handle, int flash);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ControlUSBReset(int Handle, bool active);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureResistor(int Handle, int source);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureTunerModuleIdVoltage(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureDutSupplyCurrent(int Handle, string channel);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_MeasureSpareVoltages(int Handle, string source, out double voltageread);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern double Fixture_MeasureUSBVoltage(int Handle, int usbsource);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_GetPinState(int Handle, int pin);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_UsesVacuum(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_IncrementNeedleCounter(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_IncrementDvbtCableCounter(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_IncrementHdmiCableCounter(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_CheckFixture(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_InitializeFixtureCounter(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ReleaseResetIr(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_ResetIr(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_StopIr(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_StartIr(int Handle, int frequency);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_FirmwareDownload(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_IsArmadilloFixture(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_CheckArmadilloVersion(int Handle, string RequiredArmadilloVersion);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_FindHdmiManufacturer(int Handle, string result);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_MeasureHDMISignals(int Handle, out double avgPos, out double avgNeg,
                                                              double hVHdmiP, double hVHdmiL, double lVHdmiP, double lVHdmiL,
                                                              string MeasVoltageLevelP, string MeasVoltageLevelL);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int Fixture_CheckFixtureLidThread(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_SetLed(int Handle, int Red, int Green, int Blue);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool Fixture_BBSFirmwareProgramming(int Handle, string fw_file, int timeout);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void Fixture_Destroy(int Handle);

        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, bool useArmadillo, string ArmadilloNetworkCard)
        {
            if (Fixture_GetInterfaceVersion() != 1) return false;
            return Fixture_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, useArmadillo, ArmadilloNetworkCard);
        }
        public bool InitializeIr()
        {
            return Fixture_InitializeIr(m_Handle);
        }
        public bool ActivatePowerSupply()
        {
            return Fixture_ActivatePowerSupply(m_Handle);
        }
        public bool DeActivatePowerSupply()
        {
            return Fixture_DeActivatePowerSupply(m_Handle);
        }
        public bool ActivateDUTPowerSupply(string channel, double voltage)
        {
            return Fixture_ActivateDUTPowerSupply(m_Handle, channel, voltage);
        }
        public bool DeActivateDUTPowerSupply(string channel)
        {
            return Fixture_DeActivateDUTPowerSupply(m_Handle, channel);
        }
        public void ResetDUT()
        {
            Fixture_ResetDUT(m_Handle);
        }
        public void ReleaseReset()
        {
            Fixture_ReleaseReset(m_Handle);
        }
        public void SetPowerButton(bool on)
        {
            Fixture_SetPowerButton(m_Handle, on);
        }
        public void SetAuxFunctionSelect(int func_sel)
        {
            Fixture_SetAuxFunctionSelect(m_Handle, func_sel);
        }
        public void SetAuxBlanking(int blank)
        {
            Fixture_SetAuxBlanking(m_Handle, blank);
        }
        public void SelectSoundSource(int source)
        {
            Fixture_SelectSoundSource(m_Handle, source);
        }
        public double MeasureVoltageDrop(int source)
        {
            return Fixture_MeasureVoltageDrop(m_Handle, source);
        }
        public double MeasureFunctionSelect()
        {
            return Fixture_MeasureFunctionSelect(m_Handle);
        }
        public double MeasureBlanking()
        {
            return Fixture_MeasureBlanking(m_Handle);
        }
        public bool MeasureSPDIF(out double VoltageRead_neg)
        {
            return Fixture_MeasureSPDIF(m_Handle, out VoltageRead_neg);
        }
        public bool SupportSPDIFAutoTest()
        {
            return Fixture_SupportSPDIFAutoTest(m_Handle);
        }
        public bool TestSPDIF()
        {
            return Fixture_TestSPDIF(m_Handle);
        }
        public bool TestTOSLINK()
        {
            return Fixture_TestTOSLINK(m_Handle);
        }
        public void ActivateIR()
        {
            Fixture_ActivateIR(m_Handle);
        }
        public void DeActivateIR()
        {
            Fixture_DeActivateIR(m_Handle);
        }
        public void ActivateSmartCard()
        {
            Fixture_ActivateSmartCard(m_Handle);
        }
        public void DeActivateSmartCard()
        {
            Fixture_DeActivateSmartCard(m_Handle);
        }
        public void ActivateVacuum()
        {
            Fixture_ActivateVacuum(m_Handle);
        }
        public void DeActivateVacuum()
        {
            Fixture_DeActivateVacuum(m_Handle);
        }
        public int CheckBoard()
        {
            return Fixture_CheckBoard(m_Handle);
        }
        public int CheckLid()
        {
            return Fixture_CheckLid(m_Handle);
        }
        public bool CheckSound(int channel, double ExpectedFrequency, bool bGenAudio, out double measuredAmplitude,
                              out double measuredFrequency, out double measuredHarmonics, out double measuredHarmonicsFrequency, out string szError, int nErrorLen, bool bShowBitmap)
        {
            double LowestAcceptedFrequency, HighestAcceptedFrequency, HarmonicsMargin, MinAcceptedLevel, MaxAcceptedLevel;

            StringBuilder error_string = new StringBuilder(1000000);
            bool ret;
            LowestAcceptedFrequency = -100;
            HighestAcceptedFrequency = 100;
            HarmonicsMargin = 0;
            MinAcceptedLevel = 0;
            MaxAcceptedLevel = 2000;

            ret = Fixture_CheckSound(m_Handle, channel, ExpectedFrequency, bGenAudio, out measuredAmplitude,
                              out measuredFrequency, out measuredHarmonics, out measuredHarmonicsFrequency,
                              LowestAcceptedFrequency, HighestAcceptedFrequency, HarmonicsMargin, MinAcceptedLevel, MaxAcceptedLevel,
                              "AcceptedFrequency", "HarmonicsMargin", "AcceptedLevel",
                              error_string, error_string.Capacity, bShowBitmap);
            szError = error_string.ToString();
            return ret;
        }
        public void SelectSoundInput(int destination)
        {
            Fixture_SelectSoundInput(m_Handle, destination);
        }
        public void SelectVideoSource(int source)
        {
            Fixture_SelectVideoSource(m_Handle, source);
        }
        public void EnableVideoInput(bool enable)
        {
            Fixture_EnableVideoInput(m_Handle, enable);
        }
        public void SetVideoGain(string channelName, int gain)
        {
            Fixture_SetVideoGain(m_Handle, channelName, gain);
        }
        public void SelectFlash(int flash)
        {
            Fixture_SelectFlash(m_Handle, flash);
        }
        public void ControlUSBReset(bool active)
        {
            Fixture_ControlUSBReset(m_Handle, active);
        }
        public double MeasureResistor(int source)
        {
            return Fixture_MeasureResistor(m_Handle, source);
        }
        public double MeasureTunerModuleIdVoltage()
        {
            return Fixture_MeasureTunerModuleIdVoltage(m_Handle);
        }
        public double MeasureDutSupplyCurrent(string channel)
        {
            return Fixture_MeasureDutSupplyCurrent(m_Handle, channel);
        }
        public bool MeasureSpareVoltages(string source, out double voltageread)
        {
            return Fixture_MeasureSpareVoltages(m_Handle, source, out voltageread);
        }
        public double MeasureUSBVoltage(int usbsource)
        {
            return Fixture_MeasureUSBVoltage(m_Handle, usbsource);
        }
        public bool GetPinState(int pin)
        {
            return Fixture_GetPinState(m_Handle, pin);
        }
        public bool UsesVacuum()
        {
            return Fixture_UsesVacuum(m_Handle);
        }
        public bool IncrementNeedleCounter()
        {
            return Fixture_IncrementNeedleCounter(m_Handle);
        }
        public bool IncrementDvbtCableCounter()
        {
            return Fixture_IncrementDvbtCableCounter(m_Handle);
        }
        public bool IncrementHdmiCableCounter()
        {
            return Fixture_IncrementHdmiCableCounter(m_Handle);
        }
        public bool CheckFixture()
        {
            return Fixture_CheckFixture(m_Handle);
        }
        public void InitializeFixtureCounter()
        {
            Fixture_InitializeFixtureCounter(m_Handle);
        }
        public void ReleaseResetIr()
        {
            Fixture_ReleaseResetIr(m_Handle);
        }
        public void ResetIr()
        {
            Fixture_ResetIr(m_Handle);
        }
        public void StopIr()
        {
            Fixture_StopIr(m_Handle);
        }
        public bool StartIr(int frequency)
        {
            return Fixture_StartIr(m_Handle, frequency);
        }
        public bool FirmwareDownload()
        {
            return Fixture_FirmwareDownload(m_Handle);
        }
        public bool IsArmadilloFixture()
        {
            return Fixture_IsArmadilloFixture(m_Handle);
        }
        public bool CheckArmadilloVersion(string RequiredArmadilloVersion)
        {
            return Fixture_CheckArmadilloVersion(m_Handle, RequiredArmadilloVersion);
        }
        public bool FindHdmiManufacturer(string result)
        {
            return Fixture_FindHdmiManufacturer(m_Handle, result);
        }
        public bool MeasureHDMISignals(out double avgPos, out double avgNeg)
        {
            double hVHdmiP, hVHdmiL, lVHdmiP, lVHdmiL;

            // Set wide enough hard coded dummy limits (MTP handels the limit on its own)
            // Backdoor opened in settings.ini
            int hVHdmi = GetPrivateProfileInt("VideoTest", "VoltageLevelHigh", 5, ".\\settings.ini");
            int lVHdmi = GetPrivateProfileInt("VideoTest", "VoltageLevelLow", 0, ".\\settings.ini");
            //Message.Format("Set fix voltage levels limits, high=%d low=%d\n", hVHdmi, lVHdmi);
            hVHdmiP = hVHdmi;
            hVHdmiL = hVHdmi;
            lVHdmiP = lVHdmi;
            lVHdmiL = lVHdmi;

            return Fixture_MeasureHDMISignals(m_Handle, out avgPos, out avgNeg,
                                              hVHdmiP, hVHdmiL, lVHdmiP, lVHdmiL,
                                              "VoltageLevelP", "VoltageLevelL");
        }
        public int CheckFixtureLidThread()
        {
            return Fixture_CheckFixtureLidThread(m_Handle);
        }
        public void SetLed(int Red, int Green, int Blue)
        {
            Fixture_SetLed(m_Handle, Red, Green, Blue);
        }
        public bool BBSFirmwareProgramming(string fw_file, int timeout)
        {
            return Fixture_BBSFirmwareProgramming(m_Handle, fw_file, timeout);
        }
        public void Destroy()
        {
            Fixture_Destroy(m_Handle);
        }
    }
}
