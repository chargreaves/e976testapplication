using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    struct CLabelInfo
    {
        public string PcbSerialNumber;
        public string SerialNumber;
        public string MacAddress;
        public string WiFiMacAddress;
        public string CustomerSerial;
        public string ChipID;
        public string CustomerOrderNumber;
        public string ShipmentDate;
        public string TopBom;
        public string BoxBom;
        public string PcbaBom;
        public string PcbVersion;
        public string Model;
        public string EanCode;
        public string Customer;
        public string HwFeatures;
        public string MadeInCountry;
        public string LabelLayout;
        public string PowerRatingProductLabel;
        public string PowerRatingGiftBoxLabel;
        public string MustPrint;
        public string CanPrint;

        public CLabelInfo(string sDefault)
        {
            PcbSerialNumber = string.Empty;
            SerialNumber = string.Empty;
            MacAddress = string.Empty;
            WiFiMacAddress = string.Empty;
            CustomerSerial = string.Empty;
            ChipID = string.Empty;
            CustomerOrderNumber = string.Empty;
            ShipmentDate = string.Empty;
            TopBom = string.Empty;
            BoxBom = string.Empty;
            PcbaBom = string.Empty;
            PcbVersion = string.Empty;
            Model = string.Empty;
            EanCode = string.Empty;
            Customer = string.Empty;
            HwFeatures = string.Empty;
            MadeInCountry = string.Empty;
            LabelLayout = string.Empty;
            PowerRatingProductLabel = string.Empty;
            PowerRatingGiftBoxLabel = string.Empty;
            MustPrint = string.Empty;
            CanPrint = string.Empty;
        }
    };

    class CLabelPrint
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int LabelPrint_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool LabelPrint_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool LabelPrint_LabelPrint(int Handle, ref CLabelInfo LabelInfo, int Timeout, StringBuilder FailMsg, int FailMsg_len);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void LabelPrint_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void LabelPrint_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void LabelPrint_Destroy(int Handle);

        private int m_Handle;


        public bool Initialize(string szLogSocketAddress, int FixturePosition)
        {
            if (LabelPrint_GetInterfaceVersion() != 1) return false;
            return LabelPrint_Initialize(out m_Handle, szLogSocketAddress, FixturePosition);
        }

        public bool LabelPrint(CLabelInfo LabelInfo, int Timeout, out string FailMsg)
        {
            CLabelInfo LabelInfoTmp;
            StringBuilder sbFailMsg = new StringBuilder(1000);
            bool result;
            LabelInfoTmp = LabelInfo;
            result = LabelPrint_LabelPrint(m_Handle, ref LabelInfoTmp, Timeout, sbFailMsg, sbFailMsg.Capacity);
            FailMsg = sbFailMsg.ToString();
            return result;
        }
        public void Stop()
        {
            LabelPrint_Stop(m_Handle);
        }
        public void Reset()
        {
            LabelPrint_Reset(m_Handle);
        }
        public void Destroy()
        {
            LabelPrint_Destroy(m_Handle);
        }
    }
}
