using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    struct CCagInputParam
    {
        public string RequiredCagVersion;
        public string SerialNumber;
        public string PcbSerialNumber;
        //Added for FgPartNumber support
        public string FgPartNumber;
        public string CustomerSerial;
        public string ViaccessSerial;
        public string ModelId;
        public string EthernetMac1;
        public string EthernetMac2;
        public string FormatId;
        public string PlatFormId;
        public string ChipType;
        public string ProductionData;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10000)]
        public byte[] ConfigParam;
        public int ConfigParamLen;
        public string HardwareRevision;
        public string CASEID;
        public string InputDataTypes;
        public string PkiDataTypes;
        public string OutputDataTypes;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 528)]
        public byte[] MasterKey;
        public int MasterKeyLen;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 48)]
        public byte[] GlobalEk;
        public int GlobalEkLen;
        public string VendorSpecificData;
        public string KreaTvHdcpKey;
        //Added for MSFT data support
        public string HwConfigString;
        public string CustomerId;
        public string DiscoveryServerURL;
        public string VideoFormatCapability;
        public string VideoFormatDefault;
        public string CustomerIdIRRCU;
        public string IrProtocol;
        public string HpnaMac1;
        public string Guid1;
        public bool HpnaProduct;
        public string BCMDiagOffset;
        public string CustomerIdRFRCU;
        public string BootLoaderMessage;
        //Added for MotoPLYR license support
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
        public byte[] MotoplyrLicenseTemplate;
        public int MotoplyrLicenseTemplateLen;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1024)]
        public byte[] PlaycastLicenseTemplate;
        public int PlaycastLicenseTemplateLen;
        public bool NoPKI;
        public string LCID;
        public string Oem_App_Params;
        public string Mps_Url;
        public string IrdetoPrivateData;
        public string BluetoothMac;

        public CCagInputParam(string sDefault)
        {
            RequiredCagVersion = string.Empty;
            SerialNumber = string.Empty;
            PcbSerialNumber = string.Empty;
            FgPartNumber = string.Empty;
            CustomerSerial = string.Empty;
            ViaccessSerial = string.Empty;
            ModelId = string.Empty;
            EthernetMac1 = string.Empty;
            EthernetMac2 = string.Empty;
            FormatId = string.Empty;
            PlatFormId = string.Empty;
            ChipType = string.Empty;
            ProductionData = string.Empty;
            ConfigParam = new byte[10000];
            ConfigParamLen = 0;
            HardwareRevision = string.Empty;
            CASEID = string.Empty;
            InputDataTypes = string.Empty;
            PkiDataTypes = string.Empty;
            OutputDataTypes = string.Empty;
            MasterKey = new byte[528]; //string.Empty;
            MasterKeyLen = 0;
            GlobalEk = new byte[48]; //string.Empty;
            GlobalEkLen = 0;
            VendorSpecificData = string.Empty;
            KreaTvHdcpKey = string.Empty;

            HwConfigString = string.Empty;
            CustomerId = string.Empty;
            DiscoveryServerURL = string.Empty;
            VideoFormatCapability = string.Empty;
            VideoFormatDefault = string.Empty;
            CustomerIdIRRCU = string.Empty;
            IrProtocol = string.Empty;
            HpnaMac1 = string.Empty;
            Guid1 = string.Empty;
            HpnaProduct = false;
            BCMDiagOffset = string.Empty;
            CustomerIdRFRCU = string.Empty;
            BootLoaderMessage = string.Empty;

            MotoplyrLicenseTemplate = new byte[1024];
            MotoplyrLicenseTemplateLen = 0;
            PlaycastLicenseTemplate = new byte[1024];
            PlaycastLicenseTemplateLen = 0;
            NoPKI = false;
            LCID = string.Empty;
            Oem_App_Params = string.Empty;
            Mps_Url = string.Empty;
            IrdetoPrivateData = string.Empty;
            BluetoothMac = string.Empty;
        }
    };

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    struct CCagOutputParam
    {
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string MUA;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string Mac;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string VerimatrixID;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string XpuSerialNumber;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string ViaccessSerialNumber;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string FTPDBPublicSerialNumber;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string HpnaMac1;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string Guid1;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string licenseFeatureList;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string PlaycastLicenseFeatureList;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string HDCP_KSV;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string MUA_ST231;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string SKM_PUBLIC_ID;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string MUA_RSA_V2;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string MUA_AGILUS;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string IrdetoPrivateData;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string HDCP_RX_KSV;
        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string HDCP_RX_MUA;
        
        public bool HelloOK;
        public bool TLVDataOK;

        [MarshalAs(UnmanagedType.LPStr, SizeConst = 100)]
        public string SerialNumber;

        public CCagOutputParam(string sDefault)
        {
            // fixed 100 char of memory space for unmanaged code (VIPConfigurationStation.dll) to copy data back to here (managed code)
            string empty100 = string.Empty;
            empty100 = empty100.PadLeft(100, (char)0);
            MUA = empty100;
            Mac = empty100;
            VerimatrixID = empty100;
            XpuSerialNumber = empty100;
            ViaccessSerialNumber = empty100;
            FTPDBPublicSerialNumber = empty100;
            HpnaMac1 = empty100;
            Guid1 = empty100;
            licenseFeatureList = empty100;
            PlaycastLicenseFeatureList = empty100;
            HDCP_KSV = empty100;
            MUA_ST231 = empty100;
            SKM_PUBLIC_ID = empty100;
            MUA_RSA_V2 = empty100;
            MUA_AGILUS = empty100;
            IrdetoPrivateData = empty100;
            HDCP_RX_KSV = empty100;
            HDCP_RX_MUA = empty100;
            SerialNumber = empty100;
            HelloOK = false;
            TLVDataOK = false;
        }
    };
    class CConfigurationStation
    {
        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int ConfigurationStation_GetInterfaceVersion();

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool ConfigurationStation_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool ConfigurationStation_InitializeListenSocket(out int Handle, string szLogSocketAddress, int FixturePosition);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool ConfigurationStation_ActivatePosition(int Handle, ref CCagInputParam pCagInputParam, int timeout, string szClientIP);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool ConfigurationStation_DeActivatePosition(int Handle, ref CCagOutputParam pCagOutputParam, int timeout);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int ConfigurationStation_GetClientPort(int Handle);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void ConfigurationStation_Stop(int Handle);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void ConfigurationStation_ExitListenSocket(int Handle);

        [DllImport("VIPConfigurationStation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void ConfigurationStation_Destroy(int Handle);

        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition)
        {
            if (ConfigurationStation_GetInterfaceVersion() != 1) return false;
            return ConfigurationStation_Initialize(out m_Handle, szLogSocketAddress, FixturePosition);
        }
        public bool InitializeListenSocket(string szLogSocketAddress, int FixturePosition)
        {
            if (ConfigurationStation_GetInterfaceVersion() != 1) return false;
            return ConfigurationStation_InitializeListenSocket(out m_Handle, szLogSocketAddress, FixturePosition);
        }
        public bool ActivatePosition(ref CCagInputParam pCagInputParam, int timeout, string szClientIP)
        {
            return ConfigurationStation_ActivatePosition(m_Handle, ref pCagInputParam, timeout, szClientIP);
        }
        public bool DeActivatePosition(out CCagOutputParam pCagOutputParam, int timeout)
        {
            bool ret;
            CCagOutputParam CagOutputParam = new CCagOutputParam("");
            ret = ConfigurationStation_DeActivatePosition(m_Handle, ref CagOutputParam, timeout);
            pCagOutputParam = CagOutputParam;
            return ret;
        }
        public int GetClientPort()
        {
            return ConfigurationStation_GetClientPort(m_Handle);
        }
        public void Stop()
        {
            ConfigurationStation_Stop(m_Handle);
        }
        public void ExitListenSocket()
        {
            ConfigurationStation_ExitListenSocket(m_Handle);
        }
        public void Destroy()
        {
            ConfigurationStation_Destroy(m_Handle);
        }
    }
}
