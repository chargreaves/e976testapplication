﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //=====================================================================================================================
        public void SER_001_00_00_Serialisation_Set(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1; //SER_001_00_01_Write_PCB_Number

            PrintTestName(seqContext.Step.Name);

            scannedSerialNumber = seqContext.AsPropertyObject().GetValString("RunState.Root.Locals.UUT.SerialNumber", 0);

            //Write PCBA Serial Number
            //flashtool force write --usp -b
            //Write OK
            command = "flashtool force write --usp -b " + scannedSerialNumber;
            response = "Write OK";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText = "Scanned Barcode: " + scannedSerialNumber;
                log.Debug(reportText);
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to Write PCBA Serial Number " + scannedSerialNumber;
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void Write_MAC_PCBA(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1; //SER_001_00_02_Write_MAC_PCBA

            PrintTestName(seqContext.Step.Name);

            scannedSerialNumber = seqContext.AsPropertyObject().GetValString("RunState.Root.Locals.UUT.SerialNumber", 0);
            scannedMacAddress = seqContext.AsPropertyObject().GetValString("Locals.TestInfo.UserData.UserFields[0].Data", 0);

            //Write MAC Address and PCBA Serial Number 
            //flashtool force write --usp -m 44:AA:F5:39:D8:33 -b E9768MX709999999
            //Write OK
            command = "flashtool force write --usp -m " + scannedMacAddress + " -b " + scannedSerialNumber;
            response = "Write OK";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText = "Scanned PCBA Serial Number: " + scannedSerialNumber;
                reportText += "\n" + "Scanned MAC Address: " + scannedMacAddress;
                log.Debug(reportText);
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to Write PCBA Serial Number " + scannedSerialNumber;
                reportText += "\n" + "Failed to Write MAC Address " + scannedMacAddress;
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void SER_002_00_00_Serialisation_Check(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[2];

            //set result to fail
            result[0] = -1; //SER_002_00_04_Read_PCB_Number
            result[0] = -1; //SER_002_00_28_Read_NUID

            PrintTestName(seqContext.Step.Name);

            //Read PCBA Serial Number
            //flashtool id_info force
            //PCB Serial: m_UnitInfo.PcbSerialNumber
            command = "flashtool id_info force";
            response = "PCB Serial: " + scannedSerialNumber;
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText = "Scanned Barcode: " + scannedSerialNumber + " matches PCB Serial ok";
                log.Debug(reportText);
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to Read PCBA Serial Number " + scannedSerialNumber;
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            //Read NUID
            //chip_id
            //CHIP_IDA0=
            command = "chip_id";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            string strNUID = GetNUID(strReadBuffer);

            if (strNUID != "ERROR")
            {
                //pass
                reportText += "\n" + "NUID read: " + strNUID;
                log.Debug(reportText);
                result[1] = 0;
            }
            else
            {
                //fail
                reportText += "\n" + "Failed to Read NUID";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void PCBA_Check_Scan(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1; //PCBA_Check_Scan:SER_005_00_01_Check_PCBA

            PrintTestName(seqContext.Step.Name);

            string sPCBACountryCode = string.Empty; //MX or FM
            testParameter.getParameterAsString(seqContext.Step.Name, "PCBA_Country_Code", out sPCBACountryCode, "xx");

            scannedSerialNumber = seqContext.AsPropertyObject().GetValString("RunState.Root.Locals.UUT.SerialNumber", 0);

            // check for valid country code in scanned serial number, MX or FM
            // E9768MX701123456
            if (scannedSerialNumber.Substring(5,2) == sPCBACountryCode)
            {
                //pass
                reportText = "Scanned Barcode: " + scannedSerialNumber + " Country Code " + sPCBACountryCode + " OK";
                log.Debug(reportText);
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Scanned Barcode: " + scannedSerialNumber + " Country Code " + sPCBACountryCode + " Invalid";
                log.Debug(reportText);
            }

        }

        //=====================================================================================================================
        public void Serialisation_Set_SN(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;
            string sPCBA = string.Empty; //PCBA read from UUT
            string sSN = string.Empty; //SN read from DB

            reportText = string.Empty;
            result = new double[5];

            //set result to fail
            result[0] = -99; //SER_003_00_01_Read_PCBA
            result[1] = -99; //SER_003_00_02_Get_SN_from_DB
            result[2] = -99; //SER_003_00_03_Write_PCBA_SN
            result[3] = -99; //SER_003_00_04_Check_PCBA
            result[4] = -99; //SER_003_00_05_Check_SN

            PrintTestName(seqContext.Step.Name);

            //Read PCBA
            //flashtool id_info force
            command = "flashtool id_info force";
            response = "PCB Serial:";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //extract PCBA
                sPCBA = strReadBuffer.Substring(strReadBuffer.IndexOf(response) + response.Length + 1, 16);

                //check PCBA is not blank
                if (sPCBA != string.Empty)
                {
                    //pass
                    reportText = "PCBA read from UUT: " + sPCBA;
                    log.Debug(reportText);
                    result[0] = 0;
                }
                else
                {
                    //fail
                    reportText = "Blank PCBA read from UUT";
                    log.Debug(reportText);
                    result[0] = -2;
                }
            }
            else
            {
                //fail
                reportText = "Failed to Read PCBA";
                log.Debug(reportText);
                result[0] = -1;
                return;
            }

            //Get SN from DB using PCBA Serial Number
            sSN = "12345678901234";
            result[1] = 0;

            //Write PCBA Serial Number and SN
            //flashtool force write --usp -b E9768MX709999999 -s 12345678901234
            //Write OK
            command = "flashtool force write --usp" + " -b " + sPCBA + " -s " + sSN;
            response = "Write OK";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText += "\n" + "Write PCBA and SN ok";
                log.Debug(reportText);
                result[2] = 0;
            }
            else
            {
                //fail
                reportText += "\n" + "Failed to Write PCBA and SN";
                log.Debug(reportText);
                return;
            }

            //Read PCBA and SN
            //flashtool id_info force
            command = "flashtool id_info force";
            response = "PCB Serial: " + sPCBA;
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText += "\n" + "Read PCBA: " + sPCBA + " ok";
                log.Debug(reportText);
                result[3] = 0;
            }
            else
            {
                //fail
                reportText += "\n" + "Failed to Read PCBA: " + sPCBA;
                log.Debug(reportText);
                result[3] = -1;
                return;
            }

            response = "Serial number: " + sSN;
            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText += "\n" + "Read SN: " + sSN + " ok";
                log.Debug(reportText);
                result[4] = 0;
            }
            else
            {
                //fail
                reportText += "\n" + "Failed to Read SN: " + sSN;
                log.Debug(reportText);
                result[4] = -1;
                return;
            }

        }

        //=====================================================================================================================
        public void Read_MAC_PCBA(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[3];

            //set result to fail
            result[0] = -1; //SER_002_00_04_Read_PCBA_Number
            result[1] = -1; //SER_002_00_08_Read_UUT_MAC
            result[2] = -1; //SER_002_00_28_Read_NUID

            PrintTestName(seqContext.Step.Name);

            //Read PCBA Serial Number
            //flashtool id_info force
            //PCB Serial: m_UnitInfo.PcbSerialNumber
            command = "flashtool id_info force";
            response = "PCB Serial: " + scannedSerialNumber;
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText = "Scanned PCBA Serial Number: " + scannedSerialNumber + " matches PCBA Serial Number ok";
                log.Debug(reportText);
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to Read PCBA Serial Number " + scannedSerialNumber;
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            response = "MAC address: " + scannedMacAddress;
            if (strReadBuffer.Contains(response))
            {
                //pass
                reportText += "\n" + "Scanned MAC Address: " + scannedMacAddress + " matches UUT MAC Address ok";
                log.Debug(reportText);
                result[1] = 0;
            }
            else
            {
                //fail
                reportText += "\n" + "Failed to Read UUT MAC Address " + scannedMacAddress;
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            //Read NUID
            //chip_id
            //CHIP_IDA0=
            command = "chip_id";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            string strNUID = GetNUID(strReadBuffer);

            if (strNUID != "ERROR")
            {
                //pass
                reportText += "\n" + "NUID read: " + strNUID;
                log.Debug(reportText);
                result[2] = 0;
            }
            else
            {
                //fail
                reportText += "\n" + "Failed to Read NUID";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        private string GetNUID(string chipIDData)
        {
            string sSearch, sDataFound;
            int iLoc, iLocStart;

            sDataFound = "ERROR";
            sSearch = "CHIP_IDA0=";
            iLoc = chipIDData.IndexOf(sSearch);

            if (iLoc > 0)
            {
                iLocStart = iLoc + 12;
                sDataFound = chipIDData.Substring(iLocStart, 8);
                sDataFound = sDataFound.ToUpper();
                return sDataFound;

            }
            else
            {
                return sDataFound;
            }

        }

        //=====================================================================================================================
        public void SER_001_00_02_MQS_PCB_Number(SequenceContext seqContext, out string result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = string.Empty; //SER_001_00_02_MQS_PCB_Number

            PrintTestName(seqContext.Step.Name);

            //Read PCBA Serial Number
            //flashtool id_info force
            //PCB Serial: m_UnitInfo.PcbSerialNumber
            command = "flashtool id_info force";
            response = "PCB Serial: " + scannedSerialNumber;
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            if (strReadBuffer.Contains(response))
            {
                //pass
                result = scannedSerialNumber;
            }
            else
            {
                //fail
                return;
            }

         }

        //=====================================================================================================================
        public void SER_001_00_03_MQS_NUID(SequenceContext seqContext, out string result, out string reportText)
        {
            string command;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = string.Empty; //SER_001_00_03_MQS_NUID

            PrintTestName(seqContext.Step.Name);

            //Read NUID
            //chip_id
            //CHIP_IDA0=
            command = "chip_id";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            string strNUID = GetNUID(strReadBuffer);

            if (strNUID != "ERROR")
            {
                //pass
                result = strNUID;
            }
            else
            {
                //fail
                return;
            }

        }
 
        //=====================================================================================================================
        public void VERSION_001_00_00_MTC(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99;

            PrintTestName(seqContext.Step.Name);

            string sMTCVersion = string.Empty; //mtc.3.14.0
            testParameter.getParameterAsString(seqContext.Step.Name, "Expected_MTC_Version", out sMTCVersion, "");

            string sBootloaderVersion = string.Empty; //bolt v1.26 factory-boot-loader.3.11.0
            testParameter.getParameterAsString(seqContext.Step.Name, "Expected_Bootloader_Version", out sBootloaderVersion, "");

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            command = "version_info"; //Read all version information
            response = "Version: " + sMTCVersion; //MTC Version
            //response = "Version: mtc.3.14.0"; //MTC Version
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
            }
            else
            {
                //fail
                result[0] = -1;
                reportText += command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
                return;
            }
            response = "Bootloader: " + sBootloaderVersion; //Bootloader Version
            //response = "Bootloader: bolt v1.26 factory-boot-loader.3.11.0"; //Bootloader Version
            if (strReadBuffer.Contains(response))
            {
                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                result[0] = -2;
                reportText += command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
                return;
            }

        }

    }

}
