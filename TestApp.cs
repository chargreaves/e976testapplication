using System;
using System.Reflection;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Xml;
using System.Data;

using NationalInstruments.TestStand.Interop.API;
using CoreToolkit;
using Logging;
using TestParameters;
using LV_Client;
using CAM;
using CommInterface;
using ScuHost;

using CommonCS;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private LVClient m_CoreToolkit;
        private ICoreToolkit m_LocalCoreToolkit;
        private ITestParameters testParameter;
        private ILogging log;
        //private IScuHost scuModemVoip = null;
        //private ICommInterface networkSwitch;
        private int m_iUutInstance;
        private string scannedSerialNumber;
        private string scannedMacAddress;
        //private string scannedDeviceAccessCode;
        //string scannedWifiPassword;
        //private string scannedWifiNetworkName;
        private string failedTestName;
        private string logFilename;
        //private DutInfoForm dutInfoDialog;
        // Selenium variables
        //private IWebDriver driver;
        //private StringBuilder verificationErrors;
        //private string baseIP;
        // private string baseURL;
        //private bool acceptNextAlert = true;

        //private int iScanDeviceAccessCode;
        //private int iScanMacAddress;
        //private int iScanWifiNetworkName;
        //private int iScanWifiPassword;

        //private CSemaphore semaphore;
        //private bool releasedSemaphore;

        private ICommInterface uut;
        private ICommInterface uut_ts;
        private ICommInterface usquid;
        private FirmwareLogger diagPort;
        private NetLogger netLog;
        private IScuHost scu;
        private IScuHost scu2;
        private CDHCPServer m_DHCPServer;
        private CNFSServer m_NFSServer;
        private CTFTPServer m_TFTPServer;
        private CConfigurationStation m_ConfigurationStation;
        private UnitInfo m_UnitInfo;

        private string m_sNetworkIpAddress = string.Empty;
        private int m_iNetworkIpAddressOffset = 0;
        private int m_iPingDutDisabled = 0;
        private string m_sClientIpAddress = string.Empty;
        private string m_sNfsShare = string.Empty;

        private string m_LogFilename;
        private string m_LogFilePath;
        private bool m_bSaveEachlog;
        LogClient m_LogClient;
        private string m_StationType;
        ExecProcess m_InfocastServer;

        private int iDebug;

        private const string ARMADILLO_REQUIRED_VERSION = "1.5.2";

        public TestApp()
        {
            System.IO.Directory.SetCurrentDirectory(@"c:\mtp\bin");

            log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            log.Debug("SetCurrentDirectory(@c:\\mtp\\bin)");
            log.Debug("Connecting to LV Server");
            m_CoreToolkit = new LVClient();
            m_DHCPServer = new CDHCPServer();
            m_NFSServer = new CNFSServer();
            m_TFTPServer = new CTFTPServer();
            m_ConfigurationStation = new CConfigurationStation();

            // TODO: Move all equipment init to setup to have proper test socket information
            scu = (IScuHost)m_CoreToolkit.getEquipmentObject("SCU");

            scu2 = (IScuHost)m_CoreToolkit.getEquipmentObject("SCU2");

            //dmm = (IDmm)m_CoreToolkit.getEquipmentObject("DMM");

            //video = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_ANALYZER");

            //video = new HauppaugeWinTVCaptureImpl.TVCardMethods();
            //videoA = new ArmadilloTVCaptureImpl.TVCardMethods();
            //audAnz = (IAudioAnalyzer)m_CoreToolkit.getEquipmentObject("AUDIO_ANALYZER");
            //audGen = (IAudioGenerator)audAnz;
        }

        ~TestApp()
        {
            m_CoreToolkit.UnregisterChannel();
            m_CoreToolkit = null;

            log.Debug("TestApp destructor");
        }

        //public TestApp()
        //{
        //    //log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + " DSLTestApplication");
        //    //log.Debug("TestApp Connecting to LV Server");
        //    m_CoreToolkit = new LVClient();

        //    clearVariables();
        //    Environment.CurrentDirectory = "c:\\mtp\\bin";

        //    //log.Debug("TestApp Constructed");

        //    for (int retry = 0; retry < 6; retry++)
        //    {
        //        try
        //        {
        //            if (m_CoreToolkit != null)
        //            {
        //                m_CoreToolkit.activateEquipmentManager("c:\\mtp\\cfg\\equipmentConfig.xml");
        //            }

        //            break;
        //        }
        //        catch
        //        {
        //        }
        //    }
        //}

        //~TestApp()
        //{
        //    m_CoreToolkit.UnregisterChannel();
        //    m_CoreToolkit = null;

        //    m_LocalCoreToolkit = null;



        //    log.Debug("TestApp destructor");
        //}

        private void clearVariables()
        {
            logFilename = string.Empty;
            testParameter = null;
            m_iUutInstance = -1;
            scannedSerialNumber = string.Empty;
            
            failedTestName = string.Empty;

            iDebug = 0;
        }

        //public void Setup(SequenceContext seqContext, out bool result, out string reportText)
        //{
        //    reportText = string.Empty;
        //    result = false;

        //    clearVariables();

        //    try
        //    {
        //        seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0x00FF00, "", null, false);//Clear

        //        m_iUutInstance = (int)seqContext.AsPropertyObject().GetValNumber("RunState.TestSockets.MyIndex", 0);

        //        scannedSerialNumber = seqContext.AsPropertyObject().GetValString("RunState.Root.Locals.UUT.SerialNumber", 0);

        //        try
        //        {
        //            log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + " DSLTestApplication", m_iUutInstance.ToString());

        //            logFilename = "S" + m_iUutInstance.ToString() + "_" + scannedSerialNumber + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

        //            m_CoreToolkit.BeginNamedLog(logFilename, m_iUutInstance);
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("GetLogger/PushNamedLog  exception: ex=" + ex.Message);
        //        }

        //        PrintTestName(seqContext.Step.Name);

        //        log.Debug("working directory = " + Environment.CurrentDirectory);

        //        LogAssemblyInfo(out reportText);

        //        m_LocalCoreToolkit = new ICoreToolkit();
        //        m_LocalCoreToolkit.activateEquipmentManager(CoreToolkit.InstallPath.InstPath.BuildFullPath("cfg\\equipmentConfig_Local.xml"));
        //        //string[] equipmentList = m_LocalCoreToolkit.getEquipmentsOnline();

        //        log.DebugFormat("scannedSerialNumber={0}", scannedSerialNumber);

        //        testParameter = (ITestParameters)m_CoreToolkit.getEquipmentObject("TEST_PARAMETERS", m_iUutInstance.ToString());
        //        if (testParameter == null)
        //            testParameter = (ITestParameters)m_LocalCoreToolkit.getEquipmentObject("TEST_PARAMETERS", m_iUutInstance.ToString());
        //        if (testParameter != null && seqContext != null)
        //        {
        //            testParameter.readParametersFile(seqContext.AsPropertyObject().GetValString("Locals.TestParameters", 0));
        //        }
        //        else
        //            log.Error("TEST_PARAMETERS  is offline.");

        //        int iCheckSoftwareVersion;
        //        testParameter.getParameterAsInt("global", "CHECK_SOFTWARE_VERSION", out iCheckSoftwareVersion, 0);

        //        if (CheckPartNumber() == false)
        //            return;

        //        testParameter.getParameterAsInt("global", "DEBUG", out iDebug, 0);

        //        result = true;
        //    }
        //    catch (Exception e)
        //    {
        //        reportText += "-> Exception: " + e.Message;
        //        log.Error(reportText);
        //    }

        //    log.Info("Setup done.");
        //}//setup

        public void Setup(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = String.Empty;
            result = false;

            try
            {
                //log.Debug("setup()");
                PrintTestName("setup()");

                // Initialize station parameters section...

                int iEnableNetlog = 0, iForceNetlog = 0;
                scannedSerialNumber = seqContext.AsPropertyObject().GetValString("RunState.Root.Locals.UUT.SerialNumber", 0);
                //string uutSerial = cam
                m_iUutInstance = (int)seqContext.AsPropertyObject().GetValNumber("RunState.TestSockets.MyIndex", 0);
                try
                {
                    log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, m_iUutInstance.ToString());

                    m_bSaveEachlog = seqContext.Locals.GetValBoolean("StationInfo.Log.Enabled", 0);
                    if (m_bSaveEachlog)
                    {
                        m_LogFilePath = seqContext.Locals.GetValString("StationInfo.Log.Path", 0);
                        m_LogFilename = "S" + m_iUutInstance.ToString() + "_" + scannedSerialNumber + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
                        m_CoreToolkit.BeginNamedLog(m_LogFilename, m_LogFilePath, m_iUutInstance);
                    }
                }
                catch (Exception ex)
                {
                    log.Debug("GetLogger/PushNamedLog  exception: ex=" + ex.Message);
                }

                // Computer ID, also fetch Station Alias and possibly more. Maybe try to print at the end of the log wich test case failed.
                string stationID = seqContext.Engine.ComputerName; //seqContext.AsPropertyObject().GetValString("Engine.ComputerName", 0);
                string stationAlias = seqContext.AsPropertyObject().GetValString("Locals.StationInfo.Alias", 0);
                m_StationType = seqContext.AsPropertyObject().GetValString("Locals.StationInfo.StationType", 0);
                log.InfoFormat("Station: {0} @ {1}, Station Type: {2}", stationAlias, stationID, m_StationType);


                Assembly assem = Assembly.GetExecutingAssembly();
                string path = assem.Location.ToString();
                FileVersionInfo asmFile = FileVersionInfo.GetVersionInfo(path);

                string assemblyString = asmFile.ProductName + ", Version=" + asmFile.ProductVersion + ", Desc=" + asmFile.FileDescription +
                    ", Comment=\"" + asmFile.Comments + "\"";
                log.Info(assemblyString);
                reportText = assemblyString;

                if (!seqContext.StationGlobals.Exists("TestApp", 0))
                {
                    seqContext.StationGlobals.NewSubProperty("TestApp", PropertyValueTypes.PropValType_String, false, "", 0);
                }
                seqContext.StationGlobals.SetValString("TestApp", 0, asmFile.ProductVersion);

                log.Debug("Fetching test parameter data for: " + seqContext.SequenceFile.Path);

                int status = 0;

                testParameter = (ITestParameters)m_CoreToolkit.getEquipmentObject("TEST_PARAMETERS", m_iUutInstance.ToString());
                if (testParameter != null && seqContext != null)
                {
                    // TODO: Change the functionality, to use same name in same location, or something similiar?
                    string sTestParameterFile = seqContext.AsPropertyObject().GetValString("Locals.TestParameters", 0);
                    status = testParameter.readParametersFile(sTestParameterFile);
                    if (status < 0)
                    {
                        if (sTestParameterFile.Length > 0)
                        {
                            // TODO: add functionality to try to fetch the xml file from 'NetworkPath\TestSequence'!?
                            reportText = String.Format("Error, reading XML Test parameter file '{0}', status = {1}.", sTestParameterFile, status);
                            log.Error(reportText);
                            result = false;
                            return;
                        }
                        else
                        {
                            log.Warning("Warning, no test parameter file specified!");
                        }
                    }

                    IniFile iniFile = new IniFile(@"c:\mtp\bin\settings.ini");
                    m_iNetworkIpAddressOffset = iniFile.ReadIniValueInt("Network", "NetworkOffset");
                    m_sNfsShare = iniFile.ReadIniValue("Directory", "NfsPath");
                    if (!m_sNfsShare.EndsWith("\\"))
                        m_sNfsShare += "\\";
                    iEnableNetlog = iniFile.ReadIniValueInt("MTP", "NetLog");
                    iForceNetlog = iniFile.ReadIniValueInt("MTP", "ForceNetLog");

                    string sPingDutDisable = testParameter.getParameterValueString("PING_DUT_DISABLE");
                    if (sPingDutDisable != null && sPingDutDisable.Equals("1")) m_iPingDutDisabled = 1;
                }
                else
                {
                    reportText = String.Format("Error: TEST_PARAMETERS equipment not found for TestSocket={0}. Check equipmentConfig.xml!", m_iUutInstance);
                    log.Error(reportText);
                    result = false;
                    return;
                }

                log.Debug("iUutInstance=" + m_iUutInstance.ToString());

                if (m_iUutInstance > 0)
                {
                    m_sNetworkIpAddress = string.Format("192.168.{0}.1", m_iUutInstance + m_iNetworkIpAddressOffset);
                    m_sClientIpAddress = string.Format("192.168.{0}.20", m_iUutInstance + m_iNetworkIpAddressOffset);
                }
                else
                {
                    m_sNetworkIpAddress = string.Format("192.168.{0}.1", m_iUutInstance);
                    m_sClientIpAddress = string.Format("192.168.{0}.20", m_iUutInstance);
                }

                // Initialize station parameters done!

                // Initial Services section...

                int iRunInfocastServer;
                testParameter.getParameterAsInt("global", "INFOCAST_SERVER", out iRunInfocastServer, 0);
                if (iRunInfocastServer == 1)
                {
                    m_InfocastServer = new ExecProcess(m_iUutInstance);
                    m_InfocastServer.initialize("Infocast", "java", String.Format("-jar InfocastServer.jar -v -i {0} -c infocastconfig.xml", m_sNetworkIpAddress), @"C:\MTP\Bin\TelefonicaInfocast\");
                    status = m_InfocastServer.StartProcess(5000, "Transmitting packets at 1900 kbit/s");
                    if (status != 0)
                    {
                        reportText = String.Format("Error: Failed to start Infocast process (ret code = {0}).", status);
                        log.Error(reportText);
                        result = false;
                        return;
                    }
                }

                /*
                if (0 != GetUnitInfo(ref m_UnitInfo))
                {
                    log.Error("Error, failed to get UnitInfo!");
                    result = false;
                    return;
                }
                */

                // Add logic to only create new FW logger when non existing!?!?!?!
                diagPort = null;
                diagPort = new FirmwareLogger(m_iUutInstance);
                if (diagPort != null)
                    diagPort.Initialize();

                bool bUseNetlog = (!diagPort.IsInitialized() && iEnableNetlog == 1) || iForceNetlog == 1;
                if (bUseNetlog)
                {
                    netLog = new NetLogger(m_iUutInstance, m_sNetworkIpAddress);
                    netLog.Initialize();
                }

                m_LogClient = new LogClient(m_iUutInstance.ToString());

                if (!m_DHCPServer.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance, false, false, false, m_sNetworkIpAddress, m_sClientIpAddress,
                    0, 0, @".\Settings.ini"))
                {
                    log.Error("Error: Failed to initialize DHCP Server!");
                    result = false;
                    return;
                }
                log.Debug("DHCP Server initialized!");

                if (!m_NFSServer.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance, m_sNetworkIpAddress, @".\Settings.ini", true, true))
                {
                    log.Error("Error: Failed to initialize NFS Server");
                    result = false;
                    return;
                }
                log.Debug("NFS Server initialized!");

                if (!m_TFTPServer.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance, m_sNetworkIpAddress, m_sClientIpAddress, @".\Settings.ini"))
                {
                    log.Error("Error: Failed to initialize TFTP Server!");
                    result = false;
                    return;
                }
                log.Debug("TFTP Server Initialized!");

                /*
                if (!m_ConfigurationStation.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance))
                {
                    log.Error("Error: Failed to initialize Configuration Station!");
                    result = false;
                    return;
                }
                m_bConfigStationDestroyed = false;
                log.Debug("ConfigurationStation Initialized!");
                */
                log.Error("ConfigurationStation is SKIPPED for NOW");
                // Inizialize services done!

                // Initialize equipment section...

                if (m_StationType == "FT")
                {
                    if (scu != null)
                    {
                        string sCheckVersionString = "CheckVersion";
                        if (ARMADILLO_REQUIRED_VERSION != null)
                            sCheckVersionString += "_" + ARMADILLO_REQUIRED_VERSION;
                        status = scu.Connect(sCheckVersionString);
                        if (status < 0)
                        {
                            reportText = "SCU Check version failed, please verify the version of the SCU.";
                            log.Error(reportText);
                            return;
                        }
                        if (scu.Connect("USB_RESET") != 0)
                            log.Error("Error on scu.Connect(\"USB_RESET\")");
                        if (scu.Connect("ResetDUT") != 0)
                            log.Error("Error on scu.Connect(\"ResetDUT\")");
                        if (scu.Disconnect("PowerSupply") != 0)
                            log.Error("Error on scu.Disconnect(\"PowerSupply\")");
                        if (scu.Connect("FIXTURE_FLASH") != 0)
                            log.Error("Error on scu.Connect(\"FIXTURE_FLASH\")");
                        System.Threading.Thread.Sleep(100);
                        if (scu.Disconnect("Vacuum") != 0)
                            log.Error("Error on scu.Disconnect(\"Vacuum\")");
                        scu.Disconnect("LED_ALL");

                        status = scu.Connect("InitializeFixtureCounter");
                        if (status != 0) // Move this to the initialization of the scu and use a read of fixture type instead!?
                        {
                            reportText = "Unable to initialize Fixture counter. Check fixture cabels!";
                            log.Error(reportText);
                            return;
                        }
                    }
                    else
                    {
                        reportText = "Error: SCU equipment not Initialized, check Armadillo and/or equipmentConfig.xml.";
                        log.Error(reportText);
                        return;
                    }
                }

                if (scu2 != null)
                {
                    if (scu2.Disconnect("FAIL_LED_" + m_iUutInstance.ToString()) != 0)
                        log.Error("Error on scu2.Connect(\"OFF FAIL LED\")");
                    if (scu2.Disconnect("PASS_LED_" + m_iUutInstance.ToString()) != 0)
                        log.Error("Error on scu2.Connect(\"OFF PASS LED\")");
                }

                // Initialize equipments done!

                log.Debug("setup() done!");
                result = true;

            }
            catch (Exception ex)
            {
                reportText = "Exception: " + ex.Message;
                log.Error(reportText);
                result = false;
                reportText = "Exception: " + ex.Message;
            }

        }

        public void Setup1(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;
            result = false;

            clearVariables();

            try
            {
                seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0x00FF00, "", null, false);//Clear

                m_iUutInstance = (int)seqContext.AsPropertyObject().GetValNumber("RunState.TestSockets.MyIndex", 0);

                scannedSerialNumber = seqContext.AsPropertyObject().GetValString("RunState.Root.Locals.UUT.SerialNumber", 0);

                try
                {
                    log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + " DSLTestApplication", m_iUutInstance.ToString());

                    logFilename = "S" + m_iUutInstance.ToString() + "_" + scannedSerialNumber + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

                    m_CoreToolkit.BeginNamedLog(logFilename, m_iUutInstance);
                }
                catch (Exception ex)
                {
                    log.Error("GetLogger/PushNamedLog  exception: ex=" + ex.Message);
                }

                PrintTestName(seqContext.Step.Name);

                log.Debug("working directory = " + Environment.CurrentDirectory);

                LogAssemblyInfo(out reportText);

                m_LocalCoreToolkit = new ICoreToolkit();
                m_LocalCoreToolkit.activateEquipmentManager(CoreToolkit.InstallPath.InstPath.BuildFullPath("cfg\\equipmentConfig_Local.xml"));
                //string[] equipmentList = m_LocalCoreToolkit.getEquipmentsOnline();

                m_DHCPServer = new CDHCPServer();
                m_NFSServer = new CNFSServer();
                m_TFTPServer = new CTFTPServer();

                m_LogClient = new LogClient(m_iUutInstance.ToString());

                if (!m_DHCPServer.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance, false, false, false, m_sNetworkIpAddress, m_sClientIpAddress,
                    0, 0, @".\Settings.ini"))
                {
                    log.Error("Error: Failed to initialize DHCP Server!");
                    result = false;
                    return;
                }
                log.Debug("DHCP Server initialized!");

                if (!m_NFSServer.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance, m_sNetworkIpAddress, @".\Settings.ini", true, true))
                {
                    log.Error("Error: Failed to initialize NFS Server");
                    result = false;
                    return;
                }
                log.Debug("NFS Server initialized!");

                if (!m_TFTPServer.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance, m_sNetworkIpAddress, m_sClientIpAddress, @".\Settings.ini"))
                {
                    log.Error("Error: Failed to initialize TFTP Server!");
                    result = false;
                    return;
                }
                log.Debug("TFTP Server Initialized!");

                log.DebugFormat("scannedSerialNumber={0}", scannedSerialNumber);

                testParameter = (ITestParameters)m_CoreToolkit.getEquipmentObject("TEST_PARAMETERS", m_iUutInstance.ToString());
                if (testParameter == null)
                    testParameter = (ITestParameters)m_LocalCoreToolkit.getEquipmentObject("TEST_PARAMETERS", m_iUutInstance.ToString());
                if (testParameter != null && seqContext != null)
                {
                    testParameter.readParametersFile(seqContext.AsPropertyObject().GetValString("Locals.TestParameters", 0));
                }
                else
                    log.Error("TEST_PARAMETERS  is offline.");

                int iCheckSoftwareVersion;
                testParameter.getParameterAsInt("global", "CHECK_SOFTWARE_VERSION", out iCheckSoftwareVersion, 0);

                if (CheckPartNumber() == false)
                    return;

                testParameter.getParameterAsInt("global", "DEBUG", out iDebug, 0);

                result = true;
            }
            catch (Exception e)
            {
                reportText += "-> Exception: " + e.Message;
                log.Error(reportText);
            }

            log.Info("Setup done.");
        }//setup

        public void Cleanup(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = String.Empty;
            int status = 0;
            try
            {
                PrintTestName("cleanup()");

                if (uut != null)
                    status += uut.close();

                if (uut_ts != null)
                    status += uut_ts.close();

                if (usquid != null)
                    status += usquid.close();

                if (diagPort != null)
                    diagPort.StopReadDiagPort();

                if (netLog != null)
                    netLog.StopReadNetLog();

                if (m_LogClient != null)
                    m_LogClient.Close();

                log.Debug("m_DHCPServer.Destroy");
                m_DHCPServer.Destroy();
                log.Debug("m_NFSServer.Destroy");
                m_NFSServer.Destroy();
                log.Debug("m_TFTPServer.Destroy");
                m_TFTPServer.Destroy();

                if (!m_bConfigStationDestroyed)
                {
                    log.Debug("m_ConfigurationStation.Destroy");
                    m_ConfigurationStation.Destroy();
                }

                if (m_InfocastServer != null)
                {
                    m_InfocastServer.Exit();
                    log.Debug("m_InfocastServer.Exit");
                }

                log.Debug("cleanup() done!");
                result = (status == 0) ? true : false;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Debug(reportText);
                result = false;
            }

            log.Debug("Saving log...");

            try
            {
                IniFile iniFile = new IniFile(@"C:\MTP\Bin\Settings.ini");
                bool bAllowNetworkLog = (iniFile.ReadIniValueInt("Misc", "AllowNetworkLogFolder") == 1) ? true : false;
                bool bUseNetworkLogPath = false;
                string sNetworkPathLogs = "", sNetworkPathReports = "", sReportPath = "", reportName = "";
                string stationAlias = seqContext.AsPropertyObject().GetValString("Locals.StationInfo.Alias", 0);
                bool bReportsDisabled = true;
                string sReportOptions = "", sReportFilePath = "";
                if (seqContext.AsPropertyObject().Exists("RunState.Root.Locals.ReportOptions", 0))
                {
                    sReportOptions = "RunState.Root.Locals.ReportOptions";
                    sReportFilePath = "RunState.Root.Locals.ReportFilePath";
                    bReportsDisabled = seqContext.AsPropertyObject().GetValBoolean("RunState.Root.Locals.ReportOptions.DisableReportGeneration", 0);
                }
                else if (seqContext.AsPropertyObject().Exists("RunState.Root.Parameters.ModelData.ReportOptions", 0))
                {
                    sReportOptions = "RunState.Root.Parameters.ModelData.ReportOptions";
                    sReportFilePath = "RunState.Root.Parameters.TestSocket.ReportFilePath";
                    bReportsDisabled = seqContext.AsPropertyObject().GetValBoolean("RunState.Root.Parameters.ModelData.ReportOptions.DisableReportGeneration", 0);
                }
                else
                {
                    log.Debug("Could not find ReportOptions, skipping report file operations...");
                }

                if (!bReportsDisabled)
                {
                    sReportPath = seqContext.AsPropertyObject().GetValString(sReportOptions + ".Directory", 0);
                    reportName = seqContext.AsPropertyObject().GetValString(sReportFilePath, 0);
                }
                else
                {
                    log.Debug("Reports disabled!");
                }

                if (bAllowNetworkLog && m_UnitInfo.NetworkLogFolder != null && m_UnitInfo.NetworkLogFolder != "")
                {
                    bUseNetworkLogPath = true;
                    string sNetworkPathBase = m_UnitInfo.NetworkLogFolder;
                    if (!sNetworkPathBase.EndsWith("\\")) sNetworkPathBase += "\\";
                    sNetworkPathBase += stationAlias + "\\";
                    sNetworkPathLogs = sNetworkPathBase + "Logs\\";
                    sNetworkPathReports = sNetworkPathBase + "Reports\\";
                    if (m_bSaveEachlog)
                    {
                        log.DebugFormat("Moving log files from '{0}' to '{1}'...", m_LogFilePath, sNetworkPathLogs); // appending "<station alias>\Logs"
                    }

                    if (!bReportsDisabled)
                    {
                        log.DebugFormat("Moving reports from '{0}' to '{1}'...", sReportPath, sNetworkPathReports); // appending "<station alias>\Reports"
                    }
                }

                if (m_bSaveEachlog)
                {
                    log.Debug("Log saved!");
                    m_CoreToolkit.EndNamedLog(m_LogFilename, m_iUutInstance);
                }

                if (seqContext.SequenceFailed)
                {
                    try
                    {
                        log.Debug("Sequence Failed, parsing error code...");
                        string errMsg = "";
                        int iErrorCode = GetErrorCode(seqContext, out errMsg);
                        log.DebugFormat("ErrorCode={0}", iErrorCode);
                        /*
                        if (!bReportsDisabled)
                        {
                            reportName = reportName.Substring(0, reportName.IndexOf(".html")) + String.Format("_FAIL_{0}.html", iErrorCode);
                            seqContext.AsPropertyObject().SetValString(sReportFilePath, 0, reportName);
                        }
                        */
                        if (m_bSaveEachlog)
                        {
                            if (!m_LogFilePath.EndsWith("\\")) m_LogFilePath += "\\";
                            string sLogFileOld = m_LogFilePath + m_LogFilename + ".txt";
                            string sLogFileErrorCode = m_LogFilePath + String.Format("{0}_FAIL_{1}.txt", m_LogFilename, iErrorCode);

                            System.IO.File.Move(sLogFileOld, sLogFileErrorCode);
                        }

                    }
                    catch (Exception ex)
                    {
                        reportText = "Exception: " + ex.Message;
                        log.Error(reportText);
                    }
                }

                if (bUseNetworkLogPath)
                {
                    if (m_bSaveEachlog)
                    {
                        string[] fileList = System.IO.Directory.GetFiles(m_LogFilePath, m_LogFilename + "*");
                        System.IO.Directory.CreateDirectory(sNetworkPathLogs);
                        foreach (string s in fileList)
                        {
                            string sStrippedPath = s.Remove(0, s.LastIndexOf("\\") + 1);
                            System.IO.File.Move(s, sNetworkPathLogs + sStrippedPath);
                        }
                    }

                    if (!bReportsDisabled)
                    {
                        string reportFileName = reportName.Remove(0, reportName.LastIndexOf("\\") + 1);
                        seqContext.AsPropertyObject().SetValString(sReportFilePath, 0, sNetworkPathReports + reportFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                reportText = "Exception: " + ex.Message;
                log.Error(reportText);
            }
        }

        private int GetErrorCode(SequenceContext seqContext, out string errorMessage)
        {
            try
            {
                errorMessage = "";
                string completedString = "Locals.ResultList";
                PropertyObject propObj = seqContext.AsPropertyObject().GetPropertyObject(completedString, 0);
                int numResults = propObj.GetNumElements();//seqContext.AsPropertyObject().GetNumSubProperties("RunState.Root.Locals.ResultList");
                string sStepName = "N/A", sStepGroup = "N/A", sStepType = "N/A";
                string sSequence = "MainSequence";
                for (int i = 0; i < numResults; i++)
                {
                    completedString = String.Format("Locals.ResultList[{0}].Status", i);
                    string sStatus = seqContext.AsPropertyObject().GetValString(completedString, 0);

                    if (sStatus == "Failed")
                    {
                        completedString = String.Format("Locals.ResultList[{0}].TS.StepName", i);
                        sStepName = seqContext.AsPropertyObject().GetValString(completedString, 0);

                        completedString = String.Format("Locals.ResultList[{0}].TS.StepGroup", i);
                        sStepGroup = seqContext.AsPropertyObject().GetValString(completedString, 0);

                        completedString = String.Format("Locals.ResultList[{0}].TS.StepType", i);
                        sStepType = seqContext.AsPropertyObject().GetValString(completedString, 0);

                        if (sStepType == "SequenceCall")
                        {
                            completedString = String.Format("Locals.ResultList[{0}].TS.SequenceCall.Sequence", i);
                            sSequence = seqContext.AsPropertyObject().GetValString(completedString, 0);
                            completedString = String.Format("Locals.ResultList[{0}].TS.SequenceCall.ResultList", i);
                            propObj = seqContext.AsPropertyObject().GetPropertyObject(completedString, 0);
                            int numResultsSub = propObj.GetNumElements();//seqContext.AsPropertyObject().GetNumSubProperties("RunState.Root.Locals.ResultList");
                            for (int j = 0; j < numResultsSub; j++)
                            {
                                completedString = String.Format("Locals.ResultList[{0}].TS.SequenceCall.ResultList[{1}].Status", i, j);
                                sStatus = seqContext.AsPropertyObject().GetValString(completedString, 0);

                                if (sStatus == "Failed")
                                {
                                    completedString = String.Format("Locals.ResultList[{0}].TS.SequenceCall.ResultList[{1}].TS.StepName", i, j);
                                    sStepName = seqContext.AsPropertyObject().GetValString(completedString, 0);

                                    completedString = String.Format("Locals.ResultList[{0}].TS.SequenceCall.ResultList[{1}].TS.StepGroup", i, j);
                                    sStepGroup = seqContext.AsPropertyObject().GetValString(completedString, 0);

                                    completedString = String.Format("Locals.ResultList[{0}].TS.SequenceCall.ResultList[{1}].TS.StepType", i, j);
                                    sStepType = seqContext.AsPropertyObject().GetValString(completedString, 0);

                                    break;
                                }
                            }
                        }

                        break;
                    }
                }

                completedString = String.Format("RunState.ProcessModelClient.Data.Seq[\"{0}\"].{1}[\"{2}\"].Result.Error.Code", sSequence, sStepGroup, sStepName);
                int iErrorCode = (int)seqContext.AsPropertyObject().GetValNumber(completedString, 0);

                return iErrorCode;

            }
            catch (Exception ex)
            {
                errorMessage = "Exception: " + ex.Message;
                return -1;
            }
        }

        public void Cleanup1(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;
            result = false;
            int status = 0;

            PrintTestName(seqContext.Step.Name);

            setFocusOn("Test UUTs");

            System.Threading.Thread.Sleep(500);

            int hwnd = FindWindow(null, "Test UUTs");

            if (hwnd > 0)
            {
                SetForegroundWindow(hwnd);
            }

            try
            {
                string sBuffer = string.Empty;

                if (failedTestName.Length > 0)
                {
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xFF0000, failedTestName, null, false);//RED message
                }
                else
                {
                    bool bSequenceFailed = seqContext.AsPropertyObject().GetValBoolean("RunState.SequenceFailed", 0);
                    if (bSequenceFailed == false)
                        seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0x00FF00, scannedSerialNumber + " PASS", null, false);//GREEN message
                }

                //if (telnetSession != null)
                //{
                //telnetSession.close();
                //log.Debug("telnetSession.close() done");
                //}

                if (m_LogClient != null)
                    m_LogClient.Close();

                log.Debug("m_DHCPServer.Destroy");
                m_DHCPServer.Destroy();
                //log.Debug("m_NFSServer.Destroy");
                //m_NFSServer.Destroy();
                log.Debug("m_TFTPServer.Destroy");
                m_TFTPServer.Destroy();

                result = (status == 0 ? true : false);
            }
            catch (Exception e)
            {
                log.Debug("cleanup() Exception: " + e.Message);
            }

            try
            {
                if (m_CoreToolkit != null)
                {
                    log.Debug("cleanup()->m_CoreToolkit.EndNamedLog() done.");
                    m_CoreToolkit.EndNamedLog(logFilename, m_iUutInstance);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetLogger/EndNamedLog  exception: ex=" + ex.Message);
            }

            clearVariables();

        }//cleanup

        private void LogAssemblyInfo(out string reportText)
        {
            Assembly assem = Assembly.GetExecutingAssembly();
            string path = assem.Location.ToString();
            FileVersionInfo asmFile = FileVersionInfo.GetVersionInfo(path);

            string assemblyString = asmFile.ProductName + " |DllVersion=" + asmFile.ProductVersion + "| Desc=" + asmFile.FileDescription + ", Comment=\"" + asmFile.Comments + "\"";

            log.Info(assemblyString);
            reportText = assemblyString;
        }//LogAssemblyInfo

        private bool CheckPartNumber()
        {
            log.Debug("CheckPartNumber()");

            bool result = false;

            string sPartNumberXML = string.Empty;
            testParameter.getParameterAsString("global", "PART_NUMBER", out sPartNumberXML, "");

            if (sPartNumberXML.Length > 0 && !sPartNumberXML.ToUpper().Contains("SKIP"))
            {
                ICAM cam = (ICAM)m_CoreToolkit.getEquipmentObject("CAM", m_iUutInstance.ToString());
                string[] input = { "TopBom" };
                string[] key = null;
                string[] value = null;

                int status = cam.GetUnitInfo(null, input, out key, out value);

                if (status == 0 && value != null && value[0].Length > 0)
                {
                    char[] sep = { ',' };
                    string[] sPartNumberArray = sPartNumberXML.Split(sep);

                    foreach (string partNumber in sPartNumberArray)
                    {
                        if (partNumber == value[0])
                        {
                            log.DebugFormat("PART_NUMBER in XML: {0} matched  CAM: {1}", sPartNumberXML, value[0]);
                            result = true;
                            break;
                        }
                    }
                    if (result == false)
                    {
                        log.DebugFormat("PART_NUMBER in XML: {0} did not match  CAM: {1}", sPartNumberXML, value[0]);
                    }
                }
                else
                {
                    log.Debug("CAM Error reading: " + input[0]);
                    log.Debug("status=" + status);
                    log.Debug("value[0].Length=" + value[0].Length.ToString());
                }
            }
            else if (sPartNumberXML.ToUpper().Contains("SKIP"))
            {
                log.DebugFormat("PART_NUMBER in XML set to SKIP for debug purpose: {0}", sPartNumberXML);
                result = true;
            }
            else
                log.Debug("No PART_NUMBER specified in XML file");

            return result;
        }//CheckPartNumber

        private void PrintTestName(string sTestName)
        {
            log.InfoFormat("========= {0} =========", sTestName);
        }

        private void GetValueFromXmlAsInt(string XmlFile, string SortString, string Name, string Key, ref int OutputValue)
        {
            string sOutput = "";
            try
            {
                GetValueFromXmlAsString(XmlFile, SortString, Name, Key, ref sOutput);
                OutputValue = int.Parse(sOutput);
            }
            catch (Exception ex)
            {
                log.DebugFormat("GetValueFromXmlAsInt() Exception: {0}", ex.Message);
                OutputValue = -1;
            }
        }

        private void GetValueFromXmlAsString(string XmlFile, string SortString, string Name, string Key, ref string OutputValue)
        {
            XmlReader xmlReader = null;
            try
            {
                xmlReader = XmlReader.Create(XmlFile, new XmlReaderSettings());
                DataSet ds = new DataSet();
                DataView dv;
                ds.ReadXml(xmlReader);
                dv = new DataView(ds.Tables[0]);
                dv.Sort = SortString;
                int index = dv.Find(Name);
                OutputValue = dv[index][Key].ToString();

                ds.Dispose();
                //xmlReader.Close();
            }
            catch (Exception ex)
            {
                OutputValue = "";
                log.DebugFormat("GetValueFromXmlAsString() Exception: {0}", ex.Message);
            }
            xmlReader.Close();
        }//

        class Global
        {
            public enum ERROR_CODES
            {
                Get_TestParameter_Value_Failed = 98,
                Get_Equipment_Object_Failed = 99,
                Com_open_error = 101,
                Capture_Picture_Failed = 102,
                Get_Video_Source_Failed = 103,

            }

        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(int hWnd);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern int FindWindow(string lpClassName, string lpWindowName);

        private IWin32Window mtpWindowHandle()
        {
            int hwnd = FindWindow(null, "NI TestStand - Sequence Editor [Running...]");
            
            if (hwnd == 0)
                hwnd = FindWindow(null, "Manufacturing Test Platform");
            else if (hwnd == 0)
                hwnd = FindWindow(null, "MTP");
            
            if (hwnd > 0)
                return new WindowWrapper((IntPtr)hwnd);
            else
                return null;
        }

        private void setFocusOn(string sTitle)
        {
            int hwnd = FindWindow(null, sTitle);

            if (hwnd > 0)
            {
                SetForegroundWindow(hwnd);
            }
        }

    }//TestApp

    public class WindowWrapper : System.Windows.Forms.IWin32Window
    {
        public WindowWrapper(IntPtr handle)
        {
            _hwnd = handle;
        }

        public IntPtr Handle
        {
            get { return _hwnd; }
        }

        private IntPtr _hwnd;
    }
}
