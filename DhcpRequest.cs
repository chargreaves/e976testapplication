using System;
//using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
//using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

using CommonCS;
//using CoreToolkit;
//using CommInterface;
//using Logging;
//using TestParameters;
//using LV_Client;
//using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void DhcpRequest(SequenceContext seqContext, out double result, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            string strOperatorMessage = string.Empty;
            result = -99;

            try
            {
                int iTimeOut;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeOut, 30);
                if (iTimeOut < 200)
                    iTimeOut *= 1000;

                int iRetryTimeOut;
                testParameter.getParameterAsInt(seqContext.Step.Name, "RETRY_TIMEOUT", out iRetryTimeOut, 0);
                if (iRetryTimeOut < 200)
                    iRetryTimeOut *= 1000;

                int iNoFastboot;
                testParameter.getParameterAsInt(seqContext.Step.Name, "NO_FASTBOOT", out iNoFastboot, 0);
                bool bNoFastboot = (iNoFastboot == 1);

                string sRootPath = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "ROOT PATH", out sRootPath, "");
                StringBuilder sbRootPath = new StringBuilder(sRootPath);

                string sSkipTftpDownload = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "SKIP TFTP DOWNLOAD", out sSkipTftpDownload, "");
                StringBuilder sbSkipTftpDownload = new StringBuilder(sSkipTftpDownload);

                string sHttpsDownload = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "HTTPS DOWNLOAD", out sHttpsDownload, "");
                StringBuilder sbHttpsDownload = new StringBuilder(sHttpsDownload);

                string sVendorClassID = string.Empty;
                testParameter.getParameterAsString(seqContext.Step.Name, "WAIT ON EMPTY VENDOR CLASS ID", out sVendorClassID, "");
                StringBuilder sbVendorClassID = new StringBuilder(sVendorClassID);

                System.DateTime begin = System.DateTime.Now;
                begin = begin.AddMilliseconds(iTimeOut);

                string retVendorId = "";
                int retBootloaderType = 0;

                int status = m_DHCPServer.WaitForRequest(iTimeOut, sRootPath, false, sSkipTftpDownload, sHttpsDownload, true, false, false, out retVendorId, out retBootloaderType);
                if(status != 0)
                {
                    int iTimer = System.DateTime.Compare(System.DateTime.Now, begin);

                    log.Debug("Timer: " + iTimer);

                    if (iRetryTimeOut > 0 && iTimer == 1) // TimeOut! => operator notification + retry
                    {
                        strOperatorMessage = "Please check cable connections!";
                        log.Debug(strOperatorMessage);
                        seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, strOperatorMessage, null, false);
                        seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase + 0x1002, 0, null, null, false); // Flash operator message box
   
                        status = m_DHCPServer.WaitForRequest(iRetryTimeOut, sRootPath, false, sSkipTftpDownload, sHttpsDownload, true, false, false, out retVendorId, out retBootloaderType);
                        if (status != 0)
                        {
                            reportText = "NO response from the DUT!";
                            log.Error(reportText);
                            seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 0xff0000, reportText, null, false);
                            result = status;
                            return;
                        }
                        else
                        {
                            strOperatorMessage = "Cable connections OK!";
                            log.Debug(strOperatorMessage);
                            seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, strOperatorMessage, null, false);
                        }
                    }
                    else
                    {
                        log.Error("NO response!");
                        result = status;
                        return;
                    }
                }

                if (!bNoFastboot && (eBootLoaderType)retBootloaderType == eBootLoaderType.KREATV_PLATFORM)
                {
                    //TODO: add power cycle of dut if using armadillo?!

                    // Unit in fastboot, expecting another DHCP request.
                    log.Debug("Unit assumed in fastboot, expecting another DHCP request...");
                    status = m_DHCPServer.WaitForRequest(iTimeOut, sRootPath, false, sSkipTftpDownload, sHttpsDownload, true, false, false, out retVendorId, out retBootloaderType);
                    if (status != 0)
                    {
                        reportText = "NO response from the DUT, after fastboot disable!";
                        log.Warning(reportText);
                        result = status; // remove, just for DEBUG
                        return; // remove, just for DEBUG
                    }
                }

                result = 0;              
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }
        }
    }
}
