﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;
using CVP_HDMIImplementation;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public CVP_HDMIImplementation.CVP_HDMIImpl CVP_HDMIObj = new CVP_HDMIImplementation.CVP_HDMIImpl();

        //=====================================================================================================================
        public void HDM_003_00_00_HDMI(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            string response2;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[3];

            //set result to fail
            result[0] = -1; //EDID
            result[1] = -1; //CEC Transmit
            result[2] = -1; //CEC Receive

            PrintTestName(seqContext.Step.Name);

            command = "hdmi_info"; //Read HDMI info
            testParameter.getParameterAsString(seqContext.Step.Name, "Expected_TV_EDID", out response, "xxx"); //read Expected TV EDID from testParameters.xml, default to XXX if no entry in testParameters.xml file to ensure test fails
            //testParameter.getParameterAsString(seqContext.Step.Name, "Expected_TV_EDID", out response, ""); //read Expected TV EDID from testParameters.xml
            testParameter.getParameterAsString(seqContext.Step.Name, "Expected_Splitter_EDID", out response2, "xxx"); //read Expected Splitter EDID from testParameters.xml, default to XXX if no entry in testParameters.xml file to ensure test fails
            //testParameter.getParameterAsString(seqContext.Step.Name, "Expected_Splitter_EDID", out response2, ""); //read Expected Splitter EDID from testParameters.xml
            status = SendReceive(command, "WAITING_FOR_IR", 10000, out strReadBuffer);
            //status = SendReceive(command, "/ #", 10000, out strReadBuffer);

            //check for TV EDID or Splitter EDID
            if ((strReadBuffer.Contains("Manufacturer=" + response)) || (strReadBuffer.Contains("Manufacturer=" + response2)))
            {
                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
            }

            //check CEC Transmit
            if (strReadBuffer.Contains("CEC TRANSMIT OK"))
            {
                //pass
                result[1] = 0;
            }
            else
            {
                //fail
                reportText = command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
            }

            //check CEC Receive
            if (strReadBuffer.Contains("CEC RECEIVE OK"))
            {
                //pass
                result[2] = 0;
            }
            else
            {
                //fail
                reportText = command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
            }

            return;

        }

        //=====================================================================================================================
        public void HDM_003_00_00_HDMI_HDC(SequenceContext seqContext, out double[] result, out string reportText)
        {
            // FOR USE ONLY WITH HDMI TEST BOARD, TV SHOULD NOT BE PRESENT

            string command;
            string response;
            string response2;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            bool Passed = false;
            const short high = 1;
            const short setting = 1;
            string ret = "";

            //set result to fail
            result[0] = -1; //EDID

            PrintTestName(seqContext.Step.Name);

            Passed = CVP_HDMIObj.HPD_DDC_Select(setting, ref ret, ref reportText);
            if (Passed == false)
            {
                //fail
            }
            else
            {
                //Pull high HPD first
                Passed = CVP_HDMIObj.Setting_HPD(high, ref ret, ref reportText);
            }
            
            
            if (Passed == true)
            {
                //Pass
                System.Threading.Thread.Sleep(500);

                command = "hdmi_info"; //Read HDMI info
                testParameter.getParameterAsString(seqContext.Step.Name, "Expected_TV_EDID", out response, "xxx"); //read Expected TV EDID from testParameters.xml, default to XXX if no entry in testParameters.xml file to ensure test fails
                                                                                                                   //testParameter.getParameterAsString(seqContext.Step.Name, "Expected_TV_EDID", out response, ""); //read Expected TV EDID from testParameters.xml
                testParameter.getParameterAsString(seqContext.Step.Name, "Expected_Splitter_EDID", out response2, "xxx"); //read Expected Splitter EDID from testParameters.xml, default to XXX if no entry in testParameters.xml file to ensure test fails
                                                                                                                          //testParameter.getParameterAsString(seqContext.Step.Name, "Expected_Splitter_EDID", out response2, ""); //read Expected Splitter EDID from testParameters.xml
                status = SendReceive(command, "SerialNumber", 10000, out strReadBuffer);
                //status = SendReceive(command, "/ #", 10000, out strReadBuffer);

                //check for TV EDID or Splitter EDID
                if ((strReadBuffer.Contains("Manufacturer=" + response)) || (strReadBuffer.Contains("Manufacturer=" + response2)))
                {
                    //pass
                    result[0] = 0;
                }
                else
                {
                    //fail
                    reportText = command + ", Received: " + strReadBuffer;
                    log.Debug(reportText);
                }

            }
            else
            {
                //fail
            }

            return;

        }


        //=====================================================================================================================
        public void CVP_HDMI_Start_Setting(SequenceContext seqContext, out bool result, out string reportText)
        {
            const short setting = 1;

            bool Passed = false;
            string ret = "";

            reportText = string.Empty;
            result = false;

            PrintTestName(seqContext.Step.Name);

            Passed = CVP_HDMIObj.TMDS_Select(setting, ref ret, ref reportText);
            if (Passed == false)
            {
                //fail
            }
            else
            {
                //pass
                Passed = CVP_HDMIObj.HPD_DDC_Select(setting, ref ret, ref reportText);
                if (Passed == false)
                {
                    //fail
                }
                else
                {
                    //pass
                    System.Threading.Thread.Sleep(200);

                    result = true;
                }

            }

        }


        //=====================================================================================================================
        public void CVP_HDMI_HDMI_5V(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string ret = "";

            reportText = string.Empty;
            result = new double[2];

            PrintTestName(seqContext.Step.Name);

            result[0] = CVP_HDMIObj.HDMI_5V(ref result[1], ref ret, ref reportText);

        }

        //=====================================================================================================================
        public void CVP_HDMI_CEC_Meas(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string ret = "";

            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[2];

            PrintTestName(seqContext.Step.Name);

            result[0] = CVP_HDMIObj.CEC_Meas_Setting(ref ret, ref reportText);
            if (result[0] == -1)
            {
                //fail
            }
            else
            {
                //pass
                command = "pbist hdmi_cec";
                response = "Toggling HDMI CEC pin";
                status = SendReceive(command, "/ #", 3000, out strReadBuffer); //*Sent CEC Data
                if (strReadBuffer.Contains(response))
                {
                    //pass
                    System.Threading.Thread.Sleep(500);

                    result[0] = CVP_HDMIObj.CEC_Meas(ref result[1], ref ret, ref reportText);
                }
                else
                {
                    //fail
                    reportText += command + ", Received: " + strReadBuffer;
                    log.Debug(reportText);
                    return;
                }

            }

        }

        //=====================================================================================================================
        public void CVP_HDMI_CEC_Testtask_Meas(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string ret = "";

            reportText = string.Empty;
            result = new double[2];

            PrintTestName(seqContext.Step.Name);

            result[0] = CVP_HDMIObj.CEC_Meas_Setting(ref ret, ref reportText);
            if (result[0] == -1)
            {
                //fail
            }
            else
            {
                //pass
                TestTask.Send_TT_Command("B", "0", ""); //*Sent CEC Data

                System.Threading.Thread.Sleep(500);

                result[0] = CVP_HDMIObj.CEC_Meas(ref result[1], ref ret, ref reportText);
            }

        }

        //=====================================================================================================================
        public void CVP_HDMI_HPD(SequenceContext seqContext, out bool result, out string reportText)
        {
            const short low = 0;
            const short high = 1;

            bool Passed = false;
            string response;
            string ret = "";

            reportText = string.Empty;
            result = false;

            PrintTestName(seqContext.Step.Name);

            Passed = CVP_HDMIObj.Setting_HPD(low, ref ret, ref reportText);
            if (Passed == false)
            {
                //fail
            }
            else
            {
                //pass
                System.Threading.Thread.Sleep(500);

                TestTask.Send_TT_Command("B", "4", ""); //*Get HPD State

                System.Threading.Thread.Sleep(200);

                response = TestTask.GetReturnedData();
                if (response != "0")
                {
                    //fail
                    reportText = response;
                }
                else
                {
                    //pass
                    Passed = CVP_HDMIObj.Setting_HPD(high, ref ret, ref reportText);

                    System.Threading.Thread.Sleep(500);

                    TestTask.Send_TT_Command("B", "4", ""); //*Get HPD State

                    System.Threading.Thread.Sleep(200);

                    response = TestTask.GetReturnedData();
                    if (response != "1")
                    {
                        //fail
                        reportText = response;
                    }
                    else
                    {
                        //pass
                        result = true;
                    }

                }

            }

        }

        //=====================================================================================================================
        public void CVP_HDMI_DDC(SequenceContext seqContext, out bool result, out string reportText)
        {
            const short high = 1;

            bool Passed = false;
            string response = "";
            string ret = "";

            reportText = string.Empty;
            result = false;

            PrintTestName(seqContext.Step.Name);
                        
            //Pull high HPD first
            Passed = CVP_HDMIObj.Setting_HPD(high, ref ret, ref reportText);
            if (Passed == false)
            {
                //fail
            }
            else
            {
                //pass
                TestTask.Send_TT_Command("B", "4", ""); //*Get HPD State

                System.Threading.Thread.Sleep(500);

                response = TestTask.GetReturnedData();
                if (response != "1")
                {
                    //fail
                    reportText = response;
                }
                else
                {
                    //pass
                    TestTask.Send_TT_Command("B", "5", ""); //*Sent SDA Data

                    System.Threading.Thread.Sleep(500);

                    response = TestTask.GetReturnedData();
                    log.Debug("HDMI Device : " + response);
                    if (response != "HDMI BOX")
                    {
                        //fail
                        reportText = response + ":" + "HDMI Device Command Error";
                        log.Debug("HDMI Device Command Error");
                    }
                    else
                    {
                        //pass
                        result = true;
                    }

                }

            }

        }

        //=====================================================================================================================
        public void CVP_HDMI_TMDS_Meas(SequenceContext seqContext, out double[] result, out string reportText)
        {
            const short setting = 1;

            bool Passed = false;
            string ret = "";

            reportText = string.Empty;
            result = new double[65];

            PrintTestName(seqContext.Step.Name);

            Passed = CVP_HDMIObj.TMDS_Select(setting, ref ret, ref reportText);

            System.Threading.Thread.Sleep(200);

            result[0] = CVP_HDMIObj.TMDS_Meas(ref result, ref ret, ref reportText);

            //'For i = 0 To 63
            //'    Call DBGMSG(CStr(i) & ": " & CStr(result(i)))
            //'Next

            if (result[0] == -1)
            {
                //fail
            }
            else
            {
                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS Data2+")
                //    Passed = CheckLimits_Units(result(0), LLimit, HLimit, "TMDS Data2+: ", 2, "V", 1.65)
                //End If

                //If Passed Then
                //    Passed = CheckLimits_Units(result(1), SLLimit, SHLimit, "TMDS Data2-: ", 3, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS Data2-")
                //    Passed = CheckLimits_Units(result(9), LLimit, HLimit, "TMDS Data2-: ", 4, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(8), SLLimit, SHLimit, "TMDS Data2+: ", 5, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS Data1+")
                //    Passed = CheckLimits_Units(result(18), LLimit, HLimit, "TMDS Data1+: ", 6, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(19), SLLimit, SHLimit, "TMDS Data1-: ", 7, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS Data1-")
                //    Passed = CheckLimits_Units(result(27), LLimit, HLimit, "TMDS Data1-: ", 8, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(26), SLLimit, SHLimit, "TMDS Data1+: ", 9, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS Data0+")
                //    Passed = CheckLimits_Units(result(36), LLimit, HLimit, "TMDS Data0+: ", 10, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(37), SLLimit, SHLimit, "TMDS Data0-: ", 11, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS Data0-")
                //    Passed = CheckLimits_Units(result(45), LLimit, HLimit, "TMDS Data0-: ", 12, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(44), SLLimit, SHLimit, "TMDS Data0+: ", 13, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS CLK+")
                //    Passed = CheckLimits_Units(result(54), LLimit, HLimit, "TMDS CLK+: ", 14, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(55), SLLimit, SHLimit, "TMDS CLK-: ", 15, "V", 0.33)
                //End If

                //If Passed Then
                //    Call Test.SetStatus("Pull High TMDS CLK-")
                //    Passed = CheckLimits_Units(result(63), LLimit, HLimit, "TMDS CLK-: ", 16, "V", 1.65)
                //End If
                //If Passed Then
                //    Passed = CheckLimits_Units(result(62), SLLimit, SHLimit, "TMDS CLK+: ", 17, "V", 0.33)
                //End If

                //If Passed Then
                //    Call UnihanDB_TEST_SETPASS()
                //Else
                //    Call UnihanDB_TEST_SETFAIL()
                //End If
            }

        }

        //=====================================================================================================================
        public void CVP_HDMI_End_Setting(SequenceContext seqContext, out bool result, out string reportText)
        {
            const short setting = 0;

            bool Passed = false;
            string ret = "";

            reportText = string.Empty;
            result = false;

            PrintTestName(seqContext.Step.Name);

            Passed = CVP_HDMIObj.TMDS_Select(setting, ref ret, ref reportText);
            if (Passed == false)
            {
                //fail
            }
            else
            {
                //pass
                Passed = CVP_HDMIObj.HPD_DDC_Select(setting, ref ret, ref reportText);
                if (Passed == false)
                {
                    //fail
                }
                else
                {
                    //pass
                    System.Threading.Thread.Sleep(200);

                    result = true;
                }

            }

        }

        //=====================================================================================================================

    }

}
