using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CNFSServer
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int NFSServer_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool NFSServer_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, string szNetworkCardIP, string IniFileName, bool EnabledDefaultVal, bool PrintMountPoints);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void NFSServer_Destroy(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void NFSServer_Stop(int Handle);



        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, string szNetworkCardIP, string IniFileName, bool EnabledDefaultVal, bool PrintMountPoints)
        {
            if (NFSServer_GetInterfaceVersion() != 1) return false;
            return NFSServer_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, szNetworkCardIP, IniFileName, EnabledDefaultVal, PrintMountPoints);
        }
        public void Destroy()
        {
            NFSServer_Destroy(m_Handle);
        }
        public void Stop()
        {
            NFSServer_Stop(m_Handle);
        }

    }
}
