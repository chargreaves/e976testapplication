using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace CommonCS
{
    class CTFTPServer
    {
        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TFTPServer_GetInterfaceVersion();

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool TFTPServer_Initialize(out int Handle, string szLogSocketAddress, int FixturePosition, string szNetworkCardIP, string szClientIP, string IniFileName);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern bool TFTPServer_SetClientAddress(int Handle, string szClientIP);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int TFTPServer_DownloadKernel(int Handle, int TimeOut, out int Retries, string szPath, string szDefaultFileName, string szSecondFileName, out bool bSecureKernel, out bool bWrapper, bool bReadFileRequestOnly, StringBuilder pRequestFileName, int RequestFileNameLen);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TFTPServer_Stop(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TFTPServer_Reset(int Handle);

        [DllImport("VIPServices", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern void TFTPServer_Destroy(int Handle);


        private int m_Handle;

        public bool Initialize(string szLogSocketAddress, int FixturePosition, string szNetworkCardIP, string szClientIP, string IniFileName)
        {
            if (TFTPServer_GetInterfaceVersion() != 1) return false;
            return TFTPServer_Initialize(out m_Handle, szLogSocketAddress, FixturePosition, szNetworkCardIP, szClientIP, IniFileName);
        }
        public bool SetClientAddress(string szClientIP)
        {
            return TFTPServer_SetClientAddress(m_Handle, szClientIP);
        }
        public int DownloadKernel(int TimeOut, out int Retries, string szPath, string szDefaultFileName, string szSecondFileName, out bool bSecureKernel, out bool bWrapper, bool bReadFileRequestOnly, out string pRequestFileName)
        {
            int ret;
            StringBuilder RequestFileName = new StringBuilder(1000);
            ret = TFTPServer_DownloadKernel(m_Handle, TimeOut, out Retries, szPath, szDefaultFileName, szSecondFileName, out bSecureKernel, out bWrapper, bReadFileRequestOnly, RequestFileName, RequestFileName.Capacity);
            pRequestFileName = RequestFileName.ToString();
            return ret;
        }
        public void Stop()
        {
            TFTPServer_Stop(m_Handle);
        }
        public void Reset()
        {
            TFTPServer_Reset(m_Handle);
        }
        public void Destroy()
        {
            TFTPServer_Destroy(m_Handle);
        }
    }
}
