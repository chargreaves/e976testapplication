using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void ButtonTest(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;
            string strOperatorMessage = string.Empty;
            string strButtonsToTest = string.Empty;
            int iLoop = 0;
            bool pass_flag = false;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99; //error

            strButtonsToTest = "Standby,Up,Down";
            string[] strButtons = strButtonsToTest.Split(',');
            string strButtonsResponse = "115,-1,-1"; //note Up and Down currently have same response
            string[] strResponses = strButtonsResponse.Split(',');

            PrintTestName(seqContext.Step.Name);

            try
            {
                //string strParameter = "BUTTONS_TO_TEST";
                //int status = testParamter.getParameterAsString(seqContext.Step.Name, strParameter, out strButtonsToTest);
                //if (status != 0)
                //{
                //reportText = "Parameter not specified: " + strParameter;
                //log.Error(reportText);
                //return;
                //}

                //int iLoopTime;
                //testParameter.getParameterAsInt(seqContext.Step.Name, "LOOP_TIME", out iLoopTime, 60000); // (ms) Operator input timeout

                //int iMtcPbistButtonTimeout;
                //testParameter.getParameterAsInt(seqContext.Step.Name, "MTC_PBIST_BUTTON_TIMEOUT", out iMtcPbistButtonTimeout, 10); // (s) used for "pbist button" read command MTC timeot

                //// MTP TimeOut needs to be longer than MTC Timeout for the PBIST BUTTON command
                //int iMtpPbistButtonTimeout;
                //testParamter.getParameterAsInt(seqContext.Step.Name, "MTP_PBIST_BUTTON_TIMEOUT", out iMtpPbistButtonTimeout, 18000); // (ms) used for "pbist button" read command MTP timeout

                //int iTimeout;
                //testParamter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 5000); // (ms) used for SendReceive

                command = "pbist ir";

                foreach (string strButton in strButtons)
                {
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, "", null, false); //clear message window
                    strOperatorMessage = "Press Button " + strButton;
                    log.Debug(strOperatorMessage);
                    seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, strOperatorMessage, null, false);

                    response = "OK KEY " + strResponses[iLoop];

                    do
                    {
                        strReadBuffer = string.Empty;
                        status = SendReceive(command, "Press a key", 3000, out strReadBuffer);
                        //status = SendReceive(command, "/ #", iMtcPbistButtonTimeout, out strReadBuffer);
                        if (status != 0) // No answer from the DUT!
                        {
                            seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, "", null, false); //clear message window
                            reportText = "1:No DUT response, sent: " + command;
                            log.Error(reportText);
                            reportText += "\n\nReceived: " + strReadBuffer;
                            result[0] = -1;
                            return;
                        }

                        strReadBuffer = string.Empty;
                        status = SendReceive("", "/ #", 30000, out strReadBuffer);

                        if (strReadBuffer.Contains(response)) // Correct button pressed!
                        {
                            pass_flag = true;
                            iLoop++;
                        }
                        else if (strReadBuffer.Contains("ERROR TIMEOUT")) // No operator input so far, but wait for Operator input TimeOut
                        {
                            seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, "", null, false); //clear message window
                            reportText = "2:Operator TimeOut: " + command;
                            log.Error(reportText);
                            reportText += "\n\nReceived: " + strReadBuffer;
                            result[0] = -2;
                            return;
                        }
                        else
                        {
                            pass_flag = false;
                        }

                     } while (pass_flag != true);

                }

                seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, "", null, false);
                seqContext.Thread.PostUIMessageEx(UIMessageCodes.UIMsg_UserMessageBase, 1, "", null, false);
                strOperatorMessage = "Button test OK";
                log.Debug(strOperatorMessage);

                //pass
                result[0] = 0;

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

    }

}