﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //=====================================================================================================================
        public void HPN_001_00_A1_Init_HPNA(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string command2;
            string response;
            int status;
            string strReadBuffer = string.Empty;
            string strReadBuffer2 = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99;

            PrintTestName(seqContext.Step.Name);

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            /// # init_hpna.sh -m 02:02:9B:0F:8F:94
            //Setting AON_GPIO: 0 to: 0
            //Setting AON_GPIO:0 to: 1
            //INIT_HPNA OK
            /// # connected to device
            //Configuration Mode!

            //Disconnecting
            //connected to device(with Default MAC)
            //loading image file:/ var / lib / firmware / hpna.bin
            //Loaded firmware file finished successfully
            //connected to device
            //Set MAC 02:02:9b: 0f:8f:94
            //Version: 1.9.4

            command = "init_hpna.sh -m 02:02:9B:0F:8F:94"; //
            response = "INIT_HPNA OK"; //
            status = SendReceive(command, "/ #", 6000, out strReadBuffer); //was 3000
            if (strReadBuffer.Contains(response))
            {
                command2 = ""; //
                response = "Set MAC 02:02:9b:0f:8f:94"; //MAC
                status = SendReceive(command2, "/ #", 30000, out strReadBuffer2); //was 15000

                if (strReadBuffer2.Contains(response))
                {
                    //pass
                    result[0] = 0;
                }
                else
                {
                    //fail
                    result[0] = -2;
                    reportText = "2:Fail: response not received: " + response;
                    reportText += command + ", Received: " + strReadBuffer;
                    reportText += command2 + ", Received: " + strReadBuffer2;
                    log.Debug(reportText);
                    return;
                }

            }
            else
            {
                //fail
                result[0] = -1;
                reportText = "1:Fail: response not received: " + response;
                reportText += command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
                return;
            }

        }

        //=====================================================================================================================
        public void HPNA_Init(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            //string command2;
            string response;
            int status;
            string strReadBuffer = string.Empty;
            //string strReadBuffer2 = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -99;

            PrintTestName(seqContext.Step.Name);

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            /// # init_hpna.sh -m 02:02:9B:0F:8F:94
            //Setting AON_GPIO: 0 to: 0
            //Setting AON_GPIO:0 to: 1
            //INIT_HPNA OK
            /// # connected to device
            //Configuration Mode!

            //Disconnecting
            //connected to device(with Default MAC)
            //loading image file:/ var / lib / firmware / hpna.bin
            //Loaded firmware file finished successfully
            //connected to device
            //Set MAC 02:02:9b: 0f:8f:94
            //Version: 1.9.4

            command = "init_hpna.sh -m 02:02:9B:0F:8F:94"; //
            response = "INIT_HPNA OK"; //
            status = SendReceive(command, "Version:", 20000, out strReadBuffer);
            //status = SendReceive(command, "/ #", 6000, out strReadBuffer); //was 3000
            if (strReadBuffer.Contains(response))
            {
                //command2 = ""; //
                response = "Set MAC 02:02:9B:0F:8F:94"; //MAC
                //response = "Set MAC 02:02:9b:0f:8f:94"; //MAC
                //status = SendReceive(command2, "Version:", 30000, out strReadBuffer2);
                //status = SendReceive(command2, "/ #", 30000, out strReadBuffer2); //was 15000

                if ((strReadBuffer.ToUpper()).Contains(response.ToUpper())) //check UPPER case
                //if (strReadBuffer.Contains(response))
                {
                    //pass
                    result = 0;
                }
                else
                {
                    //fail
                    result = -2;
                    reportText = "2:Fail: response not received: " + response;
                    reportText += "\n" + command + ", Received: " + strReadBuffer;
                    //reportText += command2 + ", Received: " + strReadBuffer2;
                    log.Debug(reportText);
                    //return;
                }

            }
            else
            {
                //fail
                result = -1;
                reportText = "1:Fail: response not received: " + response;
                reportText += "\n" + command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
                //return;
            }

            //stop init_hpna by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

        }

        //=====================================================================================================================

        Object thisLock;
        String m_sDataReceived;

        const string TransmitHPNA = "01";
        const string ReceiveHPNA = "02";

        public string Receive()
        {
            // Implement Receive function(s)
            string result = "";
            if (m_sDataReceived != "")
            {
                lock (thisLock)
                {
                    result = m_sDataReceived;
                    m_sDataReceived = "";
                }
            }
            return result;
        }

        private void proc_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                lock (thisLock)
                {
                    m_sDataReceived += e.Data;
                }
                log.Debug(e.Data);
            }
        }


        //=====================================================================================================================
        public void HPN_001_00_A2_Check_Devices(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99;

            PrintTestName(seqContext.Step.Name);

            try
            {
                //chris
                //thisLock = new Object();

                //string buffer;
                //ProcessStartInfo pro = new ProcessStartInfo();
                //pro.FileName = @"c:\program files\coppergate\cdk\devinf.exe";
                //Process proStart = new Process();
                //proStart.StartInfo = pro;

                //// This ensures that you get the output from the DOS application
                //proStart.StartInfo.RedirectStandardInput = true;
                //proStart.StartInfo.RedirectStandardOutput = true;

                //// These two optional flags ensure that no DOS window
                //// appears
                //proStart.StartInfo.UseShellExecute = false;
                ////proStart.StartInfo.CreateNoWindow = true;


                //// Set event handle
                //proStart.OutputDataReceived += proc_DataReceived;

                //proStart.Start();

                //if (!proStart.Start())
                //{
                //    log.ErrorFormat("Failed to start {0} process.", "");
                //    //return -1;
                //}
                //proStart.BeginOutputReadLine();

                //bool bReceivedPrompt = false;
                //System.DateTime loopTimeout = System.DateTime.Now;
                //loopTimeout = loopTimeout.AddMilliseconds(10000);
                //string strReceived = "";
                //do
                //{
                //    strReceived += Receive();
                //    if (strReceived.Contains("please select network interface:"))
                //        bReceivedPrompt = true;
                //} while (!bReceivedPrompt && loopTimeout.CompareTo(System.DateTime.Now) > 0);
                //buffer = strReceived;
                //chris

                //string strFlowFile;
                //testParameter.getParameterAsString(seqContext.Step.Name, "FLOW_FILE", out strFlowFile, "QSR1000_calib_only.txt");

                //string strIQFactVer;
                //testParameter.getParameterAsString(seqContext.Step.Name, "IQFACT_VERSION", out strIQFactVer, "IQfact+_QTN_QSR1000_3.3.0.7_Lock");

                //string cgRunPath = @"c:\windows\system32\";
                //ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                //cgRun.initialize("CopperGate", cgRunPath + "cmd.exe", @"C:\Program Files\CopperGate\CDK\devinf.exe", cgRunPath);

                string cgRunPath = @"C:\Program Files\CopperGate\CDK\";
                ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                cgRun.initialize("CopperGate", cgRunPath + "devinf.exe", "-i 192.168.0.200", cgRunPath);

                string sReadBuffer;
                int status = cgRun.StartProcess(30000, "Total number of devices: 2", out sReadBuffer);
                if (status != 0)
                {
                    //fail
                    result[0] = -1;
                    reportText = "1:Fail: devinf exit with error, return status: " + status;
                    reportText += "\n\n" + sReadBuffer;
                    log.Error(reportText);
                    return;
                }

                //pass
                result[0] = 0;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void HPNA_Check_Devices(SequenceContext seqContext, out double result, out string reportText)
        {
            reportText = string.Empty;

            //set result to fail
            result = -99;

            PrintTestName(seqContext.Step.Name);

            try
            {
                //chris
                //thisLock = new Object();

                //string buffer;
                //ProcessStartInfo pro = new ProcessStartInfo();
                //pro.FileName = @"c:\program files\coppergate\cdk\devinf.exe";
                //Process proStart = new Process();
                //proStart.StartInfo = pro;

                //// This ensures that you get the output from the DOS application
                //proStart.StartInfo.RedirectStandardInput = true;
                //proStart.StartInfo.RedirectStandardOutput = true;

                //// These two optional flags ensure that no DOS window
                //// appears
                //proStart.StartInfo.UseShellExecute = false;
                ////proStart.StartInfo.CreateNoWindow = true;


                //// Set event handle
                //proStart.OutputDataReceived += proc_DataReceived;

                //proStart.Start();

                //if (!proStart.Start())
                //{
                //    log.ErrorFormat("Failed to start {0} process.", "");
                //    //return -1;
                //}
                //proStart.BeginOutputReadLine();

                //bool bReceivedPrompt = false;
                //System.DateTime loopTimeout = System.DateTime.Now;
                //loopTimeout = loopTimeout.AddMilliseconds(10000);
                //string strReceived = "";
                //do
                //{
                //    strReceived += Receive();
                //    if (strReceived.Contains("please select network interface:"))
                //        bReceivedPrompt = true;
                //} while (!bReceivedPrompt && loopTimeout.CompareTo(System.DateTime.Now) > 0);
                //buffer = strReceived;
                //chris

                //string strFlowFile;
                //testParameter.getParameterAsString(seqContext.Step.Name, "FLOW_FILE", out strFlowFile, "QSR1000_calib_only.txt");

                //string strIQFactVer;
                //testParameter.getParameterAsString(seqContext.Step.Name, "IQFACT_VERSION", out strIQFactVer, "IQfact+_QTN_QSR1000_3.3.0.7_Lock");

                //string cgRunPath = @"c:\windows\system32\";
                //ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                //cgRun.initialize("CopperGate", cgRunPath + "cmd.exe", @"C:\Program Files\CopperGate\CDK\devinf.exe", cgRunPath);

                string cgRunPath = @"C:\Program Files\CopperGate\CDK\";
                ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                cgRun.initialize("CopperGate", cgRunPath + "devinf.exe", "-i 192.168.0.1", cgRunPath);
                //cgRun.initialize("CopperGate", cgRunPath + "devinf.exe", "-i 192.168.0.200", cgRunPath);

                string sReadBuffer;
                int status = cgRun.StartProcess(30000, "Total number of devices: 2", out sReadBuffer);
                if (status != 0)
                {
                    //fail
                    result = -1;
                    reportText = "1:Fail: devinf exit with error, return status: " + status;
                    reportText += "\n\n" + sReadBuffer;
                    log.Error(reportText);
                    return;
                }

                //pass
                result = 0;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void HPN_002_00_00_HPNA_Throughput_High_Power(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[9];

            //set result to fail
            result[0] = -99; //error
            result[1] = 0; //HPN_002_00_01_HPNA_Throughput_High_Power_Up_PER
            result[2] = 0; //HPN_002_00_02_HPNA_Throughput_High_Power_Up_SNR
            result[3] = 0; //HPN_002_00_03_HPNA_Throughput_High_Power_Up_Rate
            result[4] = 0; //HPN_002_00_04_HPNA_Throughput_High_Power_Up_Rx_Power
            result[5] = 0; //HPN_002_00_05_HPNA_Throughput_High_Power_Down_PER
            result[6] = 0; //HPN_002_00_06_HPNA_Throughput_High_Power_Down_SNR
            result[7] = 0; //HPN_002_00_07_HPNA_Throughput_High_Power_Down_Rate
            result[8] = 0; //HPN_002_00_08_HPNA_Throughput_High_Power_Down_Rx_Power

            PrintTestName(seqContext.Step.Name);

            try
            {
                string cgRunPath = @"C:\Program Files\CopperGate\CDK\";
                ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                cgRun.initialize("CopperGate", cgRunPath + "netinf.exe", "-i 192.168.0.200 -n 100000", cgRunPath);

                string sReadBuffer;
                int status = cgRun.StartProcess(120000, "disabling CERT on devices", out sReadBuffer);
                if (status != 0)
                {
                    //fail
                    result[0] = -1;
                    reportText = "1:Fail: netinf exit with error, return status: " + status;
                    reportText += "\n\n" + sReadBuffer;
                    log.Error(reportText);
                    return;
                }

                //pass
                result[0] = 0;

                //need to extract the following results from sReadBuffer
                //sReadBuffer = "...........enabling CERT on devices...testing...01) 00:01:40:24:00:19-->02:02:9b:0f:8f:94: .....01) 00:01:40:24:00:19-->02:02:9b:0f:8f:94: .....01) 00:01:40:24:00:19-->02:02:9b:0f:8f:94: pkts: 100000/100000 per: 0.10e+072 snr 39.49dB, rate: 256Mbps 32/8 Rx power: -0.59 dBm 02) 02:02:9b:0f:8f:94-->00:01:40:24:00:19: .....02) 02:02:9b:0f:8f:94-->00:01:40:24:00:19: .....02) 02:02:9b:0f:8f:94-->00:01:40:24:00:19: pkts: 100000/100000 per: 0.20e+073 snr 38.24dB, rate: 256Mbps 32/8 Rx power: -0.63 dBm disabling CERT on devices";
                result[1] = GetHPNAData(TransmitHPNA, "per", sReadBuffer); //HPN_002_00_01_HPNA_Throughput_Low_Up_PER
                result[2] = GetHPNAData(TransmitHPNA, "snr", sReadBuffer); //HPN_002_00_02_HPNA_Throughput_Low_Up_SNR
                result[3] = GetHPNAData(TransmitHPNA, "rate", sReadBuffer); //HPN_002_00_03_HPNA_Throughput_Low_Up_Rate
                result[4] = GetHPNAData(TransmitHPNA, "rx power", sReadBuffer); //HPN_002_00_04_HPNA_Throughput_Low_Up_Rx_Power
                result[5] = GetHPNAData(ReceiveHPNA, "per", sReadBuffer); //HPN_002_00_05_HPNA_Throughput_Low_Down_PER
                result[6] = GetHPNAData(ReceiveHPNA, "snr", sReadBuffer); //HPN_002_00_06_HPNA_Throughput_Low_Down_SNR
                result[7] = GetHPNAData(ReceiveHPNA, "rate", sReadBuffer); //HPN_002_00_07_HPNA_Throughput_Low_Down_Rate
                result[8] = GetHPNAData(ReceiveHPNA, "rx power", sReadBuffer); //HPN_002_00_08_HPNA_Throughput_Low_Down_Rx_Power

                //reportText = sReadBuffer;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void HPNA_Throughput(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[9];

            //set result to fail
            result[0] = -99; //error
            result[1] = 0; //01_Up_PER
            result[2] = 0; //02_Up_SNR
            result[3] = 0; //03_Up_Rate
            result[4] = 0; //04_Rx_Power
            result[5] = 0; //05_Down_PER
            result[6] = 0; //06_Down_SNR
            result[7] = 0; //07_Down_Rate
            result[8] = 0; //08_Down_Rx_Power

            PrintTestName(seqContext.Step.Name);

            try
            {
                string cgRunPath = @"C:\Program Files\CopperGate\CDK\";
                ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                cgRun.initialize("CopperGate", cgRunPath + "netinf.exe", "-i 192.168.0.200 -n 10000", cgRunPath);
                //cgRun.initialize("CopperGate", cgRunPath + "netinf.exe", "-i 192.168.0.200 -n 100000", cgRunPath);

                string sReadBuffer;
                int status = cgRun.StartProcess(120000, "disabling CERT on devices", out sReadBuffer);
                if (status != 0)
                {
                    //fail
                    result[0] = -1;
                    reportText = "1:Fail: netinf exit with error, return status: " + status;
                    reportText += "\n\n" + sReadBuffer;
                    log.Error(reportText);
                    return;
                }

                //pass
                result[0] = 0;

                //need to extract the following results from sReadBuffer
                //sReadBuffer = "...........enabling CERT on devices...testing...01) 00:01:40:24:00:19-->02:02:9b:0f:8f:94: .....01) 00:01:40:24:00:19-->02:02:9b:0f:8f:94: .....01) 00:01:40:24:00:19-->02:02:9b:0f:8f:94: pkts: 100000/100000 per: 0.10e+072 snr 39.49dB, rate: 256Mbps 32/8 Rx power: -0.59 dBm 02) 02:02:9b:0f:8f:94-->00:01:40:24:00:19: .....02) 02:02:9b:0f:8f:94-->00:01:40:24:00:19: .....02) 02:02:9b:0f:8f:94-->00:01:40:24:00:19: pkts: 100000/100000 per: 0.20e+073 snr 38.24dB, rate: 256Mbps 32/8 Rx power: -0.63 dBm disabling CERT on devices";
                result[1] = GetHPNAData(TransmitHPNA, "per", sReadBuffer); //HPN_002_00_01_HPNA_Throughput_Low_Up_PER
                result[2] = GetHPNAData(TransmitHPNA, "snr", sReadBuffer); //HPN_002_00_02_HPNA_Throughput_Low_Up_SNR
                result[3] = GetHPNAData(TransmitHPNA, "rate", sReadBuffer); //HPN_002_00_03_HPNA_Throughput_Low_Up_Rate
                result[4] = GetHPNAData(TransmitHPNA, "rx power", sReadBuffer); //HPN_002_00_04_HPNA_Throughput_Low_Up_Rx_Power
                result[5] = GetHPNAData(ReceiveHPNA, "per", sReadBuffer); //HPN_002_00_05_HPNA_Throughput_Low_Down_PER
                result[6] = GetHPNAData(ReceiveHPNA, "snr", sReadBuffer); //HPN_002_00_06_HPNA_Throughput_Low_Down_SNR
                result[7] = GetHPNAData(ReceiveHPNA, "rate", sReadBuffer); //HPN_002_00_07_HPNA_Throughput_Low_Down_Rate
                result[8] = GetHPNAData(ReceiveHPNA, "rx power", sReadBuffer); //HPN_002_00_08_HPNA_Throughput_Low_Down_Rx_Power

                //reportText = sReadBuffer;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //public string GetHPNAData(string Direction, string RequiredInfo, string HPNAData)
        //{

        //    string sSearch, sDataFound, sCleanedData, sChar;
        //    int iChr, iLoop, i, iLen, iLoc, iTmp, sStart, sEnd, sTemp, sTemp2;

        //    sSearch = Direction + ")";
        //    iLoop = 0; // 0 = transmit, 1 = receive
        //    sDataFound = "ERROR";

        //    iLoc = string.Compare(HPNAData, sSearch);
        //    if (iLoc > 0 )
        //    {
        //        sDataFound = HPNAData.ToUpper();
        //        return sDataFound;
        //    }
        //    else
        //    {
        //        return sDataFound;
        //    }

        //}

        //=====================================================================================================================
        private double GetHPNAData(string Direction, string RequiredInfo, string HPNAData)
        {

            string sSearch, sDataFound, sStart, sEnd, sTemp, sTemp2;
            int iLoc, iLocEnd, iTmp, iLensStart;
            double dDataFound;

            sSearch = Direction + ")";
            dDataFound = -1;

            iLoc = HPNAData.IndexOf(sSearch);

            if (iLoc > 0)
            {
                iLocEnd = HPNAData.Length;
                sDataFound = HPNAData.Substring(iLoc, iLocEnd - iLoc);
                sDataFound = sDataFound.ToUpper();

            }
            else
            {
                return dDataFound;
            }

            switch (RequiredInfo.ToUpper())
            {
                case "SNR":
                    sStart = "SNR ";
                    sEnd = "DB, ";
                    break;
                case "RATE":
                    sStart = "RATE: ";
                    sEnd = "MBPS ";
                    break;
                case "RX POWER":
                    sStart = "RX POWER: ";
                    sEnd = "DBM";
                    break;
                case "PER":
                    sStart = "PER: ";
                    sEnd = " SNR";
                    break;
                default:
                    sStart = "";
                    sEnd = "";
                    break;
            }

            iTmp = sDataFound.IndexOf(sStart);

            if (iTmp > 0)
            {

                iLensStart = sStart.Length;
                iTmp = iTmp + iLensStart;
                sTemp = sDataFound.Substring(iTmp, 20); // 20 is an arbitary value that will capture required data
                iTmp = sTemp.IndexOf(sEnd);
                if (iTmp > 0)
                {
                    sTemp2 = sTemp.Substring(0, iTmp);
                    sDataFound = sTemp2.Trim();

                }
                else
                {
                    dDataFound = -1;
                }


            }
            else
            {
                dDataFound = -1;
            }

            dDataFound = Convert.ToDouble(sDataFound);
            return dDataFound;

        }

        //=====================================================================================================================
        public void LitePointIQRun(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;

            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            try
            {
                string strFlowFile;
                testParameter.getParameterAsString(seqContext.Step.Name, "FLOW_FILE", out strFlowFile, "QSR1000_calib_only.txt");

                string strIQFactVer;
                testParameter.getParameterAsString(seqContext.Step.Name, "IQFACT_VERSION", out strIQFactVer, "IQfact+_QTN_QSR1000_3.3.0.7_Lock");
                string iqRunPath = @"C:\Litepoint\Iqfact_plus\" + strIQFactVer + @"\bin\";
                ExecProcess iqRun = new ExecProcess(m_iUutInstance);
                iqRun.initialize("IQRun", iqRunPath + "IQRun_Console.exe", "-run " + strFlowFile, iqRunPath);


                string sReadBuffer;
                int status = iqRun.StartProcess(300000, "Please remove DUT, press any key to continue (x or Esc to exit)....", out sReadBuffer);
                if (status != 0)
                {
                    iqRun.Exit();
                    result = -1;
                    reportText = "1:Error: IQRun_Console exit with error, return status: " + status;
                    log.Error(reportText);
                    return;
                }
                iqRun.Exit("x");

                // TODO: Find better text to parse?
                string sResult = sReadBuffer.Substring(sReadBuffer.IndexOf("Passed Run(s)"));
                sResult = sResult.Substring(0, sResult.IndexOf("Failed Run(s)")).Trim();
                if (sResult != "Passed Run(s)               :1")
                {
                    result = -2;
                    reportText = "2:Error: IQRun flow file did not pass!";
                    log.Error(reportText);
                    reportText += "\n\n" + sReadBuffer.Substring(sReadBuffer.IndexOf("Total Run(s)"));
                    reportText = reportText.Substring(0, reportText.IndexOf("---")).Trim();
                    return;
                }

                result = 0;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }
        }//LitePointIQRun

        //=====================================================================================================================
        public void Enable_UUT_Adapter(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;

            //set result to fail
            result = false;

            PrintTestName(seqContext.Step.Name);

            string interfaceName = "";
            testParameter.getParameterAsString(seqContext.Step.Name, "UUT_Adapter_Name", out interfaceName, "xxx"); //read UUT Ethernet adapter name from testParameters.xml, default to XXX if no entry in testParameters.xml file to ensure test fails

            try
            {
                //enable UUT Direct Ethernet port
                Process p = new Process();
                ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" enable");
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                p.StartInfo = psi;
                p.Start();

               result = true;

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void Disable_UUT_Adapter(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;

            //set result to fail
            result = false;

            PrintTestName(seqContext.Step.Name);

            string interfaceName = "";
            testParameter.getParameterAsString(seqContext.Step.Name, "UUT_Adapter_Name", out interfaceName, "xxx"); //read UUT Ethernet adapter name from testParameters.xml, default to XXX if no entry in testParameters.xml file to ensure test fails

            try
            {
                //Process process = new System.Diagnostics.Process();
                //ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //startInfo.FileName = "cmd.exe";
                //startInfo.Arguments = "/C arp -d 192.168.0.20";
                //process.StartInfo = startInfo;
                //process.Start();

                //System.Threading.Thread.Sleep(1000);

                //disable UUT Direct Ethernet port
                Process p = new Process();
                ProcessStartInfo psi = new ProcessStartInfo("netsh", "interface set interface \"" + interfaceName + "\" disable");
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                p.StartInfo = psi;
                p.Start();

                //wait
                //System.Threading.Thread.Sleep(10000);

                //string cgRunPath = ""; // @"C:\Program Files\CopperGate\CDK\";
                //ExecProcess cgRun = new ExecProcess(m_iUutInstance);
                //cgRun.initialize("CopperGate", cgRunPath + "notepad.exe", "", cgRunPath);

                //ProcessStartInfo startInfo1 = new ProcessStartInfo("cmd.exe", "/C dir");
                //ProcessStartInfo startInfo1 = new ProcessStartInfo("notepad.exe", "");
                //process.StartInfo = startInfo1;
                //process.Start();

                //manually add UUT 192.168.0.20 to Bridge Ethernet port 192.168.0.200
                //Process process = new System.Diagnostics.Process();
                //ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe", "/C arp -s 192.168.0.20 02-02-9b-0f-90-f2 192.168.0.200");
                //ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //startInfo.FileName = "cmd.exe";
                //startInfo.Arguments = "/C arp -s 192.168.0.20 02-02-9b-0f-90-f2 192.168.0.200";
                //process.StartInfo = startInfo;
                //process.Start();

                result = true;

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void Add_UUT_To_Arp_Table(SequenceContext seqContext, out bool result, out string reportText)
        {
            string line = "";
            int i;

            reportText = string.Empty;

            //set result to fail
            result = false;

            PrintTestName(seqContext.Step.Name);

            try
            {
                for (i=0; i<=9; i++)
                {
                    line = "";

                    //manually add UUT 192.168.0.20 to Bridge Ethernet port 192.168.0.200
                    Process process = new System.Diagnostics.Process();
                    ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe", "/C arp -s 192.168.0.20 02-02-9b-0f-90-f2 192.168.0.200");
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardOutput = true;
                    startInfo.CreateNoWindow = true;
                    process.StartInfo = startInfo;
                    process.Start();

                    while (!process.StandardOutput.EndOfStream)
                    {
                        //line += process.StandardOutput.ReadLine();
                    }

                    System.Threading.Thread.Sleep(1000);

                    //check arp table contains 192.168.0.20
                    ProcessStartInfo arpInfo = new ProcessStartInfo("cmd.exe", "/C arp -a");
                    arpInfo.UseShellExecute = false;
                    arpInfo.RedirectStandardOutput = true;
                    arpInfo.CreateNoWindow = true;
                    process.StartInfo = arpInfo;
                    process.Start();

                    while (!process.StandardOutput.EndOfStream)
                    {
                        line += process.StandardOutput.ReadLine();
                    }

                    if (line.Contains("192.168.0.20 "))
                    {
                        reportText = "retries = " + i.ToString();
                        result = true;
                        break;
                    }
                    else
                    {

                    }

                    System.Threading.Thread.Sleep(1000); //wait before retry

                }

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void Remove_UUT_From_Arp_Table(SequenceContext seqContext, out bool result, out string reportText)
        {
            string line = "";
            int i;

            reportText = string.Empty;

            //set result to fail
            result = false;

            PrintTestName(seqContext.Step.Name);

            try
            {
                for (i = 0; i <= 9; i++)
                {
                    line = "";

                    //manually delete UUT 192.168.0.20 from arp table
                    Process process = new System.Diagnostics.Process();
                    ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe", "/C arp -d 192.168.0.20");
                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardOutput = true;
                    startInfo.CreateNoWindow = true;
                    process.StartInfo = startInfo;
                    process.Start();

                    while (!process.StandardOutput.EndOfStream)
                    {
                        //line += process.StandardOutput.ReadLine();
                    }

                    System.Threading.Thread.Sleep(1000);

                    //check arp table no longer contains 192.168.0.20
                    ProcessStartInfo arpInfo = new ProcessStartInfo("cmd.exe", "/C arp -a");
                    arpInfo.UseShellExecute = false;
                    arpInfo.RedirectStandardOutput = true;
                    arpInfo.CreateNoWindow = true;
                    process.StartInfo = arpInfo;
                    process.Start();

                    while (!process.StandardOutput.EndOfStream)
                    {
                        line += process.StandardOutput.ReadLine();
                    }

                    if (!line.Contains("192.168.0.20 "))
                    {
                        reportText = "retries = " + i.ToString();
                        result = true;
                        break;
                    }
                    else
                    {

                    }

                    System.Threading.Thread.Sleep(1000); //wait before retry

                }

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void Switch_Ethernet_to_Bridge(SequenceContext seqContext, out bool result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = false;

            PrintTestName(seqContext.Step.Name);

            command = "clear_all 1";
            response = "ok"; //

            try
            {
                status = SendReceive(command, "\n", 3000, out strReadBuffer, usquid);

                if (strReadBuffer.Contains(response))
                {
                    command = "set 4 1";

                    status = SendReceive(command, "\n", 3000, out strReadBuffer, usquid);

                    if (strReadBuffer.Contains(response))
                    {
                        //pass
                        result = true;

                    }
                    else
                    {
                        //fail
                        reportText = "2:Fail: response not received: " + response;
                        reportText += "\n" + command + ", Received: " + strReadBuffer;
                        log.Debug(reportText);
                    }

                }
                else
                {
                    //fail
                    reportText = "1:Fail: response not received: " + response;
                    reportText += "\n" + command + ", Received: " + strReadBuffer;
                    log.Debug(reportText);
                }

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

        //=====================================================================================================================
        public void Switch_Ethernet_to_UUT(SequenceContext seqContext, out bool result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = false;

            PrintTestName(seqContext.Step.Name);

            command = "clear_all 1";
            response = "ok"; //

            try
            {
                status = SendReceive(command, "\n", 3000, out strReadBuffer, usquid);

                if (strReadBuffer.Contains(response))
                {
                    command = "set 7 1";

                    status = SendReceive(command, "\n", 3000, out strReadBuffer, usquid);

                    if (strReadBuffer.Contains(response))
                    {
                        //pass
                        result = true;

                    }
                    else
                    {
                        //fail
                        reportText = "2:Fail: response not received: " + response;
                        reportText += "\n" + command + ", Received: " + strReadBuffer;
                        log.Debug(reportText);
                    }

                }
                else
                {
                    //fail
                    reportText = "1:Fail: response not received: " + response;
                    reportText += "\n" + command + ", Received: " + strReadBuffer;
                    log.Debug(reportText);
                }

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Error(reportText);
            }

        }

    }

}
