﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //=====================================================================================================================
        public void AV_Setup1KHzTone(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            TestTask.Send_TT_Command("5", "5", "01000"); //output 1KHz tone

            TestTask.Send_TT_Command("5", "0", "100100"); //100% volume left and right

            //Route Audio In
            ScartCard1.AVRouting(ScartCard1.C0868ACARD_ROUTE_AUDIO_IN, ScartCard1.C0868ACARD_PAL);

            //pause 500ms to let video waveform settle
            System.Threading.Thread.Sleep(500);

            measurement = true;
            reportText = "";

        }

        //=====================================================================================================================
        public void AUDIO_AnalyseStereo0(SequenceContext seqContext, out double[] result, out string reportText)
        {
            //********************************************************************************
            //*
            //* Description: Analyse the centre set composite video signal
            //*    Composite > Blue fixture TV scart > 868(AvCard1) > 867 > PC
            //*
            //* Prerequisite:Requires Test 7500 Tuning peformed
            //*
            //* TT commands: "","",""
            //*
            //* Fail codes: 1 - Failed video analysis for CVBS1
            //*
            //********************************************************************************

            reportText = string.Empty;
            result = new double[3];

            result[1] = AudioAnalysis.TestStereo_Scart_AudMatrix1(ScartCard1, AudioAnalysis.CAUDIOANALYSIS_CHECK_STEREO_AMPLITUDE);
            //check for fail
            if (result[1] != 0)
            {
                //fail
                reportText = AudioAnalysis.GetFailMessage();
                AudioAnalysis.ClearFailMessage();
            }
            else
            {
                result[2] = AudioAnalysis.TestStereo_Scart_AudMatrix1(ScartCard1, AudioAnalysis.CAUDIOANALYSIS_CHECK_STEREO_FFT);
                if (result[2] != 0)
                {
                    //fail
                    reportText = AudioAnalysis.GetFailMessage();
                    AudioAnalysis.ClearFailMessage();
                }

            }

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            ScartCard1.Reset();

        }

        //=====================================================================================================================
        public void AUD_008_00_Phono_Amplitude(SequenceContext seqContext, out double[] result, out string reportText)
        {
            //********************************************************************************
            //*
            //* Description: Analyse the centre set composite video signal
            //*    Composite > Blue fixture TV scart > 868(AvCard1) > 867 > PC
            //*
            //* Prerequisite:Requires Test 7500 Tuning peformed
            //*
            //* TT commands: "","",""
            //*
            //* Fail codes: 1 - Failed video analysis for CVBS1
            //*
            //********************************************************************************

            reportText = string.Empty;
            result = new double[3];

            PrintTestName(seqContext.Step.Name);

            result = AudioAnalysis.TestStereo_Scart_AudMatrix(ScartCard1, AudioAnalysis.CAUDIOANALYSIS_CHECK_STEREO_AMPLITUDE);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            ScartCard1.Reset();

        }

        //=====================================================================================================================
        public void AUD_009_00_Phono_FFT(SequenceContext seqContext, out double[] result, out string reportText)
        {
            //********************************************************************************
            //*
            //* Description: Analyse the centre set composite video signal
            //*    Composite > Blue fixture TV scart > 868(AvCard1) > 867 > PC
            //*
            //* Prerequisite:Requires Test 7500 Tuning peformed
            //*
            //* TT commands: "","",""
            //*
            //* Fail codes: 1 - Failed video analysis for CVBS1
            //*
            //********************************************************************************

            reportText = string.Empty;
            result = new double[3];

            PrintTestName(seqContext.Step.Name);

            result = AudioAnalysis.TestStereo_Scart_AudMatrix(ScartCard1, AudioAnalysis.CAUDIOANALYSIS_CHECK_STEREO_FFT);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            ScartCard1.Reset();

        }

        //=====================================================================================================================
        public void AUD_008_01_Phono_Amplitude(SequenceContext seqContext, out double[] result, out string reportText)
        {
            //********************************************************************************
            //*
            //* Description: Analyse the centre set composite video signal
            //*    Composite > Blue fixture TV scart > 868(AvCard1) > 867 > PC
            //*
            //* Prerequisite:Requires Test 7500 Tuning peformed
            //*
            //* TT commands: "","",""
            //*
            //* Fail codes: 1 - Failed video analysis for CVBS1
            //*
            //********************************************************************************

            reportText = string.Empty;
            result = new double[3];

            PrintTestName(seqContext.Step.Name);

            result = AudioAnalysis.TestStereo_Scart_AudMatrix(ScartCard2, AudioAnalysis.CAUDIOANALYSIS_CHECK_STEREO_AMPLITUDE);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            ScartCard2.Reset();

        }

        //=====================================================================================================================
        public void AUD_009_01_Phono_FFT(SequenceContext seqContext, out double[] result, out string reportText)
        {
            //********************************************************************************
            //*
            //* Description: Analyse the centre set composite video signal
            //*    Composite > Blue fixture TV scart > 868(AvCard1) > 867 > PC
            //*
            //* Prerequisite:Requires Test 7500 Tuning peformed
            //*
            //* TT commands: "","",""
            //*
            //* Fail codes: 1 - Failed video analysis for CVBS1
            //*
            //********************************************************************************

            reportText = string.Empty;
            result = new double[3];

            PrintTestName(seqContext.Step.Name);

            result = AudioAnalysis.TestStereo_Scart_AudMatrix(ScartCard2, AudioAnalysis.CAUDIOANALYSIS_CHECK_HDMI_FFT);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            ScartCard2.Reset();

        }

        //=====================================================================================================================
        public void CreateTraceLimitsFileAudioPhono(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            string SeqFilePath;
            string SeqFolder;

            PrintTestName(seqContext.Step.Name);

            SeqFilePath = seqContext.SequenceFile.Path; //get path of .seq file

            SeqFolder = SeqFilePath.Substring(0, SeqFilePath.LastIndexOf("\\")); //extact folder containing .seq file

            //clear list of limit lines
            AudioAnalysis.ResetLimitsToSave();

            AudioAnalysis.CreateStereoLimits_Scart_AudMatrix(ScartCard1, true, "AUDIO_FFT_L_MAX", "AUDIO_FFT_L_MIN", "AUDIO_FFT_R_MAX", "AUDIO_FFT_R_MIN");

            //save the file to a temp .csv file - rename it to use it.
            AudioAnalysis.SaveLimitFile(SeqFolder + "\\" + "AutoOutAudioPhono.csv", TraceTools.CTRACETOOLS_SAVEMODENEWFILE);

            measurement = true;
            reportText = "";

        }

        //=====================================================================================================================
        //create auto analysis trace limit files for HDMI audio extracted from 
        //uses ScartCard2
        //
        public void CreateTraceLimitsFileAudioHDMI(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            string SeqFilePath;
            string SeqFolder;

            PrintTestName(seqContext.Step.Name);

            SeqFilePath = seqContext.SequenceFile.Path; //get path of .seq file

            SeqFolder = SeqFilePath.Substring(0, SeqFilePath.LastIndexOf("\\")); //extact folder containing .seq file

            //clear list of limit lines
            AudioAnalysis.ResetLimitsToSave();

            AudioAnalysis.CreateStereoLimits_Scart_AudMatrix(ScartCard2, true, "AUDIO_FFT_L_HDMI_MAX", "AUDIO_FFT_L_HDMI_MIN", "AUDIO_FFT_R_HDMI_MAX", "AUDIO_FFT_R_HDMI_MIN");

            //save the file to a temp .csv file - rename it to use it.
            AudioAnalysis.SaveLimitFile(SeqFolder + "\\" + "AutoOutAudioHDMI.csv", TraceTools.CTRACETOOLS_SAVEMODENEWFILE);

            measurement = true;
            reportText = "";

        }

        //=====================================================================================================================

    }

}
