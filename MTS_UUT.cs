﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NationalInstruments.TestStand.Interop.API;
using UUT_Comms;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public static UUT_Comms.TestTask TestTask = new UUT_Comms.TestTask();

        //=====================================================================================================================
        public bool UUT_Comms_Initialize(SequenceContext seqContext)
        {
            TestTask.Initialize();
            return true;

        }

        //=====================================================================================================================
        public bool UUT_Comms_Terminate(SequenceContext seqContext)
        {
            TestTask.Terminate();
            return true;

        }

        //=====================================================================================================================
        public void Check_Product_ID(SequenceContext seqContext, out string measurement, out string reportText)
        {
            TestTask.Send_TT_Command(":", "0", "");
            measurement = TestTask.GetReturnedData();
            reportText = "Check_Product_ID";

        }

        //=====================================================================================================================
        public bool Boot_UUT(SequenceContext seqContext)
        {
            return TestTask.IRDInit();

        }

        //=====================================================================================================================
        public void Power_On_PFTS(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            MainsCard.ProductPowerOn();
            measurement = !MainsCard.ErrorOccurred();
            reportText = MainsCard.ErrorLog();
            MainsCard.ClearErrorOccurred();
            MainsCard.ClearErrorLog();

        }

        //=====================================================================================================================
        public void Power_Off_PFTS(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            MainsCard.ProductPowerOff();
            measurement = !MainsCard.ErrorOccurred();
            reportText = MainsCard.ErrorLog();
            MainsCard.ClearErrorOccurred();
            MainsCard.ClearErrorLog();

        }

        //=====================================================================================================================
        public void CheckFixtureEngaged(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[2];

            PrintTestName(seqContext.Step.Name);

            //Checks the fixture down status
            if (AMB.U12.GetBit(AMB.U12.CPCF8574_BIT_P0) == AMB.U12.CPCF8574_BIT_LO)
            {
                //pass
                reportText = "Fixture Engaged";
                result[1] = 0;
            }
            else
            {
                //fail
                reportText = "Fixture Not Engaged";
                result[1] = -1;
            }

            //check for error
            result[0] = Convert.ToDouble(AMB.U12.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                reportText = reportText + "\n" + "CheckFixtureEngaged" + ": " + "Reading Fixture Status with AMB U12 (Class CPCF8574)";
                reportText = reportText + "\n" + PFTS.ErrorLog();
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            log.Debug(""); //add blank line to Output

        }

        //=====================================================================================================================
        public void CheckBoardPresent(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[2];

            PrintTestName(seqContext.Step.Name);

            //Checks the fixture down status
            if ((AMB.U12.GetBit(AMB.U12.CPCF8574_BIT_P1) == AMB.U12.CPCF8574_BIT_LO))
            //if ((AMB.U12.GetBit(AMB.U12.CPCF8574_BIT_P1) == AMB.U12.CPCF8574_BIT_LO) && (AMB.U12.GetBit(AMB.U12.CPCF8574_BIT_P2) == AMB.U12.CPCF8574_BIT_LO))
            {
                //pass
                reportText = "Board Detected";
                result[1] = 0;
            }
            else
            {
                //fail
                reportText = "Board Not Present";
                result[1] = -1;
            }

            //check for error
            result[0] = Convert.ToDouble(AMB.U12.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                reportText = reportText + "\n" + "CheckBoardPresent" + ": " + "Reading Board Status with AMB U12 (Class CPCF8574)";
                reportText = reportText + "\n" + PFTS.ErrorLog();
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

            log.Debug(""); //add blank line to Output

        }

         //=====================================================================================================================

    }

}
