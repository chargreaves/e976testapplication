using System;
using System.Reflection;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;
using System.Net;
using System.Net.Sockets;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;

namespace E976TestApplication
{
    /// <summary>
    /// Summary description for NetLogger
    /// </summary>
    public class NetLogger
    {
        ILogging log;
        LVClient m_CoreToolkit;
        private ManualResetEvent threadCreated;
        private System.Threading.Thread t;
        private Object thisLock = new Object();
        private string m_ReadBuffer;
        private bool m_exit;
        private bool m_initialized;
        private UdpClient m_udpClient;
        private IPEndPoint m_ipEnd;
        private string m_sIP;

        public NetLogger() : this(0, "192.168.0.1")
        {
        }

        public NetLogger(int testSocket, string ip)
        {
            log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, testSocket.ToString());
            log.Debug("Connecting to LV Server");
            m_CoreToolkit = new LVClient();
            m_sIP = ip;

            m_exit = false;
            m_initialized = false;
            m_ReadBuffer = string.Empty;
        }

        ~NetLogger()
        {
            StopReadNetLog();

            m_CoreToolkit.UnregisterChannel();
            m_CoreToolkit = null;
            // delete the thread also?
        }

        public void Initialize() // use a bool?
        {
            if (m_initialized == true) return;

            threadCreated = new ManualResetEvent(false);
            t = new System.Threading.Thread(new System.Threading.ThreadStart(this.ReadDiagPort));
            //t = new System.Threading.Thread(ReadDiagPort);
            t.Start();
            threadCreated.WaitOne();
            
            if (t.IsAlive)
            {
                m_initialized = true;
                log.Info("Net log initialized.");
            }
            else
            {
                log.Error("Net log not initialized!");
            }
        }

        public void StopReadNetLog()
        {
            if (m_initialized)
            {
                log.DebugFormat("Stopping the thread:{0}...", t.ManagedThreadId);
                m_exit = true;
                try
                {
                    m_udpClient.Close();
                }
                catch
                {
                    log.Debug("UdpClient already closed...");
                }
                m_initialized = false;
                //t.Join();
            }
        }

        private void read()
        {
            if (m_initialized)
                m_exit = false;

            log.Debug("Initializing UdpClient...");
            int retries = 0;
            while (!m_exit)
            {
                try
                {
                    IPAddress remoteIP = IPAddress.Parse(m_sIP);
                    m_ipEnd = new IPEndPoint(remoteIP, 19997);
                    m_udpClient = new UdpClient(m_ipEnd);
                    if (retries > 0)
                        log.DebugFormat("UdpClient active after {0} retries!", retries);
                    else
                        log.Debug("UdpClient active!");
                    break;
                }
                catch (Exception ex)
                {
                    if (retries == 0)
                    {
                        log.DebugFormat("Failed to initialize UdpClient (Exception: {0}).", ex.Message);
                        System.Threading.Thread.Sleep(1000);
                        log.Debug("Retrying until network interface online...");
                    }
                    else
                        System.Threading.Thread.Sleep(1000); // Only print first failed connection...

                    retries++;
                }
            }

            if (m_exit)
                return;

            log.Debug("Starting to read...");
            string strReadBuffer = string.Empty;
            byte[] bBuf;

            while (!m_exit)
            {
                try
                {
                    bBuf = m_udpClient.Receive(ref m_ipEnd);
                }
                catch (Exception e)
                {
                    if (m_exit != true) log.Debug("Exception: " + e.Message);
                    break;
                }
                if (bBuf.Length > 12)
                {
                    strReadBuffer = System.Text.Encoding.ASCII.GetString(bBuf).Substring(12).Replace("\0", "");
                    log.Debug(t.ManagedThreadId + ": " + strReadBuffer);
                }
            }
            log.Debug("Done reading!");
        }

        // redundant?
        private void ReadDiagPort()
        {
            threadCreated.Set();
            this.read();
        }
    }
}