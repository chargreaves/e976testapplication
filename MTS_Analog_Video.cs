﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //=====================================================================================================================
        public void CVBS_SetupColourBarTestPattern(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[2];

            //set result to fail
            result[0] = -1;
            result[1] = -1;

            PrintTestName(seqContext.Step.Name);

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            command = "gpioutil w:AON_GPIO:23:1";
            response = "Setting AON_GPIO:23 to: 1";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
            }
            else
            {
                //fail
                reportText = "Failed to enable phono audio output";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            command = "B_REFSW_BOXMODE=2 av_test -r -f /shared/streams/Auto_Analysis_Sat.mpg";
            //command = "av_test -r -f /shared/streams/Auto_Analysis_Sat.mpg";
            response = "STARTING STREAM";
            status = SendReceive(command, response, 3000, out strReadBuffer);
            //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result[1] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to start stream";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            //Route CVBS In
            ScartCard1.AVRouting(ScartCard1.C0868ACARD_ROUTE_CVBS_IN, ScartCard1.C0868ACARD_PAL);

            //pause 500ms to let video waveform settle
            System.Threading.Thread.Sleep(500);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

        //=====================================================================================================================
        public void YPbPr_SetupColourBarTestPattern(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[2];

            //set result to fail
            result[0] = -1;
            result[1] = -1;

            PrintTestName(seqContext.Step.Name);

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            command = "av_test -r -f /shared/streams/Auto_Analysis_Sat.mpg";
            response = "STARTING STREAM";
            status = SendReceive(command, response, 3000, out strReadBuffer);
            //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //set color depth
                command = "hd_colordepth 8";
                response = "Color depth: 8";
                //response = "";
                status = SendReceive(command, response, 3000, out strReadBuffer);
                //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set color depth";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //set resolution
                command = "hd_format 576p50";
                response = "Using HD_Mode 576p50";
                //response = "";
                status = SendReceive(command, response, 3000, out strReadBuffer);
                //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set resolution";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //pass
                result[1] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to start stream";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

            //Route YPbPr In to Scart Out
            //AVLoopthroughCard.RL306(AVLoopthroughCard.C1452ACARD_RELAY_ON);
            //AVLoopthroughCard.RL3078(AVLoopthroughCard.C1452ACARD_RELAY_ON);
            //AVLoopthroughCard.RL304(AVLoopthroughCard.C1452ACARD_RELAY_ON); //Left audio
            //AVLoopthroughCard.RL305(AVLoopthroughCard.C1452ACARD_RELAY_ON); //Right audio

            //Route YPbPr In
            ScartCard2.AVRouting(ScartCard2.C0868ACARD_ROUTE_RGB_IN, ScartCard2.C0868ACARD_PAL);

            //pause 500ms to let video waveform settle
            System.Threading.Thread.Sleep(500);

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

        //=====================================================================================================================
        public void VIDEO_AnalyseCVBS0(SequenceContext seqContext, out double[] result, out string reportText)
        {
            //********************************************************************************
            //*
            //* Description: Analyse the centre set composite video signal
            //*    Composite > Blue fixture TV scart > 868(AvCard1) > 867 > PC
            //*
            //* Prerequisite:Requires Test 7500 Tuning peformed
            //*
            //* TT commands: "","",""
            //*
            //* Fail codes: 1 - Failed video analysis for CVBS1
            //*
            //********************************************************************************

            reportText = string.Empty;
            result = new double[2];

            result[1] = VideoAnalysis.TestComposite_Scart_AudMatrix1(ScartCard1); //do video analysis
            //check for fail
            if (result[1] != 0)
            {
                //fail
                reportText = VideoAnalysis.GetFailMessage();
                VideoAnalysis.ClearFailMessage();
            }

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

        //=====================================================================================================================
        public void VID_013_00_00_CVBS(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[3];

            int LineNumber = 130; //video line number to analyze

            PrintTestName(seqContext.Step.Name);

            result = VideoAnalysis.TestComposite_Scart_AudMatrix(ScartCard1, LineNumber); //do video analysis

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

        //=====================================================================================================================
        public void VID_014_00_00_YPbPr(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[8];

            int LineNumber = 130; //video line number to analyze

            PrintTestName(seqContext.Step.Name);

            result = VideoAnalysis.TestYPbPr_Scart_AudMatrix(ScartCard2, LineNumber); //do video analysis

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

        //=====================================================================================================================
        public void CreateTraceLimitsFileCVBS(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            string SeqFilePath;
            string SeqFolder;

            int LineNumber = 130; //video line number to analyze

            PrintTestName(seqContext.Step.Name);

            SeqFilePath = seqContext.SequenceFile.Path; //get path of .seq file

            SeqFolder = SeqFilePath.Substring(0, SeqFilePath.LastIndexOf("\\")); //extact folder containing .seq file

            //clear list of limit lines
            VideoAnalysis.ResetLimitsToSave();

            //**** CREATE COMPOSITE LIMITS ****

            //video sample size
            // TraceTools.SetSampleSize 1350

            //Composite for TV Scart - COMMANDS ALREADY SENT TO SET-UP UNIT INTO COMPOSITE
            VideoAnalysis.CreateCompositeLimits_Scart_AudMatrix(ScartCard1, true, LineNumber);

            //save the file to a temp .csv file - rename it to use it.
            VideoAnalysis.SaveLimitFile(SeqFolder + "\\" + "AutoOutCVBS.csv", TraceTools.CTRACETOOLS_SAVEMODENEWFILE);

            measurement = true;
            reportText = "";

        }

        //=====================================================================================================================
        public void CreateTraceLimitsFileYPbPr(SequenceContext seqContext, out bool measurement, out string reportText)
        {
            string SeqFilePath;
            string SeqFolder;

            int LineNumber = 130; //video line number to analyze

            PrintTestName(seqContext.Step.Name);

            SeqFilePath = seqContext.SequenceFile.Path; //get path of .seq file

            SeqFolder = SeqFilePath.Substring(0, SeqFilePath.LastIndexOf("\\")); //extact folder containing .seq file

            //clear list of limit lines
            VideoAnalysis.ResetLimitsToSave();

            //**** CREATE YPbPr LIMITS ****

            //video sample size
            // TraceTools.SetSampleSize 1350

            //Composite for TV Scart - COMMANDS ALREADY SENT TO SET-UP UNIT INTO COMPOSITE
            VideoAnalysis.CreateYPbPrLimits_Scart_AudMatrix(ScartCard2, true, LineNumber);

            //save the file to a temp .csv file - rename it to use it.
            VideoAnalysis.SaveLimitFile(SeqFolder + "\\" + "AutoOutYPbPr.csv", TraceTools.CTRACETOOLS_SAVEMODENEWFILE);

            measurement = true;
            reportText = "";

        }

        //=====================================================================================================================
        public void VID_504_00_A1_HDMI_UUT_Play_Stream(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1;

            PrintTestName(seqContext.Step.Name);

            command = "pbist mpeg -f /shared/streams/BeePNG_2m30s_ch26.ts";
            response = "STARTING STREAM";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //set color depth
                command = "hd_colordepth 8";
                //response = "";
                status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set color depth";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //set resolution
                command = "hd_format 576p50";
                //response = "";
                status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set resolution";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to start stream";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void VID_500_00_A1_CVBS_UUT_Play_Stream(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1;

            PrintTestName(seqContext.Step.Name);

            command = "av_test -r -f /shared/streams/Auto_Analysis_Sat.mpg";
            response = "STARTING STREAM";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //set color depth
                command = "hd_colordepth 8";
                //response = "";
                status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set color depth";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //set resolution
                command = "hd_format 576p50";
                //response = "";
                status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set resolution";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                reportText = "Failed to start stream";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void CVBS_UUT_Play_Stream(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -1;

            PrintTestName(seqContext.Step.Name);

            command = "B_REFSW_BOXMODE=2 av_test -r -f /shared/streams/Auto_Analysis_Sat.mpg";
            //command = "B_REFSW_BOXMODE=2 av_test -r -f http://192.168.0.200/MTP/bin/nfsshare/shared/streams/Auto_Analysis_Sat.mpg";
            //command = "av_test -r -f /shared/streams/Auto_Analysis_Sat.mpg";
            response = "STARTING STREAM";
            status = SendReceive(command, response, 3000, out strReadBuffer);
            //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //set color depth
                command = "hd_colordepth 8";
                response = "Color depth: 8";
                //response = "";
                status = SendReceive(command, response, 3000, out strReadBuffer);
                //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set color depth";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //set resolution
                command = "hd_format 576p50";
                response = "Using HD_Mode 576p50";
                //response = "";
                status = SendReceive(command, response, 3000, out strReadBuffer);
                //status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                //if (strReadBuffer.Contains(response))
                //{
                //    //pass
                //}
                //else
                //{
                //    //fail
                //    reportText = "Failed to set resolution";
                //    log.Debug(reportText);
                //    //reportText += command + ", Received: " + strReadBuffer;
                //    return;
                //}

                //pass
                result = 0;
            }
            else
            {
                //fail
                reportText = "Failed to start stream";
                log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                return;
            }

        }

        //=====================================================================================================================
        public void UUT_Stop_Stream(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1;

            PrintTestName(seqContext.Step.Name);

            //stop stream by entering a CR
            command = "\r";
            //response = "AV_TEST EXIT OK";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            //if (strReadBuffer.Contains(response))
            //{
                //pass
                result[0] = 0;
            //}
            //else
            //{
                //fail
                //reportText = "Failed to stop stream";
                //log.Debug(reportText);
                //reportText += command + ", Received: " + strReadBuffer;
                //return;
            //}

        }

        //=====================================================================================================================
        public void CVBS_UUT_Stop_Stream(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -1;

            PrintTestName(seqContext.Step.Name);

            //stop stream by entering a CR
            command = "\r";
            //response = "AV_TEST EXIT OK";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            //if (strReadBuffer.Contains(response))
            //{
            //pass
            result = 0;
            //}
            //else
            //{
            //fail
            //reportText = "Failed to stop stream";
            //log.Debug(reportText);
            //reportText += command + ", Received: " + strReadBuffer;
            //return;
            //}

        }

        //=====================================================================================================================

    }

}
