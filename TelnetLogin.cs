using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Windows.Forms;
//using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

//using CoreToolkit;
using CommInterface;
//using Logging;
//using TestParameters;
//using LV_Client;
//using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void TelnetLogin(SequenceContext seqContext, out double result, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            int status = -1;
            result = -99;

            try
            {
                int iCheckMtcVersion;
                testParameter.getParameterAsInt(seqContext.Step.Name, "CHECK_MTC_VERSION", out iCheckMtcVersion, 0);

                int iSleepBeforeOpen;
                testParameter.getParameterAsInt(seqContext.Step.Name, "SLEEP_BEFORE_OPEN", out iSleepBeforeOpen, 50);

                int iSleepBeforeOpenRetry;
                testParameter.getParameterAsInt(seqContext.Step.Name, "SLEEP_BEFORE_OPEN_RETRY", out iSleepBeforeOpenRetry, 1000);

                if (uut != null)
                {
                    status = uut.open();
                    if (status != 0)
                    {
                        reportText = "6:Failed to open uut, status: " + status;
                        result = -6;
                        return;
                    }

                    status = uut.clear();
                    if (status != 0)
                    {
                        reportText = "3:Failed to clear uut, status: " + status;
                        result = -3;
                        return;
                    }
                }
                else
                {
                    string uutEquip;
                    testParameter.getParameterAsString(seqContext.Step.Name, "TELNET_EQUIPMENT", out uutEquip, "UUT_COMM");
                    uut = (ICommInterface)m_CoreToolkit.getEquipmentObject(uutEquip, m_iUutInstance.ToString());

                    if (uut != null)
                    {
                        log.Debug("Before open sleep (ms): " + iSleepBeforeOpen);
                        System.Threading.Thread.Sleep(iSleepBeforeOpen);

                        status = uut.open();

                        if (status != 0)
                        {
                            log.Debug("UUT Open Failed, retry after sleep (ms): " + iSleepBeforeOpenRetry);
                            System.Threading.Thread.Sleep(iSleepBeforeOpenRetry);

                            status = uut.open();
                            if (status != 0)
                            {
                                reportText = "4:Failed to open uut, status: " + status;
                                log.Debug(reportText);
                                result = -4;
                                return;
                            }
                        }
                    }
                    else
                    {
                        reportText = "5:Failed to initiate uut";
                        log.Debug(reportText);
                        result = -5;
                        return;
                    }
                }

                if (iCheckMtcVersion == 1)
                {
                    string command = "version_info";
                    string strReadBuffer = string.Empty;
                    status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                    if (status != 0)
                    {
                        reportText = "1:Failed to check version: ";
                        log.Debug(reportText);
                        reportText += command + ", Received: " + strReadBuffer;
                        result = -1;
                        return;
                    }

                    string strToLookFor = "VERSION_INFO:";
                    if (!strReadBuffer.Contains(strToLookFor))
                    {
                        reportText = "2:Failed to read: " + strToLookFor;
                        log.Debug(reportText);
                        reportText += ", Received: " + strReadBuffer;
                        result = -2;
                        return;
                    }

                    int start = strReadBuffer.IndexOf(strToLookFor);
                    int end = strReadBuffer.IndexOf("\r\n", start);

                    string strCheck = strReadBuffer.Substring(start, end - start).Trim();
                    reportText = strCheck;
                }
                                             
                result = status;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Debug(reportText);
            }
        }

        public void TelnetLogout(SequenceContext seqContext, out double result, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            int status = -1;
            result = -99;

            try
            {
                if (uut != null)
                    status += uut.close();

                result = 0; // status;

            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Debug(reportText);
            }

        }

        public void TelnetLogin_TS(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            int status = -1;
            result = false;

            try
            {
                if (uut_ts != null)
                {
                    status = uut_ts.clear();
                    if (status != 0)
                    {
                        reportText = "1:Failed to clear uut, status: " + status;
                        return;
                    }
                }
                else
                {
                    uut_ts = (ICommInterface)m_CoreToolkit.getEquipmentObject("UUT_SESSION_TS", m_iUutInstance.ToString());

                    if (uut_ts != null)
                    {
                        status = uut_ts.open();
                        if (status != 0)
                        {
                            reportText = "2:Failed to open uut, status: " + status;
                            return;
                        }
                    }
                }

                result = (status == 0 ? true : false);
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Debug(reportText);
                result = false;
            }

        }

        public void TelnetLogin_USQUID(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            int status = -1;
            result = false;

            try
            {
                if (usquid != null)
                {
                    status = usquid.clear();
                    if (status != 0)
                    {
                        reportText = "1:Failed to clear usquid, status: " + status;
                        return;
                    }
                }
                else
                {
                    usquid = (ICommInterface)m_CoreToolkit.getEquipmentObject("USQUID", m_iUutInstance.ToString());

                    if (usquid != null)
                    {
                        status = usquid.open();
                        if (status != 0)
                        {
                            reportText = "2:Failed to open usquid, status: " + status;
                            return;
                        }
                    }
                }

                result = (status == 0 ? true : false);
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Debug(reportText);
                result = false;
            }

        }

    }

}
