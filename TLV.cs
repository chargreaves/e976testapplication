namespace E976TestApplication
{
    public partial class TestApp
    {
        public class TLV :  TLVType
        {
        };

        public class TLVType
        {
            public enum Type
            {
                END_TAG = 0x0000,                          //TLV End Tag
                MODEL_ID = 0x0001,                          //TLV Model ID
                HARDWARE_REVISION = 0x0002,                    //TLV Hardware Revision #
                SERIAL_NUMBER = 0x0003,                      //TLV Serial #
                HPNA_MAC_ADDRESS_1 = 0x0004,                //TLV HPNA Mac Address
                MOTOROLA_OEM_IPTV_CERTIFICATE = 0x0005,          //TLV MOTOROLA_OEM_IPTV_CERTIFICATE
                MICROSOFT_CONTENT_PROTECTION_CERTIFICATE = 0x0006, //TLV Microsoft content protection certificate
                MICROSOFT_AUTHENTICATION_CERTIFICATE = 0x0007,     //TLV Microsoft authenthication certificate
                ETHERNET_MAC_ADDRESS_1 = 0x0008,                //TLV Ethernet MAC adddress 1
                ETHERNET_MAC_ADDRESS_2 = 0x0009,                //TLV Ethernet MAC adddress 2
                ETHERNET_MAC_ADDRESS_3 = 0x000A,                //TLV Ethernet MAC adddress 3
                GUID_1 = 0x000B,                           //TLV GUID 1
                GUID_2 = 0x000C,                           //TLV GUID 2
                GUID_3 = 0x000D,                           //TLV GUID 3
                MOTOROLA_UNIQUE_ADDRESS_1 = 0x000E,            //TLV Motorola Unique Address 1
                MOTOROLA_UNIQUE_ADDRESS_2 = 0x000F,             //TLV Motorola Unique Address 2
                MOTOROLA_UNIQUE_ADDRESS_3 = 0x0010,             //TLV Motorola Unique Address 3
                MOTOROLA_UNIQUE_ADDRESS_HDCP = 0x0011,        //TLV Motorola Unique Address HDCP
                HDCP_KSV_CONFIG_DATA = 0x0012,                //TLV HDCP Key Selection Vector
                START_TAG_DATA = 0x0013,                      //TLV START TAG TLV
                DISCOVERY_SERVER_URL = 0x0014,                //TLV Discovery Server URL
                SENTIVISION_CERTIFICATE = 0x0015,             //TLV Sentivision Certificate
                BLUETOOTH_MAC_ADDRESS = 0x0016,                //TLV Bluetooth MAC Address
                MOTOROLA_BROADBAND_CERTIFICATE = 0x0017,      //TLV MOTOROLA_BROADBAND_CERTIFICATE
                VIDEO_FORMAT_CAPABILITY = 0x0018,             //TLV VIDEO_FORMAT_CAPABILITY Bootrom
                VIDEO_FORMAT_DEFAULT = 0x0019,                //TLV VIDEO_FORMAT_DEFAULT Bootrom
                CUSTOMER_ID_IR_RCU = 0x001A,              //TLV IR remote control code
                CUSTOMER_ID_RF_RCU = 0x001B,              //TLV RF remote control code
                JANUS_DEVICE_CERTIFICATE_TEMPLATE = 0x001C,   //TLV Janus device certificate template
                IR_PROTOCOL = 0x001D,   // ########### TLV IR protocols ######### 
                LANGUAGE_ID = 0x001E,
                BOOT_LOADER_MESSAGE = 0x001F,
                CUSTOMER_ID = 0x0020,
                //VIP 1830 specific TLV's ************************
                CUSTOM_SERIAL_NUMBER = 0x0021,
                UNIT_DATA = 0x0022,
                TMDB = 0x0023,
                PRODUCTION_PARAMETER = 0x0024,
                VENDOR_SPECIFIC_CODE = 0x0025,
                GLOBAL_ENCRYPTION_KEY = 0X0027,
                MARLIN_DRM_ID = 0x0028,

                //VIP 800 specific TLV's ***************************
                MASTER_KEY = 0x0026,

                //Broadcom specific TLV 

                BCM_MS_RSA_STORAGE = 0x0029,
                BCM_MOTO_DATA = 0x002A,
                BCM_HDCP_DATA = 0x002B,
                BCM_MOTO_SUB_CA_CERT = 0x002C,
                CRC_SHA1_TLV_LIST = 0x002D,
                BCM_HW_CFG_STR = 0x0033,
                FG_PART_NUMBER = 0x0034,
                BCM_DIAG_BOOT_OFFSET = 0x0035,
                EXT_NET_DEVICE_TYPE = 0x0036,      //TLV 54
                BCM_POST = 0x0037,      //TLV 55 Power On Self Test
                CONFIGURATION_PARAMETER = 0x0038,
                //0x0038 not used
                BCM_AES_DATA_FT = 0x0039,
                VERIMATRIX_ID = 0x003A,        //For VIP22n2 verimatrix
                VERIMATRIX_RSA_BLOCK = 0x003B,
                /* Janus Specific TLV's */
                JANUS_DEVICE_CERTIFICATE = 0x3C,
                JANUS_GROUP_PRIVATE_KEY = 0x3D,
                JANUS_MOTO_UNIQUE_ID = 0x3E,
                CASE_ID_SERIAL_NUMBER = 0x003F,

                //KreaTV specific TLV's ***************************
                MOTOPLYR_LICENSE = 0x0040, // Same as USP_TLV_DATA_TYPE_CUS_KEY_INDEX
                USP_TLV_DATA_TYPE_KEY_VAR_INDEX = 0x0041,
                USP_TLV_DATA_TYPE_PROC_IN_1 = 0x0042,
                USP_TLV_DATA_TYPE_PROC_IN_2 = 0x0043,
                BCM_MOTO_DATA_2048 = 0x0044,
                BCM_MOTO_SUB_CA_CERT_2048 = 0x0045,
                MOTOROLA_OEM_IPTV_CERTIFICATE_2048 = 0x0046,
                DTCP_IP_5C = 0x0047,
                OEM_APP_PARAMS = 0x0048,  //TLV72
                PLAYCAST_LICENSE = 0x0049,
                MPS_URL = 0x004A,
                KREATV_PRODUCTION_PARAMETER = 0x00B0,
                KREATV_CONFIGURATION_PARAMETER = 0x00B1,
                KREATV_MASTER_KEY = 0x00B2,
                KREATV_ENCRYPTION_KEY = 0x00B3,
                PCB_SERIAL_NUMBER = 0x00B4,
                PLAYREADY = 0x00B5,
                MOTO_CERT_KEY_ST231 = 0x00B6,
                GENERIC_RSA_V2 = 0x00B7,
                WIDEVINE_KEY = 0x00B8,
                AGILUS_CERT_KEY = 0x00B9,
                IRDETO_PRIVATE_DATA = 0x00BA,

                VIACCESS_SERIALNUMBER = 0x0100,
                FTPDB_PUBLIC_SERIALNUMBER = 0x4102,
                BCM_FTPDB_DATA = 0x4103,


                PKI_DATA = 0x4000,                      //TLV PKI DATA
                DH_PUBLIC_KEY_HDCP = 0x4001,               //TLV DH_PUBLIC_KEY_HDCP
                DH_PUBLIC_KEY_MICROSOFT = 0x4002,              //TLV DH_PUBLIC_KEY_MICROSOFT
                DH_PUBLIC_KEY_MOTOROLA = 0x4003,               //TLV DH_PUBLIC_KEY_MOTOROLA
                DATETIME = 0x4004,
                SERVER_TIME = 0x4005,                         //TLV Server Time
                PKI_DATA_HDCP = 0x4006,                       //TLV HDCP PKI Data
                PKI_DATA_MOTOROLA = 0x4007,                   //TLV Motorola PKI Data
                PKI_DATA_MICROSOFT = 0x4008,                  //TLV Microsoft PKI Data
                PROFILE_TABLE = 0x4009,                       //TLV Profile Tables
                PROFILE_NAME = 0x400a,                        //TLV Profile Name
                XOS_VERSION_NUMBER = 0x400b,                  //TLV XOS Version
                //CUSTOMER_ID = 0x400c,                         //TLV Customer ID
                ERROR_STATUS = 0x400D,
                CERTIFICATE = 0x400e,                         //TLV JANUS PKI
                PROFILE_DESCRIPTION = 0x400f,
                MAC_POOL_SIZE = 0x4010,
                MAC_POOL_USED = 0x4011,
                MAC_POOL_THRESHOLD = 0x4012,
                SECTOR_BIND_INFO = 0x4013,
                PKI_INFO = 0x4014,
                CHIP_TYPE = 0x4015,
                PKI_DATA_REQUEST = 0x4016,
                CST_LINE_ID = 0x4017,                   // TLV CST Line id  
                BOARD_SERIAL_NUMBER = 0x4018,
                XPU_SERIAL_NUMBER = 0x4019,              //XPU serial number of STB
                CAG_BUILD_INFO = 0x401A,
                CAG_VERSION = 0x401B,
                CAG_CHIPSET = 0x401C,
                FG_PART_NUMBER_SFIS = 0x401D,

                //Broadcom specific TLV's
                OTP_ID = 0x401E,      // Broadcom OTP setting
                BOOT_MEM_CHK = 0x401F,
                PUBKEY_INDX = 0x4020,
                VIDEOVERIFY_EN = 0x4021,
                AUDIOVERIFY_EN = 0x4022,
                RAVEVERIFY_EN = 0x4023,
                CRBIT0 = 0x4024,
                CRBIT1 = 0x4025,
                EPOCH = 0x4026,
                EXTD_BOOT_PROT = 0x4027,
                CA_DES_ECB = 0x4028,
                M2M_DES_ECB = 0x4029,
                EBICHIP = 0x402A,
                M2M_DES_CBC = 0x402B,
                EXT_KEY_LOAD = 0x402C,
                CRBIT0_7043 = 0x402D,
                CRBIT1_7043 = 0x402E,
                HW_CONFIG_STRING = 0x402F,

                // New TLV to log MS sub CA cert subject name  and subject Key ID

                MS_SUBCA_SUBJECT_NAME = 0x4030,
                MS_SUBCA_SUBJECT_KEY_ID = 0x4031,
                PLATFORM_ID = 0x4032,
                HDD_LOCK = 0x4034,
                FORMAT_ID = 0x4035,
                VENDOR_SPECIFIC_DATA = 0x4036,
                KREATV_HDCP_KEY = 0x4037,
                HPNA_PRODUCT = 0x4038,
                LICENSE_CONTROL_ID = 0x4039,
                MOTOPLYR_LICENSE_TEMPLATE = 0x403A,
                PLAYCAST_LICENSE_TEMPLATE = 0x403B,

                TEST_DATA_A = 0x5000,                            //TLV TEST_DATA_A test flash
                TEST_DATA_B = 0x5001,                            //TLV TEST_DATA_B test flash
                TEST_DATA_C = 0x5002,                            //TLV TEST_DATA_C test flash
                TEST_DATA_D = 0x5003,                             //TLV TEST_DATA_D test flash

                // #####Tlvs used for logging########
                // If a DEBUG TLV is received, it will have only the string sent by the other modules. 
                // This string is only written in the log file and NOT displayed on the CST screen. 
                LOG_DEBUG = 0x5004,

                // If a DATA TLV is received, CST reads the error code and matches the corresponding error string. 
                // This string is written in to the log file as well as displayed on the screen.
                // If any extra information needs to be sent, then tlv should contain 4 byte error code
                // followed by the extra information as string.     
                LOG_DATA = 0x5005,

                // if an �USERINPUT� tlv is received then CST reads the first four bytes (int) and
                // interprets as the user input identifier. Based on the value of the identifier 
                // appropriate message box is shown.  Eg 1-OKCANCEL, 2-RETRYCANCEL etc. 
                // Whatever string follows the input identifier will be displayed as such in the dialog box. 
                // This information and its response will also be stored in the log file. 
                // The user response is sent back to the requested module as response message.
                LOG_USERINPUT = 0x5006,

                // If a PROGRESS TLV is received, CST reads the color code and message string. 
                // This string is written in to the log file as well as displayed on the screen.
                LOG_PROGRESS  =  0x5007,
                
                MUA_ST231 = 0x5008,
                SKM_PUBLIC_ID = 0x5009,
                MUA_RSA_V2 = 0x5010,
                MUA_AGILUS = 0x5011,

            };// end    public enum Type
        }//end   public class TLVType
    }
}
