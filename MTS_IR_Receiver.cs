﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using NationalInstruments.TestStand.Interop.API;
using IRSimulator;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private IIRSimulator m_IrDevice;

        //=====================================================================================================================
        public void IR_Receiver_RedRat(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            string response2;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -99; //error

            PrintTestName(seqContext.Step.Name);

            string sIrDevice = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_DEVICE", out sIrDevice, "IR_DEVICE_36");

            string sIrButton = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_BUTTON", out sIrButton, "OK");

            string sIrResponse = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_RESPONSE", out sIrResponse, "protocol: rc6_6a_omni");

            try
            {
                m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject(sIrDevice, m_iUutInstance.ToString());
                //m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject("IR_DEVICE_36", m_iUutInstance.ToString());
                //m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject("IR_DEVICE_56", m_iUutInstance.ToString());
                if (m_IrDevice != null)
                {
                    command = "pbist ir 30";
                    response = sIrResponse; // "protocol: rc6_6a_omni";
                    //response = "protocol: rc6_6a_omni";
                    //response = "protocol: sejin";
                    response2 = "OK KEY 86";
                    status = SendReceive(command, "Press a key", 3000, out strReadBuffer);

                    m_IrDevice.SendIR(sIrButton, 0, out reportText);

                    strReadBuffer = string.Empty;
                    status = SendReceive("", response, 3000, out strReadBuffer);

                    if ((strReadBuffer.Contains(response)) && (strReadBuffer.Contains(response2)))
                    {
                        //pass
                        reportText = "IR device: " + sIrDevice + " " + "IR button: " + sIrButton + " received";
                        log.Debug(reportText);
                        result[0] = 0;
                    }
                    else
                    {
                        //fail
                        reportText = "Failed to receive IR button: " + sIrButton + " from IR device: " + sIrDevice;
                        log.Debug(reportText);
                        result[0] = -2;
                        //reportText += command + ", Received: " + strReadBuffer;
                        return;
                    }

                }
                else
                {
                    log.Error("IR_DEVICE is offline.");
                    result[0] = -1;
                }

            }
            catch (Exception ex)
            {
                log.Error(string.Format("{0} Exception: {1}", seqContext.Step.Name, ex.Message));
                result[0] = -88;
            }

        }

        //=====================================================================================================================
        public void IR_Receiver(SequenceContext seqContext, out double result, out string reportText)
        {
            string command;
            string response;
            string response2;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;

            //set result to fail
            result = -99; //error

            PrintTestName(seqContext.Step.Name);

            string sIrDevice = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_DEVICE", out sIrDevice, "IR_DEVICE_36");

            string sIrButton = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_BUTTON", out sIrButton, "OK");

            string sIrResponse = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_RESPONSE", out sIrResponse, "protocol: rc6_6a_omni");

            try
            {
                m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject(sIrDevice, m_iUutInstance.ToString());
                //m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject("IR_DEVICE_36", m_iUutInstance.ToString());
                //m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject("IR_DEVICE_56", m_iUutInstance.ToString());
                if (m_IrDevice != null)
                {
                    command = "pbist ir 30";
                    response = sIrResponse; // "protocol: rc6_6a_omni";
                    //response = "protocol: rc6_6a_omni";
                    //response = "protocol: sejin";
                    response2 = "OK KEY 86";
                    status = SendReceive(command, "Press a key", 3000, out strReadBuffer);

                    m_IrDevice.SendIR(sIrButton, 0, out reportText);

                    strReadBuffer = string.Empty;
                    status = SendReceive("", response, 3000, out strReadBuffer);

                    if ((strReadBuffer.Contains(response)) && (strReadBuffer.Contains(response2)))
                    {
                        //pass
                        reportText = "IR device: " + sIrDevice + " " + "IR button: " + sIrButton + " received";
                        log.Debug(reportText);
                        result = 0;
                    }
                    else
                    {
                        //fail
                        reportText = "Failed to receive IR button: " + sIrButton + " from IR device: " + sIrDevice;
                        log.Debug(reportText);
                        result = -2;
                        //reportText += command + ", Received: " + strReadBuffer;
                        return;
                    }

                }
                else
                {
                    log.Error("IR_DEVICE is offline.");
                    result = -1;
                }

            }
            catch (Exception ex)
            {
                log.Error(string.Format("{0} Exception: {1}", seqContext.Step.Name, ex.Message));
                result = -88;
            }

        }

    }

}
