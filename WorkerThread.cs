using System;
using System.Text;
using System.Threading;
using System.IO;

namespace E976TestApplication
{
    class worker
    {
        public static AutoResetEvent resetEvent = new AutoResetEvent(false);

        public string sShellName = "";
        public string sParam0 = string.Empty;//arg0
        public string sParam1 = string.Empty;//arg1
        public string sArgv2 = string.Empty;
        public int iTimeout = 60000;

        public void workerThread()
        {
            // Initialize the process and its StartInfo properties.
            System.Diagnostics.Process cmdProcess;
            cmdProcess = new System.Diagnostics.Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";

            cmdProcess.StartInfo.Arguments = "/k " + sShellName + " " + sParam0 + " " + sParam1 + " " + sArgv2;

            cmdProcess.StartInfo.CreateNoWindow = true;

            // Set UseShellExecute to false for redirection.
            cmdProcess.StartInfo.UseShellExecute = false;// true will crash MTP

            // Redirect the standard output of the sort command.  
            // This stream is read asynchronously using an event handler.
            cmdProcess.StartInfo.RedirectStandardOutput = true;

            // Set our event handler to asynchronously read the sort output.
            cmdProcess.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(OutputHandler);

            // Redirect standard input as well.  This stream
            // is used synchronously.
            cmdProcess.StartInfo.RedirectStandardInput = true;



            // Start the process.
            cmdProcess.Start();

            // Use a stream writer to synchronously write the sort input.
            StreamWriter sortStreamWriter = cmdProcess.StandardInput;

            // Start the asynchronous read of the sort output stream.
            cmdProcess.BeginOutputReadLine();

            // End the input stream to the sort command.
            sortStreamWriter.Close();

            // Wait for the sort process to write the sorted text lines.
            cmdProcess.WaitForExit(iTimeout);


            cmdProcess.Close();

            resetEvent.Set();//set an event for thread to run
        }

        public static StringBuilder returnData = new StringBuilder("");

        private void OutputHandler(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
        {
            // Collect the sort command output.
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                // Add the text to the collected output.
                returnData.Append(Environment.NewLine + outLine.Data);
            }
        }
    }//end of Class worker
}