using System;
using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;
using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private class IniFile
        {
            public string path;

            [DllImport("kernel32")]
            private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

            [DllImport("kernel32")]
            private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

            [DllImport("kernel32")]
            private static extern int GetPrivateProfileInt(string section, string key, int def, string filePath);

            public IniFile(string INIPath)
            {
                path = INIPath;
            }

            public void WriteIniValue(string Section, string Key, string Value)
            {
                WritePrivateProfileString(Section, Key, Value, this.path);
            }

            public string ReadIniValue(string Section, string Key)
            {
                StringBuilder temp = new StringBuilder(255);
                int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
                return temp.ToString();
            }

            public int ReadIniValueInt(string Section, string Key)
            {
                int i = ReadIniValueInt(Section, Key, 0);
                return i;
            }

            public int ReadIniValueInt(string Section, string Key, int defaultValue)
            {
                int i = GetPrivateProfileInt(Section, Key, defaultValue, this.path);
                return i;
            }
        }

    }
}