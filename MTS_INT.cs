﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NationalInstruments.TestStand.Interop.API;
using PFTSLibrary;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public static PFTSLibrary.CPFTSRack PFTS = new CPFTSRack();

        //include ICards
        public static PFTSLibrary.C0867ACard AcquisitionCard = new C0867ACard();
        public static PFTSLibrary.C0868ACard ScartCard1 = new C0868ACard();
        public static PFTSLibrary.C0868ACard ScartCard2 = new C0868ACard();
        public static PFTSLibrary.C0891ACard MainsCard = new C0891ACard();
        //public static PFTSLibrary.C1452ACard AVLoopthroughCard = new C1452ACard();

        //include ISubCards
        public static PFTSLibrary.C0890ACard FixtureCard = new C0890ACard();
        public static PFTSLibrary.C0992ACard AMB = new C0992ACard();
        public static PFTSLibrary.C1060ACard DMB = new C1060ACard();
        public static PFTSLibrary.C1238ACard ComSat = new C1238ACard();

        //include IClass
        public static PFTSLibrary.CTraceTools TraceTools = new CTraceTools();
        public static PFTSLibrary.C9812 PCI9812 = new C9812();
        public static PFTSLibrary.CVideoAnalysis VideoAnalysis = new CVideoAnalysis();
        public static PFTSLibrary.CAudioAnalysis AudioAnalysis = new CAudioAnalysis();
        public static PFTSLibrary.CFixture Fixture = new CFixture();

        //public static PFTSLibrary.C1398ACard USBCard = new C1398ACard();
        //public static PFTSLibrary.C1037CCard SPDIFCard = new C1037CCard();
        //public static PFTSLibrary.C2042ACard VideoAudioMUX = new C2042ACard();

        //=====================================================================================================================
        public bool PFTS_Check_Hardware_FAT(SequenceContext seqContext)
        {
            PrintTestName(seqContext.Step.Name);

            //public static 
            //PFTSLibrary.CPFTSRack PFTS = new CPFTSRack();

            PFTS.CreatePFTS();
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(AcquisitionCard, "AcquisitionCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(ScartCard1, "ScartCard1");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(MainsCard, "MainsCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);
            //log.Debug("MainsCard Hardware Revision - " + MainsCard.GetHardwareRevision());
            //log.Debug("MainsCard EEPROM Record - " + MainsCard.GetEEPROMRecord("CardDesc"));

            //PFTS.AddCard(USBCard, "USBCard");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);
            //log.Debug("USBCard Hardware Revision - " + USBCard.GetHardwareRevision());
            //log.Debug("USBCard EEPROM Record - " + USBCard.GetEEPROMRecord("CardDesc"));

            //PFTS.AddCard(SPDIFCard, "SPDIF");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);

            //PFTS.AddCard(FixtureCard, "FixtureCard");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);
            //log.Debug("FixtureCard Hardware Revision - " + FixtureCard.GetHardwareRevision());
            //log.Debug("FixtureCard EEPROM Record - " + FixtureCard.GetEEPROMRecord("CardDesc"));

            //FixtureCard.AddCard(VideoAudioMUX, "VideoAudioMUX");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);

            PFTS.AddClass(TraceTools, "TraceTools");
            TraceTools.AddTraceFile(seqContext, "E976.trc");
            TraceTools.AddTraceFile(seqContext, "E976.lim");
            TraceTools.SetSampleSize(1350);

            PFTS.AddClass(PCI9812, "PCI-9812");

            PFTS.AddClass(VideoAnalysis, "VideoAnalysis");
            VideoAnalysis.SetSystemObjects(TraceTools, PCI9812, AcquisitionCard);
            VideoAnalysis.SetTestRepeatCount(1);

            PFTS.AddClass(AudioAnalysis, "AudioAnalysis");
            AudioAnalysis.SetSystemObjects(TraceTools, PCI9812, AcquisitionCard);
            AudioAnalysis.SetTestRepeatCount(1);

            PFTS.Initialize(); //this calls Initialize() of each card
            if (PFTS.ErrorOccurred()) return false;

            //VideoAudioMUX.Route_Left_Audio_Output();
            //VideoAudioMUX.Route_Audio_Output_Off();
            //VideoAudioMUX.Route_SK124_Output(1, 1);
            //VideoAudioMUX.Route_OUT51(1, 1);
            //VideoAudioMUX.Route_OUT71(1, 1);
            //VideoAudioMUX.Route_OUT54(1, 1);

            return true;

        }

        //=====================================================================================================================
        public bool PFTS_Check_Hardware_DEV(SequenceContext seqContext)
        {
            PFTS.CreatePFTS();
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(AcquisitionCard, "AcquisitionCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(ScartCard1, "ScartCard1");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(ScartCard2, "ScartCard2");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(MainsCard, "MainsCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(FixtureCard, "FixtureCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            FixtureCard.AddCard(AMB, "AMB");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            FixtureCard.AddCard(DMB, "DMB");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            FixtureCard.AddCard(ComSat, "ComSat");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddClass(Fixture, "Fixture");
            Fixture.SetSystemObjects(DMB, AMB);

            PFTS.Initialize(); //this calls Initialize() of each card
            if (PFTS.ErrorOccurred()) return false;

            return true;

        }

        //=====================================================================================================================
        public bool PFTS_Check_Hardware_PCB(SequenceContext seqContext)
        {
            PFTS.CreatePFTS();
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(AcquisitionCard, "AcquisitionCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(ScartCard1, "ScartCard1");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(ScartCard2, "ScartCard2");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(MainsCard, "MainsCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddCard(FixtureCard, "FixtureCard");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            FixtureCard.AddCard(AMB, "AMB");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            FixtureCard.AddCard(DMB, "DMB");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            FixtureCard.AddCard(ComSat, "ComSat");
            if (PFTS.ErrorOccurred()) return false;
            System.Threading.Thread.Sleep(500);

            PFTS.AddClass(Fixture, "Fixture");
            Fixture.SetSystemObjects(DMB, AMB);

            //PFTS.AddCard(AVLoopthroughCard, "AVLoopthroughCard");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);

            //PFTS.AddCard(USBCard, "USBCard");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);

            //PFTS.AddCard(SPDIFCard, "SPDIF");
            //if (PFTS.ErrorOccurred()) return false;
            //System.Threading.Thread.Sleep(500);

            PFTS.AddClass(TraceTools, "TraceTools");
            TraceTools.AddTraceFile(seqContext, "E976.trc");
            //TraceTools.AddTraceFile(seqContext, "E976.lim");
            TraceTools.SetSampleSize(1350);

            PFTS.AddClass(PCI9812, "PCI-9812");

            PFTS.AddClass(VideoAnalysis, "VideoAnalysis");
            VideoAnalysis.SetSystemObjects(TraceTools, PCI9812, AcquisitionCard);
            VideoAnalysis.SetTestRepeatCount(1);

            PFTS.AddClass(AudioAnalysis, "AudioAnalysis");
            AudioAnalysis.SetSystemObjects(TraceTools, PCI9812, AcquisitionCard);
            AudioAnalysis.SetTestRepeatCount(1);

            PFTS.Initialize(); //this calls Initialize() of each card
            if (PFTS.ErrorOccurred()) return false;

            return true;

        }

        //=====================================================================================================================
        public bool PFTS_Reset_Hardware(SequenceContext seqContext)
        {
            PFTS.Reset();
            if (PFTS.ErrorOccurred()) return false;

            return true;

        }

        //=====================================================================================================================
        public void ReadFQ2Clock(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[2];

            result[1] = Fixture.ReadFQClock(33333333, 2);
            //result[1] = ROUND(FQ1Clock.GetReadFrequency(), 0)

            //error
            result[0] = Convert.ToDouble(Fixture.ErrorOccurred());
            reportText = Fixture.ErrorLog();
            Fixture.ClearErrorOccurred();
            Fixture.ClearErrorLog();

        }
           
        //=====================================================================================================================

    }

}
