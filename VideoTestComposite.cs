﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;
using CAM;
using VideoFrameAnalyzer;
//using HauppaugeWinTVCaptureImpl;

namespace E976TestApplication
{
    public partial class TestApp
    {

        //private ICoreToolkit m_localCoretoolkit; 

        public void VideoTestComposite(SequenceContext seqContext, out double[] measurements, out string reportText)
        {
            measurements = new double[8];
            measurements[0] = -99;//overall 
            measurements[1] = -99;//ErrorSum R
            measurements[2] = -99;//ErrorSum G
            measurements[3] = -99;//ErrorSum B
            measurements[4] = -99;//LargestDiff R
            measurements[5] = -99;//LargestDiff G
            measurements[6] = -99;//LargestDiff B

            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            int status = 0;
            int retries = 1;
            IVideoFrameAnalyzer videoCard = null;
            int switchDelay = 100;
            int captureDelay = 1000;
            int startDelay = 10;
            string szScuCmd; // SCU string command to set interface box connection
            string uutResponse = "";

            try
            {
                int iTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 3000);

                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCH_DELAY", out switchDelay, 100);
                testParameter.getParameterAsInt(seqContext.Step.Name, "CAPTURE_DELAY", out captureDelay, 1000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "START_DELAY", out startDelay, 1000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "RETRIES", out retries, 1);

                string strVideoStandard; // NTSC, PAL
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_STANDARD", out strVideoStandard, "NTSC");

                string strVideoOutput;//COMPOSITE, SVIDEO, COMPONENT, HDMI
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_OUTPUT", out strVideoOutput, "COMPOSITE");

                testParameter.getParameterAsString(seqContext.Step.Name, "SCU_CONNECTION", out szScuCmd, "Default");

                

                if (strVideoOutput == null)
                {
                    status = -99;
                    reportText = string.Format("you must define VIDEO_OUTPUT in you xmlTestParameter file");
                    log.Debug(reportText);
                }

                string strReadBuffer = string.Empty;

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.Unknown;

                // turn ON the proper port on Video Card
                if (status == 0)
                {
                    switch (strVideoOutput)
                    {
                        case "COMPOSITE":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "SVIDEO":
                            videoSource = IVideoFrameAnalyzer.VideoSource.SVIDEO;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_SVIDEO");
                            break;
                        case "COMPONENT":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPONENT;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPONENT");
                            break;
                        case "HDMI":
                            videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "HDMIInput":
                            videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMIInput");
                            break;
                        case "VGA":
                            videoSource = IVideoFrameAnalyzer.VideoSource.VGA;
                            videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_VGA");
                            break;
                    }

                    if (videoCard == null)
                    {
                        log.Error("videoCard is OFFLINE.");
                        return;
                    }

                    // ignore the returned value of SelectVideoFormat() for now
                    videoCard.SelectVideoFormat(videoSource);
                    log.DebugFormat("Wait {0} ms for Video Input to switch before capture", switchDelay);
                    System.Threading.Thread.Sleep(switchDelay);

                    IVideoFrameAnalyzer.AVVideoStandard VideoStandard = IVideoFrameAnalyzer.AVVideoStandard.PAL_M;
                    switch (strVideoStandard)
                    {
                        case "NTSC":
                            VideoStandard = IVideoFrameAnalyzer.AVVideoStandard.NTSC_M;
                            break;
                        case "PAL":
                            VideoStandard = IVideoFrameAnalyzer.AVVideoStandard.PAL_N;
                            break;
                    }

                    videoCard.SelectColorCoding(VideoStandard);

                    status = SendReceive("pbist picture", "WAITING_FOR_COMMAND", iTimeout, out uutResponse);

                    // Start Video
                    if (status == 0)
                    {
                        if(scu != null)
                            scu.Connect(szScuCmd);
                        
                        status = videoCard.StartVideo();
                        measurements[0] = status;
                        log.DebugFormat("Wait {0} ms for Video Input to start before capture", startDelay);
                        System.Threading.Thread.Sleep(startDelay);
                    }
                }

                // Capture Image
                if (status == 0)
                {
                    double LargestDiffR = -99;
                    double LargestDiffG = -99;
                    double LargestDiffB = -99;
                    double ErrorSumR = -99;
                    double ErrorSumG = -99;
                    double ErrorSumB = -99;
                    double tErrorSum = 9999;
                    bool f_marginal = true;
                    int i = 0;
                    while ((i <= retries) && (tErrorSum > 4500 || f_marginal))
                    {
                        if (0 != i)
                        {
                            System.Threading.Thread.Sleep(1000);
                            log.DebugFormat("Wait 1000ms for on fail retry video test - attempt {0}", ++i);
                        }
                        else
                        {
                            log.Debug("First try of video test.");
                            i++;
                        }
                        status = videoCard.CapturePicture("c:\\Temp\\AverTvBmpInMemory.bmp");
                        measurements[0] = status;

                        log.DebugFormat("Wait {0} ms for Capture before Compare", captureDelay);
                        System.Threading.Thread.Sleep(captureDelay);

                        // Compare Captured Image to master and record measurements
                        if (status == 0)
                        {
                            bool bAvg = false;
                            //if (strVideoOutput.Contains("HDMI"))
                            //{
                            LargestDiffR = 0;
                            LargestDiffG = 0;
                            LargestDiffB = 0;
                            //    status = videoCard.CaptureAndVerifyVideoHDMI(ref ErrorSumR, ref ErrorSumG, ref ErrorSumB);
                            //    log.DebugFormat("CaptureAndVerifyVideoHDMI return as {0},{1},{2}", ErrorSumR, ErrorSumG, ErrorSumB);
                            //}
                            //else
                            {
                                status = videoCard.CaptureAndVerifyVideo(bAvg, ref LargestDiffR, ref LargestDiffG, ref LargestDiffB, ref ErrorSumR, ref ErrorSumG, ref ErrorSumB);
                                f_marginal = (LargestDiffB > 35 && LargestDiffB <= 40) || (LargestDiffG > 35 && LargestDiffG <= 40) || (LargestDiffR > 35 && LargestDiffR <= 40);
                                log.DebugFormat("CaptureAndVerifyVideo return as {0},{1},{2},{3},{4},{5}", ErrorSumR, ErrorSumG, ErrorSumB, LargestDiffR, LargestDiffG, LargestDiffB);
                            }
                            if (0 == status)
                            {
                                tErrorSum = ErrorSumR + ErrorSumG + ErrorSumB;
                                log.DebugFormat("TotalErrorSum is {0}", tErrorSum);
                                break;
                            }
                        }
                    }

                    if (status == 0)//make sure we have valid measurements
                    {
                        measurements[0] = status;//overall status
                        measurements[1] = ErrorSumR;
                        measurements[2] = ErrorSumG;
                        measurements[3] = ErrorSumB;
                        measurements[4] = LargestDiffR;
                        measurements[5] = LargestDiffG;
                        measurements[6] = LargestDiffB;
                    }
                }

                if (status == 0)
                {
                    measurements[0] = 0;
                }
                if (scu != null)
                    scu.Disconnect(szScuCmd);
            }
            catch (Exception e)
            {
                reportText = e.Message;
                log.Debug(reportText);
            }
            finally
            {

                // make sure to Stop Video
                if (videoCard != null)
                {
                    log.DebugFormat("Wait {0} ms before StopVideo", switchDelay);
                    System.Threading.Thread.Sleep(switchDelay);

                    status = videoCard.StopVideo();

                    log.DebugFormat("Wait {0} ms after StopVideo", switchDelay);
                    System.Threading.Thread.Sleep(switchDelay);
                }
                status = SendReceive("quit", "/ #", 3000, out uutResponse);
            }
        }//VideoTest
        /*
        public void videoCheck(SequenceContext seqContext, out bool result, out string reportText)
        {
            reportText = string.Empty;
            result = true;

            PrintTestName(seqContext.Step.Name);

            try
            {
                string displayTest = string.Empty;
                string temp = string.Empty;
                strReadBuffer = string.Empty;
                string[] strCmd = null;
                char[] separator = { ',' };
                string szImagePath = string.Empty;

                testParameter.getParameterAsString(seqContext.Step.Name, "IMAGE_PATH", out szImagePath, "C:\\mtp\\Projects\\colorbar\\Images");
                testParameter.getParameterAsString(seqContext.Step.Name, "COMMAND", out temp, null);
                if (temp != null)
                    strCmd = temp.Split(separator, StringSplitOptions.None);

                testParameter.getParameterAsString(seqContext.Step.Name, "DISPLYTEST", out displayTest, null);
                if (displayTest == null)
                {
                    log.Debug("please assign display picture file name into sequence file.");
                    result = false;
                    return;
                }

                if (displayTest != null && displayTest.Length > 0)
                {
                    string uutImage = szImagePath + "\\";
                    string[] displays;
                    string szImageFile = string.Empty; 
                    if (displayTest.Contains(","))
                    {
                        displays = displayTest.Split(',');
                        szImageFile = displays[1];
                        uutImage += displays[1];
                    }
                    else
                    {
                        uutImage += displayTest;
                        szImageFile = displayTest;
                    }

                    // Display message to Operator
                    if (mpf == null)
                        mpf = new MessagePopupForm(2, "PASS", "", "FAIL", uutImage);
                    mpf.MgsPopuptextBox.ScrollBars = ScrollBars.None;
                    switch (szImageFile)
                    {
                        case "composite.bmp":
                            //testParameter.getParameterAsString(seqContext.Step.Name, "Message", out strMessage, "Please Switch to Composite, Check Video Output!\n請切換至電視端子,并确认电视是否正常输出\n影像不能有雪花,马赛克或不清楚状况!");
                            mpf.MgsPopuptextBox.Text = "Please Switch to Composite, Check Video Output!\r\n\r\n請切換至電視端子,并确认电视是否正常输出\n影像不能有雪花,马赛克或不清楚状况!";
                            break;
                        case "component.bmp":
                            //testParameter.getParameterAsString(seqContext.Step.Name, "Message", out strMessage, "Please Switch to Component, Check Video Output!\n請切換至色差端子,並確認電視是否正常輸出\n影像不能有雪花,馬賽克或不清楚狀況!");
                            mpf.MgsPopuptextBox.Text = "Please Switch to Component, Check Video Output!\r\n\r\n請切換至色差端子,并确认电视是否正常输出\n影像不能有雪花,马赛克或不清楚状况!";
                            break;
                        case "HDMI.bmp":
                            //testParameter.getParameterAsString(seqContext.Step.Name, "Message", out strMessage, "Please Switch to HDMI, Check Video Output!\n請切換至HDMI,并确认电视是否正常输出影像\n不能有雪花,马赛克或不清楚状况!");
                            mpf.MgsPopuptextBox.Text = "Please Switch to HDMI, Check Video Output!\r\n\r\n請切換至HDMI,并确认电视是否正常输出\n影像不能有雪花,马赛克或不清楚状况!";
                            break;
                        case "HDMIInput.bmp":
                            //testParameter.getParameterAsString(seqContext.Step.Name, "Message", out strMessage, "Please Switch to HDMI, Check Video Output!\n請切換至HDMI,并确认电视是否正常输出影像\n不能有雪花,马赛克或不清楚状况!");
                            mpf.MgsPopuptextBox.Text = "Please Switch to HDMI, Check Video Output!\r\n\r\n請切換至HDMI,并确认电视是否正常输出\n影像不能有雪花,马赛克或不清楚状况!";
                            break;
                    }
                    mpf.MgsPopuptextBox.TextAlign = HorizontalAlignment.Center;
                    mpf.ShowDialog();

                    if (mpf.result == DialogResult.Yes)
                        result = true;
                    else
                        result = false;

                    if (mpf != null)// must make sure we remvoe the image
                    {
                        mpf.Close();
                        mpf = null;
                    }
                }
            }
            catch (Exception e)
            {
                reportText = e.Message;
                log.Debug(reportText);
            }
        }

        public void StartVideo(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;
            reportText = "";
            PrintTestName(seqContext.Step.Name);

            int status = 0;
            int retries = 1;
            IVideoFrameAnalyzer videoCard = null;
            try
            {
                string strVideoOutput;//COMPOSITE, SVIDEO, COMPONENT, HDMI
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_OUTPUT", out strVideoOutput);
                if (strVideoOutput == null)
                {
                    status = -99;
                    reportText = string.Format("you must define VIDEO_OUTPUT in you xmlTestParameter file");
                    log.Debug(reportText);
                }

                string strReadBuffer = string.Empty;

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.Unknown;

                // turn ON the proper port on Video Card
                if (status == 0)
                {
                    switch (strVideoOutput)
                    {
                        case "COMPOSITE":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "SVIDEO":
                            videoSource = IVideoFrameAnalyzer.VideoSource.SVIDEO;
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_SVIDEO");
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "COMPONENT":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPONENT;
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPONENT");
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "HDMI":
                            videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "VGA":
                            videoSource = IVideoFrameAnalyzer.VideoSource.VGA;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_VGA");
                            break;
                    }

                    // Start Video
                    if (status == 0)
                    {
                        status = videoCard.StartVideo();
                        result = status;
                    }

                }
            }
            catch (Exception ex)
            {
                log.Debug("StartVideo Exception:\r\n", ex);
                result = -5;
            }


        }//StartVideo

        public void StopVideo(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;
            reportText = "";
            PrintTestName(seqContext.Step.Name);

            int status = 0;
            int retries = 1;
            IVideoFrameAnalyzer videoCard = null;
            try
            {
                int stopDelay;
                testParameter.getParameterAsInt(seqContext.Step.Name, "STOP_DELAY", out stopDelay, 1000);

                string strVideoOutput;//COMPOSITE, SVIDEO, COMPONENT, HDMI
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_OUTPUT", out strVideoOutput);
                if (strVideoOutput == null)
                {
                    status = -99;
                    reportText = string.Format("you must define VIDEO_OUTPUT in you xmlTestParameter file");
                    log.Debug(reportText);
                }

                string strReadBuffer = string.Empty;

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.Unknown;

                // turn ON the proper port on Video Card
                if (status == 0)
                {
                    switch (strVideoOutput)
                    {
                        case "COMPOSITE":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPOSITE;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "SVIDEO":
                            videoSource = IVideoFrameAnalyzer.VideoSource.SVIDEO;
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_SVIDEO");
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPOSITE");
                            break;
                        case "COMPONENT":
                            videoSource = IVideoFrameAnalyzer.VideoSource.COMPONENT;
                            //videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject("VIDEO_CAPTURE_COMPONENT");
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "HDMI":
                            videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_HDMI");
                            break;
                        case "VGA":
                            videoSource = IVideoFrameAnalyzer.VideoSource.VGA;
                            videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject("VIDEO_CAPTURE_VGA");
                            break;
                    }


                    // Stop Video
                    if (status == 0)
                    {
                        log.DebugFormat("Wait {0} ms for DS Driver before stop", stopDelay);
                        System.Threading.Thread.Sleep(stopDelay);

                        status = videoCard.StopVideo();
                        result = status;
                    }

                }
            }
            catch (Exception ex)
            {
                log.Debug("StartVideo Exception:\r\n", ex);
                result = -5;
            }

        }//StopVideo

        public void SetVideoMode(SequenceContext seqContext, out double measurement, out string reportText)
        {
            measurement = -99.0;
            reportText = string.Empty;
            string Error = string.Empty;
            int status = -1;
            int timeout = 5000;
            string vOutMode = "";
            int vModeDelay = 0;


            try
            {
                log.DebugFormat("SetVideoOutMode() - {0}", seqContext.Step.Name);

                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out timeout, 5000);
                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCHDELAY", out vModeDelay, 100);
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEOMODE", out vOutMode, "CVBS");

                log.DebugFormat("Set Video out Mode is ", vOutMode);

                switch (vOutMode)
                {
                    case "CVBS":
                    case "YPBPR":
                    case "HDMI":
                        //qz status = uut.VideoTest(HmtStbComm.IHmtStbComm.VideoOutput.YPBPR, timeout, out Error);
                        break;
                    case "SVIDEO":
                        Error = string.Format("VideoTest output type {0} is not implemented.", vOutMode);
                        break;
                        break;
                        ////*****************************************************
                        //qz          public override int VideoTest(IHmtStbComm.VideoOutput Output, int Timeout, out string Error)

                        string response = "";

                        try
                        {
                            status = uut.write(string.Format("mtcvout {0}", vOutMode));

                            System.DateTime end = System.DateTime.Now.AddMilliseconds(timeout + 300);

                            while ((0 == status) && (!(response.Contains("[@VOUT_OK]")))
                                && (!(response.Contains("[$VOUT_FAIL:")))
                                && (System.DateTime.Compare(System.DateTime.Now, end) <= 0))
                            {
                                status = uut.read(ref response, 511, timeout); //uut.read(ref response, 255);
                            }
                            if (0 == status)
                                status = Convert.ToInt32(!response.Contains("[@VOUT_OK]"));
                            if (0 != status && response.Contains("[$VOUT_FAIL:"))
                            {
                                int begin = response.IndexOf("[$VOUT_FAIL:") + "[$VOUT_FAIL:".Length;
                                int endPos = response.IndexOf(']', begin);
                                status = -2;
                                //Error = HmtStbMtcErrorCode.VideoTestCommandUutReturnFail.ToString();
                                //Error = Error + " | " + response.Substring(begin, endPos - begin);
                            }
                            else if (0 != status)
                            {
                                status = -3;
                                //Error = HmtStbMtcErrorCode.VideoTestTimeoutError.ToString();
                            }
                        }
                        catch (TimeoutException ex)
                        {
                            Error = string.Format("VideoTest Timeout Exception reading response. {0}", ex.Message);
                            log.Debug(Error);
                            status = -4;
                            //Error = HmtStbMtcErrorCode.VideoTestTimeoutError.ToString();
                        }
                        catch (Exception ex)
                        {
                            log.Error("VideoTest Exception:\r\n", ex);
                            status = -5;
                            //Error = HmtStbMtcErrorCode.VideoTestException.ToString();
                        }
                    ////*******************************************************
                }
                if (status == 0)
                {
                    log.DebugFormat("Wait {0} ms for video output swap", vModeDelay);
                    System.Threading.Thread.Sleep(vModeDelay);
                    measurement = (double)status;
                }
            }
            catch (Exception ex)
            {
                log.Error("SetVideoOutMode() Exception:\r\n", ex);
                measurement = -6.0;
            }
        }//setVideoMode()
        
        private int CaptureAndCompareVideoToSpecs(SequenceContext seqContext, int maxAttempts, bool AveragePixels, ref double[] measurements)
        {
            PropertyObject po = seqContext.AsPropertyObject();
            string lowLimitLookup = "Step.Result.Measurement[{0}].Limits.Low";
            string highLimitLookup = "Step.Result.Measurement[{0}].Limits.High";
            string lowLimitString = "";
            string highLimitString = "";
            double[] lowLimit = new double[7];
            double[] highLimit = new double[7];
            int status = -1;
            int i = 0;
            int repeat = 0;
            for (i = 1; i < 7; i++)
            {
                lowLimitString = string.Format(lowLimitLookup, i);
                highLimitString = string.Format(highLimitLookup, i);
                lowLimit[i] = po.Exists(lowLimitString, 0) ? po.GetValNumber(lowLimitString, 0) : 0.0;
                highLimit[i] = po.Exists(highLimitString, 0) ? po.GetValNumber(highLimitString, 0) : 0.0;
                log.DebugFormat("VideoTest Limits for measurement[{0}]: {1} to {2}", i, lowLimit[i], highLimit[i]);
            }

            for (repeat = 0; repeat < maxAttempts; repeat++)
            {
                status = video.CaptureAndVerifyVideo(AveragePixels, ref measurements[1], ref measurements[2], ref measurements[3], ref measurements[4], ref measurements[5], ref measurements[6]);

                if (1 != status) // Capture picture failed - try again
                    continue;

                for (i = 1; i < 7; i++)
                {
                    if ((measurements[i] < lowLimit[i]) || (measurements[i] > highLimit[i]))
                        status = -3;
                    log.DebugFormat("CaptureAndCompareVideoToSpecs Result measurement[{0}]: value={1} status={2}", i, measurements[i], status);
                }
                if (1 == status)
                    break; // limit check passed, no need to test again
            }
            return status;
        }
        */

    }
}