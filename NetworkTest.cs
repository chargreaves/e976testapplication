using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Windows.Forms;
//using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

//using CoreToolkit;
//using CommInterface;
//using Logging;
//using TestParameters;
//using LV_Client;
//using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void NetworkTest(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;

            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            try
            {
                int iTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 3000);
                
                string strToLookFor;
                testParameter.getParameterAsString(seqContext.Step.Name, "EXPECTED_ANSWER", out strToLookFor, "Link speed 100 Mbps");              

                string strReadBuffer = string.Empty;

                string command = "pbist network";
                int status = SendReceive(command, "/ #", iTimeout, out strReadBuffer);
                if (status != 0)
                {
                    reportText = "1:Failed to send:" + command;
                    log.Debug(reportText);
                    reportText += "\n\nReceived: " + strReadBuffer;
                    result = -1;
                    return;
                }
                
                if (!strReadBuffer.Contains(strToLookFor))
                {
                    reportText = "2:Failed to read: " + strToLookFor;
                    log.Debug(reportText);
                    reportText += ", Received: " + strReadBuffer;
                    result = -2;
                    return;
                }

                result = 0;
            }
            catch (Exception e)
            {
                reportText = e.Message;
                log.Debug(reportText);
            }
        }
    }
}
