using System;

using NationalInstruments.TestStand.Interop.API;
using IRSimulator;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private IIRSimulator m_IrDevice;

        public void IrTest(SequenceContext seqContext, out double[] measurements, out string reportText)
        {
            reportText = string.Empty;
            measurements = new double[1];
            for (int i = 0; i < measurements.Length; i++)
            {
                measurements[i] = -99;
            }

            PrintTestName(seqContext.Step.Name);

            string sIrButton = string.Empty;
            int status = testParameter.getParameterAsString(seqContext.Step.Name, "IR_BUTTON", out sIrButton, "Power");

            try
            {
                m_IrDevice = (IIRSimulator)m_CoreToolkit.getEquipmentObject("IR_DEVICE", m_iUutInstance.ToString());
                if (m_IrDevice != null)
                {
                    m_IrDevice.SendIR(sIrButton, 0, out reportText);
                    measurements[0] = 1;
                }
                else
                {
                    log.Error("IR_DEVICE is offline.");
                    measurements[0] = -1;
                }

            }
            catch (Exception ex)
            {
                log.Error(string.Format("{0} Exception: {1}", seqContext.Step.Name, ex.Message));
                measurements[measurements.Length - 1] = -88;
            }
        }//

    }//class TestApp
}