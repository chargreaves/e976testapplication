﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NationalInstruments.TestStand.Interop.API;

namespace E976TestApplication
{
    public partial class TestApp
    {
        //=====================================================================================================================
        public void USB_001_00_01_Device_ID(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1;

            PrintTestName(seqContext.Step.Name);

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            command = "lsusb"; //Read Device ID
            response = "046d"; //ComSat USB Device ID
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                reportText += command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
                return;
            }

        }

        //=====================================================================================================================
        public void USB_002_00_00_Voltage_Short(SequenceContext seqContext, out double[] result, out string reportText)
        {
            reportText = string.Empty;
            result = new double[6];

            PrintTestName(seqContext.Step.Name);

            ComSat.Reset(); //reset ComSat board

            //pause 100ms after reset
            System.Threading.Thread.Sleep(100);

            ComSat.USBEnable(ComSat.C1238ACARD_USB_ENABLE); //USB enable
            ComSat.SetUSBLoad(ComSat.C1238ACARD_LOAD_ON); //apply load

            //pause 500ms after applying load
            System.Threading.Thread.Sleep(500);

            result[1] = AMB.SK1_A15.MeasureDC(); //read load voltage

            result[2] = 0; //read USB status before short circuit applied using MTC command - TO DO

            //apply USB short circuit
            ComSat.SetUSBLoad(ComSat.C1238ACARD_LOAD_ON);
            ComSat.SetUSBOverLoad(ComSat.C1238ACARD_LOAD_ON);
            ComSat.SetUSBSuperLoad(ComSat.C1238ACARD_LOAD_ON);

            //pause 500ms after applying short circuit
            System.Threading.Thread.Sleep(500);

            result[3] = AMB.SK1_A15.MeasureDC(); //read load voltage with short circuit applied

            result[4] = 0; //read USB status with short circuit applied using MTC command - TO DO

            //remove USB short circuit
            ComSat.SetUSBLoad(ComSat.C1238ACARD_LOAD_OFF);
            ComSat.SetUSBOverLoad(ComSat.C1238ACARD_LOAD_OFF);
            ComSat.SetUSBSuperLoad(ComSat.C1238ACARD_LOAD_OFF);

            //pause 100ms after removing short circuit
            System.Threading.Thread.Sleep(100);

            ComSat.SetUSBLoad(ComSat.C1238ACARD_LOAD_ON); //reapply load

            //pause 500ms after applying load
            System.Threading.Thread.Sleep(500);

            result[5] = AMB.SK1_A15.MeasureDC(); //read load voltage after removing short circuit

            //check for error
            result[0] = Convert.ToDouble(PFTS.ErrorOccurred());
            if (result[0] == 1)
            {
                //error
                if (reportText == "")
                {
                    reportText = PFTS.ErrorLog();
                }
                else
                {
                    reportText = reportText + "\n" + PFTS.ErrorLog();
                }
                PFTS.ClearErrorOccurred();
                PFTS.ClearErrorLog();
            }

        }

        //=====================================================================================================================
        public void USB_Loopback(SequenceContext seqContext, out double[] result, out string reportText)
        {
            string command;
            string response;
            int status;
            string strReadBuffer = string.Empty;

            reportText = string.Empty;
            result = new double[1];

            //set result to fail
            result[0] = -1;

            PrintTestName(seqContext.Step.Name);

            //ensure any previous MTC commands, like av_test, are stopped by entering a CR
            command = "\r";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);

            command = "pbist usb 1";
            response = "USB TEST OK!";
            status = SendReceive(command, "/ #", 3000, out strReadBuffer);
            if (strReadBuffer.Contains(response))
            {
                //pass
                result[0] = 0;
            }
            else
            {
                //fail
                reportText += command + ", Received: " + strReadBuffer;
                log.Debug(reportText);
                return;
            }

        }

        //=====================================================================================================================

    }

}
