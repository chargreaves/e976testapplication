using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

using Logging;

namespace E976TestApplication
{
    class ExecProcess
    {
        ILogging log;
        Process theProc;
        Object thisLock;
        String m_sDataReceived;
        bool m_RunningProcess;

        public string m_AppName;
        string m_ExitCmd = "";
        string m_TestSocket;

        public ExecProcess() : this(0)
        {
        }

        public ExecProcess(int testSocket)
        {
            m_TestSocket = testSocket.ToString();
            log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, m_TestSocket);
            thisLock = new Object();
            m_sDataReceived = String.Empty;
            m_RunningProcess = false;
        }

        public void initialize(string AppName, string AppPath, string AppArgs, string AppDir)
        {
            this.initialize(AppName, AppPath, AppArgs, AppDir, "");
        }

        public void initialize(string AppName, string AppPath, string AppArgs, string AppDir, string ExitCmd)
        {
            // Implement initialize function(s)
            m_AppName = AppName;
            m_ExitCmd = ExitCmd;
            log = LogControl.GetLogger(ILogging.LogSource.SerialPort, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + ": " + m_AppName, m_TestSocket);
            
            // StartInfo contains the startup information of
            // the new process
            theProc = new Process();
            theProc.StartInfo.FileName = AppPath;
            theProc.StartInfo.Arguments = AppArgs;
            theProc.StartInfo.WorkingDirectory = AppDir;

            // These two optional flags ensure that no DOS window
            // appears
            theProc.StartInfo.UseShellExecute = false;
            theProc.StartInfo.CreateNoWindow = true;

            // If this option is set the DOS window appears again :-/
            // ProcessObj.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            // This ensures that you get the output from the DOS application
            theProc.StartInfo.RedirectStandardInput = true;
            theProc.StartInfo.RedirectStandardOutput = true;

            // Set event handle
            theProc.OutputDataReceived += proc_DataReceived;

            log.DebugFormat("{0} process initialized.", m_AppName);
        }

        public int StartProcess(int timeout, string prompt)
        {
            string tmpBuffer;
            return StartProcess(timeout, prompt, out tmpBuffer);
        }

        public int StartProcess(int timeout, string prompt, out string buffer)
        {
            buffer = String.Empty;
            if (m_RunningProcess) return 0;

            // Kill if another process already running?!

            // Wait for reboot to complete (if using reboot of JTAG device)

            // Start the process
            log.DebugFormat("Starting {0} process...", m_AppName);
            if (!theProc.Start())
            {
                log.ErrorFormat("Failed to start {0} process.", m_AppName);
                return -1;
            }
            theProc.BeginOutputReadLine();

            bool bReceivedPrompt = false;
            System.DateTime loopTimeout = System.DateTime.Now;
            loopTimeout = loopTimeout.AddMilliseconds(timeout);
            string strReceived = "";
            do
            {
                strReceived += Receive();
                if (strReceived.Contains(prompt))
                    bReceivedPrompt = true;
            } while (!bReceivedPrompt && loopTimeout.CompareTo(System.DateTime.Now) > 0);
            buffer = strReceived;

           if (!bReceivedPrompt)
            {
                log.ErrorFormat("Timout occured when starting {0} process.", m_AppName);
                return -2;
            }

            m_RunningProcess = true;
            return 0;
        }

        public int Exit()
        {
            return Exit(m_ExitCmd);
        }

        public int Exit(string cmd)
        {
            // Implement exit method
            if (cmd.Length > 0)
            {

                Send(cmd);
                if (!theProc.WaitForExit(10000))
                {
                    log.ErrorFormat("Failed to quit {0} process.", m_AppName);
                    return -1;
                }
            }
            else
            {
                theProc.Kill();
                if (!theProc.WaitForExit(10000))
                {
                    log.ErrorFormat("Failed to quit {0} process.", m_AppName);
                    return -2;
                }
            }
            theProc.CancelOutputRead();
            m_RunningProcess = false;
            return 0;
        }

        public void Clear()
        {
            System.Threading.Thread.Sleep(100);
            lock (thisLock)
            {
                m_sDataReceived = "";
            }
        }

        public void Send(string cmd)
        {
            // Implement send function(s)
            if (!m_RunningProcess)
            {
                log.ErrorFormat("Failed to send, since {0} process isn't running!");
                return;
            }
            theProc.StandardInput.WriteLine(cmd);
        }

        public string Receive()
        {
            // Implement Receive function(s)
            string result = "";
            if (m_sDataReceived != "")
            {
                lock (thisLock)
                {
                    result = m_sDataReceived;
                    m_sDataReceived = "";
                }
            }
            return result;
        }

        public int SendReceive(string cmd, string prompt, int timeout, out string response)
        {
            // Implement SendReceive function(s)
            string strReceived = "";
            bool bReceivedPrompt = false;
            Clear(); // clear buffer before sending new command!

            if (!m_RunningProcess)
            {
                response = "";
                log.ErrorFormat("SendReceive failed, since {0} process isn't running!");
                return -1;
            }

            Send(cmd);

            System.DateTime loopTimeout = System.DateTime.Now;
            loopTimeout = loopTimeout.AddMilliseconds(timeout);
            do
            {
                strReceived += Receive();
                if (strReceived.Contains(prompt))
                    bReceivedPrompt = true;
            }while(!bReceivedPrompt && loopTimeout.CompareTo(System.DateTime.Now)>0);

            response = strReceived;

            if (!bReceivedPrompt)
            {
                log.ErrorFormat("Timout occured when waiting for prompt: '{0}'!", prompt);
                return -2;
            }
            return 0;
        }

        public bool IsProcessRunning()
        {
            return m_RunningProcess;
        }

        private void proc_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                lock (thisLock)
                {
                    m_sDataReceived += e.Data;
                }
                log.Debug(e.Data);
            }
        }
    }
}
