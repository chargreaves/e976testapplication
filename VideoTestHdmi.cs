using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

using CoreToolkit;
using CommInterface;
using Logging;
using TestParameters;
using LV_Client;
using CAM;
//using ScuHost;
using VideoFrameAnalyzer;


namespace E976TestApplication
{
    public partial class TestApp
    {
        public void VideoTestHdmi(SequenceContext seqContext, out double[] measurements, out string reportText)
        {
            measurements = new double[5];
            measurements[0] = -99;//overall
            measurements[1] = 9999;//HDMI Counter
            measurements[2] = -99;//Diff R
            measurements[3] = -99;//Diff G
            measurements[4] = -99;//Diff B

            reportText = string.Empty;
            string uutResponseTotal = "";
            string uutResponseTemp = "";
            log.Debug(seqContext.Step.Name);
            IVideoFrameAnalyzer videoCard = null;

            int status = 0;

            PrintTestName(seqContext.Step.Name);

            try
            {
                string sVideoEquipment;
                testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_EQUIPMENT", out sVideoEquipment, "VIDEO_CAPTURE_HDMI");

                videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject(sVideoEquipment, m_iUutInstance.ToString());
                if (videoCard == null)
                    videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject(sVideoEquipment, m_iUutInstance.ToString());

                if (videoCard == null)
                {
                    log.Error("videoCard is OFFLINE.");
                    return;
                }

                int iHdmiLoopTime;
                testParameter.getParameterAsInt(seqContext.Step.Name, "HDMI_LOOPTIME", out iHdmiLoopTime, 5000);

                int iTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 8000);

                IVideoFrameAnalyzer.VideoSource videoSource = IVideoFrameAnalyzer.VideoSource.HDMI;

                log.Debug("Displaying picture");

                //if(scu != null)
                //    scu.Connect("VideoSource_RCA_CVBS");//m_NISystem.SelectVideoSource(RCA_CVBS);
                //CJK!!video.SelectVideoFormat((int)IVideoFrameAnalyzer.VideoSource.CVBS);//m_TVCard.SelectVideoFormat(TV_FORMAT_CVBS);

                //CJK!! ignore the returned value of SelectVideoFormat() for now

                int switchDelay ;
                testParameter.getParameterAsInt(seqContext.Step.Name, "SWITCH_DELAY", out switchDelay, 1000);

                int iDelay;
                testParameter.getParameterAsInt(seqContext.Step.Name, "DELAY", out iDelay, 2000);

                int iRepeat;
                testParameter.getParameterAsInt(seqContext.Step.Name, "REPEAT", out iRepeat, 1);

                videoCard.SelectVideoFormat(videoSource);

                log.DebugFormat("Wait {0} ms for Video Input to switch before capture", switchDelay);
                System.Threading.Thread.Sleep(switchDelay);
               
                bool bHdmiInfoOK = false;
                //int iSynchOK = 0;
                int iPictureOK = 0;
                //bool bStatus = false;
                int iStatus = -1;
                //string strBuffer = string.Empty;

                System.DateTime loop = System.DateTime.Now;
                loop = loop.AddMilliseconds(iHdmiLoopTime);

                while (!bHdmiInfoOK && (System.DateTime.Compare(System.DateTime.Now, loop) <= 0))
                {
                    iStatus = SendReceive("pbist picture hdmi", "CEC TRANSMIT OK", iTimeout, out uutResponseTemp);
                    //bStatus = SendReceive("pbist picture hdmi", "CEC TRANSMIT OK", iTimeout, out uutResponseTemp);

                    if (iStatus == -1)
                    //if (bStatus == false)
                    {
                        measurements[0] = -1;

                        reportText = "1:Error running 'pbist picture hdmi'";
                        log.Debug(reportText);
                    }
                    else
                    {
                        uutResponseTotal = uutResponseTemp;

                        log.Debug("Testing HDMI");

                        System.Threading.Thread.Sleep(iDelay);
                        videoCard.CapturePicture("c:\\temp\\dummy.bmp");

                        // HDMI cable counter

                        // Calibrate the DVI adapter via CalibrationMode in Settings.ini

                        System.DateTime begin = System.DateTime.Now;
                        begin = begin.AddMilliseconds(iTimeout);

                        do
                        {
                            iPictureOK = CaptureAndCompareVideoHDMIToSpecs(seqContext, iRepeat, ref measurements);//m_TVCard.CaptureAndVerifyVideoHDMI(pTestCaseParam, pTestCaseReturnParam);
                                                                                                                  //measurements[2] = -99;//Diff R
                                                                                                                  //measurements[3] = -99;//Diff G
                                                                                                                  //measurements[4] = -99;//Diff B

                            log.Debug("iPictureOK=" + iPictureOK.ToString());

                            if (iPictureOK != 0)
                            {
                                log.Debug("Please insert the HDMI cable!\n");

                                System.Threading.Thread.Sleep(500);

                                //log.Debug("Turning off HDMI adapters for one second\r\n");
                                //scu.Connect("FIXTURE_FLASH");//m_NISystem.SelectFlash(FIXTURE_FLASH);
                                //System.Threading.Thread.Sleep(1000);
                                //scu.Connect("ONBOARD_FLASH");//m_NISystem.SelectFlash(ONBOARD_FLASH);
                                //log.Debug("Turning on HDMI again\r\n");
                                //System.Threading.Thread.Sleep(500);
                            }
                        } while (iPictureOK != 0 && (System.DateTime.Compare(System.DateTime.Now, begin) <= 0));
                        /*
                        if (status == 0 )
                        {
                            bStatus = SendReceive("quit", "/ #", 3000, out uutResponseTemp);

                            measurements[0] = -2;

                            reportText = "2:Error: HDMI sync FAILED";
                            log.Debug(reportText);
                        }
                        else */
                        if (status == 0 && iPictureOK != 0)
                        {
                            iStatus = SendReceive("quit", "/ #", 3000,out uutResponseTemp);
                            //bStatus = SendReceive("quit", "/ #", 3000, out uutResponseTemp);

                            measurements[0] = -3;

                            reportText = "3:Error: HDMI picture FAILED";
                            log.Debug(reportText);
                        }
                        else if (status == 0)
                        {
                            //string strReadBuffer = string.Empty;
                            iStatus = SendReceive("quit", "/ #", 3000, out uutResponseTemp);
                            //bStatus = SendReceive("quit", "/ #", 3000, out uutResponseTemp);
                            uutResponseTotal += uutResponseTemp;
                            if (uutResponseTotal.Contains("HDMI connected") || uutResponseTotal.Contains("Manufacturer=MEI") || uutResponseTotal.Contains("Manufacturer=CYP"))
                            {
                                bHdmiInfoOK = true;
                                //log.Debug("bHdmiInfoOK is true. uutResponseTotal=" + uutResponseTotal);
                            }
                            else
                            {
                                uutResponseTemp = string.Empty;
                                iStatus = SendReceive("quit", "/ #", 3000, out uutResponseTemp);
                                //bStatus = SendReceive("quit", "/ #", 3000, out uutResponseTemp);

                                measurements[0] = -4;

                                reportText = "HDMI Info not OK";
                                log.Debug(reportText);

                                log.Debug("Please insert the HDMI cable!\n");
                            }
                        }
                    }
                }

                if (bHdmiInfoOK)
                {
                    measurements[0] = 0;
                }
            }
            catch (Exception e)
            {
                reportText = e.Message;
                log.Debug(reportText);
            }
        }//VideoTestHdmi

        private int CaptureAndCompareVideoHDMIToSpecs(SequenceContext seqContext, int maxAttempts, ref double[] measurements)
        {
            PropertyObject po = seqContext.AsPropertyObject();
            string lowLimitLookup = "Step.Result.Measurement[{0}].Limits.Low";
            string highLimitLookup = "Step.Result.Measurement[{0}].Limits.High";
            string lowLimitString = "";
            string highLimitString = "";
            double[] lowLimit = new double[5];
            double[] highLimit = new double[5];
            int status = -1;
            int i = 0;
            int repeat = 0;
            for (i = 2; i < 5; i++)
            {
                lowLimitString = string.Format(lowLimitLookup, i);
                highLimitString = string.Format(highLimitLookup, i);
                lowLimit[i] = po.Exists(lowLimitString, 0) ? po.GetValNumber(lowLimitString, 0) : 0.0;
                highLimit[i] = po.Exists(highLimitString, 0) ? po.GetValNumber(highLimitString, 0) : 0.0;
                log.DebugFormat(" {0} Limits for measurement[{1}]: {2} to {3}", seqContext.Step.Name, i, lowLimit[i], highLimit[i]);
            }

            string sVideoEquipment;
            testParameter.getParameterAsString(seqContext.Step.Name, "VIDEO_EQUIPMENT", out sVideoEquipment, "VIDEO_CAPTURE_HDMI");

            IVideoFrameAnalyzer videoCard = (IVideoFrameAnalyzer)m_CoreToolkit.getEquipmentObject(sVideoEquipment, m_iUutInstance.ToString());
            if (videoCard == null) 
                 videoCard = (IVideoFrameAnalyzer)m_LocalCoreToolkit.getEquipmentObject(sVideoEquipment, m_iUutInstance.ToString());

            for (repeat = 0; repeat < maxAttempts; repeat++)
            {
                log.DebugFormat(" repeat={0}", repeat);

                status = videoCard.CaptureAndVerifyVideoHDMI(ref measurements[2], ref measurements[3], ref measurements[4]);

                if (1 != status) // Capture picture failed - try again
                    continue;

                for (i = 2; i < 5; i++)
                {
                    if ((measurements[i] < lowLimit[i]) || (measurements[i] > highLimit[i]))
                        status = -3;
                    log.DebugFormat(" CaptureAndCompareVideoHDMIToSpecs Result measurement[{0}]: value={1} status={2}", i, measurements[i], status);
                }
                if (1 == status)
                    break; // limit check passed, no need to test again
            }
            return status;
        }
    }
}