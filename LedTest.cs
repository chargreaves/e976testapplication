using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using NationalInstruments.TestStand.Interop.API;
using LEDAnalyzer;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private ILEDAnalyzer m_LedAnalyzer;

        public void LedTest(SequenceContext seqContext, out double[] measurements, out string reportText)
        {
            reportText = string.Empty;
            measurements = new double[7];
            for (int i = 0; i < measurements.Length; i++)
            {
                measurements[i] = -99;
            }

            PrintTestName(seqContext.Step.Name);

            int status = -1;
            string sBuffer = string.Empty;

            string sLedOnCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_ON_COMMAND", out sLedOnCmd, "");
            /*if (sLedOnCmd == null || sLedOnCmd.Length == 0)
            {
                log.Error("You must provide LED_ON_COMMAND in XML test parameter.");
                return;
            }*/

            string sLedOffCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_OFF_COMMAND", out sLedOffCmd, "");
            /*if (sLedOffCmd == null || sLedOffCmd.Length == 0)
            {
                log.Error("You must provide LED_OFF_COMMAND in XML test parameter.");
                return;
            }*/

            int iFiber = 0;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "FIBER_NUMBER", out iFiber);//1 to 10
            if (iFiber < 1 || iFiber > 10)
            {
                log.Error("You must provide FIBER number for LED ANALYZER in XML test parameter.");
                return;
            }

            int iExposure = 0;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "EXPOSURE", out iExposure);//1 to 15

            string sManualRange = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "MANUAL_RANGE", out sManualRange);//LOW=1, MEDIUM, HIGH, SUPER, ULTRA=5

            string sLedSetupCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_SETUP_COMMAND", out sLedSetupCmd);//  ./sw 0xfffe8708 00000000

            string sLedCleanupCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_CLEANUP_COMMAND", out sLedCleanupCmd);

            string sCmdPrompt = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "COMMAND_PROMPT", out sCmdPrompt);

            int iTimeout;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 4000);

            try
            {
                if (sCmdPrompt == null || sCmdPrompt.Length == 0)
                    sCmdPrompt = _sPrompt;

                bool bState = false;

                if (sLedSetupCmd != null && sLedSetupCmd.Length > 0)
                {
                    string[] arrSetupCmds = sLedSetupCmd.Split(',');

                    foreach (string cmd in arrSetupCmds)
                    {
                        bState = SendReceive(cmd.Trim(), sCmdPrompt, iTimeout, out sBuffer, telnetSession);

                        if (bState == false)
                        {
                            log.Error("setup test command failed.");
                            return;
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                }

                if (sLedOnCmd.Length > 0)
                {
                    bState = SendReceive(sLedOnCmd, sCmdPrompt, iTimeout, out sBuffer, telnetSession);

                    if (bState == false)
                    {
                        log.Error("test command failed.");
                        return;
                    }
                }

                m_LedAnalyzer = (ILEDAnalyzer)m_CoreToolkit.getEquipmentObject("LED_FEASA", m_iUutInstance.ToString());

                if (m_LedAnalyzer != null)
                {
                    DateTime dt = DateTime.Now;

                    if (iExposure >= 1 && iExposure <= 15)//for test time saving, set it once or change as needed.
                    {
                        status = m_LedAnalyzer.SetFACTOR(iExposure);
                        log.DebugFormat("SetFACTOR({0}) status={1}", iExposure, status);
                        log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);
                        /*
                        int factor = m_LedAnalyzer.getFACTOR();
                        SetText("getFACTOR() factor=" + factor.ToString());
                        SetText(string.Format("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds));
                        */
                    }

                    if (sManualRange != null && sManualRange.Length > 0)
                    {
                        int iManualRange = (int)Enum.Parse(typeof(ILEDAnalyzer.ManualCaptureRange), sManualRange.ToUpper()); ;

                        status = m_LedAnalyzer.Capture(iManualRange);
                        log.DebugFormat("Capture({0}) {1} status={2}", iManualRange, sManualRange, status);
                        log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);
                    }

                    double[] hsi = m_LedAnalyzer.getHSI(iFiber);
                    for (int cnt = 0; cnt < hsi.Length; cnt++)
                    {
                        log.DebugFormat("Fiber={2} hsi[{0}]={1}", cnt, hsi[cnt], iFiber);
                        measurements[cnt] = hsi[cnt];
                    }
                    log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);


                    double[] rgbi = m_LedAnalyzer.getRGBI(iFiber);
                    for (int cnt = 0; cnt < rgbi.Length; cnt++)
                    {
                        log.DebugFormat("Fiber={2} rgbi[{0}]={1}", cnt, rgbi[cnt], iFiber);
                        measurements[hsi.Length + cnt] = rgbi[cnt];
                    }
                    log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);

                }
                else
                    log.Error("LED_FEASA is offline.");
            }
            catch (Exception ex)
            {
                log.Error(string.Format("{0} Exception: {1}", seqContext.Step.Name, ex.Message));
                measurements[measurements.Length - 1] = -88;
            }
            finally
            {
                if (sLedOffCmd != null && sLedOffCmd.Length > 0)
                    SendReceive(sLedOffCmd, sCmdPrompt, iTimeout, out sBuffer, telnetSession);

                if (sLedCleanupCmd != null && sLedCleanupCmd.Length > 0)
                {
                    if (sLedCleanupCmd.Equals("exit"))
                        SendReceive(sLedCleanupCmd, _sPrompt, 1000, out sBuffer, telnetSession);
                    else
                        SendReceive(sLedCleanupCmd, sCmdPrompt, iTimeout, out sBuffer, telnetSession);
                }
            }
        }//LedTest

        public void LedTestWavelength(SequenceContext seqContext, out double[] measurements, out string reportText)
        {
            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            string sLoggingOrder = string.Empty;
            testParameter.getParameterAsString(seqContext.Step.Name, "LOGGING_ORDER", out sLoggingOrder, "");//6,7,8,9,5,4,3,2,1
            string[] arrLed = sLoggingOrder.Split(',');

            int iNumberOfLed = arrLed.Length;
            log.DebugFormat("sLoggingOrder={0}, iNumberOfLed={1}", sLoggingOrder, iNumberOfLed);

            measurements = new double[iNumberOfLed * 2]; ;
            for (int i = 0; i < measurements.Length; i++)
            {
                measurements[i] = -99;
            }

            if (sLoggingOrder == null || sLoggingOrder.Length == 0)
            {
                log.Error("You must provide LOGGING_ORDER in XML test parameter.");
                return;
            }

            // Feasa fiber=1    Rear panel - power adapter
            // Feasa fiber=2    Rear panel - Ethernet4
            // Feasa fiber=3    Rear panel - Ethernet3
            // Feasa fiber=4    Rear panel - Ethernet2
            // Feasa fiber=5    Rear panel - Ethernet1
            // Feasa fiber=6    Front panel - Power
            // Feasa fiber=7    Front panel - Broadband
            // Feasa fiber=8    Front panel - Phone
            // Feasa fiber=9    Front panel - WPS
            // Feasa fiber=10   not used

            int status = -1;
            string sBuffer = string.Empty;

            string sLedOnCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_ON_COMMAND", out sLedOnCmd, "");

            string sLedOffCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_OFF_COMMAND", out sLedOffCmd, "");
            /*
            int iFiber = 0;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "FIBER_NUMBER", out iFiber);//1 to 10
            if (iFiber < 1 || iFiber > 10)
            {
                log.Error("You must provide FIBER number for LED ANALYZER in XML test parameter.");
                return;
            }
            */
            string sFibers = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "FIBER_NUMBER", out sFibers, "");
            if (sFibers == null || sFibers.Length == 0)
            {
                log.Error("You must provide FIBERS_NUMBER for LED ANALYZER in XML test parameter.");
                return;
            }
            string[] arrFibers = sFibers.Split(',');

            int iExposure = 0;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "EXPOSURE", out iExposure);//1 to 15

            string sManualRange = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "MANUAL_RANGE", out sManualRange);//LOW=1, MEDIUM, HIGH, SUPER, ULTRA=5

            string sLedSetupCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_SETUP_COMMAND", out sLedSetupCmd);//  ./sw 0xfffe8708 00000000

            string sLedCleanupCmd = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "LED_CLEANUP_COMMAND", out sLedCleanupCmd);

            string sSetupCmdPrompt = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "SETUP_COMMAND_PROMPT", out sSetupCmdPrompt);

            string sCmdPrompt = string.Empty;
            status = testParameter.getParameterAsString(seqContext.Step.Name, "COMMAND_PROMPT", out sCmdPrompt);

            int iTimeout;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 5000);

            int iDelayMSec = 0;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "DELAY_MSEC", out iDelayMSec, 500);

            int debug = 0;
            status = testParameter.getParameterAsInt(seqContext.Step.Name, "DEBUG", out debug, 0);

            try
            {
                if (sCmdPrompt == null || sCmdPrompt.Length == 0)
                    sCmdPrompt = _sPrompt;

                if (sSetupCmdPrompt == null || sSetupCmdPrompt.Length == 0)
                    sSetupCmdPrompt = _sPrompt;

                bool bState = false;

                killProcess("wps_monitor", " -s 40");

                if (sLedSetupCmd != null && sLedSetupCmd.Length > 0)
                {
                    log.Debug("LED_SETUP_COMMAND");
                    string[] arrSetupCmds = sLedSetupCmd.Split(',');

                    foreach (string cmd in arrSetupCmds)
                    {
                        if (cmd.Contains("exit"))
                            bState = SendReceive(cmd.Trim(), sCmdPrompt, iTimeout, out sBuffer, telnetSession);
                        else
                            bState = SendReceive(cmd.Trim(), sSetupCmdPrompt, iTimeout, out sBuffer, telnetSession);

                        if (bState == false)
                        {
                            log.Error("setup test command failed.");
                            measurements[measurements.Length - 1] = -1;
                            return;
                        }
                        System.Threading.Thread.Sleep(iDelayMSec);
                    }
                }
                /*
                if (sLedOnCmd.Length > 0)
                {
                    string[] arrLedOnCmds = sLedOnCmd.Split(',');

                    foreach (string cmd in arrLedOnCmds)
                    {
                        bState = SendReceive(cmd.Trim(), sCmdPrompt, iTimeout, out sBuffer, telnetSession);

                        if (bState == false)
                        {
                            log.Error("test command failed.");
                            measurements[measurements.Length - 1] = -2;
                            return;
                        }
                        System.Threading.Thread.Sleep(iDelayMSec);
                    }
                }
                */
                m_LedAnalyzer = (ILEDAnalyzer)m_CoreToolkit.getEquipmentObject("LED_FEASA", m_iUutInstance.ToString());

                if (m_LedAnalyzer != null)
                {
                    double[] arrayWavelength = null;
                    int[] arrayIntensity = null;
                    DateTime dt = DateTime.Now;
                    for (int exp = iExposure; exp <= 15; exp++)
                    {
                        sendCommands(sLedOnCmd, sCmdPrompt, iTimeout, iDelayMSec);

                        bState = checkLedColors();

                        if (exp >= 1 && exp <= 15)//for test time saving, set it once or change as needed.
                        {
                            status = m_LedAnalyzer.SetFACTOR(exp);
                            log.DebugFormat("EXPOSURE SetFACTOR({0}) status={1}", exp, status);
                            log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);
                        }
                        else
                            log.ErrorFormat("EXPOSURE must be 1 to 15, not {0}", exp);

                        if (status < 0)
                            break;

                        /*
                        AutoCapture has NTFs for wavelength and intensity 
                        status = m_LedAnalyzer.Capture();
                        log.DebugFormat("AutoCapture() status={0}", status);
                        log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);
                        */
                        // Capture
                        if (sManualRange != null && sManualRange.Length > 0)
                        {
                            int iManualRange = (int)Enum.Parse(typeof(ILEDAnalyzer.ManualCaptureRange), sManualRange.ToUpper()); ;

                            status = m_LedAnalyzer.Capture(iManualRange);
                            log.DebugFormat("Capture({0}) {1} status={2}", iManualRange, sManualRange, status);
                            log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);
                        }
                        
                        if (status < 0)
                            break;

                        arrayWavelength = m_LedAnalyzer.getWAVELENGTHALL();//2x-2
                        log.Debug("m_LedAnalyzer.getWAVELENGTHALL()");
                        log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);

                        for (int led = 1; led <= 10; led++)
                        {
                            log.DebugFormat(" led={0}, arrayWavelength[{1}]={2} arrayWavelength[{3}]={4}", led, 2 * led - 2, arrayWavelength[2 * led - 2], 2 * led - 1, arrayWavelength[2 * led - 1]);
                        }
                        //arrayWavelength[17] = 1;//is Wps wavelength ...for debug only

                        bool bWavelengthReading = true;
                        foreach (string fiber in arrFibers)
                        {
                            int iFiber = Convert.ToInt32(fiber.Trim());
                            log.DebugFormat("  iFiber={0}, arrayWavelength[{1}]={2} arrayWavelength[{3}]={4}", iFiber, 2 * iFiber - 2, arrayWavelength[2 * iFiber - 2], 2 * iFiber - 1, arrayWavelength[2 * iFiber - 1]);
                            if (arrayWavelength[2 * iFiber - 1] < 100)
                                bWavelengthReading = false;
                        }

                        if (bWavelengthReading == true || exp >= 15 || seqContext.Step.Name.Contains("LedOff"))
                        {
                            for (int trial = 1; trial <= 6; trial++)
                            {
                                bState = checkLedColors();

                                // INTENSITY
                                arrayIntensity = m_LedAnalyzer.getINTENSITYALL();
                                log.Debug("m_LedAnalyzer.getINTENSITYALL()");
                                log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);

                                for (int led = 1; led <= 10; led++)
                                {
                                    log.DebugFormat(" led={0}, arrayIntensity[{1}]={2} arrayIntensity[{3}]={4}", led, 2 * led - 2, arrayIntensity[2 * led - 2], 2 * led - 1, arrayIntensity[2 * led - 1]);
                                }
                                //arrayIntensity[17] = 2;//is Wps intensity...for debug only

                                bool bIntensityReading = true;
                                foreach (string fiber in arrFibers)
                                {
                                    int iFiber = Convert.ToInt32(fiber.Trim());
                                    log.DebugFormat("  iFiber={0}, arrayIntensity[{1}]={2} arrayIntensity[{3}]={4}", iFiber, 2 * iFiber - 2, arrayIntensity[2 * iFiber - 2], 2 * iFiber - 1, arrayIntensity[2 * iFiber - 1]);
                                    if (arrayIntensity[2 * iFiber - 1] < 1000)
                                        bIntensityReading = false;
                                }

                                if (bIntensityReading == true || exp >= 15 || seqContext.Step.Name.Contains("LedOff"))
                                    break;
                                else if (trial + 1 <= 6)
                                {
                                    log.DebugFormat(" try to Capture again since no Intensity value, trial={0}", trial + 1);

                                    status = m_LedAnalyzer.SetFACTOR(++exp);
                                    log.DebugFormat("EXPOSURE SetFACTOR({0}) status={1}", exp, status);
                                    log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);

                                    // Capture
                                    if (sManualRange != null && sManualRange.Length > 0)
                                    {
                                        int iManualRange = (int)Enum.Parse(typeof(ILEDAnalyzer.ManualCaptureRange), sManualRange.ToUpper()); ;

                                        status = m_LedAnalyzer.Capture(iManualRange);
                                        log.DebugFormat("Capture({0}) {1} status={2}", iManualRange, sManualRange, status);
                                        log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);
                                    }
                                }
                            }//for-loop trial for intensity

                            break;
                        }
                        log.Debug("not a valid Wavelength reading. retrying...");
                    }// for-loop exposure for wavelength

                    // Logging
                    int index = 0;
                    foreach (string led in arrLed)
                    {
                        int fiber = Convert.ToInt32(led.Trim());

                        if (arrayWavelength != null && arrayWavelength.Length > 0)
                            measurements[index] = arrayWavelength[2 * fiber - 1];
                        log.DebugFormat("measurements[{0}]={1} is for FIBER={2} Wavelength", index, measurements[index], fiber);

                        if (arrayIntensity != null && arrayIntensity.Length > 0)
                            measurements[index + 1] = arrayIntensity[2 * fiber - 1];
                        log.DebugFormat("measurements[{0}]={1} is for FIBER={2} Intensity", index + 1, measurements[index + 1], fiber);

                        index += 2;
                    }

                    // RGBI
                    foreach (string fiber in arrFibers)
                    {
                        int iFiber = Convert.ToInt32(fiber);
                        double[] rgbi = m_LedAnalyzer.getRGBI(iFiber);
                        for (int cnt = 0; cnt < rgbi.Length; cnt++)
                        {
                            log.DebugFormat(" Fiber={2} rgbi[{0}]={1}", cnt, rgbi[cnt], iFiber);
                        }
                        log.DebugFormat("  elapsed: {0} seconds", ((TimeSpan)(DateTime.Now - dt)).TotalSeconds);

                        if (debug > 0 && rgbi[3] < 1000)
                            MessageBox.Show("Check ALL LEDs are ON with proper color (all GREEN or all RED).", "LED wavelength and intensity");
                    }
                }
                else
                    log.Error("LED_FEASA is offline.");
            }
            catch (Exception ex)
            {
                log.Error(string.Format("{0} Exception: {1}", seqContext.Step.Name, ex.Message));
                measurements[measurements.Length - 1] = -88;
            }
            finally
            {
                if (sLedOffCmd != null && sLedOffCmd.Length > 0)
                {
                    SendReceive(sLedOffCmd, sCmdPrompt, iTimeout, out sBuffer, telnetSession);

                    System.Threading.Thread.Sleep(iDelayMSec);
                }

                if (sLedCleanupCmd != null && sLedCleanupCmd.Length > 0)
                {
                    if (sLedCleanupCmd.Equals("exit"))
                        SendReceive(sLedCleanupCmd, _sPrompt, 1000, out sBuffer, telnetSession);
                    else
                        SendReceive(sLedCleanupCmd, sCmdPrompt, iTimeout, out sBuffer, telnetSession);

                    System.Threading.Thread.Sleep(iDelayMSec);
                }
            }
        }//LedTestWavelength

        private bool sendCommands(string sCommands, string sPrompt, int iTimeout, int iDelayMSec)
        {
            log.Debug(" sendCommands()");
            string[] arrCmds = sCommands.Split(',');
            bool bState = false;
            string sBuffer = string.Empty;

            foreach (string cmd in arrCmds)
            {
                bState = SendReceive(cmd.Trim(), sPrompt, iTimeout, out sBuffer, telnetSession);

                if (bState == false)
                {
                    log.Error(cmd.Trim()+" test command failed.");

                    return false;
                }
                System.Threading.Thread.Sleep(iDelayMSec);
            }
            return bState;
        }//sendCommands

        private bool checkLedColors()
        {
            log.Debug(" checkLedColors()");
            string sBuffer = string.Empty;
            bool status = SendReceive("! cat /sys/motopia/leds/Power/color", _sPrompt, 2000, out sBuffer, telnetSession);
            if (status == true)
                status = SendReceive("! cat /sys/motopia/leds/Dsl/color", _sPrompt, 2000, out sBuffer, telnetSession);
            if (status == true)
                status = SendReceive("! cat /sys/motopia/leds/VOIP1/color", _sPrompt, 2000, out sBuffer, telnetSession);
            if (status == true)
                status = SendReceive("! cat /sys/motopia/leds/SimpleConfig/color", _sPrompt, 2000, out sBuffer, telnetSession);
            return status;
        }//checkLedColors

        private bool killProcess(string sProcess, string sParam)
        {
            log.DebugFormat(" killProcess({0})",sProcess);
            string sBuffer = string.Empty;
            bool status = SendReceive("!", "#", 2000, out sBuffer, telnetSession);
            if (status == true)
            {
                status = SendReceive("ps -T | grep " + sProcess, "#", 2000, out sBuffer, telnetSession);
                if (status == true && sBuffer.Contains(sProcess + sParam))
                {
                    Match match = Regex.Match(sBuffer, @"(\S*) root");

                    if (match.Success)
                    {
                        string pid = match.Groups[1].ToString();
                        status = SendReceive("kill -9 " + pid, "#", 2000, out sBuffer, telnetSession);

                        status = SendReceive("ps -T | grep " + sProcess, "#", 2000, out sBuffer, telnetSession);
                    }
                    else
                        log.Error(@"no match using RegEx: '(\S*) root'");
                }
                
                status = SendReceive("exit", ">", 2000, out sBuffer, telnetSession);
            }
            return status;
        }//killProcess

    }//class TestApp
}