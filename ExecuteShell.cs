using System;
using System.Text;
using System.Threading;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private StringBuilder returnData = null;
        private AutoResetEvent resetEvent = null;
        private System.Threading.Thread t = null;

        private bool ExecuteShell(string sCmd, string sCmdParam, out string sOutput)
        {
            return ExecuteShell(sCmd, sCmdParam, out sOutput, 60);
        }
        private bool ExecuteShell(string sCmd, string sCmdParam, out string sOutput, int iTimeoutSec)
        {
            sOutput = string.Empty;
            try
            {
                worker wk = new worker();
                wk.sShellName = sCmd;//"wlan.exe";
                wk.sParam0 = sCmdParam;// Parameters;
                wk.iTimeout = iTimeoutSec * 1000;

                resetEvent = worker.resetEvent;
                resetEvent.Reset();
                //log.Debug("resetEvent.Reset()");

                t = new System.Threading.Thread(new ThreadStart(wk.workerThread));
                t.IsBackground = true;
                t.Start();
                log.Debug(" ExecuteShell '" + sCmd + " " + sCmdParam + "' thread started.");
                resetEvent.WaitOne();
                //log.Debug("done waiting.");

                returnData = worker.returnData;
                log.Debug(" ExecuteShell returnData=" + returnData.ToString());
                sOutput = returnData.ToString();

                returnData.Remove(0, returnData.Length);
                returnData.Length = 0;

                return true;
            }
            catch (Exception ex)
            {
                log.Error(string.Format(" ExecuteShell Exception: {0}", ex.Message));
                return false;
            }
        }//executeShell

    }//class TestApp
}