using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Windows.Forms;
//using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;

//using CoreToolkit;
//using CommInterface;
//using Logging;
//using TestParameters;
//using LV_Client;
//using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        public void CheckMtcVersion(SequenceContext seqContext, out double result, out string reportText)
        {
            result = -99;//overall

            reportText = string.Empty;

            PrintTestName(seqContext.Step.Name);

            try
            {
                string strMtcVersionRequired;
                string strParameter = "MTC_VERSION";
                int status = testParameter.getParameterAsString(seqContext.Step.Name, strParameter, out strMtcVersionRequired);
                if (status != 0)
                {
                    reportText = "Parameter not specified: " + strParameter;
                    log.Debug(reportText);
                    return;
                }

                string command = "version_info";
                string strReadBuffer = string.Empty;
                status = SendReceive(command, "/ #", 3000, out strReadBuffer);
                if (status != 0)
                {
                    reportText = "11:Failed to check version: ";
                    log.Debug(reportText);
                    reportText += command + ", Received: " + strReadBuffer;
                    result = -11;
                    return;
                }

                string strToLookFor = "incorrectly installed!!!";
                if (strReadBuffer.Contains(strToLookFor))
                {
                    reportText = "12:MTC version is incorrectly installed!";
                    log.Debug(reportText);
                    reportText += ", Received: " + strReadBuffer;
                    result = -12;
                    return;
                }

                strToLookFor = "VERSION_INFO:";
                if (!strReadBuffer.Contains(strToLookFor))
                {
                    reportText = "13:Failed to read: " + strToLookFor;
                    log.Debug(reportText);
                    reportText += ", Received: " + strReadBuffer;
                    result = -13;
                    return;
                }             

                int start = strReadBuffer.IndexOf(strToLookFor) + strToLookFor.Length;
                int end = strReadBuffer.IndexOf("\r\n", start);

                string strMtcVersionRead = strReadBuffer.Substring(start, end - start).Trim();
                log.Debug("MTC version read: " + strMtcVersionRead + ", MTC version Required: " + strMtcVersionRequired);

                string[] strMtcVersionRequiredList = strMtcVersionRequired.Split('.');
                string[] strMtcVersionReadList = strMtcVersionRead.Split('.');

                for (int i = 0; i < strMtcVersionRequiredList.Length; i++)
                {
                    try
                    {
                        result = Convert.ToInt32(strMtcVersionReadList[i]) - Convert.ToInt32(strMtcVersionRequiredList[i]);
                    }
                    catch (Exception ex)
                    {
                        reportText = "2:Unsupported MTC version: " + strMtcVersionRead + ", Required: " + strMtcVersionRequired + ", Exception: " + ex.Message;
                        log.Debug(reportText);
                        result = -2;
                        return;
                    }
                    
                    if (result > 0)
                    {
                        result = 1;
                        break;
                    }
                    else if (result < 0)
                    {
                        reportText = "1:Unsupported MTC version: " + strMtcVersionRead + ", Required: " + strMtcVersionRequired;
                        log.Debug(reportText);
                        reportText += ", Received: " + strReadBuffer;
                        result = -1;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                reportText = e.Message;
                log.Debug(reportText);
                result = -98;
            }
        }//CheckMtcVersion        
    }
}