using System;
//using System.Collections.Generic;
using System.Text;
//using System.Windows.Forms;
using System.Runtime.InteropServices;
using NationalInstruments.TestStand.Interop.API;
using System.IO;
using System.Xml;

//using CoreToolkit;
//using CommInterface;
//using Logging;
//using TestParameters;
//using LV_Client;
//using CAM;

namespace E976TestApplication
{
    public partial class TestApp
    {
        private bool m_bConfigStationDestroyed = false;
        public void CreateSecureUSP(SequenceContext seqContext, out double result, out string reportText)
        {
            reportText = string.Empty;


            PrintTestName(seqContext.Step.Name);

            result = -99;

            try
            {
                if (m_bConfigStationDestroyed)
                {
                    log.Debug("ConfigStation is destroyed, try to re-initialize...");
                    if (m_ConfigurationStation.Initialize(m_LogClient.GetSocketAddress(), m_iUutInstance))
                        m_bConfigStationDestroyed = false;
                    else
                    {
                        result = -98;
                        log.Error("Error: Config station failed to initialize...");
                        return;
                    }
                }

                int iTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "TIMEOUT", out iTimeout, 30000);

                string strCagInputParamList;
                string strParameter = "CAG_INPUT_PARAM_LIST";
                int status = testParameter.getParameterAsString(seqContext.Step.Name, strParameter, out strCagInputParamList);
                if (status != 0)
                {
                    reportText = "Parameter not specified: " + strParameter;
                    log.Debug(reportText);
                    return;
                }

                string strBoxModel;
                strParameter = "BOX_MODEL";
                status = testParameter.getParameterAsString(seqContext.Step.Name, strParameter, out strBoxModel);//param2
                if (status != 0)
                {
                    reportText = "Parameter not specified: " + strParameter;
                    log.Debug(reportText);
                    return;
                }

                string strInputDAtaTypes;
                strParameter = "INPUT_DATA_TYPES";
                status = testParameter.getParameterAsString(seqContext.Step.Name, strParameter, out strInputDAtaTypes);//param3
                if (status != 0)
                {
                    reportText = "Parameter not specified: " + strParameter;
                    log.Debug(reportText);
                    return;
                }

                string strPkiDataTypes;
                strParameter = "PKI_DATA_TYPES";
                status = testParameter.getParameterAsString(seqContext.Step.Name, strParameter, out strPkiDataTypes);//param4
                if (status != 0)
                {
                    reportText = "Parameter not specified: " + strParameter;
                    log.Debug(reportText);
                    return;
                }

                string strOutputDataTypes;
                strParameter = "OUTPUT_DATA_TYPES";
                status = testParameter.getParameterAsString(seqContext.Step.Name, strParameter, out strOutputDataTypes);//param5
                if (status != 0)
                {
                    reportText = "Parameter not specified: " + strParameter;
                    log.Debug(reportText);
                    return;
                }

                string strParam6List;
                testParameter.getParameterAsString(seqContext.Step.Name, "PARAM_6_LIST", out strParam6List, "");//param6

                string strMsftTlvTypes;
                testParameter.getParameterAsString(seqContext.Step.Name, "MSFT_TLV_TYPES", out strMsftTlvTypes, "");//param7 MSFT data

                string strParam8List;
                testParameter.getParameterAsString(seqContext.Step.Name, "PARAM_8_LIST", out strParam8List, "");//param8

                string strParam9List;
                testParameter.getParameterAsString(seqContext.Step.Name, "PARAM_9_LIST", out strParam9List, "");//param9

                string strCaseIdList;
                testParameter.getParameterAsString(seqContext.Step.Name, "CASE_ID_LIST", out strCaseIdList, "");//Used to add CASE_ID to OutputStr if wanted

                string strProductionDataFile;
                testParameter.getParameterAsString(seqContext.Step.Name, "PRODUCTION_DATA", out strProductionDataFile, "");

                string strConfigParamFile;
                testParameter.getParameterAsString(seqContext.Step.Name, "CONGIG_PARAM", out strConfigParamFile, "");

                int iSecureConfigParamFile;
                testParameter.getParameterAsInt(seqContext.Step.Name, "SECURE_CONGIG_PARAM", out iSecureConfigParamFile, 0);

                int iSaveXmlDebugFiles;
                testParameter.getParameterAsInt(seqContext.Step.Name, "SAVE_XML_DEBUG", out iSaveXmlDebugFiles, 0);

                string strIgnoreUnitInfoHdcpWhenUsingPKI;
                testParameter.getParameterAsString(seqContext.Step.Name, "IGNORE_UNIT_INFO_HDCP_WHEN_USING_PKI", out strIgnoreUnitInfoHdcpWhenUsingPKI, "");

                string strUseHDCPIfExist;
                testParameter.getParameterAsString(seqContext.Step.Name, "USE_HDCP_IF_EXIST", out strUseHDCPIfExist, "");

                int iOverridePingTimeout;
                testParameter.getParameterAsInt(seqContext.Step.Name, "OVERRIDE_PING_TIMEOUT", out iOverridePingTimeout, 0);

                char[] sep = { ',' };
                string[] strArrayCagInputParam = strCagInputParamList.Split(sep);

                string HardwareRevision = string.Empty;
                string Vendor = string.Empty;
                string CustomerParam = string.Empty;
                string BoxModel = string.Empty;
                string ManufacturerId = string.Empty;
                string MkFile = string.Empty;
                string KreaTV_Ek = string.Empty;

                CommonCS.CCagInputParam CagInputParam = new CommonCS.CCagInputParam("");
                CommonCS.CCagOutputParam CagOutputParam;

                string NetworkPath = string.Empty;
                string NetworkFile = string.Empty;
                string LocalFile = string.Empty;
                string FileName = string.Empty;
                string OutputStr = string.Empty;

                string Motoplyr_License_Template = string.Empty;

                bool EncryptHdcp = false;
                bool DontEncryptHdcp = false;

                FileStream fileStream = null;
                int length = 0;

                CagOutputParam.TLVDataOK = false;
                CagOutputParam.HelloOK = false;
                CagInputParam.RequiredCagVersion = "1.0.0";
                CagInputParam.PlatFormId = "1";
                CagInputParam.NoPKI = false;

                Vendor = strArrayCagInputParam[0];
                Vendor.ToLower();

                if (strArrayCagInputParam.Length > 1)
                {
                    CagInputParam.ChipType = strArrayCagInputParam[1];
                }
                if (strArrayCagInputParam.Length > 2)
                {
                    CagInputParam.PlatFormId = strArrayCagInputParam[2];
                }
                if (strArrayCagInputParam.Length > 3)
                {
                    CagInputParam.FormatId = strArrayCagInputParam[3];
                }
                if (strArrayCagInputParam.Length > 4)
                {
                    HardwareRevision = strArrayCagInputParam[4];
                }
                if (strArrayCagInputParam.Length > 5)
                {
                    CustomerParam = strArrayCagInputParam[5];
                }

                CagInputParam.InputDataTypes = strInputDAtaTypes;
                CagInputParam.PkiDataTypes = strPkiDataTypes;
                CagInputParam.OutputDataTypes = strOutputDataTypes;

                if (strParam6List.Length > 0)
                {
                    string[] strArrayParam6 = strParam6List.Split(sep);

                    MkFile = strArrayParam6[0];
                    if (strArrayParam6.Length > 1)
                        KreaTV_Ek = strArrayParam6[1];

                    if (strArrayParam6.Length > 2)
                    {
                        if (strArrayParam6[2] == "DontEncryptHdcp")
                        {
                            DontEncryptHdcp = true;
                        }
                        else if (strArrayParam6[2] == "EncryptHdcp")
                        {
                            EncryptHdcp = true;
                        }
                        else
                        {
                            Motoplyr_License_Template = strArrayParam6[2];
                        }
                    }
                    if (strArrayParam6.Length > 3)
                    {
                        if (strArrayParam6[3] == "NoPKI")
                        {
                            CagInputParam.NoPKI = true;
                        }
                    }
                }

                NetworkPath = m_UnitInfo.NetworkPath;
                if (NetworkPath == null || NetworkPath.Length == 0)
                {
                    log.Debug("NetworkPath not specified in UnitInfo");
                }

                // TODO: evaluate if this is the proper handling, or if it always should use UnitInfo data for this.
                // If so then it should probably be moved to the switch for where the data is first used.
                if (m_UnitInfo.HardwareRevision != null && m_UnitInfo.HardwareRevision.Length > 0)
                {
                    HardwareRevision = m_UnitInfo.HardwareRevision;
                }
                else
                {
                    log.DebugFormat("HardwareRevision not found in UnitInfo, HardwareRevison='{0}'.", HardwareRevision);
                }

                log.Debug("Creating Secure USP area for " + strBoxModel);

                CagInputParam.HpnaProduct = false;
                CagInputParam.MasterKeyLen = 0;
                CagInputParam.GlobalEkLen = 0;
                CagInputParam.MotoplyrLicenseTemplateLen = 0;
                CagInputParam.LCID = "";

                if (CagInputParam.PkiDataTypes.Contains("0x116"))
                {
                    string strHdcpKey = m_UnitInfo.HdcpKey;

                    if (strHdcpKey == null || strHdcpKey.Length == 0)
                    {
                        result = -91;
                        reportText = String.Format("{0}: Error: HDCP Key not found in UnitInfo.", result);
                        log.Debug(reportText);
                        return;
                    }

                    if (strHdcpKey.Length != 0)
                    {
                        if (strIgnoreUnitInfoHdcpWhenUsingPKI.Contains("IgnoreUnitInfoHdcpWhenUsingPKI"))
                        {
                            reportText = ("Ignoring UnitInfo HDCP key\n");
                            log.Debug(reportText);
                        }
                        else
                        {
                            reportText = "HDCP key found in UnitInfo and PKI HDCP key requested. Please remove HDCP key from SFS database!";
                            log.Debug(reportText);
                            result = -92;
                            return;
                        }
                    }
                    if (DontEncryptHdcp)
                    {
                        CagInputParam.KreaTvHdcpKey = "02";
                    }
                }

                string[] strArrayInputDataTypes = CagInputParam.InputDataTypes.Split(sep);

                FileInfo fi;

                TLV.Type currType;

                for (int i = 0; i < strArrayInputDataTypes.Length; i++)
                {
                    currType = (TLV.Type)Convert.ToInt32(strArrayInputDataTypes[i], 16);

                    switch (currType)
                    {
                        case TLV.Type.MODEL_ID://TLV::MODEL_ID:
                            if (m_UnitInfo.Model != null && m_UnitInfo.Model.Length > 0)
                            {
                                CagInputParam.ModelId = m_UnitInfo.Model;//m_UnitInfo->Model;
                                log.Debug("Model = " + CagInputParam.ModelId + " ,uutInstance = " + m_iUutInstance);
                            }
                            else
                            {
                                result = -71;
                                reportText = "Model not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.HARDWARE_REVISION://TLV::HARDWARE_REVISION:
                            CagInputParam.HardwareRevision = HardwareRevision;
                            log.Debug("HardwareRevision = " + CagInputParam.HardwareRevision + " ,uutInstance = " + m_iUutInstance);
                            break;

                        case TLV.Type.SERIAL_NUMBER://TLV::SERIAL_NUMBER:
                            if (m_UnitInfo.SerialNumber != null && m_UnitInfo.SerialNumber.Length > 0)
                            {
                                CagInputParam.SerialNumber = m_UnitInfo.SerialNumber;//m_UnitInfo->SerialNumber;
                                log.Debug("SerialNo = " + CagInputParam.SerialNumber + " ,uutInstance = " + m_iUutInstance);
                            }
                            else
                            {
                                result = -72;
                                reportText = "SerialNumber not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.PCB_SERIAL_NUMBER:
                            if (m_UnitInfo.PcbSerialNumber != null && m_UnitInfo.PcbSerialNumber.Length > 0)
                            {
                                CagInputParam.PcbSerialNumber = m_UnitInfo.PcbSerialNumber;
                                log.Debug("PcbSerialNo = " + CagInputParam.PcbSerialNumber + " ,uutInstance = " + m_iUutInstance);
                            }
                            else
                            {
                                result = -73;
                                reportText = "PcbSerialNo not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.FG_PART_NUMBER://TLV::FG_PART_NUMBER:
                            if (m_UnitInfo.TopBom != null && m_UnitInfo.TopBom.Length > 0)
                            {
                                CagInputParam.FgPartNumber = m_UnitInfo.TopBom;
                                log.Debug("TopBom = " + CagInputParam.FgPartNumber + " ,uutInstance = " + m_iUutInstance);
                            }
                            else
                            {
                                result = -74;
                                reportText = "TopBom not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.ETHERNET_MAC_ADDRESS_1://TLV::ETHERNET_MAC_ADDRESS_1:
                            if (m_UnitInfo.MacAddress != null && m_UnitInfo.MacAddress.Length > 0)
                            {
                                CagInputParam.EthernetMac1 = m_UnitInfo.MacAddress;//m_UnitInfo->MacAddress;
                                log.Debug("MacAddress = " + CagInputParam.EthernetMac1 + " ,uutInstance = " + m_iUutInstance);
                                CagInputParam.EthernetMac1 = CagInputParam.EthernetMac1.Replace(":", "");
                            }
                            else
                            {
                                result = -75;
                                reportText = "MacAddress not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.ETHERNET_MAC_ADDRESS_2:
                            if (m_UnitInfo.WiFiMacAddress != null && m_UnitInfo.WiFiMacAddress.Length > 0)
                            {
                                CagInputParam.EthernetMac2 = m_UnitInfo.WiFiMacAddress;
                                log.Debug("WiFiMacAddress = " + CagInputParam.EthernetMac2 + " ,uutInstance = " + m_iUutInstance);
                                CagInputParam.EthernetMac2 = CagInputParam.EthernetMac2.Replace(":", "");
                            }
                            else
                            {
                                result = -76;
                                reportText = "WiFiMacAddress not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.BLUETOOTH_MAC_ADDRESS:
                            if (m_UnitInfo.BluetoothMac != null && m_UnitInfo.BluetoothMac.Length > 0)
                            {
                                CagInputParam.BluetoothMac = m_UnitInfo.BluetoothMac;
                                log.Debug("BluetoothMac = " + CagInputParam.BluetoothMac + " ,uutInstance = " + m_iUutInstance);
                                CagInputParam.BluetoothMac = CagInputParam.BluetoothMac.Replace(":", "");
                            }
                            else
                            {
                                result = -79;
                                reportText = "BluetoothMac not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.HPNA_MAC_ADDRESS_1:
                            if (m_UnitInfo.HpnaMacAddress != null && m_UnitInfo.HpnaMacAddress.Length > 0)
                            {
                                CagInputParam.HpnaMac1 = m_UnitInfo.HpnaMacAddress;
                                log.Debug("HpnaMacAddress = " + CagInputParam.HpnaMac1 + " ,uutInstance = " + m_iUutInstance);
                                CagInputParam.HpnaMac1 = CagInputParam.HpnaMac1.Replace(":", "");
                            }
                            else
                            {
                                result = -77;
                                reportText = "HpnaMacAddress not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.GUID_1:
                            if (m_UnitInfo.GUID != null && m_UnitInfo.GUID.Length > 0)
                            {
                                CagInputParam.Guid1 = m_UnitInfo.GUID;//m_UnitInfo->GUID;
                                log.Debug("GUID_1 = " + CagInputParam.Guid1 + " ,uutInstance = " + m_iUutInstance);
                            }
                            else
                            {
                                result = -78;
                                reportText = "GUID not found in UnitInfo.";
                                log.Error(reportText);
                                return;
                            }
                            break;

                        case TLV.Type.HPNA_PRODUCT:
                            CagInputParam.HpnaProduct = true;
                            break;

                        case TLV.Type.CUSTOM_SERIAL_NUMBER://TLV::CUSTOM_SERIAL_NUMBER:
                            CagInputParam.CustomerSerial = m_UnitInfo.CustomerSerial;//m_UnitInfo->CustomerSerial;
                            log.Debug("CustomerSerial = " + CagInputParam.CustomerSerial + " ,uutInstance = " + m_iUutInstance);

                            if (CustomerParam.Contains("Telefonica"))
                            {
                                CustomerParam.Remove(0, ("Telefonica").Length);

                                if (!CheckTelefonicaSerial(CustomerParam, CagInputParam.SerialNumber, CagInputParam.CustomerSerial))
                                {
                                    reportText = "1:CustomerSerial not a valid Telefonica serial number";
                                    log.Debug(reportText);
                                    result = -1;
                                    return;
                                }
                            }
                            break;

                        case TLV.Type.VIACCESS_SERIALNUMBER://TLV::VIACCESS_SERIALNUMBER:
                            if (m_UnitInfo.CustomerSerial != null && m_UnitInfo.CustomerSerial.Length == 24)
                            {
                                // TODO: verify this is proper behavior!?
                                CagInputParam.ViaccessSerial = CagInputParam.CustomerSerial;// m_UnitInfo->CustomerSerial;
                                log.Debug("CustomerSerial = " + CagInputParam.CustomerSerial + " ,uutInstance = " + m_iUutInstance);
                            }
                            else
                            {
                                CagInputParam.ViaccessSerial = "generate";
                            }
                            break;

                        case TLV.Type.KREATV_PRODUCTION_PARAMETER://TLV::KREATV_PRODUCTION_PARAMETER:

                            if (strProductionDataFile == "")
                            {
                                if (m_UnitInfo.ProductionData != null && m_UnitInfo.ProductionData.Length > 0)
                                {
                                    strProductionDataFile = m_UnitInfo.ProductionData;
                                }
                                else
                                {
                                    result = -61;
                                    reportText = String.Format("{0}: Error: ProductionData not specified in UnitInfo!", result);
                                    log.Error(reportText);
                                    return;
                                }
                            }

                            NetworkFile = NetworkPath + "ProductionData\\" + strProductionDataFile + ".xml";
                            LocalFile = m_sNfsShare + "Shared\\ProductionData\\" + strProductionDataFile + ".xml";
                            log.Debug("Reading ProductionData: " + LocalFile);

                            fi = new FileInfo(LocalFile);
                            if (!fi.Exists)
                            {
                                System.IO.File.Copy(NetworkFile, LocalFile, true);

                                fi = new FileInfo(LocalFile);
                                if (!fi.Exists)
                                {
                                    reportText = "2:Failed to copy file from:" + NetworkFile + "\r\nTo:\r\n" + LocalFile + "\r\n";
                                    log.Debug(reportText);
                                    result = -2;
                                    return;
                                }
                            }

                            try
                            {
                                XmlDocument XMLDom = new XmlDocument();
                                XMLDom.PreserveWhitespace = false; // do not load with whitespace included
                                XMLDom.Load(LocalFile);

                                if (XMLDom == null)
                                {
                                    reportText = "3:Could not open production data XML file: " + LocalFile;
                                    log.Debug(reportText);
                                    result = -3;
                                    return;
                                }
                                else
                                {
                                    XmlNode newXMLNode = XMLDom.SelectSingleNode("/ProductionData/PCBVersion");
                                    if (newXMLNode != null)
                                    {
                                        if (m_UnitInfo.PcbVersion != null && m_UnitInfo.PcbVersion.Length > 0)
                                        {
                                            newXMLNode.InnerText = m_UnitInfo.PcbVersion;
                                            log.Debug("Adding: PcbVersion data: " + newXMLNode.InnerText);
                                        }
                                        else
                                        {
                                            result = -4;
                                            reportText = String.Format("{0}: Error: PcbVersion not specified in UnitInfo.", result);
                                            log.Debug(reportText);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        log.Debug("PCBVersion not included in the ProductionData XML file");
                                    }

                                    newXMLNode = XMLDom.SelectSingleNode("/ProductionData/ProductNumber");
                                    if (newXMLNode != null)
                                    {
                                        if (m_UnitInfo.PcbaBom != null && m_UnitInfo.PcbaBom.Length > 0)
                                        {
                                            newXMLNode.InnerText = m_UnitInfo.PcbaBom;
                                            log.Debug("Adding: PcbaBom data: " + newXMLNode.InnerText);
                                        }
                                        else
                                        {
                                            result = -5;
                                            reportText = String.Format("{0}: Error: PcbaBom not specified in UnitInfo.", result);
                                            log.Debug(reportText);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        log.Debug("ProductNumber not included in the ProductionData XML file");
                                    }

                                    newXMLNode = XMLDom.SelectSingleNode("/ProductionData/BoxVersion");
                                    if (newXMLNode != null)
                                    {
                                        if (m_UnitInfo.BoxBom != null && m_UnitInfo.BoxBom.Length > 0)
                                        {
                                            newXMLNode.InnerText = m_UnitInfo.BoxBom;
                                            log.Debug("Adding: BoxBom data: " + newXMLNode.InnerText);
                                        }
                                        else
                                        {
                                            result = -6;
                                            reportText = String.Format("{0}: Error: BoxBom not specified in UnitInfo.", result);
                                            log.Debug(reportText);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        log.Debug("BoxVersion not included in the ProductionData XML file");
                                    }

                                    newXMLNode = XMLDom.SelectSingleNode("/ProductionData/TopVersion");
                                    if (newXMLNode != null)
                                    {
                                        if (m_UnitInfo.TopBom != null && m_UnitInfo.TopBom.Length  > 0)
                                        {
                                            newXMLNode.InnerText = m_UnitInfo.TopBom;
                                            log.Debug("Adding: TopBom data: " + newXMLNode.InnerText);
                                        }
                                        else
                                        {
                                            result = -7;
                                            reportText = String.Format("{0}: Error: TopBom not specified in UnitInfo.", result);
                                            log.Debug(reportText);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        log.Debug("TopVersion not included in the ProductionData XML file");
                                    }

                                    newXMLNode = XMLDom.SelectSingleNode("/ProductionData/BoardType");
                                    if (newXMLNode != null)
                                    {
                                        if (m_UnitInfo.HardwareRevision != null && m_UnitInfo.HardwareRevision.Length > 0)
                                        {
                                            newXMLNode.InnerText = m_UnitInfo.HardwareRevision;
                                            log.Debug("Adding: HardwareRevision data: " + newXMLNode.InnerText);
                                        }
                                        else
                                        {
                                            result = -8;
                                            reportText = String.Format("{0}: Error: HardwareRevision not specified in UnitInfo.", result);
                                            log.Debug(reportText);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        log.Debug("BoardType not included in the ProductionData XML file");
                                    }
                                }

                                CagInputParam.ProductionData = XmlAutoIndent(XMLDom); // Autoindent, if load without whitespace

                                if (iSaveXmlDebugFiles == 1)
                                {
                                    string path = m_sNfsShare + "Shared\\ProductionData\\" + strProductionDataFile + "_debug.xml";
                                    using (StreamWriter outfile = new StreamWriter(path))
                                    {
                                        outfile.Write(CagInputParam.ProductionData);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                reportText = "88:Exception occured while updating ProductionData XML file: " + ex.Message;
                                log.Debug(reportText);
                                result = -88;
                                return;
                            }

                            break;

                        case TLV.Type.KREATV_CONFIGURATION_PARAMETER://TLV::KREATV_CONFIGURATION_PARAMETER:

                            if (strConfigParamFile == "")
                            {
                                if (m_UnitInfo.ConfigParam != null && m_UnitInfo.ConfigParam.Length > 0)
                                {
                                    strConfigParamFile = m_UnitInfo.ConfigParam;
                                }
                                else
                                {
                                    result = -62;
                                    reportText = String.Format("{0}: Error: ConfigParam not specified in UnitInfo.", result);
                                    log.Debug(reportText);
                                    return;
                                }
                            }

                            NetworkFile = NetworkPath + "ConfigParam\\" + strConfigParamFile + ".xml";
                            LocalFile = m_sNfsShare + "Shared\\ConfigParam\\" + strConfigParamFile + ".xml";
                            log.Debug("Reading ConfigParam: " + LocalFile);

                            fi = new FileInfo(LocalFile);
                            if (!fi.Exists)
                            {
                                System.IO.File.Copy(NetworkFile, LocalFile, true);

                                fi = new FileInfo(LocalFile);
                                if (!fi.Exists)
                                {
                                    reportText = "9:Failed to copy file from:" + NetworkFile + "\r\nTo:\r\n" + LocalFile + "\r\n";
                                    log.Debug(reportText);
                                    result = -9;
                                    return;
                                }
                            }

                            try

                            {
                                if (iSecureConfigParamFile == 0)
                                {
                                    XmlDocument XMLDomConfig = new XmlDocument();
                                    XMLDomConfig.PreserveWhitespace = true; // load with whitespace included
                                    XMLDomConfig.Load(LocalFile);

                                    if (XMLDomConfig == null)
                                    {
                                        reportText = "101:Could not open config data XML file: " + LocalFile;
                                        log.Debug(reportText);
                                        result = -101;
                                        return;
                                    }
                                }

                                fileStream = new FileStream(LocalFile, FileMode.Open, FileAccess.Read);
                                length = (int)fileStream.Length;

                                if ((fileStream == null) || (length > CagInputParam.ConfigParam.Length))
                                {
                                    reportText = "102:Failed to open config param XML file: " + LocalFile;
                                    log.Debug(reportText);
                                    result = -102;
                                    return;
                                }

                                byte[] byteArray = new byte[length];

                                int lenRead = fileStream.Read(byteArray, 0, length);

                                if (length != lenRead) // Check that the number of read bytes matches the length of the file
                                {
                                    reportText = "103:Failed to read config param XML file. Length read: " + lenRead + " Expected: " + length;
                                    log.Debug(reportText);
                                    result = -103;
                                    return;
                                }
                                fileStream.Close();
                                fileStream = null;

                                for (int j = 0; j < length; j++)
                                    CagInputParam.ConfigParam[j] += byteArray[j];

                                CagInputParam.ConfigParamLen = length;

                                log.DebugFormat("fileStream.Length={0}, byteArray.Length={1}, CagInputParam.ConfigParam.Length={2}", length, byteArray.Length, CagInputParam.ConfigParam.Length);

                                if (iSaveXmlDebugFiles == 1)
                                {
                                    string path = m_sNfsShare + "Shared\\ConfigParam\\" + strConfigParamFile + "_debug.xml";

                                    using (FileStream fsDebug = new FileStream(path, FileMode.Create, FileAccess.Write))
                                    {
                                        fsDebug.Write(byteArray, 0, length);
                                    }
                                }
                            }

                            catch (Exception ex)
                            {
                                reportText = "11:Exception occured while loading ConfigParam data XML file: " + ex.Message;
                                log.Debug(reportText);
                                result = -11;
                                return;
                            }

                            break;

                        case TLV.Type.KREATV_MASTER_KEY://TLV::KREATV_MASTER_KEY:

                            FileName = m_sNfsShare + "Shared\\Keys\\" + MkFile;
                            log.Debug("Reading MK: " + FileName);

                            fileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read);//hFile = CreateFile(FileName, GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,0);
                            length = (int)fileStream.Length;//len = GetFileSize(hFile, NULL);
                            if (fileStream == null || length > CagInputParam.MasterKey.Length)//if ((hFile == INVALID_HANDLE_VALUE) || (len > sizeof(CagInputParam.MasterKey)))
                            {
                                reportText = "12:Failed to open MK file" + FileName;
                                log.Debug(reportText);
                                result = -12;
                                return;
                            }

                            byte[] buff = new byte[length];

                            int lengthRead = fileStream.Read(buff, 0, length);

                            if (length != lengthRead) // Check that the number of read bytes matches the length of the file
                            {
                                reportText = "13:Failed to read MK file. Length read: " + lengthRead + " Expected: " + length;
                                log.Debug(reportText);
                                result = -13;
                                return;
                            }
                            fileStream.Close();//hFile=INVALID_HANDLE_VALUE;
                            fileStream = null;

                            for (int j = 0; j < length; j++)
                                CagInputParam.MasterKey[j] += buff[j];

                            CagInputParam.MasterKeyLen = length;//len;

                            break;

                        case TLV.Type.KREATV_ENCRYPTION_KEY://TLV::KREATV_ENCRYPTION_KEY:

                            FileName = m_sNfsShare + "Shared\\Keys\\" + KreaTV_Ek;
                            log.Debug("Reading EK: " + FileName);

                            fileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read);//hFile = CreateFile(FileName, GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,0);
                            length = (int)fileStream.Length;//len = GetFileSize(hFile, NULL);
                            if (fileStream == null || length > CagInputParam.GlobalEk.Length)//if ((hFile == INVALID_HANDLE_VALUE) || (len > sizeof(CagInputParam.GlobalEk)))
                            {
                                reportText = "14:Failed to open EK file" + FileName;
                                log.Debug(reportText);
                                result = -14;
                                return;
                            }

                            buff = new byte[length];

                            lengthRead = fileStream.Read(buff, 0, length);

                            if (length != lengthRead) // Check that the number of read bytes matches the length of the file
                            {
                                reportText = "15:Failed to read EK file. Length read: " + lengthRead + " Expected: " + length;
                                log.Debug(reportText);
                                result = -15;
                                return;
                            }
                            fileStream.Close();//hFile=INVALID_HANDLE_VALUE;
                            fileStream = null;

                            for (int j = 0; j < length; j++)
                                CagInputParam.GlobalEk[j] += buff[j];

                            CagInputParam.GlobalEkLen = length;// len;

                            break;

                        case TLV.Type.KREATV_HDCP_KEY://TLV::KREATV_HDCP_KEY:

                            string strHdcpKey = string.Empty;

                            if (m_UnitInfo.HdcpKey != null)
                            {
                                strHdcpKey = m_UnitInfo.HdcpKey;
                            }
                            else
                            {
                                result = -81;
                                reportText = String.Format("{0}: Error: HdcpKey not specified in UnitInfo.", result);
                                log.Debug(reportText);
                                return;
                            }

                            //bUseHDCPIfExist = strArrayInputDataTypes[i].Contains("UseHDCPIfExist");

                            if (strHdcpKey.Length == 0)
                            {
                                if (strUseHDCPIfExist.Contains("UseHDCPIfExist"))
                                {
                                    reportText = "82:UnitInfo must contain a HdcpKey from the database";
                                    log.Debug(reportText);
                                    result = -82;
                                    return;
                                }
                            }

                            if ((strHdcpKey.Length != 608) && (strHdcpKey.Length != 0))
                            {
                                reportText = "83:HdcpKey (from the Database/UnitInfo) must be exactly 608 bytes";
                                log.Debug(reportText);
                                result = -83;
                                return;
                            }
                            CagInputParam.KreaTvHdcpKey = strHdcpKey;

                            if (EncryptHdcp)
                            {
                                CagInputParam.KreaTvHdcpKey += "01";
                            }

                            break;

                        case TLV.Type.VENDOR_SPECIFIC_DATA://TLV::VENDOR_SPECIFIC_DATA:

                            if (Vendor == "selfsign")
                            {
                                log.Debug("VendorSpecificData: selfsign");
                            }

                            else if (Vendor == "conax")
                            {
                                log.Debug("Signing USP with RSA.");
                                CagInputParam.VendorSpecificData = Vendor + ":" + "rsasign";
                            }

                            else if (Vendor == "verimatrix")
                            {
                                log.Debug("VendorSpecificData: " + Vendor);
                                CagInputParam.VendorSpecificData = Vendor;
                            }

                            else if (Vendor == "irdeto")
                            {
                                log.Debug("VendorSpecificData: " + Vendor);
                                CagInputParam.VendorSpecificData = Vendor;
                            }

                            else
                            {
                                log.Debug("VendorSpecificData: " + Vendor);
                                result = -84;
                                return;
                            }

                            break;

                        case TLV.Type.HW_CONFIG_STRING:
                        case TLV.Type.BCM_HW_CFG_STR:
                            if (strParam8List.Length>0)
                              {
                                  CagInputParam.HwConfigString = strParam8List;
                              }
                              else
                              {
                                  reportText = "24:HW_CONFIG_STRING Rcvd without Param8";
                                  log.Debug(reportText);
                                  return;
                              }
                            break;
                        case TLV.Type.MOTOPLYR_LICENSE_TEMPLATE:

                            FileName = m_sNfsShare + "Shared\\Keys\\" + Motoplyr_License_Template;
                            log.Debug("Reading MotoPLYR License template file: " + FileName);

                            fileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read);//hFile = CreateFile(FileName, GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,0);                     
                            length = (int)fileStream.Length;//len = GetFileSize(hFile, NULL); 

                            if ((fileStream == null) || (length > CagInputParam.MotoplyrLicenseTemplate.Length))
                            {
                                reportText = "25:Failed to open MotoPLYR License template file: " + FileName;
                                log.Debug(reportText);
                                result = -25;
                                return;
                            }

                            buff = new byte[length];

                            lengthRead = fileStream.Read(buff, 0, length);

                            if (length != lengthRead) // Check that the number of read bytes matches the length of the file
                            {
                                reportText = "26:Failed to read MotoPLYR License template file. Length read: " + lengthRead + " Expected: " + length;
                                log.Debug(reportText);
                                result = -26;
                                return;
                            }
                            fileStream.Close();//hFile=INVALID_HANDLE_VALUE;
                            fileStream = null;

                            for (int j = 0; j < length; j++)
                                CagInputParam.MotoplyrLicenseTemplate[j] += buff[j];

                            CagInputParam.MotoplyrLicenseTemplateLen = length;

                            break;
                        case TLVType.Type.IRDETO_PRIVATE_DATA:
                            // Tell ConfigAgent to generate the random part of private data.
                            CagInputParam.IrdetoPrivateData = "1"; // Should be set to something different from empty string.
                            log.Debug("Requesting to generate IrdetoPrivateData");
                            break;
                        default:
                            break;
                    }
                }//for strArrayInputDataTypes.Length

                if (strMsftTlvTypes.Length >= 1)
                {
                    string[] strArrayMsftTlvTypes = strMsftTlvTypes.Split(',');

                    for (int i = 0; i < strArrayMsftTlvTypes.Length; i++)
                    {
                        //CStringArray SplitValuesType;
                        //(Split(SplitValuesType, SplitTLVs[i], "=") );
                        //sscanf(SplitValuesType[0],"0x%X", &currType);
                        string[] strMsftTlvValue = strArrayMsftTlvTypes[i].Split('=');

                        currType = (TLV.Type)Convert.ToInt32(strMsftTlvValue[0], 16);

                        switch (currType)
                        {
                            case TLV.Type.DISCOVERY_SERVER_URL:
                                CagInputParam.DiscoveryServerURL = strMsftTlvValue[1];//SplitValuesType[1];
                                break;
                            case TLV.Type.VIDEO_FORMAT_CAPABILITY:
                                CagInputParam.VideoFormatCapability = strMsftTlvValue[1];//SplitValuesType[1];
                                break;
                            case TLV.Type.VIDEO_FORMAT_DEFAULT:
                                CagInputParam.VideoFormatDefault = strMsftTlvValue[1];//SplitValuesType[1];
                                break;
                            case TLV.Type.CUSTOMER_ID:
                                CagInputParam.CustomerId = strMsftTlvValue[1];//SplitValuesType[1];
                                break;
                            case TLV.Type.BCM_DIAG_BOOT_OFFSET:
                                CagInputParam.BCMDiagOffset = strMsftTlvValue[1];//SplitValuesType[1];
                                break;
                            case TLV.Type.LICENSE_CONTROL_ID:
                                CagInputParam.LCID = strMsftTlvValue[1];//SplitValuesType[1];
                                break;
                            default:
                                reportText = string.Format("27:Unhandled TLV type Rcvd in param7 = 0x{0}", currType);
                                log.Debug(reportText);
                                return;
                        }
                    }
                }
  
                // Change to parse param 9 for comma separated strings for MSFT data support

                //NoOfTypes = Split(SplitTLVs, pTestCaseParam->Param[9], "|");
                //if ( NoOfTypes>= 1)

                if (strParam9List.Length >= 1)
                {
                    string[] strArrayParam9 = strParam9List.Split('|');

                    log.DebugFormat("No. of TLV in Param9 = {0}", strArrayParam9.Length);

                    for (int i = 0; i < strArrayParam9.Length; i++)
                    {
                        //CStringArray SplitValuesType;
                        //(Split(SplitValuesType, SplitTLVs[i], "=") );
                        //sscanf(SplitValuesType[0],"0x%X", &currType);
                        string[] strParam9Value = strArrayParam9[i].Split('=');

                        currType = (TLV.Type)Convert.ToInt32(strParam9Value[0], 16);

                        log.DebugFormat("currType = 0x{0}, {1} ", currType, strArrayParam9[i]);

                        switch (currType)
                        {
                            case TLV.Type.CUSTOMER_ID_IR_RCU:
                                CagInputParam.CustomerIdIRRCU = strParam9Value[1];
                                break;
                            case TLV.Type.IR_PROTOCOL:
                                CagInputParam.IrProtocol = strParam9Value[1];
                                break;
                            case TLV.Type.CUSTOMER_ID_RF_RCU:
                                CagInputParam.CustomerIdRFRCU = strParam9Value[1];
                                break;
                            case TLV.Type.BOOT_LOADER_MESSAGE:
                                CagInputParam.BootLoaderMessage = strParam9Value[1];
                                break;
                            default:
                                reportText = string.Format("28:Unhandled TLV type Rcvd in param9 = 0x{0}", currType);
                                log.Debug(reportText);
                                return;
                        }
                    }
                }

               if(m_ConfigurationStation.ActivatePosition(ref CagInputParam, iTimeout, m_sClientIpAddress))
                    log.Info("ConfigStationActivatePosition OK!");
                else
                {
                    log.Error("ConfigStationActivatePosition FAILED!");
                    log.Error("Please check the PKI server settings!");
                }

                log.Debug("Starting ConfigAgent...");

                string strReadBuffer = string.Empty;

                int iOldPingTimeout = m_iPingTimeout;
                if (iOverridePingTimeout > 0)
                {
                    m_iPingTimeout = iOverridePingTimeout;
                    log.DebugFormat("Overriding ping timeout ({0} ms)", iOverridePingTimeout);
                }

                int iClientPort = m_ConfigurationStation.GetClientPort();
                log.DebugFormat("Using client port {0} for config agent.", iClientPort);
                string command;
                if (iClientPort == 0xC000)
                {
                    command = "ConfigAgent " + m_sNetworkIpAddress;
                }
                else
                {
                    command = String.Format("ConfigAgent {0} 0x{1}", m_sNetworkIpAddress, Convert.ToString(iClientPort, 16));
                }

                status = SendReceive(command, "/ #", iTimeout, out strReadBuffer);
                m_iPingTimeout = iOldPingTimeout; // Restore ping timeout.
                if (status != 0 || !strReadBuffer.Contains("***** Box Configured SUCCESSFULLY *****"))
                {
                    reportText = "29:Factory Config Agent failed";
                    log.Debug(reportText);
                    reportText += ", Received: " + strReadBuffer;
                    result = -29;
                    return;
                }

                if (m_ConfigurationStation.DeActivatePosition(out CagOutputParam, iTimeout))
                    log.Info("ConfigStationDeActivatePosition OK!");
                else
                    log.Error("ConfigStationDeActivatePosition FAILED!");

                // Config Station done, destroy handle...
                m_ConfigurationStation.Destroy();
                m_bConfigStationDestroyed = true;

                if (!CagOutputParam.TLVDataOK)
                {
                    reportText = "30:Data from ConfigAgent was not OK";
                    log.Debug(reportText);
                    result = -30;
                    return;
                }

                if (CagOutputParam.HelloOK == false)
                {
                    reportText = "31:CAG is not OK";
                    log.Debug(reportText);
                    result = -31;
                    return;
                }

                string[] strArrayOutputDataTypes = new string[0];
                if (CagInputParam.OutputDataTypes.Length > 0)
                {
                    strArrayOutputDataTypes = CagInputParam.OutputDataTypes.Split(sep);
                }

                for (int j = 0; j < strArrayOutputDataTypes.Length; j++)
                {
                    currType = (TLV.Type)Convert.ToInt32(strArrayOutputDataTypes[j], 16);

                    switch (currType)
                    {
                        case TLV.Type.SERIAL_NUMBER://TLV::SERIAL_NUMBER:
                            log.Debug("CagOutputParam.SerialNumber = " + CagOutputParam.SerialNumber);
                            if (!CagInputParam.SerialNumber.Equals(CagOutputParam.SerialNumber))
                            {
                                reportText = "60:CagInput Serial Number did not match CagOutput.";
                                log.Debug(reportText);
                                result = -60;
                                return;
                            }
                            break;
                        case TLV.Type.ETHERNET_MAC_ADDRESS_1://TLV::ETHERNET_MAC_ADDRESS_1:
                            if (CagOutputParam.Mac.Length != 17)
                            {
                                reportText = "32:No MAC address";
                                log.Debug(reportText);
                                result = -32;
                                return;
                            }

                            log.Debug("MAC Address = " + CagOutputParam.Mac);

                            //m_UnitInfo->MacAddress = CagOutputParam.Mac;
                            OutputStr += "|MAC=" + CagOutputParam.Mac;
                            break;

                        case TLV.Type.MOTOROLA_UNIQUE_ADDRESS_1://TLV::MOTOROLA_UNIQUE_ADDRESS_1:
                            if ((CagOutputParam.MUA == "000000000000") || (CagOutputParam.MUA == ""))
                            {
                                reportText = "33:MUA is empty";
                                log.Debug(reportText);
                                result = -33;
                                return;
                            }

                            log.Debug("Motorola Unique Address = " + CagOutputParam.MUA);

                            OutputStr += "|MUA=" + CagOutputParam.MUA;
                            break;

                        case TLV.Type.VERIMATRIX_ID://TLV::VERIMATRIX_ID:
                            if (CagOutputParam.VerimatrixID == "")
                            {
                                reportText = "34:VerimatrixID is empty";
                                log.Debug(reportText);
                                result = -34;
                                return;
                            }

                            log.Debug("Verimatrix ID = " + CagOutputParam.VerimatrixID);

                            OutputStr += "|VerimatrixID=" + CagOutputParam.VerimatrixID;
                            break;

                        case TLV.Type.XPU_SERIAL_NUMBER://TLV::XPU_SERIAL_NUMBER:
                            if (CagOutputParam.XpuSerialNumber == "")
                            {
                                reportText = "35:FT Chip ID is empty";
                                log.Debug(reportText);
                                result = -35;
                                return;
                            }

                            log.Debug("FT Chip ID = " + CagOutputParam.XpuSerialNumber);

                            OutputStr += "|FTChipID=" + CagOutputParam.XpuSerialNumber;
                            break;

                        case TLV.Type.VIACCESS_SERIALNUMBER://TLV::VIACCESS_SERIALNUMBER:
                            if (CagOutputParam.ViaccessSerialNumber == "")
                            {
                                reportText = "36:Viaccess Serial Number is empty";
                                log.Debug(reportText);
                                result = -36;
                                return;
                            }

                            log.Debug("Viaccess Serial Number = " + CagOutputParam.ViaccessSerialNumber);

                            OutputStr += "|ViaccessSN=" + CagOutputParam.ViaccessSerialNumber;
                            break;

                        case TLV.Type.FTPDB_PUBLIC_SERIALNUMBER://TLV::FTPDB_PUBLIC_SERIALNUMBER:
                            if (CagOutputParam.FTPDBPublicSerialNumber == "")
                            {
                                reportText = "37:FTPDBPublic Serial Number is empty";
                                log.Debug(reportText);
                                result = -37;
                                return;
                            }

                            log.Debug("FT PDB Public Serial Number = " + CagOutputParam.FTPDBPublicSerialNumber);

                            OutputStr += "|FTPDBPublicSN=" + CagOutputParam.FTPDBPublicSerialNumber;
                            break;

                        case TLV.Type.HPNA_MAC_ADDRESS_1:
                            if (CagOutputParam.HpnaMac1.Length != 17)
                            {
                                reportText = "38:No HPNA MAC address";
                                log.Debug(reportText);
                                result = -38;
                                return;
                            }
                            log.Debug("HPNA MAC Address = " + CagOutputParam.HpnaMac1);

                            OutputStr += "|HPNAMAC=" + CagOutputParam.HpnaMac1;
                            break;

                        case TLV.Type.GUID_1:
                            if (CagOutputParam.Guid1 == "")
                            {
                                reportText = "39:No MS GUID";
                                log.Debug(reportText);
                                result = -39;
                                return;
                            }
                            log.Debug("MS GUID = " + CagOutputParam.Guid1);

                            OutputStr += "|GUID=" + CagOutputParam.Guid1;
                            break;

                        case TLV.Type.MOTOPLYR_LICENSE:
                            if (CagOutputParam.licenseFeatureList == "")
                            {
                                reportText = "40:No MotoPLYR License";
                                log.Debug(reportText);
                                result = -40;
                                return;
                            }
                            log.Debug("License FeatureList = " + CagOutputParam.licenseFeatureList);

                            OutputStr += "|LicenseFeatureList=" + CagOutputParam.licenseFeatureList;
                            break;

                        case TLV.Type.HDCP_KSV_CONFIG_DATA:
                            if (CagOutputParam.HDCP_KSV == "")
                            {
                                reportText = "41:No HDCP_KSV";
                                log.Debug(reportText);
                                result = -41;
                                return;
                            }
                            log.Debug("HDCP_KSV = " + CagOutputParam.HDCP_KSV);

                            OutputStr += "|HDCP_KSV=" + CagOutputParam.HDCP_KSV;
                            break;

                        case TLV.Type.MUA_ST231:
                            if (CagOutputParam.MUA_ST231 == "")
                            {
                                reportText = "42:No MUA_ST231";
                                log.Debug(reportText);
                                result = -42;
                                return;
                            }
                            log.Debug("MUA_ST231 = " + CagOutputParam.MUA_ST231);

                            OutputStr += "|MUA_ST231=" + CagOutputParam.MUA_ST231;
                            break;

                        case TLV.Type.SKM_PUBLIC_ID:
                            if (CagOutputParam.SKM_PUBLIC_ID == "")
                            {
                                reportText = "43:No SKM_PUBLIC_ID";
                                log.Debug(reportText);
                                result = -43;
                                return;
                            }
                            log.Debug("SKM_PUBLIC_ID = " + CagOutputParam.SKM_PUBLIC_ID);

                            OutputStr += "|SKM_PUBLIC_ID=" + CagOutputParam.SKM_PUBLIC_ID;
                            break;

                        case TLV.Type.MUA_RSA_V2:
                            if (CagOutputParam.MUA_RSA_V2 == "")
                            {
                                reportText = "44:No MUA_RSA_V2";
                                log.Debug(reportText);
                                result = -44;
                                return;
                            }
                            log.Debug("MUA_RSA_V2 = " + CagOutputParam.MUA_RSA_V2);

                            OutputStr += "|MUA_RSA_V2=" + CagOutputParam.MUA_RSA_V2;
                            break;

                        case TLV.Type.MUA_AGILUS:
                            if (CagOutputParam.MUA_AGILUS == "")
                            {
                                reportText = "45:No MUA_AGILUS";
                                log.Debug(reportText);
                                result = -45;
                                return;
                            }
                            log.Debug("MUA_AGILUS = " + CagOutputParam.MUA_AGILUS);

                            OutputStr += "|MUA_AGILUS=" + CagOutputParam.MUA_AGILUS;
                            break;

                        case TLVType.Type.IRDETO_PRIVATE_DATA:
                            if (CagOutputParam.IrdetoPrivateData == "")
                            {
                                result = -46;
                                reportText = "46:Error: No IRDETO_PRIVATE_DATA.";
                                log.Error(reportText);
                                return;
                            }
                            log.Debug("IrdetoPrivateData = " + CagOutputParam.IrdetoPrivateData);

                            OutputStr += "|IrdetoPrivateData=" + CagOutputParam.IrdetoPrivateData;
                            break;

                        default:
                            reportText = string.Format("50:Output type=0x{0} not supported ", currType.ToString());
                            log.Debug(reportText);
                            result = -50;
                            return;
                    }
                }

                if (OutputStr != "")
                {
                    if (strCaseIdList.Length > 1 && CagInputParam.SerialNumber != "") // If CASE_ID is wanted to be added to the string
                    {
                        string[] strArrayCaseId = strCaseIdList.Split(sep);
                        if (strArrayCaseId[0] == "1")
                        {
                            if (strArrayCaseId.Length > 1)
                                OutputStr += "|CASEID=" + strArrayCaseId[1] + CagInputParam.SerialNumber;
                            if (strArrayCaseId.Length > 2)
                                OutputStr += strArrayCaseId[2];
                        }
                    }

                    reportText = OutputStr + "|";

                    log.Debug("OutputStr:'" + reportText + "'\n");
                }

                log.Debug("ConfigAgent finished OK");

                result = 0;
            }
            catch (Exception e)
            {
                reportText = "Exception: " + e.Message;
                log.Debug(reportText);
            }
        }//Create Secure USP

        string XmlAutoIndent(XmlDocument doc)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            string strHeader = doc.FirstChild.OuterXml;
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = "\r\n";
            settings.NewLineHandling = NewLineHandling.Replace;
            settings.OmitXmlDeclaration = true;
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }

            //string strReturnString = String.Format("<?xml version=\"1.0\"?>{1}{0}{1}", sb.ToString(), settings.NewLineChars);
            string strReturnString = sb.ToString();
            strReturnString = strHeader + settings.NewLineChars + strReturnString + settings.NewLineChars;

            return strReturnString;
        }
        /*
        public void CreateSecureUSP(SequenceContext seqContext, out bool result, out string reportText)
        {
            double tmpResult;
            CreateSecureUSP(seqContext, out tmpResult, out reportText);

            result = (tmpResult < 0 ? false : true);
        }
        */
        bool CheckTelefonicaSerial(string Prefix, string KreatelSerial, string TelefonicaSerialNoOrg)
        {
            return TelefonicaSerialNoOrg == GetTelefonicaSerial(Prefix, KreatelSerial);
        }

        string GetTelefonicaSerial(string Prefix, string KreatelSerial)
        {
            string Serial7Digit;
            string TelefonicaSerialNo;
            int controldigit;

            if (KreatelSerial.Length != 10)
            {
                return "";
            }

            Serial7Digit = KreatelSerial.Substring(2, 7);

            controldigit = 3 * (Convert.ToInt32(Prefix.Substring(0, 1)) + Convert.ToInt32(Prefix.Substring(1, 1))) + 5 * (Convert.ToInt32(Prefix.Substring(2, 1)) + Convert.ToInt32(Prefix.Substring(3, 1))) +
                           7 * (Convert.ToInt32(Serial7Digit.Substring(0, 1)) + Convert.ToInt32(Serial7Digit.Substring(1, 1)) + Convert.ToInt32(Serial7Digit.Substring(2, 1)) + Convert.ToInt32(Serial7Digit.Substring(3, 1)) +
                                Convert.ToInt32(Serial7Digit.Substring(4, 1)) + Convert.ToInt32(Serial7Digit.Substring(5, 1)) + Convert.ToInt32(Serial7Digit.Substring(6, 1)));

            TelefonicaSerialNo = string.Format(Prefix + Serial7Digit + (controldigit % 10).ToString());

            return TelefonicaSerialNo;
        }
    }
}